﻿<%@ Page Language="C#" MasterPageFile="~/_includes/MasterPageMenu.master" AutoEventWireup="true" CodeBehind="Overview.aspx.cs" Inherits="XOM.BIS.Prototype.Web.Project.Overview" %>

<%@ Register Src="~/_includes/Menu_Project.ascx" TagName="Menu" TagPrefix="app" %>
<asp:Content ID="Content4" ContentPlaceHolderID="PageMenu" runat="Server">
    <app:Menu runat="server" />
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="Title" runat="Server">
    Project 12342 - GVS Day 1 IT Enablement
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="SubTitle" runat="Server">
    Overview
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">  
      <div class="em-c-page-header em-c-page-header--small">
                <h1 class="em-c-page-header__title">General Info</h1>
            </div>
    <div class="em-l-grid em-l-grid--2up">
        
        <div class="em-l-grid__item">
          

            <div class="em-l-grid em-l-grid--2up ">
                <div class="em-l-grid__item">
                    <label>Work ID:</label>
                </div>
                <div class="em-l-grid__item">
                    <div>12342</div>
                </div>
            </div>
            <div class="em-l-grid em-l-grid--2up ">
                <div class="em-l-grid__item">
                    <label>Work Name:</label>
                </div>
                <div class="em-l-grid__item">
                    <div>GVS Day 1 IT Enablement</div>
                </div>
            </div>
            <div class="em-l-grid em-l-grid--2up ">
                <div class="em-l-grid__item">
                    <label>Work Class:</label>
                </div>
                <div class="em-l-grid__item">
                    <div>Discretionary</div>
                </div>
            </div>
            <div class="em-l-grid em-l-grid--2up ">
                <div class="em-l-grid__item">
                    <label>Work Type:</label>
                </div>
                <div class="em-l-grid__item">
                    <div>Project</div>
                </div>
            </div>
            <div class="em-l-grid em-l-grid--2up ">
                <div class="em-l-grid__item">
                    <label>T - Code:</label>
                </div>
                <div class="em-l-grid__item">
                    <div>12342</div>
                </div>
            </div>
            <div class="em-l-grid em-l-grid--2up ">
                <div class="em-l-grid__item">
                    <label>T-Code Description:</label>
                </div>
                <div class="em-l-grid__item">
                    <div>All Emit Projects < $15M</div>
                </div>
            </div>
            <div class="em-l-grid em-l-grid--2up ">
                <div class="em-l-grid__item">
                    <label>Funding Division:</label>
                </div>
                <div class="em-l-grid__item">
                    <div>Downstream Portfolio</div>
                </div>
            </div>
            <div class="em-l-grid em-l-grid--2up ">
                <div class="em-l-grid__item">
                    <label>Funding Division Function:</label>
                </div>
                <div class="em-l-grid__item">
                    <div>Downstream - Downstream Business Services</div>
                </div>
            </div>
          
        </div>

          <div class="em-l-grid__item">
                <div class="em-l-grid em-l-grid--2up ">
                <div class="em-l-grid__item">
                    <label>Project Manager's Name:</label>
                </div>
                <div class="em-l-grid__item">
                    Mary Mayweather
                    <div></div>
                </div>
                     <div class="em-l-grid__item">
                    <label>PCA:</label>
                </div>
                <div class="em-l-grid__item">
                    John Doe
                    <div></div>
                </div>
            </div>
            <div class="em-l-grid em-l-grid--2up ">
                <div class="em-l-grid__item">
                    <label>Total Amount Approved:</label>
                </div>
                <div class="em-l-grid__item">
                    <div>$550K</div>
                </div>
            </div>
            <div class="em-l-grid em-l-grid--2up ">
                <div class="em-l-grid__item">
                    <label>Total Spent:</label>
                </div>
                <div class="em-l-grid__item">
                    <div>$250K</div>
                </div>
            </div>
            <div class="em-l-grid em-l-grid--2up ">
                <div class="em-l-grid__item">
                    <label>Balance:</label>
                </div>
                <div class="em-l-grid__item">
                    <div>$300K</div>
                </div>
            </div>
             <div class="em-l-grid em-l-grid--2up ">
                <div class="em-l-grid__item">
                    <label>Start Date:</label>
                </div>
                <div class="em-l-grid__item">
                    <div>Jan 3rd 2013</div>
                </div>
            </div>
          </div>
       
        <!-- end em-l-grid__item -->
    </div>
    <div class="em-l-grid em-l-grid--1up">
         <div class="em-l-grid__item">
            <div class="em-c-page-header em-c-page-header--small">
                <h1 class="em-c-page-header__title">Requests</h1>
            </div>

            <div class="em-c-table-object ">                
                <div class="em-c-table-object__body">
                    <div class="em-c-table-object__body-inner">
                        <table class="em-c-table" style="min-width:100%">
                            <thead class="em-c-table__header">
                                <tr class="em-c-table__header-row">
                                    <th scope="col" class="em-c-table__header-cell ">BIS #</th>
                                      <th scope="col" class="em-c-table__header-cell ">Request Type</th>
                                     <th scope="col" class="em-c-table__header-cell ">Created Date</th>
                                    <th scope="col" class="em-c-table__header-cell ">Amount</th>
                                     <th scope="col" class="em-c-table__header-cell ">Status</th>
                                   
                                </tr>
                                <!-- em-c-table__header-row -->
                            </thead>
                            <!-- end em-c-table__header -->
                            <tbody class="em-c-table__body ">
                                <tr class="em-c-table__row ">
                                    <td class="em-c-table__cell em-js-cell"><a href="../requests/overview.aspx">2345</a>
                                    </td>
                                    <td class="em-c-table__cell em-js-cell">Request Funding
                                    </td>
                                     <td class="em-c-table__cell em-js-cell">August 2nd 2017
                                    </td>
                                    <td class="em-c-table__cell em-js-cell">$100K
                                    </td>
                                     <td class="em-c-table__cell em-js-cell">Submitted For Approval
                                    </td>
                                   
                                </tr>
                                    <tr class="em-c-table__row ">
                                    <td class="em-c-table__cell em-js-cell"><a href="../requests/overview.aspx">2346</a>
                                    </td>
                                    <td class="em-c-table__cell em-js-cell">Request Funding
                                    </td>
                                     <td class="em-c-table__cell em-js-cell">Jan 1st 2016
                                    </td>
                                    <td class="em-c-table__cell em-js-cell">$450K
                                    </td>
                                     <td class="em-c-table__cell em-js-cell">Approved
                                    </td>
                                   
                                </tr>
                            </tbody>
                            <!-- end em-c-table__body -->
                            <tfoot class="em-c-table__footer">
                                <tr class="em-c-table__footer-row">
                                </tr>
                            </tfoot>
                            <!-- end em-c-table__footer -->
                        </table>
                        <!--end em-c-table-->
                    </div>
                    <!--end em-c-table-object__body-inner-->
                </div>
                <!--end em-c-table-object__body-->
            </div>

        </div>

    </div>
</asp:Content>
