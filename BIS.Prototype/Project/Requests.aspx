﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_includes/MasterPageMenu.master" AutoEventWireup="true" CodeBehind="Requests.aspx.cs" Inherits="XOM.BIS.Prototype.Web.Project.Requests" %>
<%@ Register src="~/_includes/Menu_Project.ascx" tagname="Menu" tagprefix="app" %>
<asp:Content ID="Content6" ContentPlaceHolderID="PageMenu" Runat="Server"><app:Menu runat=server /></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Title" runat="server">
      Project 12342 - GVS Day 1 IT Enablement
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="SubTitle" runat="server">
    Requests
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="server">
    
        <div class="em-c-page-header em-c-page-header--small">
                    <h1 class="em-c-page-header__title">Requests</h1>
                </div>
        <div class="em-c-table-object ">                
                <div class="em-c-table-object__body">
                    <div class="em-c-table-object__body-inner">
                        <table class="em-c-table" style="min-width:100%">
                            <thead class="em-c-table__header">
                                <tr class="em-c-table__header-row">
                                    <th scope="col" class="em-c-table__header-cell ">BIS #</th>
                                      <th scope="col" class="em-c-table__header-cell ">Request Type</th>
                                     <th scope="col" class="em-c-table__header-cell ">Created Date</th>
                                    <th scope="col" class="em-c-table__header-cell ">Amount</th>
                                     <th scope="col" class="em-c-table__header-cell ">Status</th>
                                   
                                </tr>
                                <!-- em-c-table__header-row -->
                            </thead>
                            <!-- end em-c-table__header -->
                            <tbody class="em-c-table__body ">
                                <tr class="em-c-table__row ">
                                    <td class="em-c-table__cell em-js-cell"><a href="../requests/overview.aspx">2345</a>
                                    </td>
                                    <td class="em-c-table__cell em-js-cell">Request Funding
                                    </td>
                                     <td class="em-c-table__cell em-js-cell">August 2nd 2017
                                    </td>
                                    <td class="em-c-table__cell em-js-cell">$100K
                                    </td>
                                     <td class="em-c-table__cell em-js-cell">Submitted For Approval
                                    </td>
                                   
                                </tr>
                                    <tr class="em-c-table__row ">
                                    <td class="em-c-table__cell em-js-cell"><a href="../requests/overview.aspx">2346</a>
                                    </td>
                                    <td class="em-c-table__cell em-js-cell">Request Funding
                                    </td>
                                     <td class="em-c-table__cell em-js-cell">Jan 1st 2016
                                    </td>
                                    <td class="em-c-table__cell em-js-cell">$450K
                                    </td>
                                     <td class="em-c-table__cell em-js-cell">Approved
                                    </td>
                                   
                                </tr>
                            </tbody>
                            <!-- end em-c-table__body -->
                            <tfoot class="em-c-table__footer">
                                <tr class="em-c-table__footer-row">
                                </tr>
                            </tfoot>
                            <!-- end em-c-table__footer -->
                        </table>
                        <!--end em-c-table-->
                    </div>
                    <!--end em-c-table-object__body-inner-->
                </div>
                <!--end em-c-table-object__body-->
            </div>
</asp:Content>