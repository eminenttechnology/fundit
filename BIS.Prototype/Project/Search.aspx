﻿<%@ Page Language="C#" MasterPageFile="~/_includes/MasterPage.master" AutoEventWireup="true" CodeBehind="Search.aspx.cs" Inherits="XOM.BIS.Prototype.Web.Project.Search" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Title" runat="Server">
    Projects
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="SubTitle" runat="Server">
    Search Projects
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">

    
        <div class="em-c-page-header em-c-page-header--small">
            <h1 class="em-c-page-header__title">Project Search Criteria</h1>
        </div>
        <div class="em-c-collapsible-toolbar__panel em-js-toolbar-panel">
            <div class="em-c-toolbar">
                <div class="em-l-grid em-l-grid--4up ">
                    <div class="em-l-grid__item">
                        <div class="em-c-field ">
                            <label for="" class="em-c-field__label">Work ID</label>
                            <div class="em-c-field__body">
                                <input type="text" id="" class="em-c-input" value="" />
                            </div>
                        </div>
                    </div>
                    <div class="em-l-grid__item">
                        <div class="em-c-field ">
                            <label for="" class="em-c-field__label">Work Name</label>
                            <div class="em-c-field__body">
                                <input type="text" id="" class="em-c-input" value="" />
                            </div>
                        </div>
                    </div>
                    <div class="em-l-grid__item">
                        <div class="em-c-field ">
                            <label for="" class="em-c-field__label">Work Class</label>
                            <div class="em-c-field__body">
                                <select class="em-c-select em-c-select em-u-width-100">
                                    <option value=""></option>
                                    <option value="option-1">Discretionary</option>
                                    <option value="option-2">Non-Discretionary</option>                                    
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="em-l-grid__item">
                        <div class="em-c-field ">
                            <label for="" class="em-c-field__label">Work Type</label>
                            <div class="em-c-field__body">
                                <select class="em-c-select em-c-select em-u-width-100">
                                    <option value=""></option>
                                    <option value="option-1">Base</option>
                                    <option value="option-2">Project</option>
                                    <option value="option-3">SWI</option>
                                </select>
                            </div>
                        </div>
                    </div>   
                    <div class="em-l-grid__item">
                        <div class="em-c-field ">
                            <label for="" class="em-c-field__label">Project Manager</label>
                            <div class="em-c-field__body">
                                <select class="em-c-select em-c-select em-u-width-100">
                                    <option value=""></option>
                                    <option value="option-1">Zachery Roberts</option>
                                    <option value="option-2">Mary Mayweather</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="em-l-grid__item">
                        <div class="em-c-field">
                            <label for="" class="em-c-field__label">Funding Division</label>
                            <div class="em-c-field__body">
                                <select class="em-c-select em-c-select">
                                   z <option value=""></option>
                                    <option value="option-1">Downstream Portfolio</option>
                                    <option value="option-2">General Interest Portfolio</option>
                                    <option value="option-2">Service Portfolio</option>
                                </select>
                            </div>
                        </div>
                    </div>
                     <div class="em-l-grid__item">
                        <div class="em-c-field ">
                            <br />
         
                            
                            <button class="em-c-btn ">

                                <span class="em-c-btn__text">Search</span>
                            </button>
                            <!-- end em-c-btn -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="em-l-container">

        <div class="em-c-table-object ">
            <div class="em-c-table-object__header"></div>
            <div class="em-c-table-object__body">
                <div class="em-c-table-object__body-inner">
                    <table class="em-c-table ">
                        <thead class="em-c-table__header">
                            <tr class="em-c-table__header-row">
                                <th scope="col" class="em-c-table__header-cell ">Work ID</th>
                                <th scope="col" class="em-c-table__header-cell ">Work Name</th>
                                <th scope="col" class="em-c-table__header-cell ">Work Class</th>
                                <th scope="col" class="em-c-table__header-cell ">Work Type</th>
                                <th scope="col" class="em-c-table__header-cell ">Funding Division</th>
                                <th scope="col" class="em-c-table__header-cell ">Division Function</th>
                                <th scope="col" class="em-c-table__header-cell ">Project Manager</th>
                            </tr>
                        </thead>
                        <tbody class="em-c-table__body ">
                            <tr class="em-c-table__row ">
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable"><a href="Overview.aspx">71267</a></td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">GVS Day 1 IT Enablement</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Discretionary</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Project</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">General Interest Portfolio</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">General Interest - All</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Mary Mayweather</td>
                            </tr>
                            <tr class="em-c-table__row ">
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable"><a href="Overview.aspx">71268</a></td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">GVS Day 1 IT Enablement</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Non-Discretionary</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Base</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Downstream Portfolio</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Downstream Business Services</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Zachery Roberts</td>
                            </tr>
                            <tr class="em-c-table__row ">
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable"><a href="Overview.aspx">71269</a></td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">GVS Day 1 IT Enablement</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Discretionary</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">SWI</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Chemical & Corporate Portfolio</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">CrossBP</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Mary Mayweather</td>
                            </tr>
                        </tbody>
                        <tfoot class="em-c-table__footer">
                            <tr class="em-c-table__footer-row"></tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    


</asp:Content>
