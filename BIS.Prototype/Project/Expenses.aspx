﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_includes/MasterPageMenu.master" AutoEventWireup="true" CodeBehind="Expenses.aspx.cs" Inherits="XOM.BIS.Prototype.Web.Project.Expenses" %>
<%@ Register src="~/_includes/Menu_Project.ascx" tagname="Menu" tagprefix="app" %>
<asp:Content ID="Content1" ContentPlaceHolderID="PageMenu" Runat="Server"><app:Menu runat=server /></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Title" runat="server">
      Project 12342 - GVS Day 1 IT Enablement
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="SubTitle" runat="server">
    Expenses
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">
       
           <div class="em-c-page-header em-c-page-header--small">
                    <h1 class="em-c-page-header__title">Expenses</h1>
                </div>
  <div class="em-c-table-object ">
        <div class="em-c-table-object__header">
        </div>
        <!--end em-c-table-object__header-->
        <div class="em-c-table-object__body">
        <div class="em-c-table-object__body-inner">
            <table class="em-c-table ">
            <thead class="em-c-table__header">
                <tr class="em-c-table__header-row">
                    <th scope="col" class="em-c-table__header-cell ">ID #</th>
                     <th scope="col" class="em-c-table__header-cell "> Date</th>
                     <th scope="col" class="em-c-table__header-cell "> Description</th>
                    <th scope="col" class="em-c-table__header-cell ">Amount</th>
                   
                </tr>
                <!-- em-c-table__header-row -->
            </thead>
            <!-- end em-c-table__header -->
            <tbody class="em-c-table__body ">
                <tr class="em-c-table__row ">
                    <td class="em-c-table__cell em-js-cell">
                        234
                    </td>
                     <td class="em-c-table__cell em-js-cell">
                       Jan 23rd 2017
                    </td>
                     <td class="em-c-table__cell em-js-cell">
                      Utility
                    </td>
                    <td class="em-c-table__cell em-js-cell">
                       $25K
                    </td>
                   
                </tr>
            <tr class="em-c-table__row ">
                    <td class="em-c-table__cell em-js-cell">
                        235
                    </td>
                     <td class="em-c-table__cell em-js-cell">
                       Jan 30th 2017
                    </td>
                     <td class="em-c-table__cell em-js-cell">
                      Printing
                    </td>
                    <td class="em-c-table__cell em-js-cell">
                       $5K
                    </td>
                   
                </tr>
                 <tr class="em-c-table__row ">
                    <td class="em-c-table__cell em-js-cell">
                        233
                    </td>
                     <td class="em-c-table__cell em-js-cell">
                       Feb 18th 2017
                    </td>
                     <td class="em-c-table__cell em-js-cell">
                      Transportation
                    </td>
                    <td class="em-c-table__cell em-js-cell">
                       $23K
                    </td>
                   
                </tr>
            </tbody>
            <!-- end em-c-table__body -->
            <tfoot class="em-c-table__footer">
                <tr class="em-c-table__footer-row">
                </tr>
            </tfoot>
            <!-- end em-c-table__footer -->
            </table>
            <!--end em-c-table-->
        </div>
        <!--end em-c-table-object__body-inner-->
        </div>
        <!--end em-c-table-object__body-->
    </div>
</asp:Content>
