﻿// Create the chart
Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Approved Request.'
    },
    //subtitle: {
    //    text: 'Click the columns to view versions. Source: <a href="http://netmarketshare.com">netmarketshare.com</a>.'
    //},
    xAxis: {
        type: 'category'
    },
    yAxis: {
        title: {
            text: 'Total approved till date($K)'
        }

    },
    legend: {
        enabled: false
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true
                //,
                //format: '${point.y:.1f}%'
            }
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>${point.y}K</b><br/>'
    },

    series: [{
        name: 'Brands',
        colorByPoint: true,
        data: [{
            name: 'Jan',
            y: 5000,
            drilldown: null
        }, {
            name: 'Feb',
            y: 15000,
            drilldown: null
        }, {
            name: 'Mar',
            y: 10000,
            drilldown: null
        }, {
            name: 'Apr',
            y: 20000,
            drilldown: null
        }, {
            name: 'May',
            y: 100000,
            drilldown: null
        }, {
            name: 'Jun',
            y:50000,
            drilldown: null
        }, {
            name: 'Jul',
            y:10000,
            drilldown: null
        }, {
            name: 'Aug',
            y: 15000,
            drilldown: null
        }]
    }],
});

Highcharts.chart('piechartcontainer', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: 'Requests made January, 2017 to September, 2017'
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                style: {
                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                }
            }
        }
    },
    series: [{
        name: 'Request Type',
        colorByPoint: true,
        data: [{
            name: 'Capital requests',
            y: 60.00
        }, {
            name: 'Expenses',
            y: 40.00,
            sliced: true,
            selected: true
        }]
    }]
});

Highcharts.chart('fundingchart', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Funding Request.'
    },
    //subtitle: {
    //    text: 'Click the columns to view versions. Source: <a href="http://netmarketshare.com">netmarketshare.com</a>.'
    //},
    xAxis: {
        type: 'category'
    },
    yAxis: {
        title: {
            text: 'Number of funding request'
        }

    },
    legend: {
        enabled: false
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true
                //,
                //format: '${point.y:.1f}%'
            }
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>'
        //,
        //pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>${point.y}K</b><br/>'
    },

    series: [{
        name: 'Brands',
        colorByPoint: true,
        data: [{
            name: 'Chemical & Corporate Portfolio',
            y: 7,
            drilldown: null
        }, {
            name: 'Downstream Portfolio',
            y: 10,
            drilldown: null
        }, {
            name: 'General Interest Portfolio',
            y: 8,
            drilldown: null
        }, {
            name: 'Service Portfolio',
            y: 4,
            drilldown: null
        }, {
            name: 'Upstream portfolio',
            y: 2,
            drilldown: null
        }]
    }],
});