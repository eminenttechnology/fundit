﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="System.Web.UI.UserControl" %>
<ul class="em-c-link-list ">
    <li class="em-c-link-list__item">
        <a href="../Requests/Overview.aspx" class="em-c-link-list__link ">Overview
        </a>
    </li>
    <li class="em-c-link-list__item">
        <a href="../Requests/FundingRequestEdit.aspx" class="em-c-link-list__link ">Edit
        </a>
    </li>
    <li class="em-c-link-list__item">
        <a href="../Requests/Appropriation.aspx" class="em-c-link-list__link ">Allocation Info
        </a>
    </li>
     <li class="em-c-link-list__item">
        <a href="javascript:alert("Not Yet Implemented");" class="em-c-link-list__link ">Print
        </a>
    </li>
     <li class="em-c-link-list__item">
        <a href="../Requests/Comment.aspx" class="em-c-link-list__link ">Comments
        </a>
    </li>
    <li class="em-c-link-list__item">
        <a href="../Requests/Document.aspx" class="em-c-link-list__link ">Documents
        </a>
    </li>
  <%--   <li class="em-c-link-list__item">
        <a href="../Requests/Document.aspx" class="em-c-link-list__link ">Edit Allocation Info
        </a>
    </li>--%>
   
   
    <li class="em-c-link-list__item">
        <a href="../Requests/AuditTrail.aspx" class="em-c-link-list__link ">Audit Trail
        </a>
    </li>
</ul>

