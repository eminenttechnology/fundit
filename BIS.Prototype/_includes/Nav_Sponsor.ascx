﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="System.Web.UI.UserControl" %>


<ul class="menu">
  
    <li class="has-menu"><a href="../sponsor/myemployees.aspx">Employees</a>
    </li>
    <li class="has-menu"><a href="../sponsor/myforms.aspx">My Forms</a>
    </li>
    <li class="has-menu"><a href="#">Submit Form</a><ul>

        <li class="has-menu"><a href="#">General</a>
            <ul>
                <li><a href="#">Visit Access Request Form</a></li>
            </ul>
        </li>
        <li class="has-menu"><a href="#">Clearance</a>
            <ul>
                <li><a href="#">Classified System Account Request</a></li>
                <li><a href="#">Personnel Clearance Justification</a></li>
                <li><a href="#">Security Pre-Screen for SCI Positions</a></li>
            </ul>
        </li>
        <li class="has-menu"><a href="#">Employee Actions</a>
            <ul>
                <li><a href="../Onboard/Create.aspx" target="form">Onboard an Employee to Dell Federal</a></li>
                <li><a href="#">Verify or Renew Employee Dell Federal Access</a></li>
                <li><a href="#">Change/Update Employee Federal Sponsor Information</a></li>
                
                <li><a href="#">Initiate External Transfer to Other Dell Business Unit</a></li>
                <li><a href="#">Offboard Employee From Dell Inc</a></li>
            </ul>
        </li>
        <li class="has-menu"><a href="#">Travel</a>
            <ul>
                <li><a href="#">Pre Departure International Travel Reporting</a></li>
                <li><a href="#">Post Departure International Travel Reporting</a></li>
            </ul>
        </li>
        <li class="has-menu"><a href="#">Misc</a>
            <ul>
                <li><a href="#">Submit General User Acknowledgement Statement</a></li>
            </ul>
        </li>

     </ul>
    </li>
 
    
        
</ul>
