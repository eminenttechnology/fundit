﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="System.Web.UI.UserControl" %>
<ul class="em-c-link-list ">
    <li class="em-c-link-list__item">
        <a href="Overview.aspx" class="em-c-link-list__link ">Overview
        </a>
    </li>
    <!-- end em-c-link-list__item -->
    <li class="em-c-link-list__item">
        <a href="Budget.aspx" class="em-c-link-list__link ">Budget
        </a>
    </li>
    <!-- end em-c-link-list__item -->
    <li class="em-c-link-list__item">
        <a href="AppropriationRequest.aspx" class="em-c-link-list__link ">Appropriation Request
        </a>
    </li>
    <!-- end em-c-link-list__item -->
    <li class="em-c-link-list__item">
        <a href="Expenses.aspx" class="em-c-link-list__link ">Expenses
        </a>
    </li>
    <!-- end em-c-link-list__item -->
    <li class="em-c-link-list__item">
        <a href="Requests.aspx" class="em-c-link-list__link ">Requests
        </a>
    </li>
    <!-- end em-c-link-list__item -->
</ul>
<!-- end em-c-link-list -->

