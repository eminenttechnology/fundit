﻿<%@ Page Language="C#" MasterPageFile="~/_includes/MasterPage.master" AutoEventWireup="true" CodeBehind="Default.aspx.cs"  Inherits="XOM.BIS.Prototype.Web.Default" %>
<%--<asp:Content ID="Content1" ContentPlaceHolderID="SubTitle" Runat="Server">
</asp:Content>--%>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
<!-- begin left pane-->
<div class="heading">
   <strong>Global Information</strong> 
</div>
    <hr />
<div class="row">
         <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
          <div class="dashboard-stat2">
            <div class="display">
              <div class="number">
                <h3 class="font-green-sharp"><small class="font-green-sharp"></small>25 Form</h3>
                <small class="font-green-sharp"> Unassigned forms</small> </div>
              <div class="icon"> <i class="icon-basket-loaded"></i> </div>
            </div>
            <div class="progress-info">
              <div class="progress"> <span style="width: 76%;" class="progress-bar progress-bar-success green-sharp"> </span> </div>
            </div>
          </div>
        </div>

     <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
          <div class="dashboard-stat2">
            <div class="display">
              <div class="number">
                <h3 class="font-blue-sharp"><small class="font-blue-sharp"></small>100/200</h3>
                <small class="font-blue-sharp"> Forms completed</small> </div>
              <div class="icon"> <i class="icon-basket-loaded"></i> </div>
            </div>
            <div class="progress-info">
              <div class="progress"> <span style="width: 76%;" class="progress-bar progress-bar-success blue-sharp"> </span> </div>
            </div>
          </div>
        </div>

         <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
          <div class="dashboard-stat2">
            <div class="display">
              <div class="number">
                <h3 class="font-red-haze"><small class="font-red-haze"></small>5 Days</h3>
                <small class="font-red-haze">Ave processing time</small> </div>
              <div class="icon"> <i class="icon-basket"></i> </div>
            </div>
            <div class="progress-info">
              <div class="progress"> <span style="width: 76%;" class="progress-bar progress-bar-success red-haze"> </span> </div>
            </div>
          </div>
        </div>
      </div>

<div class="heading">
    <strong>My dashboard Information</strong>
</div>
    <hr />
<div class="row">

         <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
          <div class="dashboard-stat2">
            <div class="display">
              <div class="number">
                <h3 class="font-green-sharp"><small class="font-green-sharp"></small>5 Form</h3>
                <small class="font-green-sharp"> Unassigned forms</small> </div>
              <div class="icon"> <i class="icon-basket-loaded"></i> </div>
            </div>
            <div class="progress-info">
              <div class="progress"> <span style="width: 76%;" class="progress-bar progress-bar-success green-sharp"> </span> </div>
            </div>
          </div>
        </div>

     <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
          <div class="dashboard-stat2">
             <div class="display">
              <div class="number">
                <h3 class="font-blue-sharp"><small class="font-blue-sharp"></small>4/20</h3>
                <small class="font-blue-sharp"> Forms completed</small> </div>
              <div class="icon"> <i class="icon-basket-loaded"></i> </div>
            </div>
            <div class="progress-info">
              <div class="progress"> <span style="width: 76%;" class="progress-bar progress-bar-success blue-sharp"> </span> </div>
            </div>
          </div>
        </div>

         <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
          <div class="dashboard-stat2">
             <div class="display">
              <div class="number">
                <h3 class="font-red-haze"><small class="font-red-haze"></small>3 Days</h3>
                <small class="font-red-haze">Ave processing time</small> </div>
              <div class="icon"> <i class="icon-basket"></i> </div>
            </div>
            <div class="progress-info">
              <div class="progress"> <span style="width: 76%;" class="progress-bar progress-bar-success red-haze"> </span> </div>
            </div>
          </div>
        </div>
      </div>
<hr />

             <table class="table table-scrollable-borderless">
             <thead class="text-right">
                <tr> 
                 <td>  </td>             
                 <th> Today </th>
                 <th> Yesterday </th>
                 <th> 7 days </th>
                 <th> 30 days </th>
                 <th> 6 months </th>
                 <th> 12 months </th>
                </tr>
               </thead>
           <tbody>
               <tr> 
                 <td class="item-label"> Pre-departure international travel reporting </td>                 
                 <td> 7 </td>
                 <td> 7 </td>
                 <td> 70 </td>
                 <td> 70 </td>
                 <td> 7 </td>
                 <td> 70 </td>
                </tr>

             <tr> 
                <td class="item-label"> Post-departure international travel reporting </td>                 
                 <td> 7 </td>
                 <td> 7 </td>
                 <td> 7 </td>
                 <td> 7 </td>
                 <td> 7</td>
                 <td> 70 </td>
                </tr>
            </tbody>
              </table> 

</asp:Content>


