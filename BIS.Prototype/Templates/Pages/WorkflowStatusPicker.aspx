﻿<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1">
    <title>Change Status
    </title>
    <script src="https://code.jquery.com/jquery.js"></script>
    <link type="text/css" href="http://d2ml0zxrbmwn5n.cloudfront.net/assets/standard/css/style.css" rel="stylesheet" />
</head>
<body>
    <form method="post" action="WorkflowStatusPicker.aspx?WorkflowCode=ONBOARD&amp;EntityID=1783&amp;FieldName=MainContent_General_Info_Status_ctrl&amp;Reload=True" id="form1">
        <div class="aspNetHidden">
            <input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwUKMTI5MDQ3OTEyNA8WBh4MV29ya2Zsb3dDb2RlBQdPTkJPQVJEHghFbnRpdHlJRAUEMTc4Mx4XUmVsb2FkQWZ0ZXJTdGF0dXNDaGFuZ2VnFgICAw9kFgYCAw8WAh4EVGV4dAUmJ01haW5Db250ZW50X0dlbmVyYWxfSW5mb19TdGF0dXNfY3RybCdkAgsPDxYCHwMFGlN1Ym1pdHRlZCBwZW5kaW5nIHNlY3VyaXR5ZGQCDQ8QZBAVBQwtLSBTZWxlY3QgLS0WUGVuZGluZyBzcG9uc29yIGFjdGlvbhdQZW5kaW5nIGVtcGxveWVlIGFjdGlvbhBDbG9zZWQgbm8gYWN0aW9uD0Nsb3NlZCBjb21wbGV0ZRUFAAE1AjI1Ai0xATAUKwMFZ2dnZ2dkZGQKVMIYbHwdH6A2LmQrVyCvqMQ4UNaH1GFzos9Bcq9I7A==" />
        </div>

        <div class="aspNetHidden">

            <input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="8392A3FD" />
            <input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="/wEdAAqC69tbvCx/elGXonlrBAWWYCvnz5eqQvnh92w0LRtqNvuONXWRrR59Lda+QyNiINy/IgC7a2nDZUmOm//p3CvWE2n0vqDaYl7HQVjUYAcfP95g8X6YYxLzK0+pZi/SKTRgZ5AvPOl1TXLnsmFmCu2z/U7XX1lxuUzqOB1lFHIF3jNmsQM+htDoARTgsroZ52koISq9Y1iHrc6A5tb5GjqbcAuwS86BfjS+YugMLH0N3fQ918J7oh+rQ2dSZo6Y1iQ=" />
        </div>

        <script type="text/javascript">
            function querySt(ji) {
                hu = window.location.search.substring(1);
                gy = hu.split("&");
                for (i = 0; i < gy.length; i++) {
                    ft = gy[i].split("=");
                    if (ft[0] == ji)
                    { return ft[1]; }
                }
            }
            function CloseWindowWithStatusUpdate(status, reload) {
                var ctrl = window.parent.document.getElementById('MainContent_General_Info_Status_ctrl');
                ctrl.innerText = status;

                if (reload == 'True') {
                    self.parent.location.reload();
                }

                parent.$.fancybox.close();
            }

            function CloseWindowCancel() {
                parent.$.fancybox.close();
            }



        </script>
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Change Status</h3>
            </div>
            <div class="panel-body">




                <table cellspacing="1" cellpadding="1" width="95%" align="center" border="0">
                    <tr>
                        <td colspan="2">
                            <input name="returnvalue" type="hidden" id="returnvalue" />
                            <input name="returnname" type="hidden" id="returnname" />
                            <span id="Message"></span>
                        </td>
                    </tr>
                    <tr>
                        <td width="100">Current Status:
                        </td>
                        <td>
                            <span id="CurrentStatusLabel">Draft</span>
                        </td>
                    </tr>
                    <tr>
                        <td>Next Status:
                        </td>
                        <td>
                            <select name="StatusList" id="StatusList">
                                <option value="">-- Select --</option>
                                <option value="5">Submitted for review</option>
                                <option value="25">Submitted for approval</option>
                                   <option value="25">Approved</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">Comments:</td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <textarea name="Comments" rows="2" cols="20" id="Comments" style="height: 70px; width: 100%;">
</textarea></td>
                    </tr>
                </table>
                <br />
                <br />
                <table cellspacing="1" cellpadding="1" width="100%" align="center" border="0">
                    <tr>
                        <td align="center">&nbsp;
                <%--<input type="submit" name="Save" value="Change Status" id="Save" class="FormButton" onclick="CloseWindowCancel()"  />--%>
                                <input type="button" name="Save" value="Change Status" id="Save" class="FormButton" onclick="CloseWindowCancel()"  />
                            &nbsp;&nbsp; &nbsp;
                <input type="button" value="Cancel" class="FormButton" onclick="CloseWindowCancel()" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <script type="text/javascript" language="javascript">
            $("#StatusList").change(function () {
                $("#Save").val($("#StatusList option:selected").text());
            });
        </script>
    </form>
</body>
</html>
