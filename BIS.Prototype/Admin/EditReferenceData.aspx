﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_includes/MasterPage.master" AutoEventWireup="true" CodeBehind="EditReferenceData.aspx.cs" Inherits="XOM.BIS.Prototype.Web.Admin.ReferenceData.EditReferenceData" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Title" runat="server">
    Edit Budget Class
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SubTitle" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
        <div class="em-l__main">
        <div class="em-l-linelength-container">
            <fieldset class="em-c-fieldset">
                <div class="em-c-field ">
                    <label for="file" class="em-c-field__label">Description</label>
                    <div class="em-c-field__body">
                        <input class="em-c-input" placeholder="" value="$250K - $500K">
                    </div>
                </div>
                <div class="em-c-field ">
                    <label for="file" class="em-c-field__label">Sort</label>
                    <div class="em-c-field__body">
                        <input class="em-c-input" placeholder="" value="1">
                    </div>
                </div>
                <div class="em-c-btn-group ">
                    <a href="ReferenceDataList.aspx" class="em-c-btn em-c-btn--primary">
                        <span class="em-c-btn__text">Submit</span>
                    </a>
                    <!-- end em-c-btn -->
                     <a href="ReferenceDataList.aspx" class="em-c-btn em-c-btn--inverted">
                        <span class="em-c-btn__text">Delete</span>
                    </a>
                    <a href="ReferenceDataList.aspx" class="em-c-btn em-c-btn--secondary">
                        <span class="em-c-btn__text">Cancel</span>
                    </a>
                    <!-- end em-c-btn -->
                </div>
                <!-- end em-c-btn-group -->
            </fieldset>
        </div>
    </div>

</asp:Content>
