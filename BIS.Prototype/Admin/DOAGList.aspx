﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_includes/MasterPage.master" AutoEventWireup="true" CodeBehind="DOAGList.aspx.cs" Inherits="XOM.BIS.Prototype.Web.Admin.DOAGList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Title" runat="server">
    DOAG List
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SubTitle" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">

    <div class="em-u-text-align-right em-u-margin-bottom-double">
        <a class="em-c-btn em-c-btn--primary" href="AddDOAG.aspx"><span class="em-c-btn__text">Add</span></a>
    </div>
    <div class="em-c-table-object">
        <div class="em-c-table-object__header">
        </div>
        <!--end em-c-table-object__header-->
        <div class="em-c-table-object__body">
            <div class="em-c-table-object__body-inner">
                <table class="em-c-table ">
                    <thead class="em-c-table__header">
                        <tr class="em-c-table__header-row">
                            <th scope="col" class="em-c-table__header-cell ">DOAG Level</th>
                            <th scope="col" class="em-c-table__header-cell ">Description</th>
                            <th scope="col" class="em-c-table__header-cell ">Max Amount ($K)</th>
                            <th scope="col" class="em-c-table__header-cell "></th>
                        </tr>
                        <!-- em-c-table__header-row -->
                    </thead>
                    <!-- end em-c-table__header -->
                    <tbody class="em-c-table__body ">
                        <tr class="em-c-table__row ">
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">DOAG Level 9
                            </td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable"></td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">5,000
                            </td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                <a class="em-c-btn" href="EditDOAG.aspx"><span class="em-c-btn__text">Edit</span></a>
                            </td>
                        </tr>
                        <tr class="em-c-table__row ">
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">DOAG Level 8
                            </td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable"></td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">10,000
                            </td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                <a class="em-c-btn" href="EditDOAG.aspx"><span class="em-c-btn__text">Edit</span></a>
                            </td>
                        </tr>
                        <tr class="em-c-table__row ">
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">DOAG Level 7
                            </td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable"></td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">15,000
                            </td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                <a class="em-c-btn" href="EditDOAG.aspx"><span class="em-c-btn__text">Edit</span></a>
                            </td>
                        </tr>
                        <tr class="em-c-table__row ">
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">DOAG Level 6
                            </td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable"></td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">20,000
                            </td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                <a class="em-c-btn" href="EditDOAG.aspx"><span class="em-c-btn__text">Edit</span></a>
                            </td>
                        </tr>
                        <tr class="em-c-table__row ">
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">DOAG Level 5
                            </td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable"></td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">25,000
                            </td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                <a class="em-c-btn" href="EditDOAG.aspx"><span class="em-c-btn__text">Edit</span></a>
                            </td>
                        </tr>
                        <tr class="em-c-table__row ">
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">DOAG Level 6
                            </td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable"></td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">30,000
                            </td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                <a class="em-c-btn" href="EditDOAG.aspx"><span class="em-c-btn__text">Edit</span></a>
                            </td>
                        </tr>
                        <tr class="em-c-table__row ">
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">DOAG Level 5
                            </td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">EMIT Division Mgr</td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">35,000
                            </td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                <a class="em-c-btn" href="EditDOAG.aspx"><span class="em-c-btn__text">Edit</span></a>
                            </td>
                        </tr>
                        <tr class="em-c-table__row ">
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">DOAG Level 4
                            </td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">EMIT VP / IT Ops Mgr.</td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">40,000
                            </td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                <a class="em-c-btn" href="EditDOAG.aspx"><span class="em-c-btn__text">Edit</span></a>
                            </td>
                        </tr>
                        <tr class="em-c-table__row ">
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">DOAG Level 3
                            </td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">GSC President</td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">45,000
                            </td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                <a class="em-c-btn" href="EditDOAG.aspx"><span class="em-c-btn__text">Edit</span></a>
                            </td>
                        </tr>
                        <tr class="em-c-table__row ">
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">DOAG Level 2
                            </td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">BD, MC, or CE</td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">50,000
                            </td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                <a class="em-c-btn" href="EditDOAG.aspx"><span class="em-c-btn__text">Edit</span></a>
                            </td>
                        </tr>
                        <tr class="em-c-table__row ">
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">DOAG Level 1
                            </td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable"> BD, MC, or CE</td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">Unlimited
                            </td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                <a class="em-c-btn" href="EditDOAG.aspx"><span class="em-c-btn__text">Edit</span></a>
                            </td>
                        </tr>
                    </tbody>
                    <!-- end em-c-table__body -->
                    <tfoot class="em-c-table__footer">
                        <tr class="em-c-table__footer-row">
                        </tr>
                    </tfoot>
                    <!-- end em-c-table__footer -->
                </table>
                <!--end em-c-table-->
            </div>
            <!--end em-c-table-object__body-inner-->
        </div>
        <!--end em-c-table-object__body-->
    </div>
    <!--end em-c-table-object-->

</asp:Content>
