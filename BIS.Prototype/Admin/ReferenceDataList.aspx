﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_includes/MasterPage.master" AutoEventWireup="true" CodeBehind="ReferenceDataList.aspx.cs" Inherits="XOM.BIS.Prototype.Web.Admin.ReferenceData.AddReferenceData" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Title" runat="server">
    Reference Data
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SubTitle" runat="server">
    Budget Class
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <div>

        <div class="em-c-btn-bar ">
            <ul class="em-c-btn-bar__list">
                <li class="em-c-btn-bar__item ">
                    <button class="em-c-btn em-c-btn--small em-js-btn-selectable">
                        <span class="em-c-btn__text">Budget Class</span>
                    </button>
                    <!-- end em-c-btn -->
                </li>
                <li class="em-c-btn-bar__item em-c-btn-bar__item--separator"></li>
                <li class="em-c-btn-bar__item ">
                    <button class="em-c-btn em-c-btn--small em-js-btn-selectable">
                        <span class="em-c-btn__text">Funding</span>
                    </button>
                    <!-- end em-c-btn -->
                </li>
                <li class="em-c-btn-bar__item em-c-btn-bar__item--separator"></li>
                <li class="em-c-btn-bar__item ">
                    <button class="em-c-btn em-c-btn--small em-js-btn-selectable">
                        <span class="em-c-btn__text">Invest</span>
                    </button>
                    <!-- end em-c-btn -->
                </li>
                <li class="em-c-btn-bar__item em-c-btn-bar__item--separator"></li>
                <li class="em-c-btn-bar__item ">
                    <button class="em-c-btn em-c-btn--small em-js-btn-selectable">
                        <span class="em-c-btn__text">Type</span>
                    </button>
                    <!-- end em-c-btn -->
                </li>
            </ul>
            <!-- end em-c-btn-bar__list -->
        </div>
        <!-- end em-c-btn-bar -->
    </div>
    <div class="em-u-text-align-right em-u-margin-bottom-double">
        <a class="em-c-btn em-c-btn--primary" href="AddReferenceData.aspx"><span class="em-c-btn__text">Add</span></a>
    </div>
    <div class="em-c-table-object">
        <div class="em-c-table-object__header">
        </div>
        <!--end em-c-table-object__header-->
        <div class="em-c-table-object__body">
            <div class="em-c-table-object__body-inner">
                <table class="em-c-table ">
                    <thead class="em-c-table__header">
                        <tr class="em-c-table__header-row">
                            <th scope="col" class="em-c-table__header-cell ">Description</th>
                            <th scope="col" class="em-c-table__header-cell ">Sort</th>
                            <th scope="col" class="em-c-table__header-cell ">Status</th>
                            <th scope="col" class="em-c-table__header-cell "></th>
                        </tr>
                        <!-- em-c-table__header-row -->
                    </thead>
                    <!-- end em-c-table__header -->
                    <tbody class="em-c-table__body ">
                        <tr class="em-c-table__row ">
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">$250K - $500K 
                            </td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">1</td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">Active</td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                <a class="em-c-btn" href="EditReferenceData.aspx"><span class="em-c-btn__text">Edit</span></a>
                            </td>
                        </tr>
                        <tr class="em-c-table__row ">
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">$500K - $1M 
                            </td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">2</td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">Active</td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                <a class="em-c-btn" href="EditReferenceData.aspx"><span class="em-c-btn__text">Edit</span></a>
                            </td>
                        </tr>
                        <tr class="em-c-table__row ">
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">$1M - $5M 
                            </td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">3</td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">Active</td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                <a class="em-c-btn" href="EditReferenceData.aspx"><span class="em-c-btn__text">Edit</span></a>
                            </td>
                        </tr>
                        <tr class="em-c-table__row ">
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">$5M - $10M
                            </td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">4</td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">Active</td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                <a class="em-c-btn" href="EditReferenceData.aspx"><span class="em-c-btn__text">Edit</span></a>
                            </td>
                        </tr>
                        <tr class="em-c-table__row ">
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">$10M - $15M
                            </td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">5</td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">Active</td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                <a class="em-c-btn" href="EditReferenceData.aspx"><span class="em-c-btn__text">Edit</span></a>
                            </td>
                        </tr>
                        <tr class="em-c-table__row ">
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">$15M-$50M
                            </td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">6</td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">Active</td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                <a class="em-c-btn" href="EditReferenceData.aspx"><span class="em-c-btn__text">Edit</span></a>
                            </td>
                        </tr>
                        <tr class="em-c-table__row ">
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">>$50M
                            </td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">7</td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">Active</td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                <a class="em-c-btn" href="EditReferenceData.aspx"><span class="em-c-btn__text">Edit</span></a>
                            </td>
                        </tr>
                    </tbody>
                    <!-- end em-c-table__body -->
                    <tfoot class="em-c-table__footer">
                        <tr class="em-c-table__footer-row">
                        </tr>
                    </tfoot>
                    <!-- end em-c-table__footer -->
                </table>
                <!--end em-c-table-->
            </div>
            <!--end em-c-table-object__body-inner-->
        </div>
        <!--end em-c-table-object__body-->
    </div>
    <!--end em-c-table-object-->

</asp:Content>
