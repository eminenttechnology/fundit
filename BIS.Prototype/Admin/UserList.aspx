﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_includes/MasterPage.master" AutoEventWireup="true" CodeBehind="UserList.aspx.cs" Inherits="XOM.BIS.Prototype.Web.Admin.RolesList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Title" runat="server">
    Users
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SubTitle" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
        <div class="em-u-text-align-right em-u-margin-bottom-double">
        <a class="em-c-btn em-c-btn--primary" href="AddUser.aspx"><span class="em-c-btn__text">Add</span></a>
    </div>
    <div class="em-c-table-object">
        <div class="em-c-table-object__header">
        </div>
        <!--end em-c-table-object__header-->
        <div class="em-c-table-object__body">
            <div class="em-c-table-object__body-inner">
                <table class="em-c-table ">
                    <thead class="em-c-table__header">
                        <tr class="em-c-table__header-row">
                            <th scope="col" class="em-c-table__header-cell ">Name</th>
                            <th scope="col" class="em-c-table__header-cell ">Role(s)</th>
                            <th scope="col" class="em-c-table__header-cell ">Status</th>
                            <th></th>
                        </tr>
                        <!-- em-c-table__header-row -->
                    </thead>
                    <!-- end em-c-table__header -->
                    <tbody class="em-c-table__body ">
                        <tr class="em-c-table__row ">
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">Jeffrey L. Williams</td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">Controller Endorser</td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">Active</td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                <a class="em-c-btn" href="EditUser.aspx"><span class="em-c-btn__text">Edit</span></a>
                            </td>
                        </tr>
                        <tr class="em-c-table__row ">
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">Suzanne Daroowala</td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">Controller Endorser</td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">In-active</td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                <a class="em-c-btn" href="EditUser.aspx"><span class="em-c-btn__text">Edit</span></a>
                            </td>
                        </tr>
                        <tr class="em-c-table__row ">
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">Zain Willoughby</td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">Controller Endorser</td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">Active</td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                <a class="em-c-btn" href="EditUser.aspx"><span class="em-c-btn__text">Edit</span></a>
                            </td>
                        </tr>
                        <tr class="em-c-table__row ">
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">Shantell D. Brown</td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">Division Function Planner</td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">Active</td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                <a class="em-c-btn" href="EditUser.aspx"><span class="em-c-btn__text">Edit</span></a>
                            </td>
                        </tr>
                        <tr class="em-c-table__row ">
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">Mark Frazier</td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">Division Function Planner</td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">Active</td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                <a class="em-c-btn" href="EditUser.aspx"><span class="em-c-btn__text">Edit</span></a>
                            </td>
                        </tr>
                    </tbody>
                    <!-- end em-c-table__body -->
                    <tfoot class="em-c-table__footer">
                        <tr class="em-c-table__footer-row">
                        </tr>
                    </tfoot>
                    <!-- end em-c-table__footer -->
                </table>
                <!--end em-c-table-->
            </div>
            <!--end em-c-table-object__body-inner-->
        </div>
        <!--end em-c-table-object__body-->
    </div>
    <!--end em-c-table-object-->

</asp:Content>
