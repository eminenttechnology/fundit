﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_includes/MasterPage.master" AutoEventWireup="true" CodeBehind="AddUser.aspx.cs" Inherits="XOM.BIS.Prototype.Web.Admin.AddUser" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Title" runat="server">
    Add User
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SubTitle" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <div class="em-l__main">
        <div class="em-l-linelength-container">
            <fieldset class="em-c-fieldset">
                <div class="em-c-field ">
                    <label for="file" class="em-c-field__label">Name</label>
                    <div class="em-c-field__body">
                        <input class="em-c-input" placeholder="" value="">
                    </div>
                </div>
                <div class="em-c-field ">

                    <div class="em-c-dropdown-check em-js-dropdown-check">
                        <button class="em-c-btn em-c-btn--small em-c-btn--is-dropdown em-js-dropdown-trigger">
                            <div class="em-c-btn__inner">
                                <span class="em-c-btn__text">Roles</span>
                                <svg class="em-c-icon em-c-btn__icon" style="height:10px; width:10px">
                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down"></use>
                                </svg>
                            </div>
                        </button>
                        <!-- end em-c-btn -->
                        <div class="em-c-dropdown-check__panel">
                            <ul class="em-c-dropdown-check__list">
                                <li class="em-c-dropdown-check__item">
                                    <label class="em-c-input-group " for="dropdown-check-option-1">
                                        <input id="dropdown-check-option-1" type="checkbox" name="" value="" class="em-c-input-group__control">
                                        <span class="em-c-input-group__text">Controller Endorser</span>
                                    </label>
                                    <!-- end em-c-input-group -->
                                </li>
                                <li class="em-c-dropdown-check__item">
                                    <label class="em-c-input-group " for="dropdown-check-option-2">
                                        <input id="dropdown-check-option-2" type="checkbox" name="" value="" class="em-c-input-group__control">
                                        <span class="em-c-input-group__text">Division Function Planner</span>
                                    </label>
                                    <!-- end em-c-input-group -->
                                </li>
                                <li class="em-c-dropdown-check__item">
                                    <label class="em-c-input-group " for="dropdown-check-option-3">
                                        <input id="dropdown-check-option-3" type="checkbox" name="" value="" class="em-c-input-group__control">
                                        <span class="em-c-input-group__text">Endorsing Functional Manager</span>
                                    </label>
                                    <!-- end em-c-input-group -->
                                </li>
                            </ul>
                        </div>
                        <!-- End em-c-dropdown-check__panel -->
                    </div>
                    <!-- End em-c-dropdown-check -->
                </div>
                <div class="em-c-btn-group ">
                    <a href="UserList.aspx" class="em-c-btn em-c-btn--primary">
                        <span class="em-c-btn__text">Submit</span>
                    </a>
                    <!-- end em-c-btn -->
                    <a href="UserList.aspx" class="em-c-btn em-c-btn--secondary">
                        <span class="em-c-btn__text">Cancel</span>
                    </a>
                    <!-- end em-c-btn -->
                </div>
                <!-- end em-c-btn-group -->
            </fieldset>
        </div>
    </div>

</asp:Content>
