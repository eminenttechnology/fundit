﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_includes/MasterPageMenu.master" AutoEventWireup="true" CodeBehind="Edit.aspx.cs" Inherits="XOM.BIS.Prototype.Web.TransferFunding.Edit" %>

<%@ Register Src="~/_includes/Menu_TransferFund.ascx" TagName="Menu" TagPrefix="app" %>
<asp:Content ID="Content1" ContentPlaceHolderID="PageMenu" runat="server">
    <app:Menu runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Title" runat="server">
    Request 3443 - Transfer Funds
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="SubTitle" runat="server">
    Edit Request
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="server">
    <div class="em-c-page-header em-c-page-header--small">
        <h1 class="em-c-page-header__title">Edit Request</h1>
    </div>
    <div class="em-c-table-object__body">
        <div class="em-c-table-object__body-inner">
            <table class="em-c-table ">
                <thead class="em-c-table__header">
                    <tr class="em-c-table__header-row">
                        <th scope="col" class="em-c-table__header-cell ">Category</th>
                        <th scope="col" class="em-c-table__header-cell ">PPL #</th>
                        <th scope="col" class="em-c-table__header-cell ">AR #</th>
                        <th scope="col" class="em-c-table__header-cell ">Cap ex</th>
                        <th scope="col" class="em-c-table__header-cell ">Cap Remaining</th>
                        <th scope="col" class="em-c-table__header-cell ">Cap Spend</th>
                        <th scope="col" class="em-c-table__header-cell ">Description</th>
                        <th scope="col" class="em-c-table__header-cell ">Quantity</th>
                        <th scope="col" class="em-c-table__header-cell ">Company Code</th>
                        <th scope="col" class="em-c-table__header-cell ">Country</th>
                        <th scope="col" class="em-c-table__header-cell ">Organization</th>
                        <th scope="col" class="em-c-table__header-cell ">Location</th>
                        <th scope="col" class="em-c-table__header-cell ">Type</th>
                        <th scope="col" class="em-c-table__header-cell ">Cost Center</th>
                        <th scope="col" class="em-c-table__header-cell ">WBS</th>
                        <th scope="col" class="em-c-table__header-cell ">Network Order</th>
                    </tr>
                    <!-- em-c-table__header-row -->
                </thead>
                <!-- end em-c-table__header -->
                <tbody class="em-c-table__body ">
                    <tr class="em-c-table__row ">
                        <td class="em-c-table__cell em-js-cell">Hardware
                        </td>
                        <td class="em-c-table__cell em-js-cell">23
                        </td>
                        <td class="em-c-table__cell em-js-cell">245
                        </td>
                        <td class="em-c-table__cell em-js-cell">$200K
                        </td>
                        <td class="em-c-table__cell em-js-cell">
                            <div class="em-c-field__body">
                                <input style="width: 100px;" type="number" id="" class="em-c-input" value="100">
                            </div>
                        </td>
                        <td class="em-c-table__cell em-js-cell">$100K
                        </td>
                        <td class="em-c-table__cell em-js-cell">TDI Lenovo Servers, HBA Fibre Cards
                        </td>
                        <td class="em-c-table__cell em-js-cell">8
                        </td>
                        <td class="em-c-table__cell em-js-cell">0970
                        </td>
                        <td class="em-c-table__cell em-js-cell">United States
                        </td>
                        <td class="em-c-table__cell em-js-cell"></td>
                        <td class="em-c-table__cell em-js-cell">Texas - Houston - Data Centers
                        </td>
                        <td class="em-c-table__cell em-js-cell">SAP Midrange
                        </td>
                        <td class="em-c-table__cell em-js-cell"></td>
                        <td class="em-c-table__cell em-js-cell"></td>
                        <td class="em-c-table__cell em-js-cell"></td>
                    </tr>
                    <tr class="em-c-table__row ">
                        <td class="em-c-table__cell em-js-cell">Software
                        </td>
                        <td class="em-c-table__cell em-js-cell">56
                        </td>
                        <td class="em-c-table__cell em-js-cell">112
                        </td>
                        <td class="em-c-table__cell em-js-cell">$200K
                        </td>
                        <td class="em-c-table__cell em-js-cell">
                            <div class="em-c-field__body">
                                <input type="number" id="" class="em-c-input" value="56">
                            </div>
                        </td>
                        <td class="em-c-table__cell em-js-cell">$144K
                        </td>
                        <td class="em-c-table__cell em-js-cell">Microsoft
                        </td>
                        <td class="em-c-table__cell em-js-cell">100                           
                        </td>
                        <td class="em-c-table__cell em-js-cell">0970
                        </td>
                        <td class="em-c-table__cell em-js-cell">United States
                        </td>
                        <td class="em-c-table__cell em-js-cell"></td>
                        <td class="em-c-table__cell em-js-cell">Texas - Houston - Data Centers
                        </td>
                        <td class="em-c-table__cell em-js-cell">SAP Midrange
                        </td>
                        <td class="em-c-table__cell em-js-cell"></td>
                        <td class="em-c-table__cell em-js-cell"></td>
                        <td class="em-c-table__cell em-js-cell"></td>
                    </tr>
                </tbody>
                <!-- end em-c-table__body -->
                <tfoot class="em-c-table__footer">
                    <tr class="em-c-table__footer-row">
                        <td>
                            <label>Total: $400k</label>
                        </td>
                    </tr>
                </tfoot>
                <!-- end em-c-table__footer -->
            </table>
            <!--end em-c-table-->
        </div>
        <br />
        <a href="Overview.aspx" class="em-c-btn em-c-btn--primary">
            <span class="em-c-btn__text">Save</span>
        </a>
        <a href="Overview.aspx" class="em-c-btn em-c-btn--secondary">
            <span class="em-c-btn__text">Cancel</span>
        </a>
        <!--end em-c-table-object__body-inner-->
    </div>

</asp:Content>
