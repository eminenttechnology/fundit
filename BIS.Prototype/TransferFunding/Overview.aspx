﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_includes/MasterPageMenu.master" AutoEventWireup="true" CodeBehind="Overview.aspx.cs" Inherits="XOM.BIS.Prototype.Web.TransferFunding.Overview" %>

<%@ Register Src="~/_includes/Menu_TransferFund.ascx" TagName="Menu" TagPrefix="app" %>
<asp:Content ID="Content1" ContentPlaceHolderID="PageMenu" runat="server">
    <app:Menu runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Title" runat="server">
    Request 3443 - Transfer Funds
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="SubTitle" runat="server">
    Overview
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="server">
    <div class="em-c-page-header em-c-page-header--small">
        <h1 class="em-c-page-header__title">General Info</h1>
    </div>

    <div class="em-l-grid em-l-grid--2up">
        <div class="em-l-grid__item">
            <div class="em-c-table-object">

                <div class="em-l-grid em-l-grid--2up ">
                    <div class="em-l-grid__item">
                        <label>BIS #:</label>
                    </div>
                    <div class="em-l-grid__item">
                        <div>3443</div>
                    </div>
                </div>
                <div class="em-l-grid em-l-grid--2up ">
                    <div class="em-l-grid__item">
                        <label>Work ID:</label>
                    </div>
                    <div class="em-l-grid__item">
                        <a href="../project/overview.aspx" style="color: blue; text-decoration: underline;">12342</a>
                    </div>
                </div>
                <div class="em-l-grid em-l-grid--2up ">
                    <div class="em-l-grid__item">
                        <label>Work Name:</label>
                    </div>
                    <div class="em-l-grid__item">
                        <a href="../project/overview.aspx" style="color: blue; text-decoration: underline;">GVS Day 1 IT Enablement</a>
                    </div>
                </div>
                <div class="em-l-grid em-l-grid--2up ">
                    <div class="em-l-grid__item">
                        <label>Funding Type:</label>
                    </div>
                    <div class="em-l-grid__item">
                        <div>Cumulative Advance Funding</div>
                    </div>
                </div>
                <div class="em-l-grid em-l-grid--2up ">
                    <div class="em-l-grid__item">
                        <label>Requestor</label>
                    </div>
                    <!-- end em-l-grid__item -->
                    <div class="em-l-grid__item">
                        <div>Shantell D. Brown</div>
                    </div>
                </div>

                <div class="em-l-grid em-l-grid--2up ">
                    <div class="em-l-grid__item">
                        <label>Project Manager:</label>
                    </div>
                    <div class="em-l-grid__item">
                        <div>Mary Mayweather</div>
                    </div>
                </div>
                <div class="em-l-grid em-l-grid--2up ">
                    <div class="em-l-grid__item">
                        <label>PCA:</label>
                    </div>
                    <div class="em-l-grid__item">
                        <div>John Doe</div>
                    </div>
                </div>
                <div class="em-l-grid em-l-grid--2up ">
                    <div class="em-l-grid__item">
                        <label>Endorsing Controller:</label>
                    </div>
                    <div class="em-l-grid__item">
                        Jeffrey L. Williams
                    </div>
                </div>

                <div class="em-l-grid em-l-grid--2up ">
                        <div class="em-l-grid__item">
                            <label>Approver:</label>
                        </div>
                        <div class="em-l-grid__item">
                           Mike Brown
                        </div>
                    </div>


                <div class="em-l-grid em-l-grid--2up ">
                    <div class="em-l-grid__item">
                        <label>Status :</label>
                    </div>
                    <div class="em-l-grid__item">
                        <span id="MainContent_General_Info_Status_ctrl" style="display:none">Draft</span>
                           <%-- <button class="em-c-btn em-c-btn--primary em-u-padding-half" onclick="Status_ChangeWorkflowStatus(MainContent_General_Info_Status_ctrl)">
                                        <span>Draft - Change Status</span>
                                    </button>--%>
                             <span class="em-c-btn em-c-btn--primary em-u-padding-half" onclick="Status_ChangeWorkflowStatus(MainContent_General_Info_Status_ctrl)">
                                        <span>Draft - Change Status</span>
                                    </span>

                    </div>
                </div>


            </div>
        </div>
        <div class="em-l-grid__item">
            <div class="em-l-grid em-l-grid--2up ">
                <div class="em-l-grid__item">
                    <label>Amount Requested</label>
                </div>
                <!-- end em-l-grid__item -->
                <div class="em-l-grid__item">
                    <div>$400K</div>
                </div>
            </div>
            <div class="em-l-grid em-l-grid--2up ">
                <div class="em-l-grid__item">
                    <label>Total Amount Approved:</label>
                </div>
                <div class="em-l-grid__item">
                    <div>$650K</div>
                </div>
            </div>
            <div class="em-l-grid em-l-grid--2up ">
                <div class="em-l-grid__item">
                    <label>Amount Spent:</label>
                </div>
                <div class="em-l-grid__item">
                    <div>$150K</div>
                </div>
            </div>
            <div class="em-l-grid em-l-grid--2up ">
                <div class="em-l-grid__item">
                    <label>Balance:</label>
                </div>
                <div class="em-l-grid__item">
                    <div>$500K</div>
                </div>
            </div>
        </div>
        <!-- end em-l-grid__item -->
    </div>

    <div class="em-l-grid em-l-grid--1up">
        <div class="em-l-grid__item">
            <div class="em-c-table-object">
                <div class="em-l-grid em-l-grid--2up ">
                    <div class="em-l-grid__item">
                        <div class="em-c-page-header em-c-page-header--small">
                            <h1 class="em-c-page-header__title">Appropriation Requests</h1>
                        </div>
                    </div>
                </div>
                <!--end em-c-table-object__header-->
                <div class="em-c-table-object__body">
                    <div class="em-c-table-object__body-inner">
                        <table class="em-c-table ">
                            <thead class="em-c-table__header">
                                <tr class="em-c-table__header-row">
                                    <th scope="col" class="em-c-table__header-cell ">Category</th>
                                    <th scope="col" class="em-c-table__header-cell ">PPL #</th>
                                    <th scope="col" class="em-c-table__header-cell ">AR #</th>
                                    <th scope="col" class="em-c-table__header-cell ">Company Code</th>
                                    <th scope="col" class="em-c-table__header-cell ">Country</th>
                                    <th scope="col" class="em-c-table__header-cell ">Organization</th>
                                    <th scope="col" class="em-c-table__header-cell ">Location</th>
                                    <th scope="col" class="em-c-table__header-cell ">Type</th>
                                    <th scope="col" class="em-c-table__header-cell ">Cost Center</th>
                                    <th scope="col" class="em-c-table__header-cell ">WBS</th>
                                    <th scope="col" class="em-c-table__header-cell ">Network Order</th>
                                    <th scope="col" class="em-c-table__header-cell ">Description</th>
                                    <th scope="col" class="em-c-table__header-cell ">Quantity</th>
                                    <th scope="col" class="em-c-table__header-cell ">Capex</th>
                                    <th scope="col" class="em-c-table__header-cell ">Cap Spend</th>
                                    <th scope="col" class="em-c-table__header-cell ">Cap Remaining</th>
                                </tr>
                                <!-- em-c-table__header-row -->
                            </thead>
                            <!-- end em-c-table__header -->
                            <tbody class="em-c-table__body ">
                                <tr class="em-c-table__row ">
                                    <td class="em-c-table__cell em-js-cell">Hardware
                                    </td>
                                    <td class="em-c-table__cell em-js-cell">23
                                    </td>
                                    <td class="em-c-table__cell em-js-cell">245
                                    </td>
                                    <td class="em-c-table__cell em-js-cell">0970
                                    </td>
                                    <td class="em-c-table__cell em-js-cell">United States
                                    </td>
                                    <td class="em-c-table__cell em-js-cell"></td>
                                    <td class="em-c-table__cell em-js-cell">Texas - Houston - Data Centers
                                    </td>
                                    <td class="em-c-table__cell em-js-cell">SAP Midrange
                                    </td>
                                    <td class="em-c-table__cell em-js-cell"></td>
                                    <td class="em-c-table__cell em-js-cell"></td>
                                    <td class="em-c-table__cell em-js-cell"></td>
                                    <td class="em-c-table__cell em-js-cell">TDI Lenovo Servers, HBA Fibre Cards
                                    </td>
                                    <td class="em-c-table__cell em-js-cell">8
                                    </td>
                                    <td class="em-c-table__cell em-js-cell">$200K
                                    </td>
                                    <td class="em-c-table__cell em-js-cell">$100K
                                    </td>
                                    <td class="em-c-table__cell em-js-cell">$100K
                                    </td>
                                </tr>
                                <tr class="em-c-table__row ">
                                    <td class="em-c-table__cell em-js-cell">Software
                                    </td>
                                    <td class="em-c-table__cell em-js-cell">56
                                    </td>
                                    <td class="em-c-table__cell em-js-cell">112
                                    </td>

                                    <td class="em-c-table__cell em-js-cell">0970
                                    </td>
                                    <td class="em-c-table__cell em-js-cell">United States
                                    </td>
                                    <td class="em-c-table__cell em-js-cell"></td>
                                    <td class="em-c-table__cell em-js-cell">Texas - Houston - Data Centers
                                    </td>
                                    <td class="em-c-table__cell em-js-cell">SAP Midrange
                                    </td>
                                    <td class="em-c-table__cell em-js-cell"></td>
                                    <td class="em-c-table__cell em-js-cell"></td>
                                    <td class="em-c-table__cell em-js-cell"></td>
                                    <td class="em-c-table__cell em-js-cell">Microsoft
                                    </td>
                                    <td class="em-c-table__cell em-js-cell">100
                                    </td>
                                    <td class="em-c-table__cell em-js-cell">$200K
                                    </td>
                                    <td class="em-c-table__cell em-js-cell">$56K
                                    </td>
                                    <td class="em-c-table__cell em-js-cell">$144K
                                    </td>
                                </tr>
                            </tbody>
                            <!-- end em-c-table__body -->
                            <tfoot class="em-c-table__footer">
                                <tr class="em-c-table__footer-row">
                                </tr>
                            </tfoot>
                            <!-- end em-c-table__footer -->
                        </table>
                        <!--end em-c-table-->
                    </div>
                    <!--end em-c-table-object__body-inner-->
                </div>
                <!--end em-c-table-object__body-->
            </div>
            <!--end em-c-table-object-->
        </div>

    </div>
    <!-- end em-l-grid -->
      <script>
        function Status_ChangeWorkflowStatus(field) {
            $.fancybox.open({
                type: 'iframe',
                href: '../../Templates/Pages/WorkflowStatusPicker.aspx?WorkflowCode=ONBOARD&EntityID=1783&FieldName=MainContent_General_Info_Status_ctrl&Reload=True',
                height: 310,
                width: 600
            });

        }

    </script>

</asp:Content>
