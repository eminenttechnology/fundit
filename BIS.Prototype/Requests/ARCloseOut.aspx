﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_includes/MasterPageMenu.master" AutoEventWireup="true" CodeBehind="ARCloseOut.aspx.cs" Inherits="XOM.BIS.Prototype.Web.Requests.ARCloseOut" %>
<%@ Register src="~/_includes/Menu_Request.ascx" tagname="Menu" tagprefix="app" %>
<asp:Content ID="Content1" ContentPlaceHolderID="PageMenu" runat="server"><app:Menu runat=server />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Title" runat="server">
    Request 001 - Discretionary Project
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="SubTitle" runat="server">
    AR CloseOut
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="server">
    <div class="em-l-container">
    <div class="em-c-table-object ">
        <div class="em-c-page-header em-c-page-header--small">
            <h1 class="em-c-page-header__title">AR Closeout List</h1>
        </div>
        <!--end em-c-table-object__header-->
        <div class="em-c-table-object__body">
            <div class="em-c-table-object__body-inner">
                <table class="em-c-table ">
                    <thead class="em-c-table__header">
                        <tr class="em-c-table__header-row">                          
                            <th scope="col" class="em-c-table__header-cell "></th>
                            <th scope="col" class="em-c-table__header-cell ">Category</th>
                            <th scope="col" class="em-c-table__header-cell ">PPL #</th>
                            <th scope="col" class="em-c-table__header-cell ">AR #</th>
                            <th scope="col" class="em-c-table__header-cell ">Status</th>
                            <th scope="col" class="em-c-table__header-cell ">Company Code</th>
                            <th scope="col" class="em-c-table__header-cell ">Country</th>
                            <th scope="col" class="em-c-table__header-cell ">Organization</th>
                            <th scope="col" class="em-c-table__header-cell ">Location</th>
                            <th scope="col" class="em-c-table__header-cell ">Type</th>
                            <th scope="col" class="em-c-table__header-cell ">Cost Center</th>
                            <th scope="col" class="em-c-table__header-cell ">Description</th>
                            <th scope="col" class="em-c-table__header-cell ">Quantity</th>
                            <th scope="col" class="em-c-table__header-cell ">Capex ($K)</th>
                            <th scope="col" class="em-c-table__header-cell ">Cap Spend ($K)</th>  
                      </tr>
                      <!-- em-c-table__header-row -->
                    </thead>
                    <!-- end em-c-table__header -->
                    <tbody class="em-c-table__body ">
                        <tr class="em-c-table__row ">
                            <td class="em-c-table__cell em-js-cell">
                                <button class="em-c-btn em-c-btn--secondary em-c-btn--small em-c-btn--inverted">
                                    <span class="em-c-btn__text">Close Out</span>
                                </button>
                            </td>
                            <td class="em-c-table__cell em-js-cell">Hardware</td>
                            <td class="em-c-table__cell em-js-cell em-u-width-10">01</td>
                            <td class="em-c-table__cell em-js-cell">
                                001
                            </td>
                            <td class="em-c-table__cell em-js-cell">
                                Approved
                            </td>
                            <td class="em-c-table__cell em-js-cell">
                                0970
                            </td>
                            <td class="em-c-table__cell em-js-cell">
                                United States
                            </td>
                            <td class="em-c-table__cell em-js-cell">
                                
                            </td>
                            <td class="em-c-table__cell em-js-cell">
                                Texas - Houston - Data Centers
                            </td>
                            <td class="em-c-table__cell em-js-cell">
                                SAP Midrange
                            </td>
                            <td class="em-c-table__cell em-js-cell">
                                Contact EMIT BAR
                            </td>
                            <td class="em-c-table__cell em-js-cell">
                                TDI Lenovo Servers, HBA Fibre Cards
                            </td>
                            <td class="em-c-table__cell em-js-cell">
                                8
                            </td>
                            <td class="em-c-table__cell em-js-cell">
                                621.06
                            </td>
                            <td class="em-c-table__cell em-js-cell">
                                4,968.48
                            </td>
                        </tr>
                        <tr class="em-c-table__row ">
                            <td class="em-c-table__cell em-js-cell">
                                <button class="em-c-btn em-c-btn--secondary em-c-btn--small em-c-btn--inverted">
                                    <span class="em-c-btn__text">Close Out</span>
                                </button>
                            </td>
                            <td class="em-c-table__cell em-js-cell">
                                Hardware
                            </td>
                            <td class="em-c-table__cell em-js-cell">
                                02
                            </td>
                            <td class="em-c-table__cell em-js-cell">
                                001
                            </td>
                            <td class="em-c-table__cell em-js-cell">
                                Approved
                            </td>
                            <td class="em-c-table__cell em-js-cell">
                                0970
                            </td>
                            <td class="em-c-table__cell em-js-cell">
                                United States
                            </td>
                            <td class="em-c-table__cell em-js-cell">
                                
                            </td>
                            <td class="em-c-table__cell em-js-cell">
                                Texas - Houston - Data Centers
                            </td>
                            <td class="em-c-table__cell em-js-cell">
                                SAP Midrange
                            </td>
                            <td class="em-c-table__cell em-js-cell">
                                Contact EMIT BAR
                            </td>
                            <td class="em-c-table__cell em-js-cell">
                                Primary and Backup NetApp/Symmetrix
                            </td>
                            <td class="em-c-table__cell em-js-cell">
                                4
                            </td>
                            <td class="em-c-table__cell em-js-cell">
                                978.86
                            </td>
                            <td class="em-c-table__cell em-js-cell">
                                3,915.44
                            </td>
                        </tr>
                        <tr class="em-c-table__row ">
                            <td class="em-c-table__cell em-js-cell">
                                <button class="em-c-btn em-c-btn--secondary em-c-btn--small em-c-btn--inverted">
                                    <span class="em-c-btn__text">Close Out</span>
                                </button>
                            </td>
                            <td class="em-c-table__cell em-js-cell">
                                Software
                            </td>
                            <td class="em-c-table__cell em-js-cell">
                                03
                            </td>
                            <td class="em-c-table__cell em-js-cell">
                                001
                            </td>
                            <td class="em-c-table__cell em-js-cell">
                                Approved
                            </td>
                            <td class="em-c-table__cell em-js-cell">
                                0970
                            </td>
                            <td class="em-c-table__cell em-js-cell">
                                United States
                            </td>
                            <td class="em-c-table__cell em-js-cell">
                                Applications
                            </td>
                            <td class="em-c-table__cell em-js-cell">
                                Texas - Houston - Data Centers
                            </td>
                            <td class="em-c-table__cell em-js-cell">
                                NSAP Common Cost
                            </td>
                            <td class="em-c-table__cell em-js-cell">
                                C0802ANL05
                            </td>
                            <td class="em-c-table__cell em-js-cell">
                                Sitecore and MangoDB licenses
                            </td>
                            <td class="em-c-table__cell em-js-cell">
                                4
                            </td>
                            <td class="em-c-table__cell em-js-cell">
                                37
                            </td>
                            <td class="em-c-table__cell em-js-cell">
                                151 
                            </td>
                        </tr>
                    </tbody>
                    <!-- end em-c-table__body -->
                    <tfoot class="em-c-table__footer">
                        <tr class="em-c-table__footer-row">
                        </tr>
                    </tfoot>
                <!-- end em-c-table__footer -->
                </table>
                <!--end em-c-table-->
            </div>
            <!--end em-c-table-object__body-inner-->
        </div>
            <!--end em-c-table-object__body-->
    </div>
            <!--end em-c-table-object-->
    </div>
</asp:Content>
