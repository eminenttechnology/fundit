﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_includes/MasterPage.master" AutoEventWireup="true" CodeBehind="AddDocument.aspx.cs" Inherits="XOM.BIS.Prototype.Web.Requests.AddDocument" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Title" runat="server">
    Request 001 - Discretionary Project
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SubTitle" runat="server">
    Add Document
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    
    <fieldset class="em-c-fieldset">
        <div class="em-c-page-header em-c-page-header--small">
            <h1 class="em-c-page-header__title">Document</h1>
        </div>
        <div class="em-c-field ">
            <label for="" class="em-c-field__label">Name</label>
            <div class="em-c-field__body">
                <input type="text" id="" class="em-c-input" placeholder="" value="" />
            </div>
        </div>
      
        <div class="em-c-field em-c-field--file-upload ">
            <label for="file" class="em-c-field__label">File</label>
            <div class="em-c-field__body">
                <svg class="em-c-icon em-c-field__block-icon" style="height:40px">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-upload"></use>
                </svg>
                <input type="file" name="file[]" id="file" class="em-c-file-upload" value="" multiple="" />
                <ul class="em-c-field__list em-js-field-list" style="align">
                    <li class="em-c-field__item">Drag files here</li>
                    <li class="em-c-field__item em-c-field__item--small">Or click to choose file</li>
                </ul>
            </div>
        </div>
        
        <a href="Document.aspx" class="em-c-btn em-c-btn--primary">
            <span class="em-c-btn__text">Submit</span>
        </a>
        <a href="Document.aspx" class="em-c-btn em-c-btn--secondary">
            <span class="em-c-btn__text">Cancel</span>
        </a>
    </fieldset>
</asp:Content>
