﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_includes/MasterPage.master" AutoEventWireup="true" CodeBehind="Search.aspx.cs" Inherits="XOM.BIS.Prototype.Web.Requests.Search" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Title" runat="server">
    Request
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SubTitle" runat="server">
    Search
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">

        <div class="em-c-page-header em-c-page-header--small">
            <h1 class="em-c-page-header__title">Request Search Criteria</h1>
        </div>
        <div class="em-c-collapsible-toolbar__panel em-js-toolbar-panel">
            <div class="em-c-toolbar">
                <div class="em-l-grid em-l-grid--5up ">
                    <div class="em-l-grid__item">
                        <div class="em-c-field ">
                            <label for="" class="em-c-field__label">BIS #</label>
                            <div class="em-c-field__body">
                                <input type="text" id="" class="em-c-input" value="" />
                            </div>
                        </div>
                    </div>
                    <div class="em-l-grid__item">
                        <div class="em-c-field ">
                            <label for="" class="em-c-field__label">Requestor</label>
                            <div class="em-c-field__body">
                                <input type="text" id="" class="em-c-input" value="" />
                            </div>
                        </div>
                    </div>
                    <div class="em-l-grid__item">
                        <div class="em-c-field ">
                            <label for="" class="em-c-field__label">Request type</label>
                            <div class="em-c-field__body">
                                <select class="em-c-select em-c-select em-u-width-100">
                                    <option value=""></option>
                                    <option value="option-1">Request Funding</option>
                                    <option value="option-2">Transfer Funds</option>
                                    <option value="option-2">AR Closeout</option>
                                    <option value="option-2">Project Closeout</option>
                                     <option value="option-2">Request WBS</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="em-l-grid__item">
                        <div class="em-c-field ">
                            <label for="" class="em-c-field__label">Project Manager</label>
                            <div class="em-c-field__body">
                                <input type="text" id="" class="em-c-input" value="" />
                            </div>
                        </div>
                    </div>
                    <div class="em-l-grid__item">
                        <div class="em-c-field ">
                            <label for="" class="em-c-field__label">Status</label>
                            <div class="em-c-field__body">
                                <select class="em-c-select em-c-select em-u-width-100">
                                    <option value=""></option>
                                       <option value="option-2">Draft</option>
                                    <option value="option-1">Submitted for Review</option>
                                      <option value="option-2">Submtted for Approval</option>
                                    <option value="option-2">Submitted for Endorsement</option>
                                    <option value="option-2">Approved</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="em-l-grid__item">
                        <div class="em-c-field ">
                            <button class="em-c-btn ">
                                <span class="em-c-btn__text em-c-icon">Search</span>
                            </button>
                            <!-- end em-c-btn -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end em-l-container -->

    <div class="em-l-container">
        <div class="em-c-table-object ">
            <div class="em-c-table-object__header">
            </div>
            <!--end em-c-table-object__header-->
            <div class="em-c-table-object__body">
                <div class="em-c-table-object__body-inner">
                    <table class="em-c-table ">
                        <thead class="em-c-table__header">
                            <tr class="em-c-table__header-row">
                                <th scope="col" class="em-c-table__header-cell em-u-width-5">BIS #</th>
                                <th scope="col" class="em-c-table__header-cell ">Work Name</th>
                                 <th scope="col" class="em-c-table__header-cell ">Request Type</th>
                                <th scope="col" class="em-c-table__header-cell ">Requestor Name</th>
                               
                                <th scope="col" class="em-c-table__header-cell ">Project Manager</th>
                                <th scope="col" class="em-c-table__header-cell ">Amount Requested</th>
                                <th scope="col" class="em-c-table__header-cell ">Status</th>
                            </tr>
                            <!-- em-c-table__header-row -->
                        </thead>
                        <!-- end em-c-table__header -->
                        <tbody class="em-c-table__body ">
                            <tr class="em-c-table__row ">
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable"><a href="Overview.aspx">12324</a></td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">GVS Day 1 IT Enablement</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Request Funding</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Shantell D. Brown</td>
                                
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Mary Mayweather</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">$50K</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Approved</td>
                               
                            </tr>
                          <tr class="em-c-table__row ">
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable"><a href="Overview.aspx">12325</a></td>
                                 <td class="em-c-table__cell em-js-cell em-js-cell-editable">GVS Day 1 IT Enablement</td>
                               <td class="em-c-table__cell em-js-cell em-js-cell-editable">Request Funding</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Shantell D. Brown</td>
                               
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Mary Mayweather</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">$500K</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Submitted For Approval</td>
                               
                            </tr>
                             <tr class="em-c-table__row ">
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable"><a href="Overview.aspx">12360</a></td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">GVS Day 1 IT Enablement</td>
                                 <td class="em-c-table__cell em-js-cell em-js-cell-editable">Request Funding</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Shantell D. Brown</td>
                                
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Mary Mayweather</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">$45K</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Draft</td>
                               
                            </tr>
                            <tr class="em-c-table__row ">
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable"><a href="../WBSRequest/WBSRequest.aspx">12365</a></td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">GVS Day 1 IT Enablement</td>
                                 <td class="em-c-table__cell em-js-cell em-js-cell-editable">Request WBS</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Shantell D. Brown</td>
                                
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Mary Mayweather</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">$200K</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Open</td>                               
                            </tr>
                            <tr class="em-c-table__row ">
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable"><a href="../transferfunding/overview.aspx">3443</a></td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">GVS Day 1 IT Enablement</td>
                                 <td class="em-c-table__cell em-js-cell em-js-cell-editable">Transfer Fund</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Shantell D. Brown</td>
                                
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Mary Mayweather</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">$400K</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Draft</td>                               
                            </tr>
                        </tbody>
                        <!-- end em-c-table__body -->
                        <tfoot class="em-c-table__footer">
                            <tr class="em-c-table__footer-row">
                            </tr>
                        </tfoot>
                        <!-- end em-c-table__footer -->
                    </table>
                    <!--end em-c-table-->
                </div>
                <!--end em-c-table-object__body-inner-->
            </div>
            <!--end em-c-table-object__body-->
        </div>
        <!--end em-c-table-object-->
    <!-- end em-l-container -->
</asp:Content>
