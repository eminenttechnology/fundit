﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_includes/MasterPageMenu.master" AutoEventWireup="true" CodeBehind="Comment.aspx.cs" Inherits="XOM.BIS.Prototype.Web.Project.Request.Comment" %>
<%@ Register src="~/_includes/Menu_Request.ascx" tagname="Menu" tagprefix="app" %>
<asp:Content ID="Content4" ContentPlaceHolderID="PageMenu" runat="server"><app:Menu runat=server />
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="Title" runat="server">
      Request 233 - Request Funding
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SubTitle" runat="server">
    Comment
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">


    <div class="em-c-field ">
  <label for="" class="em-c-field__label">Add Comment</label>
  <div class="em-c-field__body">
    <textarea class="em-c-textarea " id="" placeholder="Enter Comment...." rows="5"></textarea>
  </div>
        <button class="em-c-btn ">
  <span class="em-c-btn__text">Add</span>
</button>
<!-- end em-c-btn -->
</div>
<!-- end em-c-field -->    
    <div class="em-c-table-object ">
  <div class="em-c-table-object__header">
  </div>
  <!--end em-c-table-object__header-->
  <div class="em-c-table-object__body">
    <div class="em-c-table-object__body-inner">
      <table class="em-c-table ">
        <thead class="em-c-table__header">
          <tr class="em-c-table__header-row">
            <th scope="col" class="em-c-table__header-cell ">Comment</th>
            <th scope="col" class="em-c-table__header-cell ">User</th>
          </tr>
          <!-- em-c-table__header-row -->
        </thead>
        <!-- end em-c-table__header -->
        <tbody class="em-c-table__body ">
          <tr class="em-c-table__row ">
            <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">
              Include additional description for  Capital Asset Description
            </td>
            <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">
             Salvador Figueroa
            </td>
          </tr>
          <!-- end em-c-table__row -->
          <tr class="em-c-table__row ">
            <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">
             Remove unnecessary document in Work Description
            </td>
            <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">
             Shantell D. Brown
            </td>
          </tr>
          <!-- end em-c-table__row -->
          <tr class="em-c-table__row ">
            <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">
              Include details Capital Asset Startup Date
            </td>
            <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">
              Mark Frazier
            </td>
          </tr>
          <!-- end em-c-table__row -->
        </tbody>
        <!-- end em-c-table__body -->
        <tfoot class="em-c-table__footer">
          <tr class="em-c-table__footer-row">
          </tr>
        </tfoot>
        <!-- end em-c-table__footer -->
      </table>
      <!--end em-c-table-->
    </div>
    <!--end em-c-table-object__body-inner-->
  </div>
  <!--end em-c-table-object__body-->
</div>
</asp:Content>

