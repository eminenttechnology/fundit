﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_includes/MasterPage.master" AutoEventWireup="true" CodeBehind="AllocationRequests.aspx.cs" Inherits="XOM.BIS.Prototype.Web.Requests.AllocationRequests" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Title" runat="server">
    Request 001 - Allocation Requests
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="SubTitle" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="server">
    <div class="em-c-table-object ">
        <div class="em-c-table-object__header">
        </div>
        <div class="em-c-table-object__body">
            <div class="em-c-table-object__body-inner">
                <table class="em-c-table ">
                    <thead class="em-c-table__header">
                        <tr class="em-c-table__header-row">
                            <th scope="col" class="em-c-table__header-cell ">Category</th>
                            <th scope="col" class="em-c-table__header-cell ">Company Code</th>
                            <th scope="col" class="em-c-table__header-cell ">Country</th>
                            <th scope="col" class="em-c-table__header-cell ">Organization</th>
                            <th scope="col" class="em-c-table__header-cell ">Location</th>
                            <th scope="col" class="em-c-table__header-cell ">Type</th>
                            <th scope="col" class="em-c-table__header-cell ">Cost Center</th>
                            <th scope="col" class="em-c-table__header-cell ">Description</th>
                            <th scope="col" class="em-c-table__header-cell ">Quantity</th>
                            <th scope="col" class="em-c-table__header-cell ">Capex ($K)</th>
                            <th scope="col" class="em-c-table__header-cell ">Status</th>
                            <th scope="col" class="em-c-table__header-cell "></th>
                        </tr>
                    </thead>
                    <tbody class="em-c-table__body ">
                        <tr class="em-c-table__row ">
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                <div class="em-c-field__body">
                                    <select class="em-c-select em-c-select em-u-width-100" id="">
                                        <option value=""></option>
                                        <option value="option-1" selected="selected">Hardware</option>
                                        <option value="option-2">Software</option>
                                        <option value="option-3">Capitalized Staffing</option>
                                        <option value="option-4">Expense</option>
                                        <option value="option-5">Unallocated</option>
                                    </select>
                                </div>
                            </td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                <div class="em-c-field__body">
                                    <input type="text" id="" class="em-c-input" value="0970">
                                </div>
                            </td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                <div class="em-c-field__body">
                                    <input type="text" id="" class="em-c-input" value="United States" disabled="disabled">
                                </div>
                            </td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                <div class="em-c-field__body">
                                    <input type="text" id="" class="em-c-input" value="Applications" disabled="disabled">
                                </div>
                            </td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                <div class="em-c-field__body">
                                    <select class="em-c-select em-c-select em-u-width-100" id="">
                                        <option value=""></option>
                                        <option value="option-1">Alabama</option>
                                        <option value="option-2">Colorodo</option>
                                        <option value="option-3">Florida</option>
                                        <option value="option-4" selected="selected">New Jersey</option>
                                        <option value="option-5">Texas-Dallas</option>
                                    </select>
                                </div>
                            </td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                <div class="em-c-field__body">
                                    <select class="em-c-select em-c-select em-u-width-100" id="">
                                        <option value=""></option>
                                        <option value="option-1">Non-SAP Mainframe</option>
                                        <option value="option-2" selected="selected">SAP Mainframe</option>
                                        <option value="option-3">Non-SAP Storage</option>
                                        <option value="option-4">SAP Storage</option>
                                        <option value="option-5">Desktop</option>
                                    </select>
                                </div>
                            </td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                <div class="em-c-field__body">
                                    <input type="number" id="" class="em-c-input" value="">
                                </div>
                            </td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                <div class="em-c-field__body">
                                    <input type="text" id="" class="em-c-input" value="TDI Lenovo Servers, HBA Fibre Cards">
                                </div>
                            </td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                <div class="em-c-field__body">
                                    <input type="text" id="" class="em-c-input" value="40">
                                </div>
                            </td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                <div class="em-c-field__body">
                                    <input type="number" id="" class="em-c-input" value="621.06">
                                </div>
                            </td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                <div class="em-c-field__body">
                                    <input type="text" id="" class="em-c-input" value="Submitted for Approval" disabled="disabled">
                                </div>
                            </td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                <button class="em-c-btn em-c-btn--secondary">
                                    <span class="em-c-btn__text">Save</span>
                                </button>
                            </td>
                        </tr>
                        
                        <tr class="em-c-table__row ">
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                <div class="em-c-field__body">
                                    Unallocated
                                </div>
                            </td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                <div class="em-c-field__body">
                                    0970
                                </div>
                            </td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                <div class="em-c-field__body">
                                    United States
                                </div>
                            </td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                <div class="em-c-field__body">
                                </div>
                            </td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                <div class="em-c-field__body">
                                    Florida
                                </div>
                            </td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                <div class="em-c-field__body">SAP Mainframe
                                </div>
                            </td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                <div class="em-c-field__body">
                                    
                                </div>
                            </td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                <div class="em-c-field__body">
                                    HBA Fibre Cards
                                </div>
                            </td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                <div class="em-c-field__body">
                                    45
                                </div>
                            </td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                <div class="em-c-field__body">
                                    621.06
                                </div>
                            </td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                <div class="em-c-field__body">
                                    Submitted for Approval
                                </div>
                            </td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                <button class="em-c-btn em-c-btn--secondary">
                                    <span class="em-c-btn__text">Edit</span>
                                </button>
                            </td>
                        </tr>

                        <tr class="em-c-table__row ">
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                <div class="em-c-field__body">
                                    <select class="em-c-select em-c-select em-u-width-100" id="">
                                        <option value=""></option>
                                        <option value="option-1">Hardware</option>
                                        <option value="option-2">Software</option>
                                        <option value="option-3">Capitalized Staffing</option>
                                        <option value="option-4">Expense</option>
                                        <option value="option-5">Unallocated</option>
                                    </select>
                                </div>
                            </td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                <div class="em-c-field__body">
                                    <input type="text" id="" class="em-c-input" value="">
                                </div>
                            </td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                <div class="em-c-field__body">
                                    <input type="text" id="" class="em-c-input" value="" disabled="disabled">
                                </div>
                            </td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                <div class="em-c-field__body">
                                    <input type="text" id="" class="em-c-input" value="" disabled="disabled">
                                </div>
                            </td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                <div class="em-c-field__body">
                                    <select class="em-c-select em-c-select em-u-width-100" id="">
                                        <option value=""></option>
                                        <option value="option-1">Alabama</option>
                                        <option value="option-2">Colorodo</option>
                                        <option value="option-3">Florida</option>
                                        <option value="option-4">New Jersey</option>
                                        <option value="option-5">Texas-Dallas</option>
                                    </select>
                                </div>
                            </td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                <div class="em-c-field__body">
                                    <select class="em-c-select em-c-select em-u-width-100" id="">
                                        <option value=""></option>
                                        <option value="option-1">Non-SAP Mainframe</option>
                                        <option value="option-2">SAP Mainframe</option>
                                        <option value="option-3">Non-SAP Storage</option>
                                        <option value="option-4">SAP Storage</option>
                                        <option value="option-5">Desktop</option>
                                    </select>
                                </div>
                            </td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                <div class="em-c-field__body">
                                    <input type="text" id="" class="em-c-input" value="">
                                </div>
                            </td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                <div class="em-c-field__body">
                                    <input type="text" id="" class="em-c-input" value="">
                                </div>
                            </td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                <div class="em-c-field__body">
                                    <input type="number" id="" class="em-c-input" value="">
                                </div>
                            </td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                <div class="em-c-field__body">
                                    <input type="number" id="" class="em-c-input" value="">
                                </div>
                            </td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                <div class="em-c-field__body">
                                    <input type="text" id="" class="em-c-input" value="Submitted for Approval" disabled="disabled">
                                </div>
                            </td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                <button class="em-c-btn em-c-btn--secondary">
                                    <span class="em-c-btn__text">Add</span>
                                </button>
                            </td>
                        </tr>
                    </tbody>
                    <tfoot class="em-c-table__footer">
                        <tr class="em-c-table__footer-row">
                        </tr>
                    </tfoot>
                </table>
                <br />
                <div>
                    <a href="Overview.aspx" class="em-c-btn em-c-btn--primary" onclick="show_alert()">
                        <span class="em-c-btn__text">Save</span>
                    </a>
                    <a href="Search.aspx" class="em-c-btn em-c-btn--secondary">
                        <span class="em-c-btn__text">Cancel</span>
                    </a>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
