﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_includes/MasterPage.master" AutoEventWireup="true" CodeBehind="FundingRequestAllocation.aspx.cs" Inherits="XOM.BIS.Prototype.Web.Requests.ARAssignment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Title" runat="server">
     Request 233 - Request Funding
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SubTitle" runat="server">
    Allocations
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <div class="em-l--two-column">
        <div class="em-l__main">

            <%--<fieldset class="em-c-fieldset">
                <div class="em-l-grid em-l-grid--2up">
                    <div class="em-l-grid__item">
                        <div class="em-c-field ">
                            <label for="" class="em-c-field__label">Category</label>
                            <div class="em-c-field__body">
                                <select class="em-c-select em-c-select em-u-width-100" id="">
                                    <option value=""></option>
                                    <option value="option-1">Hardware</option>
                                    <option value="option-2">Software</option>
                                    <option value="option-3">Capitalized Staffing</option>
                                    <option value="option-4">Expense</option>
                                    <option value="option-5">Unallocated</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="em-l-grid__item">
                        <div class="em-c-field ">
                            <label for="" class="em-c-field__label">Company Code</label>
                            <div class="em-c-field__body">
                                <input type="text" id="" class="em-c-input" value="" />
                            </div>
                        </div>
                    </div>
                    <div class="em-l-grid__item">
                        <div class="em-c-field ">
                            <label for="" class="em-c-field__label">Country</label>
                            <div class="em-c-field__body">
                                <input type="text" id="" class="em-c-input" value="" disabled="disabled" />
                            </div>
                        </div>
                    </div>
                    <div class="em-l-grid__item">
                        <div class="em-c-field ">
                            <label for="" class="em-c-field__label">Organization</label>
                            <div class="em-c-field__body">
                                <input type="text" id="" class="em-c-input" value="" disabled="disabled" />
                            </div>
                        </div>
                    </div>
                    <div class="em-l-grid__item">
                        <div class="em-c-field ">
                            <label for="" class="em-c-field__label">Location</label>
                            <div class="em-c-field__body">
                                <select class="em-c-select em-c-select em-u-width-100" id="">
                                    <option value=""></option>
                                    <option value="option-1">Alabama</option>
                                    <option value="option-2">Colorodo</option>
                                    <option value="option-3">Florida</option>
                                    <option value="option-4">New Jersey</option>
                                    <option value="option-5">Texas-Dallas</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="em-l-grid__item">
                        <div class="em-c-field ">
                            <label for="" class="em-c-field__label">Type</label>
                            <div class="em-c-field__body">
                                <select class="em-c-select em-c-select em-u-width-100" id="">
                                    <option value=""></option>
                                    <option value="option-1">Non-SAP Mainframe</option>
                                    <option value="option-2">SAP Mainframe</option>
                                    <option value="option-3">Non-SAP Storage</option>
                                    <option value="option-4">SAP Storage</option>
                                    <option value="option-5">Desktop</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="em-l-grid__item">
                        <div class="em-c-field ">
                            <label for="" class="em-c-field__label">Cost Center</label>
                            <div class="em-c-field__body">
                                <input type="text" id="" class="em-c-input" value="" />
                            </div>
                        </div>
                    </div>
                    <div class="em-l-grid__item">
                        <div class="em-c-field ">
                            <label for="" class="em-c-field__label">Description</label>
                            <div class="em-c-field__body">
                                <input type="text" id="" class="em-c-input" value="" />
                            </div>
                        </div>
                    </div>

                    <div class="em-l-grid__item">
                        <div class="em-c-field ">
                            <label for="" class="em-c-field__label">Quantity</label>
                            <div class="em-c-field__body">
                                <input type="number" id="" class="em-c-input" value="" />
                            </div>
                        </div>
                    </div>
                    <div class="em-l-grid__item">
                        <div class="em-c-field ">
                            <label for="" class="em-c-field__label">Capex ($KUSD)</label>
                            <div class="em-c-field__body">
                                <input type="text" id="" class="em-c-input" value="" placeholder="$" />
                            </div>
                        </div>
                    </div>

                </div>                
                
                <button class="em-c-btn em-c-btn--inverted-blue">
                    <span class="em-c-btn__text">Add</span>
                </button>
                <a href="Overview.aspx" class="em-c-btn em-c-btn--secondary">
                    <span class="em-c-btn__text">Cancel</span>
                </a>
                <a href="Overview.aspx" class="em-c-btn em-c-btn--primary  em-u-margin-left-quad">
                    <span class="em-c-btn__text">Save</span>
                </a>
            </fieldset>--%>
            <!-- end em-c-fieldset -->
            <div class="em-u-text-align-right">
                <a href="#" class="em-c-btn em-c-btn--primary em-c-btn--small">
                    <span class="em-c-btn__text">Add Item</span></a>
            </div>
            <br />
            <div class="em-c-table-object ">
                <div class="em-c-table-object__header">
                </div>
                <div class="em-c-table-object__body">
                    <div class="em-c-table-object__body-inner">
                        <table class="em-c-table ">
                            <thead class="em-c-table__header">
                                <tr class="em-c-table__header-row">
                                    <th scope="col" class="em-c-table__header-cell ">Category</th>
                                    <th scope="col" class="em-c-table__header-cell ">Company Code</th>
                                    <th scope="col" class="em-c-table__header-cell ">Country</th>
                                    <th scope="col" class="em-c-table__header-cell ">Organization</th>
                                    <th scope="col" class="em-c-table__header-cell ">Location</th>
                                    <th scope="col" class="em-c-table__header-cell ">Type</th>
                                    <th scope="col" class="em-c-table__header-cell ">Cost Center</th>
                                    <th scope="col" class="em-c-table__header-cell ">Description</th>
                                    <th scope="col" class="em-c-table__header-cell ">Quantity</th>
                                    <th scope="col" class="em-c-table__header-cell ">Capex ($K)</th>
                                    <th scope="col" class="em-c-table__header-cell "></th>
                                </tr>
                            </thead>
                            <tbody class="em-c-table__body ">
                                <tr class="em-c-table__row ">
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                        <div class="em-c-field__body">
                                            Unallocated
                                        </div>
                                    </td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                        <div class="em-c-field__body">
                                            0970
                                        </div>
                                    </td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                        <div class="em-c-field__body">
                                            United States
                                        </div>
                                    </td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                        <div class="em-c-field__body">
                                        </div>
                                    </td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                        <div class="em-c-field__body">
                                            Florida
                                        </div>
                                    </td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                        <div class="em-c-field__body">
                                            SAP Mainframe
                                        </div>
                                    </td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                        <div class="em-c-field__body">
                                        </div>
                                    </td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                        <div class="em-c-field__body">
                                            HBA Fibre Cards
                                        </div>
                                    </td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                        <div class="em-c-field__body">
                                            45
                                        </div>
                                    </td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                        <div class="em-c-field__body">
                                            621.06
                                        </div>
                                    </td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                        <button class="em-c-btn em-c-btn--secondary">
                                            <span class="em-c-btn__text">Edit</span>
                                        </button>
                                    </td>
                                </tr>
                            </tbody>
                            <tfoot class="em-c-table__footer">
                                <tr class="em-c-table__footer-row">
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
            <div>
                <div class="em-u-text-align-left">
                    <a href="Overview.aspx" class="em-c-btn em-c-btn--primary">
                        <span class="em-c-btn__text">Save</span>
                    </a>
                         <a href="Overview.aspx" class="em-c-btn em-u-margin-left-double">
                        <span class="em-c-btn__text">Cancel</span>
                    </a>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
