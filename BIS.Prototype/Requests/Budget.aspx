﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_includes/MasterPageMenu.master" AutoEventWireup="true" CodeBehind="Budget.aspx.cs" Inherits="XOM.BIS.Prototype.Web.Requests.Budget" %>
<%@ Register src="~/_includes/Menu_Request.ascx" tagname="Menu" tagprefix="app" %>
<asp:Content ID="Content4" ContentPlaceHolderID="PageMenu" runat="server"><app:Menu runat=server />
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="Title" runat="server">
    Request 001 - Discretionary Project
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SubTitle" runat="server">
    Budget
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <div class="em-l-container">
    <div class="em-c-table-object ">
         <div class="em-c-page-header em-c-page-header--small">
            <h1 class="em-c-page-header__title">Budgets</h1>
        </div>
        <!--end em-c-table-object__header-->
        <div class="em-c-table-object__body">
        <div class="em-c-table-object__body-inner">
            <table class="em-c-table ">
            <thead class="em-c-table__header">
                <tr class="em-c-table__header-row">
                    <th scope="col" class="em-c-table__header-cell ">BIS #</th>
                    <th scope="col" class="em-c-table__header-cell ">Budget Category</th>
                    <th scope="col" class="em-c-table__header-cell ">Requestor </th>
                    <th scope="col" class="em-c-table__header-cell ">Endorsing Controller</th>
                    <th scope="col" class="em-c-table__header-cell ">Division Function Planner</th>
                    <th scope="col" class="em-c-table__header-cell ">Endorsing Functional Manager</th>
                    <th scope="col" class="em-c-table__header-cell ">Current Funding Request($K)</th>
                    <th scope="col" class="em-c-table__header-cell ">MPCE Percentage Requested</th>
                </tr>
                <!-- em-c-table__header-row -->
            </thead>
            <!-- end em-c-table__header -->
            <tbody class="em-c-table__body ">
                <tr class="em-c-table__row ">
                    <td class="em-c-table__cell em-js-cell">
                        001
                    </td>
                    <td class="em-c-table__cell em-js-cell">
                        $100K - $250K 
                    </td>
                    <td class="em-c-table__cell em-js-cell">
                        James Origin
                    </td>
                    <td class="em-c-table__cell em-js-cell">
                        Jeffrey L. Williams
                    </td>
                    <td class="em-c-table__cell em-js-cell">
                        Mark Frazier
                    </td>
                    <td class="em-c-table__cell em-js-cell">
                        Dan Murray
                    </td>
                    <td class="em-c-table__cell em-js-cell">
                        1000
                    </td>
                    <td class="em-c-table__cell em-js-cell">
                        50%
                    </td>
                </tr>
                <tr class="em-c-table__row ">
                    <td class="em-c-table__cell em-js-cell">
                        002
                    </td>
                    <td class="em-c-table__cell em-js-cell">
                        $500K - $1M 
                    </td>
                    <td class="em-c-table__cell em-js-cell">
                        Nina Lent
                    </td>
                    <td class="em-c-table__cell em-js-cell">
                        Suzanne Daroowala
                    </td>
                    <td class="em-c-table__cell em-js-cell">
                        Shantell D. Brown
                    </td>
                    <td class="em-c-table__cell em-js-cell">
                        John Mark
                    </td>
                    <td class="em-c-table__cell em-js-cell">
                        500
                    </td>
                    <td class="em-c-table__cell em-js-cell">
                        20%
                    </td>
                </tr>
                <tr class="em-c-table__row ">
                    <td class="em-c-table__cell em-js-cell">
                        003
                    </td>
                    <td class="em-c-table__cell em-js-cell">
                        $250K - $500K 
                    </td>
                    <td class="em-c-table__cell em-js-cell">
                        Gary Holt
                    </td>
                    <td class="em-c-table__cell em-js-cell">
                        Zain Willoughby
                    </td>
                    <td class="em-c-table__cell em-js-cell">
                        Salvador Figueroa
                    </td>
                    <td class="em-c-table__cell em-js-cell">
                        Ben Solar
                    </td>
                    <td class="em-c-table__cell em-js-cell">
                        $700.00
                    </td>
                    <td class="em-c-table__cell em-js-cell">
                        60%
                    </td>
                </tr>
            </tbody>
            <!-- end em-c-table__body -->
            <tfoot class="em-c-table__footer">
                <tr class="em-c-table__footer-row">
                </tr>
            </tfoot>
            <!-- end em-c-table__footer -->
            </table>
            <!--end em-c-table-->
        </div>
        <!--end em-c-table-object__body-inner-->
        </div>
        <!--end em-c-table-object__body-->
    </div>
    </div>
</asp:Content>
