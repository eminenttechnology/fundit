﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_includes/MasterPage.master" AutoEventWireup="true" CodeBehind="NewRequest.aspx.cs" Inherits="XOM.BIS.Prototype.Web.Requests.NewRequest" %>

<asp:Content ID="Content2" ContentPlaceHolderID="Title" runat="server">
    Request
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="SubTitle" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="server">
    <div class="em-c-field ">
                                <label for="" class="em-c-field__label">Work ID</label>
                                <div class="em-c-field__body">
                                    <select class="em-c-select em-c-select em-u-width-100" id="">
                                        <option value="option-1"  >GVS Day 1 IT Enablement</option>
                                    </select>
                                </div>
                            </div>
    <div class="em-c-field ">
        <label for="" class="em-c-field__label">Request Type</label>
        <div class="em-c-field__body">
            <asp:DropDownList runat="server" ID="request" class="em-c-select em-c-select em-u-width-100">
                <asp:ListItem Value="1">Request Funding</asp:ListItem>
                <asp:ListItem Value="2">Transfer Fund </asp:ListItem>
                <asp:ListItem Value="3">Request WBS</asp:ListItem>
                <asp:ListItem Value="4">AR Closeout</asp:ListItem>
                
            </asp:DropDownList>
           <%-- <select class="em-c-select em-c-select em-u-width-100" id="request" runat="server">
                <option value="">Select Request</option>
                <option value="1">Request Funding</option>
                <option value="2">Transfer Fund</option>
                <option value="3">Request WBS</option>
                <option value="4">AR Closeout</option>
                <option value="5">Project Closeout</option>
            </select>--%>
        </div>
    </div>
    <asp:Button runat="server" Text="Next" ID="btnNext" OnClick="btnNext_Click"/>
  <%--  <button class="em-c-btn em-c-btn--primary" onclick="Next()">Next</button>--%>
    <%-- <a href="Overview.aspx" class="em-c-btn em-c-btn--primary">
                        <span class="em-c-btn__text">Save</span>
                    </a>--%>

    <script>
        function Next() {
            var request = document.getElementById("request").value;
            if (request == "1") {
                alert("one")
                window.location = "http://www.w3.org";
            }
               
        }
    </script>
</asp:Content>
