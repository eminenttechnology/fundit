﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_includes/MasterPage.master" AutoEventWireup="true" CodeBehind="FundingRequestPage.aspx.cs" Inherits="XOM.BIS.Prototype.Web.Requests.FundingRequestPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Title" runat="server">
    Request Funding
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SubTitle" runat="server">
    New Request
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">

        <div class="em-c-page-header em-c-page-header--small">
            <h1 class="em-c-page-header__title">Request</h1>
        </div>
        <!-- end em-c-page-header -->
        <div class="em-l--two-column">
            <div class="em-l__main">

                <fieldset class="em-c-fieldset">
                    <div class="em-l-grid em-l-grid--2up">
                        <div class="em-l-grid__item">

                            <div class="em-c-field em-js-typeahead">
                                <label for="" class="em-c-field__label">Work ID</label>
                                <div class="em-c-field__body">
                                    <input id="" class="em-c-input em-js-typeahead" placeholder="Type something..." value="23456" />
                                    <div class="em-c-field__menu  em-js-typeahead-menu">
                                        <ul class="em-c-typeahead-list">
                                            <li class="em-c-typeahead-list__item">
                                                <span class="em-c-typeahead__suggestion"><b>Op</b>tion</span>
                                            </li>
                                            <li class="em-c-typeahead-list__item">
                                                <span class="em-c-typeahead__suggestion">Co<b>op</b>eration</span>
                                            </li>
                                            <li class="em-c-typeahead-list__item">
                                                <span class="em-c-typeahead__suggestion">P<b>op</b> P<b>op</b> P<b>op</b></span>
                                            </li>
                                            <li class="em-c-typeahead-list__item">
                                                <span class="em-c-typeahead__suggestion"><b>Op</b>ulence</span>
                                            </li>
                                            <li class="em-c-typeahead-list__item">
                                                <span class="em-c-typeahead__suggestion">Current <b>Op</b>erations</span>
                                            </li>
                                            <li class="em-c-typeahead-list__item">
                                                <span class="em-c-typeahead__suggestion">Co<b>op</b>eration</span>
                                            </li>
                                            <li class="em-c-typeahead-list__item">
                                                <span class="em-c-typeahead__suggestion"><b>Op</b>rah Winfrey</span>
                                            </li>
                                            <li class="em-c-typeahead-list__item">
                                                <span class="em-c-typeahead__suggestion"><b>Op</b>era House</span>
                                            </li>
                                            <li class="em-c-typeahead-list__item">
                                                <span class="em-c-typeahead__suggestion"><b>Op</b>inions</span>
                                            </li>
                                        </ul>
                                    </div>
                                    <!--end em-c-field__menu-->
                                </div>
                            </div>
                            <!-- end em-c-field -->
                        </div>
                        <div class="em-l-grid__item">
                            <div class="em-c-field ">
                                <label for="" class="em-c-field__label">Work Name</label>
                                <div class="em-c-field__body">
                                    <input type="text" id="" class="em-c-input" value=" GVS Day 1 IT Enablement"/>
                                </div>
                            </div>
                        </div>


                        <div class="em-l-grid__item">
                            <div class="em-c-field ">
                                <label for="" class="em-c-field__label">Work Type</label>
                                <div class="em-c-field__body">
                                    <select class="em-c-select em-c-select em-u-width-100" id="">
                                        <option value=""></option>
                                        <option value="option-1">Base</option>
                                        <option value="option-2" selected="selected">Project</option>
                                        <option value="option-2">SWI</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="em-l-grid__item">
                            <div class="em-c-field ">
                                <label for="" class="em-c-field__label">Funding Division</label>
                                <div class="em-c-field__body">
                                    <select class="em-c-select em-c-select em-u-width-100" id="">
                                        <option value=""></option>
                                        <option value="option-1">Chemical & Corporate Portfolio</option>
                                        <option value="option-2" selected="selected">Downstream Portfolio</option>
                                        <option value="option-2">General Interest Portfolio</option>
                                        <option value="option-2">Service Portfolio</option>
                                        <option value="option-2">Upstream Portfolio</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="em-l-grid__item">
                            <div class="em-c-field ">
                                <label for="" class="em-c-field__label">Division Function</label>
                                <div class="em-c-field__body">
                                    <select class="em-c-select em-c-select em-u-width-100" id="">
                                        <option value=""></option>
                                        <option value="option-1">Downstream - Downstream Business Services</option>
                                        <option value="option-2">Downstream - Downstream SHE</option>
                                        <option value="option-2">Downstream - FLS Marketing</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="em-l-grid__item">
                            <div class="em-c-field ">
                                <label for="" class="em-c-field__label">Funding Type</label>
                                <div class="em-c-field__body">
                                    <select class="em-c-select em-c-select em-u-width-100" id="">
                                        <option value=""></option>
                                        <option value="option-1">Cumulative Advance Funding</option>
                                        <option value="option-2">Cumulative Advance Funding (>25% of MPCE)</option>
                                        <option value="option-3">Full Funding</option>
                                        <option value="option-4">Supplemental Funding</option>
                                        <option value="option-5">Release of Previously Appropriated Funds</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="em-l-grid__item">
                            <div class="em-c-field ">
                                <label for="" class="em-c-field__label">DOAG Required</label>
                                <div class="em-c-field__body">
                                    <select class="em-c-select em-c-select em-u-width-100" id="">
                                        <option value=""></option>
                                        <option value="option-1">DOAG_Level</option>
                                        <option value="option-2">DOAG_Level 1-BD, MC, or CE ($50M)</option>
                                        <option value="option-3">DOAG_Level 2-BD, MC, or CE ($50M)</option>
                                        <option value="option-4">DOAG_Level 3-GSC President</option>
                                        <option value="option-5">DOAG_Level 4-EMIT VP / IT Ops Mgr.</option>
                                        <option value="option-6">DOAG_Level 5-EMIT Division Mgr</option>
                                        <option value="option-7">DOAG_Level 6</option>
                                        <option value="option-8">DOAG_Level 7</option>
                                        <option value="option-9">DOAG_Level 8</option>
                                        <option value="option-9">DOAG_Level 9</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="em-l-grid__item">
                            <div class="em-c-field ">
                                <label for="" class="em-c-field__label">Division Function Planner</label>
                                <div class="em-c-field__body">
                                    <select class="em-c-select em-c-select em-u-width-100" id="">
                                        <option value=""></option>
                                        <option value="option-1">Shantell D. Brown</option>
                                        <option value="option-2">Salvador Figueroa</option>
                                        
                                        <option value="option-4">Mark Frazier</option>
                                        <option value="option-5">Joe Mockaitis</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="em-l-grid__item">
                        <div class="em-c-field ">
                            <label for="" class="em-c-field__label">Approver</label>
                            <div class="em-c-field__body">
                                <select class="em-c-select em-c-select em-u-width-100" id="">
                                    <option value=""></option>
                                    <option value="option-1">Mike Brown</option>
                                     <option value="option-1">Susan Gate</option>
                                </select>
                            </div>
                        </div>
                    </div>
                        <div class="em-l-grid__item">
                            <div class="em-c-field ">
                                <label for="" class="em-c-field__label">Endorsing Controller</label>
                                <div class="em-c-field__body">
                                    <select class="em-c-select em-c-select em-u-width-100" id="">
                                        <option value=""></option>
                                        <option value="option-1">Jeffrey L. Williams</option>
                                        <option value="option-2">Suzanne Daroowala</option>
                                        <option value="option-3">Zain Willoughby</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="em-l-grid__item">
                            <div class="em-c-field ">
                                <label for="" class="em-c-field__label">Work Lead</label>
                                <div class="em-c-field__body">
                                    <input type="text" id="" class="em-c-input" value="Mary Mayweather" disabled />
                                </div>
                            </div>
                        </div>
                        <div class="em-l-grid__item">
                            <div class="em-c-field ">
                                <label for="" class="em-c-field__label">PCA</label>
                                <div class="em-c-field__body">
                                    <input type="text" id="" class="em-c-input" value="John Doe" disabled />
                                </div>
                            </div>
                        </div>
                        <div class="em-l-grid__item">
                            <div class="em-c-field ">
                                <label for="" class="em-c-field__label">Previouly Endorsed($K)</label>
                                <div class="em-c-field__body">
                                    <input type="text" id="" class="em-c-input" value="300" disabled />
                                </div>
                                <div class="em-c-field__note">attach evidence to override</div>
                            </div>
                        </div>
                        <div class="em-l-grid__item">
                            <div class="em-c-field ">
                                <label for="" class="em-c-field__label">Current Funding Request($K)</label>
                                <div class="em-c-field__body">
                                    <input type="text" id="" class="em-c-input" value="" />
                                </div>
                            </div>
                        </div>
                        <div class="em-l-grid__item">
                            <div class="em-c-field ">
                                <label for="" class="em-c-field__label">Current Business Costs($K) </label>
                                <div class="em-c-field__body">
                                    <input type="text" id="" class="em-c-input" value="" />
                                </div>
                                <div class="em-c-field__note">if applicable</div>
                            </div>
                        </div>
                        <div class="em-l-grid__item">
                            <div class="em-c-field ">
                                <label for="" class="em-c-field__label">MPCE Most Probable Cost Estimate($K) </label>
                                <div class="em-c-field__body">
                                    <input type="text" id="" class="em-c-input" value="" />
                                </div>
                            </div>
                        </div>
                        <div class="em-l-grid__item">

                            <div class="em-c-field em-c-field--date-picker">
                                <label for="date-1" class="em-c-field__label">Capital Asset Startup Date</label>
                                <div class="em-c-field__body">
                                    <input type="text" step="1" id="date-1" class="em-c-field__input em-js-datepicker" value="" />
                                    <svg class="em-c-icon em-c-field__icon" height="10" width="10">
                                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-calendar"></use>
                                    </svg>
                                </div>
                            </div>
                            <!-- end em-c-field--date-picker -->
                            <!-- IMPORTANT!
     In script tag below, update the src to use the correct path to Unity's
     js directory. For example:
     http://example.com/unity-1.1.1/js/vendor/pikaday.js
	Pikaday date picker script
-->
                            <script type="text/javascript" src="../Assets/standard/unity-1.2.0/js/vendor/pikaday.js"></script>
                            <script>
                                var datePickers = document.querySelectorAll('.em-js-datepicker');
                                for (i = 0; i < datePickers.length; i++) {
                                    var picker = new Pikaday(
                                        {
                                            field: datePickers[i]
                                        });
                                }
                            </script>
                        </div>
                        <div class="em-l-grid__item">
                            
                        </div>

                        <div class="em-l-grid__item">

                            <div class="em-c-field ">
                                <label for="" class="em-c-field__label">Work Description</label>
                                <div class="em-c-field__body">
                                    <textarea class="em-c-textarea " id="" placeholder="Placeholder" value="" rows="5">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur</textarea>
                                </div>
                                <!-- end em-c-field__body -->
                               
                            </div>
                            <!-- end em-c-field -->
                        </div>
                        <div class="em-l-grid__item">
                            <div class="em-c-field ">
                                <label for="" class="em-c-field__label">Capital Asset Description</label>
                                <div class="em-c-field__body">
                                    <textarea class="em-c-textarea " id="" placeholder="Placeholder" value="" rows="5"></textarea>
                                </div>
                                <!-- end em-c-field__body -->
                               
                            </div>
                        </div>

                    </div>
                    <a href="fundingrequestallocation.aspx" class="em-c-btn em-c-btn--primary">
                        <span class="em-c-btn__text">Next</span>
                    </a>


                </fieldset>
            </div>
        </div>
 
    <script type="text/javascript">
                                function show_alert() {
                                    alert("Your  request has been submitted!");
                                }
    </script>
</asp:Content>
