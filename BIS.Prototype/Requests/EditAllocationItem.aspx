﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_includes/MasterPage.master" AutoEventWireup="true" CodeBehind="EditAllocationItem.aspx.cs" Inherits="XOM.BIS.Prototype.Web.Requests.EditAllocationItem" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Title" runat="server">
           Request 233 - Request Funding

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="SubTitle" runat="server">
    Edit Allocation
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="server">
            <div class="em-l-container">
            <!-- end em-c-page-header -->
            <div class="em-l--two-column">
                <div class="em-l__main">
                            
                    <fieldset class="em-c-fieldset">
                        <div class="em-l-grid em-l-grid--2up">
                            <div class="em-l-grid__item">
                                <div class="em-c-field ">
                                    <label for="" class="em-c-field__label">Category</label>
                                    <div class="em-c-field__body">
                                        <select class="em-c-select em-c-select em-u-width-100" id="">
                                            <option value=""></option>
                                            <option value="option-1">Hardware</option>
                                            <option value="option-2">Software</option>
                                            <option value="option-3">Capitalized Staffing</option>
                                            <option value="option-4">Expense</option>
                                            <option value="option-5">Unallocated</option>
                                        </select>
                                    </div>                            
                                </div>
                            </div>
                            <div class="em-l-grid__item">
                                <div class="em-c-field ">
                                    <label for="" class="em-c-field__label">Company Code</label>
                                    <div class="em-c-field__body">
                                        <input type="text" id="" class="em-c-input" value="" />
                                    </div>
                                </div>
                            </div>
                            <div class="em-l-grid__item">
                                <div class="em-c-field ">
                                    <label for="" class="em-c-field__label">Country</label>
                                    <div class="em-c-field__body">
                                        <input type="text" id="" class="em-c-input" value="" readonly />
                                    </div>                            
                                </div>
                            </div>
                            <div class="em-l-grid__item">
                                <div class="em-c-field ">
                                    <label for="" class="em-c-field__label">Organization</label>
                                    <div class="em-c-field__body">
                                        <input type="text" id="" class="em-c-input" value="" readonly />
                                    </div>                            
                                </div>
                            </div>
                            <div class="em-l-grid__item">
                                <div class="em-c-field ">
                                    <label for="" class="em-c-field__label">Location</label>
                                    <div class="em-c-field__body">
                                         <select class="em-c-select em-c-select em-u-width-100" id="">
                                            <option value=""></option>
                                            <option value="option-1">Alabama</option>
                                            <option value="option-2">Colorodo</option>
                                            <option value="option-3">Florida</option>
                                            <option value="option-4">New Jersey</option>
                                            <option value="option-5">Texas-Dallas</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="em-l-grid__item">
                                <div class="em-c-field ">
                                    <label for="" class="em-c-field__label">Type</label>
                                    <div class="em-c-field__body">
                                        <select class="em-c-select em-c-select em-u-width-100" id="">
                                            <option value=""></option>
                                            <option value="option-1">Non-SAP Mainframe</option>
                                            <option value="option-2">SAP Mainframe</option>
                                            <option value="option-3">Non-SAP Storage</option>
                                            <option value="option-4">SAP Storage</option>
                                            <option value="option-5">Desktop</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="em-l-grid__item">
                                <div class="em-c-field ">
                                    <label for="" class="em-c-field__label">Cost Center</label>
                                    <div class="em-c-field__body">
                                        <input type="text" id="" class="em-c-input" value="" readonly />
                                    </div>                            
                                </div>
                            </div>
                            <div class="em-l-grid__item">
                                <div class="em-c-field ">
                                    <label for="" class="em-c-field__label">Description</label>
                                    <div class="em-c-field__body">
                                        <input type="text" id="" class="em-c-input" value="" />
                                    </div>                            
                                </div>
                            </div>

                            <div class="em-l-grid__item">
                                <div class="em-c-field ">
                                    <label for="" class="em-c-field__label">Quantity</label>
                                    <div class="em-c-field__body">
                                        <input type="number" id="" class="em-c-input" value="" />
                                    </div>
                                </div>
                            </div>
                            <div class="em-l-grid__item">
                                <div class="em-c-field ">
                                    <label for="" class="em-c-field__label">Capex ($K)</label>
                                    <div class="em-c-field__body">
                                        <input type="text" id="" class="em-c-input" value="" placeholder="$" />
                                    </div>                            
                                </div>
                            </div>

                            

                        </div>

                        <a href="Overview.aspx" class="em-c-btn em-c-btn--primary" onclick="show_alert()">
                            <span class="em-c-btn__text">Submit</span>
                        </a>
                        <a href="Search.aspx" class="em-c-btn em-c-btn--secondary">
                            <span class="em-c-btn__text">Cancel</span>
                        </a>

                    </fieldset>
                            <!-- end em-c-fieldset -->
                    
                </div>
            </div>
        </div>

</asp:Content>
