﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_includes/MasterPageMenu.master" AutoEventWireup="true" CodeBehind="Document.aspx.cs" Inherits="XOM.BIS.Prototype.Web.Requests.Document" %>

<%@ Register Src="~/_includes/Menu_Request.ascx" TagName="Menu" TagPrefix="app" %>
<asp:Content ID="Content1" ContentPlaceHolderID="PageMenu" runat="server">
    <app:Menu runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Title" runat="server">
     Request 233 - Request Funding
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="SubTitle" runat="server">
    Document
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="server">
        <div class="em-c-table-object ">
            <div class="em-c-page-header em-c-page-header--small">
                <h1 class="em-c-page-header__title">Document List</h1>
            </div>
            <!--end em-c-table-object__header-->
            <div class="em-c-table-object__body">
                <div class="em-c-table-object__body-inner">
                    <%--<div class="em-u-text-align-right em-u-margin-bottom">
                    <a class="em-c-btn em-c-btn--primary" href="AddDocument.aspx"><span class="em-c-btn__text">Add</span></a>
                </div>--%>

                    <table class="em-c-table ">
                        <thead class="em-c-table__header">
                            <tr class="em-c-table__header-row">
                                <th scope="col" class="em-c-table__header-cell">Name</th>
                                <th scope="col" class="em-c-table__header-cell "></th>
                            </tr>
                            <!-- em-c-table__header-row -->
                        </thead>
                        <!-- end em-c-table__header -->
                        <tbody class="em-c-table__body ">
                            <tr class="em-c-table__row ">
                                <td class="em-c-table__cell em-js-cell">Project meeting minutes.doc
                                </td>
                                <td class="em-c-table__cell em-js-cell">
                                    <button class="em-c-btn em-c-btn--secondary em-c-btn--small">
                                        <span class="em-c-btn__text">Download</span>
                                    </button>
                                </td>
                            </tr>
                            <!-- end em-c-table__row -->
                            <tr class="em-c-table__row ">
                                <td class="em-c-table__cell em-js-cell">Sprint planning.doc
                                </td>
                                <td class="em-c-table__cell em-js-cell">
                                    <button class="em-c-btn em-c-btn--secondary em-c-btn--small">
                                        <span class="em-c-btn__text">Download</span>
                                    </button>
                                </td>
                            </tr>
                            <!-- end em-c-table__row -->
                            <tr class="em-c-table__row ">
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Planning.doc
                                </td>
                                <td class="em-c-table__cell em-js-cell">
                                    <button class="em-c-btn em-c-btn--secondary em-c-btn--small">
                                        <span class="em-c-btn__text">Download</span>
                                    </button>
                                </td>
                            </tr>
                            <!-- end em-c-table__row -->
                        </tbody>
                        <!-- end em-c-table__body -->
                        <tfoot class="em-c-table__footer">
                            <tr class="em-c-table__footer-row">
                            </tr>
                        </tfoot>
                        <!-- end em-c-table__footer -->
                    </table>

                    <div class="em-u-margin-top-double">
                        <fieldset class="em-c-fieldset">
                            <div class="em-c-page-header em-c-page-header--small">
                                <h1 class="em-c-page-header__title">Add Document</h1>
                            </div>
                            <div class="em-c-field ">
                                <label for="" class="em-c-field__label">Name</label>
                                <div class="em-c-field__body">
                                    <input type="text" id="" class="em-c-input" placeholder="" value="" />
                                </div>
                            </div>

                            <div class="em-c-field em-c-field--file-upload ">
                                <label for="file" class="em-c-field__label">File</label>
                                <div class="em-c-field__body">
                                    <svg class="em-c-icon em-c-field__block-icon" style="height: 40px">
                                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-upload"></use>
                                    </svg>
                                    <input type="file" name="file[]" id="file" class="em-c-file-upload" value="" multiple="" />
                                    <ul class="em-c-field__list em-js-field-list" style="align">
                                        <li class="em-c-field__item">Drag files here</li>
                                        <li class="em-c-field__item em-c-field__item--small">Or click to choose file</li>
                                    </ul>
                                </div>
                            </div>

                            <a href="Document.aspx" class="em-c-btn em-c-btn--primary">
                                <span class="em-c-btn__text">Add</span>
                            </a>
                            <a href="Document.aspx" class="em-c-btn em-c-btn--secondary">
                                <span class="em-c-btn__text">Cancel</span>
                            </a>
                        </fieldset>
                    </div>
                    <!--end em-c-table-->
                </div>
                <!--end em-c-table-object__body-inner-->
            </div>
            <!--end em-c-table-object__body-->
        </div>
</asp:Content>
