﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_includes/MasterPageMenu.master" AutoEventWireup="true" CodeBehind="Overview.aspx.cs" Inherits="XOM.BIS.Prototype.Web.Requests.Overview" %>

<%@ Register Src="~/_includes/Menu_Request.ascx" TagName="Menu" TagPrefix="app" %>
<asp:Content ID="Content1" ContentPlaceHolderID="PageMenu" runat="server">
    <app:Menu runat="server" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Title" runat="server">
     Request 233 - Request Funding
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="SubTitle" runat="server">
    Overview
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="server">
      <div class="em-c-page-header em-c-page-header--small">
                        <h1 class="em-c-page-header__title">General Info</h1>
                    </div>

        <div class="em-l-grid em-l-grid--2up">
            <div class="em-l-grid__item">
                <div class="em-c-table-object">
                  
                    <div class="em-l-grid em-l-grid--2up ">
                        <div class="em-l-grid__item">
                            <label>BIS #:</label>
                        </div>
                        <div class="em-l-grid__item">
                            <div>3443</div>
                        </div>
                    </div>
                    <div class="em-l-grid em-l-grid--2up ">
                        <div class="em-l-grid__item">
                            <label>Work ID:</label>
                        </div>
                        <div class="em-l-grid__item">
                            <a href="../project/overview.aspx" style="color:blue; text-decoration:underline;">12342</a>
                        </div>
                    </div>
                    <div class="em-l-grid em-l-grid--2up ">
                        <div class="em-l-grid__item">
                            <label>Work Name:</label>
                        </div>
                        <div class="em-l-grid__item">
                            <a href="../project/overview.aspx" style="color:blue; text-decoration:underline;">GVS Day 1 IT Enablement</a>
                        </div>
                    </div>
                    <div class="em-l-grid em-l-grid--2up ">
                        <div class="em-l-grid__item">
                            <label>Funding Type:</label>
                        </div>
                        <div class="em-l-grid__item">
                            <div>Cumulative Advance Funding</div>
                        </div>
                    </div>
                    <div class="em-l-grid em-l-grid--2up ">
                        <div class="em-l-grid__item">
                            <label>Requestor</label>
                        </div>
                        <!-- end em-l-grid__item -->
                        <div class="em-l-grid__item">
                            <div>Shantell D. Brown</div>
                        </div>
                    </div>

                    <div class="em-l-grid em-l-grid--2up ">
                        <div class="em-l-grid__item">
                            <label>Project Manager:</label>
                        </div>
                        <div class="em-l-grid__item">
                            <div>Mary Mayweather</div>
                        </div>
                    </div>
                     <div class="em-l-grid em-l-grid--2up ">
                        <div class="em-l-grid__item">
                            <label>PCA:</label>
                        </div>
                        <div class="em-l-grid__item">
                            <div>John Doe</div>
                        </div>
                    </div>
                     <div class="em-l-grid em-l-grid--2up ">
                        <div class="em-l-grid__item">
                            <label>Endorsing Controller:</label>
                        </div>
                        <div class="em-l-grid__item">
                            Jeffrey L. Williams
                        </div>
                    </div>
                     <div class="em-l-grid em-l-grid--2up ">
                        <div class="em-l-grid__item">
                            <label>Approver:</label>
                        </div>
                        <div class="em-l-grid__item">
                           Mike Brown
                        </div>
                    </div>

                    <div class="em-l-grid em-l-grid--2up ">
                        <div class="em-l-grid__item">
                            <label>Status :</label>
                        </div>
                        <div class="em-l-grid__item">
                             <span id="MainContent_General_Info_Status_ctrl" style="display:none">Draft</span>
                           <%-- <button class="em-c-btn em-c-btn--primary em-u-padding-half" onclick="Status_ChangeWorkflowStatus(MainContent_General_Info_Status_ctrl)">
                                        <span>Draft - Change Status</span>
                                    </button>--%>
                             <span class="em-c-btn em-c-btn--primary em-u-padding-half" onclick="Status_ChangeWorkflowStatus(MainContent_General_Info_Status_ctrl)">
                                        <span>Draft - Change Status</span>
                                    </span>
                            
                        </div>
                    </div>
                   
                   
                </div>
            </div>
             <div class="em-l-grid__item">
                   <div class="em-l-grid em-l-grid--2up ">
                        <div class="em-l-grid__item">
                            <label>Amount Requested:</label>
                        </div>
                        <!-- end em-l-grid__item -->
                        <div class="em-l-grid__item">
                            <div>$400K</div>
                        </div>
                    </div>
                  <div class="em-l-grid em-l-grid--2up ">
                        <div class="em-l-grid__item">
                            <label>Total Amount Approved:</label>
                        </div>
                        <div class="em-l-grid__item">
                            <div>$650K</div>
                        </div>
                    </div>
                    <div class="em-l-grid em-l-grid--2up ">
                        <div class="em-l-grid__item">
                            <label>Amount Spent:</label>
                        </div>
                        <div class="em-l-grid__item">
                            <div>$150K</div>
                        </div>
                    </div>
                    <div class="em-l-grid em-l-grid--2up ">
                        <div class="em-l-grid__item">
                            <label>Balance:</label>
                        </div>
                        <div class="em-l-grid__item">
                            <div>$500K</div>
                        </div>
                    </div>
            </div>
            <!-- end em-l-grid__item -->
        </div>

        <div class="em-l-grid em-l-grid--1up">
            <div class="em-l-grid__item">
                <div class="em-c-table-object">
                    <div class="em-l-grid em-l-grid--2up ">
                        <div class="em-l-grid__item">
                            <div class="em-c-page-header em-c-page-header--small">
                                <h1 class="em-c-page-header__title">Status History</h1>
                            </div>
                        </div>
                        <%--  <div class="em-l-grid__item">
                           <a href="Overview.aspx" class="em-c-btn em-c-btn--primary em-c-btn--small">
                            <span class="em-c-btn__text">New Appropriation Request</span>
                        </a>
                        </div>--%>
                    </div>
                    <!--end em-c-table-object__header-->
                    <div class="em-c-table-object__body">
                        <div class="em-c-table-object__body-inner">
                            <table class="em-c-table" style="min-width: 100%">
                                <thead class="em-c-table__header">
                                    <tr class="em-c-table__header-row">
                                        <th scope="col" class="em-c-table__header-cell ">Date</th>
                                        <th scope="col" class="em-c-table__header-cell ">Status</th>
                                        <th scope="col" class="em-c-table__header-cell ">User</th>
                                        <th scope="col" class="em-c-table__header-cell ">Comments</th>
                                    </tr>
                                    <!-- em-c-table__header-row -->
                                </thead>
                                <!-- end em-c-table__header -->
                                <tbody class="em-c-table__body ">
                                    <tr class="em-c-table__row">
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable">9/7/2017 8:57:36 PM</td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable">Draft</td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable">Mary Mayweather</td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable"></td>
                                    </tr>
                                    <tr class="em-c-table__row ">
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable">9/5/2017 9:17:12 AM</td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable">Submitted for Approval</td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable">Zachery Roberts</td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable"></td>
                                    </tr>
                                    <tr class="em-c-table__row ">
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable">9/1/2017 8:34:21 PM</td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable">Approved</td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable">Mary Mayweather</td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable"></td>
                                    </tr>

                                </tbody>
                                <!-- end em-c-table__body -->
                                <tfoot class="em-c-table__footer">
                                    <tr class="em-c-table__footer-row">
                                    </tr>
                                </tfoot>
                                <!-- end em-c-table__footer -->
                            </table>
                            <!--end em-c-table-->
                        </div>
                        <!--end em-c-table-object__body-inner-->
                    </div>
                    <!--end em-c-table-object__body-->
                </div>
                <!--end em-c-table-object-->
            </div>

        </div>
        <!-- end em-l-grid -->
    <script>
        function Status_ChangeWorkflowStatus(field) {
            $.fancybox.open({
                type: 'iframe',
                href: '../Templates/Pages/WorkflowStatusPicker.aspx?WorkflowCode=ONBOARD&EntityID=1783&FieldName=MainContent_General_Info_Status_ctrl&Reload=True',
                height: 310,
                width: 600
            });

        }

    </script>

</asp:Content>
