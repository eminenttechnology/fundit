﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_includes/MasterPageMenu.master" AutoEventWireup="true" CodeBehind="WBSRequestEdit.aspx.cs" Inherits="XOM.BIS.Prototype.Web.Requests.WBSRequestEdit" %>

<%@ Register Src="~/_includes/Menu_WBS.ascx" TagName="Menu" TagPrefix="app" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PageMenu" runat="server">
    <app:Menu runat="server" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Title" runat="server">
    Request 12365 - Request WBS
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="SubTitle" runat="server">
    Edit Request
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="server">

    <fieldset class="em-c-fieldset">
        <div class="em-c-page-header em-c-page-header--small">
            <h1 class="em-c-page-header__title">Edit Request</h1>
        </div>
        <div class="em-c-field ">
            <label for="" class="em-c-field__label">WBS</label>
            <div class="em-c-field__body">
                <input type="number" id="" class="em-c-input" value="" />
            </div>
        </div>
        <div class="em-c-field ">
            <label for="" class="em-c-field__label">Network Order</label>
            <div class="em-c-field__body">
                <input type="number" id="" class="em-c-input" value="" />
            </div>
        </div>

        <a href="WBSRequest.aspx" class="em-c-btn em-c-btn--primary">
            <span class="em-c-btn__text">Save</span>
        </a>
        <a href="WBSRequest.aspx" class="em-c-btn em-c-btn--secondary">
            <span class="em-c-btn__text">Cancel</span>
        </a>


    </fieldset>
</asp:Content>
