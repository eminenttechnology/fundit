﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_includes/MasterPage.master" AutoEventWireup="true" CodeBehind="Allocation.aspx.cs" Inherits="XOM.BIS.Prototype.Web.WBSRequest.Allocation" %>
<asp:Content ID="Content2" ContentPlaceHolderID="Title" runat="server">
        Project 233 - Request WBS

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="SubTitle" runat="server">
    Allocation Info
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="server">

        <div class="em-c-table-object ">
            <div class="em-c-page-header em-c-page-header--small">
                <h1 class="em-c-page-header__title">Allocation Info List</h1>
            </div>

            <br />
            <!--end em-c-table-object__header-->
        <div class="em-c-table-object__body">
                <div class="em-c-table-object__body-inner">
                    <table class="em-c-table ">
                    <thead class="em-c-table__header">
                        <tr class="em-c-table__header-row">
                             <th scope="col" class="em-c-table__header-cell "></th>
                            <th scope="col" class="em-c-table__header-cell ">Category</th>
                            <th scope="col" class="em-c-table__header-cell ">PPL #</th>
                            <th scope="col" class="em-c-table__header-cell ">AR #</th>
                            <th scope="col" class="em-c-table__header-cell ">Company Code</th>
                            <th scope="col" class="em-c-table__header-cell ">Country</th>
                            <th scope="col" class="em-c-table__header-cell ">Organization</th>
                            <th scope="col" class="em-c-table__header-cell ">Location</th>
                            <th scope="col" class="em-c-table__header-cell ">Type</th>
                            <th scope="col" class="em-c-table__header-cell ">Cost Center</th>
                            <th scope="col" class="em-c-table__header-cell ">Description</th>
                            <th scope="col" class="em-c-table__header-cell ">Quantity</th>
                            <th scope="col" class="em-c-table__header-cell ">Capex</th>
                            <th scope="col" class="em-c-table__header-cell ">Cap Spend</th> 
                            <th scope="col" class="em-c-table__header-cell ">Cap Remaining</th>                           
                      </tr>
                      <!-- em-c-table__header-row -->
                    </thead>
                    <!-- end em-c-table__header -->
                    <tbody class="em-c-table__body ">
                        <tr class="em-c-table__row ">
                             <td class="em-c-table__cell em-js-cell">
                                <a href="WBSRequest.aspx">Select</a>
                            </td>
                            <td class="em-c-table__cell em-js-cell">
                                Hardware
                            </td>
                            <td class="em-c-table__cell em-js-cell">
                                23
                            </td>
                            <td class="em-c-table__cell em-js-cell">
                                245
                            </td>
                          <td class="em-c-table__cell em-js-cell">
                                0970
                            </td>
                            <td class="em-c-table__cell em-js-cell">
                                United States
                            </td>
                            <td class="em-c-table__cell em-js-cell">
                                
                            </td>
                            <td class="em-c-table__cell em-js-cell">
                                Texas - Houston - Data Centers
                            </td>
                            <td class="em-c-table__cell em-js-cell">
                                SAP Midrange
                            </td>
                            <td class="em-c-table__cell em-js-cell">
                               2322222222
                            </td>
                            <td class="em-c-table__cell em-js-cell">
                                TDI Lenovo Servers, HBA Fibre Cards
                            </td>
                            <td class="em-c-table__cell em-js-cell">
                                8
                            </td>
                            <td class="em-c-table__cell em-js-cell">
                                $200K
                            </td>
                            <td class="em-c-table__cell em-js-cell">
                                $100K
                            </td>
                            <td class="em-c-table__cell em-js-cell">
                                $100K
                            </td>
                        </tr>
                       <tr class="em-c-table__row ">
                             <td class="em-c-table__cell em-js-cell">
                              <a href="WBSRequest.aspx">Select</a>
                            </td>
                            <td class="em-c-table__cell em-js-cell">
                                Software
                            </td>
                            <td class="em-c-table__cell em-js-cell">
                                56
                            </td>
                            <td class="em-c-table__cell em-js-cell">
                                112
                            </td>
                           
                            <td class="em-c-table__cell em-js-cell">
                                0970
                            </td>
                            <td class="em-c-table__cell em-js-cell">
                                United States
                            </td>
                            <td class="em-c-table__cell em-js-cell">
                                
                            </td>
                            <td class="em-c-table__cell em-js-cell">
                                Texas - Houston - Data Centers
                            </td>
                            <td class="em-c-table__cell em-js-cell">
                                SAP Midrange
                            </td>
                            <td class="em-c-table__cell em-js-cell">
                               2322222222
                            </td>
                            <td class="em-c-table__cell em-js-cell">
                                Microsoft
                            </td>
                            <td class="em-c-table__cell em-js-cell">
                                100
                            </td>
                            <td class="em-c-table__cell em-js-cell">
                                $200K
                            </td>
                            <td class="em-c-table__cell em-js-cell">
                                $56K
                            </td>
                            <td class="em-c-table__cell em-js-cell">
                                $144K
                            </td>
                        </tr>
                    </tbody>
                    <!-- end em-c-table__body -->
                    <tfoot class="em-c-table__footer">
                        <tr class="em-c-table__footer-row">
                        </tr>
                    </tfoot>
                <!-- end em-c-table__footer -->
                </table>
                    <!--end em-c-table-->
                </div>
                <!--end em-c-table-object__body-inner-->
            </div>
            <!--end em-c-table-object__body-->
        </div>
        <!--end em-c-table-object-->
</asp:Content>
