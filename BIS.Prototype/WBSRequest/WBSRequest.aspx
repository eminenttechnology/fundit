﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_includes/MasterPageMenu.master" AutoEventWireup="true" CodeBehind="WBSRequest.aspx.cs" Inherits="XOM.BIS.Prototype.Web.Requests.WBSRequest" %>

<%@ Register Src="~/_includes/Menu_WBS.ascx" TagName="Menu" TagPrefix="app" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PageMenu" runat="server">
    <app:Menu runat="server" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Title" runat="server">
    Request 12365 - Request WBS
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="SubTitle" runat="server">
    Overview
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="server">
    <div class="em-c-page-header em-c-page-header--small">
        <h1 class="em-c-page-header__title">General Info</h1>
    </div>

    <div class="em-l-grid em-l-grid--2up">
        <div class="em-l-grid__item">
            <div class="em-c-table-object">
                <div class="em-l-grid em-l-grid--2up ">
                    <div class="em-l-grid__item">
                        <label>BIS #:</label>
                    </div>
                    <div class="em-l-grid__item">
                        <div>12365</div>
                    </div>
                </div>
                <div class="em-l-grid em-l-grid--2up ">
                    <div class="em-l-grid__item">
                        <label>Work ID:</label>
                    </div>
                    <div class="em-l-grid__item">
                        <a href="../project/overview.aspx" style="color: blue; text-decoration: underline;">12342</a>
                    </div>
                </div>
                <div class="em-l-grid em-l-grid--2up ">
                    <div class="em-l-grid__item">
                        <label>Work Name:</label>
                    </div>
                    <div class="em-l-grid__item">
                        <a href="../project/overview.aspx" style="color: blue; text-decoration: underline;">GVS Day 1 IT Enablement</a>
                    </div>
                </div>
                <div class="em-l-grid em-l-grid--2up ">
                    <div class="em-l-grid__item">
                        <label>Category:</label>
                    </div>
                    <div class="em-l-grid__item">
                        <div>Hardware</div>
                    </div>
                </div>
                <div class="em-l-grid em-l-grid--2up ">
                    <div class="em-l-grid__item">
                        <label>Company Code:</label>
                    </div>
                    <!-- end em-l-grid__item -->
                    <div class="em-l-grid__item">
                        <div>0970</div>
                    </div>
                </div>

                <div class="em-l-grid em-l-grid--2up ">
                    <div class="em-l-grid__item">
                        <label>Cost Center:</label>
                    </div>
                    <div class="em-l-grid__item">
                        <div>09098475673</div>
                    </div>
                </div>
                <div class="em-l-grid em-l-grid--2up ">
                    <div class="em-l-grid__item">
                        <label>Description:</label>
                    </div>
                    <div class="em-l-grid__item">
                        <div>TDI Lenovo Servers, HBA Fibre Cards</div>
                    </div>
                </div>
                <div class="em-l-grid em-l-grid--2up ">
                    <div class="em-l-grid__item">
                        <label>Quantity:</label>
                    </div>
                    <div class="em-l-grid__item">
                        8
                    </div>
                </div>

                <div class="em-l-grid em-l-grid--2up ">
                        <div class="em-l-grid__item">
                            <label>Approver:</label>
                        </div>
                        <div class="em-l-grid__item">
                           Mike Brown
                        </div>
                    </div>

                <div class="em-l-grid em-l-grid--2up ">
                    <div class="em-l-grid__item">
                        <label>Status:</label>
                    </div>
                    <div class="em-l-grid__item">
                         <span id="MainContent_General_Info_Status_ctrl" style="display:none">Draft</span>
                           <%-- <button class="em-c-btn em-c-btn--primary em-u-padding-half" onclick="Status_ChangeWorkflowStatus(MainContent_General_Info_Status_ctrl)">
                                        <span>Draft - Change Status</span>
                                    </button>--%>
                             <span class="em-c-btn em-c-btn--primary em-u-padding-half" onclick="Status_ChangeWorkflowStatus(MainContent_General_Info_Status_ctrl)">
                                        <span>Draft - Change Status</span>
                                    </span>

                    </div>
                </div>
            </div>
        </div>
        <div class="em-l-grid__item">
            <div class="em-l-grid em-l-grid--2up ">
                <div class="em-l-grid__item">
                    <label>CAPEX</label>
                </div>
                <!-- end em-l-grid__item -->
                <div class="em-l-grid__item">
                    <div>$200K</div>
                </div>
            </div>
            <div class="em-l-grid em-l-grid--2up ">
                <div class="em-l-grid__item">
                    <label>CAP Spend:</label>
                </div>
                <div class="em-l-grid__item">
                    <div>$100K</div>
                </div>
            </div>
            <div class="em-l-grid em-l-grid--2up ">
                <div class="em-l-grid__item">
                    <label>CAP Remaining:</label>
                </div>
                <div class="em-l-grid__item">
                    <div>$100K</div>
                </div>
            </div>
        </div>
        <!-- end em-l-grid__item -->
    </div>
     <script>
        function Status_ChangeWorkflowStatus(field) {
            $.fancybox.open({
                type: 'iframe',
                href: '../../Templates/Pages/WorkflowStatusPicker.aspx?WorkflowCode=ONBOARD&EntityID=1783&FieldName=MainContent_General_Info_Status_ctrl&Reload=True',
                height: 310,
                width: 600
            });

        }

    </script>
</asp:Content>
