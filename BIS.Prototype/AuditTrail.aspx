﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_includes/MasterPageMenu.master" AutoEventWireup="true" CodeBehind="AuditTrail.aspx.cs" Inherits="XOM.BIS.Prototype.Web.AuditTrail" %>
    <%@ Register src="~/_includes/Menu_Request.ascx" tagname="Menu" tagprefix="app" %>
    <asp:Content ID="Content1" ContentPlaceHolderID="PageMenu" runat="server"><app:Menu runat=server />
    </asp:Content>
    <asp:Content ID="Content2" ContentPlaceHolderID="Title" runat="server">
    
    </asp:Content>
    <asp:Content ID="Content3" ContentPlaceHolderID="SubTitle" runat="server">
        AuditTrail
    </asp:Content>
    <asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="server">
        <div class="em-l-container">

            <div class="em-c-page-header em-c-page-header--small">
                <h1 class="em-c-page-header__title">Audit Trail List</h1>
            </div>

            <div class="em-c-collapsible-toolbar__panel em-js-toolbar-panel">
                <div class="em-c-toolbar">
                    <div class="em-l-grid em-l-grid--1up ">
                        <div class="em-l-grid__item">
                            <div class="em-c-table-object ">
            <div class="em-c-table-object__header">
            </div>
            <!--end em-c-table-object__header-->
            <div class="em-c-table-object__body">
            <div class="em-c-table-object__body-inner">
                <table class="em-c-table ">
                <thead class="em-c-table__header">
                    <tr class="em-c-table__header-row">
                    
                        <th scope="col" class="em-c-table__header-cell ">Event</th>
                        <th scope="col" class="em-c-table__header-cell ">Comment </th>
                        <th scope="col" class="em-c-table__header-cell ">Created By</th>
                        <th scope="col" class="em-c-table__header-cell ">Date</th>

                    </tr>
                    <!-- em-c-table__header-row -->
                </thead>
                <!-- end em-c-table__header -->
                <tbody class="em-c-table__body ">
                    <tr class="em-c-table__row ">
                        <td class="em-c-table__cell em-js-cell">CREATE</td>
                        <td class="em-c-table__cell em-js-cell">
                            Created BIS Number = 2017001 <br />
                            Work Class = Discretionary <br />
                            Work Type = Project <br />
                            Funding Division = Service Portfolio <br />
                            Division Function = Downstream - Downstream SHE <br />
                            Funding Type = Base <br />
                            Budget Category = $100K - $250K <br />
                            Endorsing Controller = Jeffery L. Williams
                        </td>
                        <td class="em-c-table__cell em-js-cell">
                            Mary Mayweather
                        </td>
                        <td class="em-c-table__cell em-js-cell">
                            9/7/2017 11:25:56 AM
                        </td>
                    </tr>
                    <tr class="em-c-table__row ">
                        <td class="em-c-table__cell em-js-cell">UPDATE</td>
                        <td class="em-c-table__cell em-js-cell">
                            Modified Funding Division from Service Portfolio  to Upstream Portfolio <br />
                            Funding Type from Base to Project
                        </td>
                        <td class="em-c-table__cell em-js-cell">
                            Zachery Roberts
                        </td>
                        <td class="em-c-table__cell em-js-cell">
                            9/10/2017 7:29:39 PM
                        </td>
                    </tr>
                </tbody>
                <!-- end em-c-table__body -->
                <tfoot class="em-c-table__footer">
                    <tr class="em-c-table__footer-row">
                    </tr>
                </tfoot>
                <!-- end em-c-table__footer -->
                </table>
                <!--end em-c-table-->
            </div>
            <!--end em-c-table-object__body-inner-->
            </div>
            <!--end em-c-table-object__body-->
        </div>

                        </div>
                    </div>
                    <!-- end em-l-grid -->
                </div>
            </div>



        
        </div>
    </asp:Content>
