﻿<%@ Page Language="C#" MasterPageFile="~/_includes/MasterPage.master"  AutoEventWireup="true" CodeBehind="Home-Controller.aspx.cs" Inherits="XOM.BIS.Prototype.Web.Home.Home_Controller" %>



<asp:Content ID="Content1" ContentPlaceHolderID="Title" runat="Server">
    Home
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="SubTitle" runat="Server">
    Project Controller
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">

         <div class="em-c-page-header em-c-page-header--small">
                    <h1 class="em-c-page-header__title">Pending Endorsement</h1>
                </div>
        <div class="em-c-table-object ">
            <div class="em-c-table-object__header">
            </div>
            <!--end em-c-table-object__header-->
            <div class="em-c-table-object__body">
                <div class="em-c-table-object__body-inner">
                    <table class="em-c-table ">
                        <thead class="em-c-table__header">
                            <tr class="em-c-table__header-row">
                                <th scope="col" class="em-c-table__header-cell ">BIS #</th>
                                <th scope="col" class="em-c-table__header-cell ">Work Class</th>
                                <th scope="col" class="em-c-table__header-cell ">Work Type</th>
                                <th scope="col" class="em-c-table__header-cell ">Funding Division</th>
                                <th scope="col" class="em-c-table__header-cell ">Division Function</th>
                                <th scope="col" class="em-c-table__header-cell ">Funding Type</th>
                                <th scope="col" class="em-c-table__header-cell ">Budget Category</th>
                                <th scope="col" class="em-c-table__header-cell ">Endorsing Controller</th>
                            </tr>
                            <!-- em-c-table__header-row -->
                        </thead>
                        <!-- end em-c-table__header -->
                        <tbody class="em-c-table__body ">
                            <tr class="em-c-table__row ">
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable"><a href="../Requests/Overview.aspx">2017001</a></td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Discretionary</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Project</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Service Portfolio</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Downstream - Downstream SHE</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Base</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">$100K - $250K</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Jeffrey L. Williams</td>                                
                            </tr>
                            <tr class="em-c-table__row ">
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable"><a href="../Requests/Overview.aspx">2017002</a></td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Discretionary</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Project</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Upstream Portfolio</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Downstream - FLS Marketing</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">SWI</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">$5M - $10M</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Suzanne Daroowala</td>                                
                            </tr>
                            <tr class="em-c-table__row ">
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable"><a href="../Requests/Overview.aspx">2017003</a></td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Non-Discretionary</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Project</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">General Interest Portfolio</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Downstream - Research & Engineering</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Project</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">$500K - $1M</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Zain Willoughby</td>                                
                            </tr>
                            
                        </tbody>
                        <!-- end em-c-table__body -->
                        <tfoot class="em-c-table__footer">
                            <tr class="em-c-table__footer-row">
                            </tr>
                        </tfoot>
                        <!-- end em-c-table__footer -->
                    </table>
                    <!--end em-c-table-->
                </div>
                <!--end em-c-table-object__body-inner-->
            </div>
            <!--end em-c-table-object__body-->
        </div>
        <!--end em-c-table-object-->

</asp:Content>
