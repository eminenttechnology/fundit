﻿<%@ Page Language="C#" MasterPageFile="~/_includes/MasterPage.master" AutoEventWireup="true" CodeBehind="Home-Executive.aspx.cs" Inherits="XOM.BIS.Prototype.Web.Home.Home_Executive" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Title" runat="Server">
    Home
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="SubTitle" runat="Server">
    Executive
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">



        <div class="em-l-grid em-l-grid--2up ">
            <div class="em-l-grid__item">
                <!-- 'fpo' stands for 'for placement only'. Replace the 'fpo-block' div element with your own markup-->
                <div id="fundingchart" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
            </div>
            <!-- end em-l-grid__item -->
            <div class="em-l-grid__item">
                 <div id="piechartcontainer" style="min-width: 500px; height: 500px; margin: 0 auto"></div>
            </div>


            <!--end em-c-table-object-->
            <!-- end em-l-grid__item -->
        </div>
        <!-- end em-l-grid -->
         <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/data.js"></script>
    <script src="https://code.highcharts.com/modules/drilldown.js"></script>
    <script src="../Scripts/RequestChart.js"></script>
</asp:Content>
