﻿<%@ Page Language="C#" MasterPageFile="~/_includes/MasterPage.master" AutoEventWireup="true" CodeBehind="Home-PM.aspx.cs" Inherits="XOM.BIS.Prototype.Web.Home.Home_PM" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Title" runat="Server">
    Home
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="SubTitle" runat="Server">
    Project Manager
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">


        <div class="em-l-grid em-l-grid--2up ">
            <div class="em-l-grid__item">
                <!-- 'fpo' stands for 'for placement only'. Replace the 'fpo-block' div element with your own markup-->
                <div class="em-c-page-header em-c-page-header--small">
                    <h1 class="em-c-page-header__title">My Projects</h1>
                </div>
                <div class="em-c-table-object ">
                    <div class="em-c-table-object__header">
                    </div>
                    <!--end em-c-table-object__header-->
                    <div class="em-c-table-object__body">
                        <div class="em-c-table-object__body-inner">
                            <table class="em-c-table ">
                                <thead class="em-c-table__header">
                                    <tr class="em-c-table__header-row">
                                        <th scope="col" class="em-c-table__header-cell ">Work ID</th>
                                        <th scope="col" class="em-c-table__header-cell ">Work Name</th>
                                        <th scope="col" class="em-c-table__header-cell ">Work Class</th>
                                        <th scope="col" class="em-c-table__header-cell ">Funding Division</th>
                                    </tr>
                                    <!-- em-c-table__header-row -->
                                </thead>
                                <!-- end em-c-table__header -->
                                <tbody class="em-c-table__body ">
                                    <tr class="em-c-table__row ">
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan=""><a href="../Project/Overview.aspx">71267</a>
                                        </td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">GVS Day 1 IT Enablement
                                        </td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">Discretionary
                                        </td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">Downstream Portfolio</td>
                                    </tr>
                                    <!-- end em-c-table__row -->
                                    <tr class="em-c-table__row ">
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan=""><a href="../Project/Overview.aspx">71268</a>
                                        </td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">GVS Day 1 IT Enablement
                                        </td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">Discretionary
                                        </td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">Downstream Portfolio
                                        </td>
                                    </tr>
                                    <!-- end em-c-table__row -->
                                    <tr class="em-c-table__row ">
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan=""><a href="../Project/Overview.aspx">71269</a>
                                        </td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">GVS Day 1 IT Enablement
                                        </td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">Discretionary
                                        </td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">Downstream Portfolio
                                        </td>
                                    </tr>
                                    <!-- end em-c-table__row -->
                                </tbody>
                                <!-- end em-c-table__body -->
                                <tfoot class="em-c-table__footer">
                                    <tr class="em-c-table__footer-row">
                                    </tr>
                                </tfoot>
                                <!-- end em-c-table__footer -->
                            </table>
                            <!--end em-c-table-->
                        </div>
                        <!--end em-c-table-object__body-inner-->
                    </div>
                    <!--end em-c-table-object__body-->
                </div>
            </div>
            <!-- end em-l-grid__item -->
            <div class="em-l-grid__item">
                <!-- 'fpo' stands for 'for placement only'. Replace the 'fpo-block' div element with your own markup-->
                <div class="em-c-page-header em-c-page-header--small">
                    <h1 class="em-c-page-header__title">My Requests</h1>
                </div>
                <div class="em-c-table-object ">
                    <div class="em-c-table-object__header">
                    </div>
                    <!--end em-c-table-object__header-->
                    <div class="em-c-table-object__body">
                        <div class="em-c-table-object__body-inner">
                            <table class="em-c-table ">
                                <thead class="em-c-table__header">
                                    <tr class="em-c-table__header-row">
                                        <th scope="col" class="em-c-table__header-cell ">BIS #</th>
                                        <th scope="col" class="em-c-table__header-cell ">Work Class</th>
                                        <th scope="col" class="em-c-table__header-cell ">Budget Category</th>
                                        <th scope="col" class="em-c-table__header-cell ">Project</th>
                                        <th scope="col" class="em-c-table__header-cell ">Endorsing Controller</th>
                                    </tr>
                                    <!-- em-c-table__header-row -->
                                </thead>
                                <!-- end em-c-table__header -->
                                <tbody class="em-c-table__body ">
                                    <tr class="em-c-table__row ">
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable"><a href="../Requests/Overview.aspx">2017001</a></td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable">Discretionary</td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable">$100K - $250K</td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable">Project 442</td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable">Jeffrey L. Williams</td>
                                    </tr>
                                    <tr class="em-c-table__row ">
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable"><a href="../Requests/Overview.aspx">2017002</a></td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable">Discretionary</td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable">$5M - $10M</td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable">Project 024</td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable">Suzanne Daroowala</td>
                                    </tr>
                                    <tr class="em-c-table__row ">
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable"><a href="../Requests/Overview.aspx">2017003</a></td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable">Non-Discretionary</td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable">$500K - $1M</td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable">Project 124</td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable">Zain Willoughby</td>
                                    </tr>

                                </tbody>
                                <!-- end em-c-table__body -->
                                <tfoot class="em-c-table__footer">
                                    <tr class="em-c-table__footer-row">
                                    </tr>
                                </tfoot>
                                <!-- end em-c-table__footer -->
                            </table>
                            <!--end em-c-table-->
                        </div>
                        <!--end em-c-table-object__body-inner-->
                    </div>
                    <!--end em-c-table-object__body-->
                </div>
                <!--end em-c-table-object-->
            </div>


            <!--end em-c-table-object-->
            <!-- end em-l-grid__item -->
        </div>
        <!-- end em-l-grid -->

        <div class="em-l-grid em-l-grid--2up ">
            <div class="em-l-grid__item">
                <!-- 'fpo' stands for 'for placement only'. Replace the 'fpo-block' div element with your own markup-->
                <div class="em-c-page-header em-c-page-header--small">
                    <h1 class="em-c-page-header__title">Request Pending Review</h1>
                </div>
                <div class="em-c-table-object ">
                    <div class="em-c-table-object__header">
                    </div>
                    <!--end em-c-table-object__header-->
                    <div class="em-c-table-object__body">
                        <div class="em-c-table-object__body-inner">
                            <table class="em-c-table ">
                                <thead class="em-c-table__header">
                                    <tr class="em-c-table__header-row">
                                        <th scope="col" class="em-c-table__header-cell ">BIS #</th>
                                        <th scope="col" class="em-c-table__header-cell ">Work Class</th>
                                        <th scope="col" class="em-c-table__header-cell ">Budget Category</th>
                                        <th scope="col" class="em-c-table__header-cell ">Project</th>
                                        <th scope="col" class="em-c-table__header-cell ">Endorsing Controller</th>
                                    </tr>
                                    <!-- em-c-table__header-row -->
                                </thead>
                                <!-- end em-c-table__header -->
                                <tbody class="em-c-table__body ">
                                    <tr class="em-c-table__row ">
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable"><a href="../Requests/Overview.aspx">2017001</a></td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable">Discretionary</td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable">$100K - $250K</td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable">Project 991</td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable">Jeffrey L. Williams</td>
                                    </tr>
                                    <tr class="em-c-table__row ">
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable"><a href="../Requests/Overview.aspx">2017002</a></td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable">Discretionary</td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable">$5M - $10M</td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable">Project 420</td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable">Suzanne Daroowala</td>
                                    </tr>
                                    <tr class="em-c-table__row ">
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable"><a href="../Requests/Overview.aspx">2017003</a></td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable">Non-Discretionary</td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable">$500K - $1M</td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable">Project 440</td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable">Zain Willoughby</td>
                                    </tr>

                                </tbody>
                                <!-- end em-c-table__body -->
                                <tfoot class="em-c-table__footer">
                                    <tr class="em-c-table__footer-row">
                                    </tr>
                                </tfoot>
                                <!-- end em-c-table__footer -->
                            </table>
                            <!--end em-c-table-->
                        </div>
                        <!--end em-c-table-object__body-inner-->
                    </div>
                    <!--end em-c-table-object__body-->
                </div>
            </div>
            <!-- end em-l-grid__item -->
            <div class="em-l-grid__item">
                <!-- 'fpo' stands for 'for placement only'. Replace the 'fpo-block' div element with your own markup-->
                <div class="em-c-page-header em-c-page-header--small">
                    <h1 class="em-c-page-header__title">Request Pending Endorsement</h1>
                </div>
                <div class="em-c-table-object ">
                    <div class="em-c-table-object__header">
                    </div>
                    <!--end em-c-table-object__header-->
                    <div class="em-c-table-object__body">
                        <div class="em-c-table-object__body-inner">
                            <table class="em-c-table ">
                                <thead class="em-c-table__header">
                                    <tr class="em-c-table__header-row">
                                        <th scope="col" class="em-c-table__header-cell ">BIS #</th>
                                        <th scope="col" class="em-c-table__header-cell ">Work Class</th>
                                        <th scope="col" class="em-c-table__header-cell ">Budget Category</th>
                                        <th scope="col" class="em-c-table__header-cell ">Project</th>
                                        <th scope="col" class="em-c-table__header-cell ">Endorsing Controller</th>
                                    </tr>
                                    <!-- em-c-table__header-row -->
                                </thead>
                                <!-- end em-c-table__header -->
                                <tbody class="em-c-table__body ">
                                    <tr class="em-c-table__row ">
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable"><a href="../Requests/Overview.aspx">2017001</a></td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable">Discretionary</td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable">$100K - $250K</td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable">Project 421</td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable">Jeffrey L. Williams</td>
                                    </tr>
                                    <tr class="em-c-table__row ">
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable"><a href="../Requests/Overview.aspx">2017002</a></td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable">Discretionary</td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable">$5M - $10M</td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable">Project 401</td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable">Suzanne Daroowala</td>
                                    </tr>
                                    <tr class="em-c-table__row ">
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable"><a href="../Requests/Overview.aspx">2017003</a></td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable">Non-Discretionary</td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable">$500K - $1M</td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable">Project 400</td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable">Zain Willoughby</td>
                                    </tr>

                                </tbody>
                                <!-- end em-c-table__body -->
                                <tfoot class="em-c-table__footer">
                                    <tr class="em-c-table__footer-row">
                                    </tr>
                                </tfoot>
                                <!-- end em-c-table__footer -->
                            </table>
                            <!--end em-c-table-->
                        </div>
                        <!--end em-c-table-object__body-inner-->
                    </div>
                    <!--end em-c-table-object__body-->
                </div>
                <!--end em-c-table-object-->
            </div>


            <!--end em-c-table-object-->
            <!-- end em-l-grid__item -->
        </div>
        <!-- end em-l-grid -->

</asp:Content>
