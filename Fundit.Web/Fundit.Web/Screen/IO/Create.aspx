﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_includes/MasterPageWithPrint.master" AutoEventWireup="true" %>

<%@ Register Src="~/UserControls/IORequestProjectInfo.ascx" TagPrefix="uc1" TagName="IORequestProjectInfo" %>
<%@ Register Src="~/UserControls/InternalOrderRequests.ascx" TagPrefix="uc1" TagName="InternalOrderRequests" %>





<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="em-l-container">
    <div class="em-c-tabs em-u-margin-bottom-none em-c-tabs--underline em-js-tabs">
        <ul class="em-c-tabs__list" style="width: 100%">
            <li class="em-c-tabs__item" style="width: 250px">
                <a href="#tab-panel-1" class="em-c-tabs__link em-js-tab ">Project Information</a>
            </li>
            <!-- end em-c-tabs__item -->
            <li class="em-c-tabs__item" style="width: 250px">
                <a href="#tab-panel-2" class="em-c-tabs__link em-js-tab ">Internal Order</a>
            </li>
            <!-- end em-c-tabs__item -->
        </ul>
        <!-- end em-c-tabs__list -->
        <div class="em-c-tabs__body em-c-tabs__body--no-border em-u-padding-bottom-none">
            <div class="em-c-tabs__panel em-js-tabs-panel" id="tab-panel-1">
                <uc1:IORequestProjectInfo runat="server" ID="IORequestProjectInfo" />
            </div>
            <div class="em-c-tabs__panel em-js-tabs-panel" id="tab-panel-2">
                <uc1:InternalOrderRequests runat="server" ID="InternalOrderRequests" />
            </div>

        </div>
    </div>
    <!-- end em-c-tabs -->
</div>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="footer_section" runat="server">
    <div class="align-right" style="width: auto; float: right;">
        <div class="em-c-btn-group em-c-btn-group--responsive">
            <!---->
            <button class="em-c-btn em-c-btn--primary" id="cancelDraft" name="cancel-draft">
                <span class="em-c-btn__text">Cancel Draft</span>
            </button>
            <!---->
            <!---->
            <button class="em-c-btn" id="submitReview" name="submitReview">
                <span class="em-c-btn__text">Submit for Review</span>
            </button>
            <!---->
            <!---->
            <!---->
            <!---->
            <!---->
        </div>
    </div>
</asp:Content>
