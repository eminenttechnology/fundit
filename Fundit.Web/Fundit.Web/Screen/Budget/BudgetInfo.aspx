﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_includes/MasterPageWithPrint.master" AutoEventWireup="true" CodeBehind="BudgetInfo.aspx.cs" Inherits="Fundit.Web.Screen.Budget.BudgetInfo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="em-l-container">
    <div class="em-l em-l--two-column">
        <div class="em-l__main">

            <fieldset class="em-c-fieldset">
                <div class="em-l-grid em-l-grid--3up">

                    <div class="em-l-grid__item">
                        <div class="em-c-field ">
                            <label for="" class="em-c-field__label">Type of Funding</label>
                            <div class="em-c-field__body em-c-field--small">
                                <select class="em-c-select em-c-select em-u-width-100" id="">
                                    <option value=""></option>
                                    <option value="option-1">Cumulative Advance Funding</option>
                                    <option value="option-2">Cumulative Advance Funding (>25% of MPCE)</option>
                                    <option value="option-3">Full Funding</option>
                                    <option value="option-4">Supplemental Funding</option>
                                    <option value="option-5">Release of Previously Appropriated Funds</option>
                                </select>
                            </div>
                        </div>
                        <div class="em-c-field__note">Type of Funding</div>
                    </div>

                    <div class="em-l-grid__item">
                        <div class="em-c-field em-is-readonly">
                            <label for="" class="em-c-field__label">Budget Category</label>
                            <div class="em-c-field__body">
                                <input type="text" id="" class="em-c-input em-c-input--small" placeholder="Placeholder" value="" readonly />
                                <svg class="em-c-icon em-c-field__icon em-c-icon--small">
                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-lock"></use>
                                </svg>
                            </div>
                            <!-- end em-c-field__body -->
                            <div class="em-c-field__note">Budget Category</div>
                        </div>

                    </div>

                    <div class="em-l-grid__item"></div>

                    <div class="em-l-grid__item">
                        <div class="em-c-field ">
                            <label for="" class="em-c-field__label">DOAG Required</label>
                            <div class="em-c-field__body em-c-field--small">
                                <select class="em-c-select em-c-select em-u-width-100 em-c-input--small" id="">
                                    <option value=""></option>
                                    <option value="option-1">DOAG_Level</option>
                                    <option value="option-2">DOAG_Level 1-BD, MC, or CE ($50M)</option>
                                    <option value="option-3">DOAG_Level 2-BD, MC, or CE ($50M)</option>
                                    <option value="option-4">DOAG_Level 3-GSC President</option>
                                    <option value="option-5">DOAG_Level 4-EMIT VP / IT Ops Mgr.</option>
                                    <option value="option-6">DOAG_Level 5-EMIT Division Mgr</option>
                                    <option value="option-7">DOAG_Level 6</option>
                                    <option value="option-8">DOAG_Level 7</option>
                                    <option value="option-9">DOAG_Level 8</option>
                                    <option value="option-9">DOAG_Level 9</option>
                                </select>
                            </div>
                            <div class="em-c-field__note">DOAG Required</div>
                        </div>
                    </div>

                    <div class="em-l-grid__item">
                        <div class="em-c-field ">
                            <label for="" class="em-c-field__label">DOAG Approver</label>
                            <div class="em-c-field__body em-c-field--small">
                                <select class="em-c-select em-c-select em-u-width-100 em-c-input--small" id="">
                                    <option value=""></option>
                                    <option value="option-1">Mike Brown</option>
                                    <option value="option-1">Susan Gate</option>
                                </select>
                            </div>
                            <div class="em-c-field__note">DOAG Approver</div>
                        </div>
                    </div>

                    <div class="em-l-grid__item"></div>

                    <div class="em-l-grid__item">
                        <div class="em-c-field em-is-readonly">
                            <label for="" class="em-c-field__label">Requestor Name</label>
                            <div class="em-c-field__body">
                                <input type="text" id="" class="em-c-input em-c-input--small" placeholder="Placeholder" value="" readonly />
                                <svg class="em-c-icon em-c-field__icon em-c-icon--small">
                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-lock"></use>
                                </svg>
                            </div>
                            <!-- end em-c-field__body -->
                            <div class="em-c-field__note">Requestor Name</div>
                        </div>

                    </div>

                    <div class="em-l-grid__item">
                        <div class="em-c-field ">
                            <label for="" class="em-c-field__label">Controller Endorser</label>
                            <div class="em-c-field__body em-c-field--small">
                                <select class="em-c-select em-c-select em-u-width-100 em-c-input--small" id="">
                                    <option value=""></option>
                                    <option value="option-1">Jeffrey L. Williams</option>
                                    <option value="option-2">Suzanne Daroowala</option>
                                    <option value="option-3">Zain Willoughby</option>
                                </select>
                            </div>
                            <div class="em-c-field__note">Funding Division Function</div>
                        </div>
                    </div>

                    <div class="em-l-grid__item"></div>

                    <div class="em-l-grid__item">
                        <div class="em-c-field">
                            <label for="" class="em-c-field__label">Controller's Contact</label>
                            <div class="em-c-field__body ">
                                <input type="text" id="" class="em-c-input em-c-input--small" placeholder="Placeholder" value="" />
                            </div>
                            <!-- end em-c-field__body -->
                            <div class="em-c-field__note">This is a required field</div>
                        </div>
                        <!-- end em-c-field -->
                    </div>

                    <div class="em-l-grid__item"></div>

                    <div class="em-l-grid__item"></div>

                    <div class="em-c-field ">
                        <label for="" class="em-c-field__label">Asset Description</label>
                        <div class="em-c-field__body">
                            <textarea class="em-c-textarea " id="" placeholder="Placeholder" rows="4" cols="95"></textarea>
                        </div>
                        <!-- end em-c-field__body -->
                        <div class="em-c-field__note">Asset Description</div>
                    </div>
                    <!-- end em-c-field -->
                </div>
            </fieldset>

        </div>
    </div>
    </div>

</asp:Content>
