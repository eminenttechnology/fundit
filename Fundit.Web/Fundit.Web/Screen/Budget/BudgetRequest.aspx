﻿<%@ Page Language="C#" MasterPageFile="~/_includes/MasterPageWithPrint.master" AutoEventWireup="true" CodeBehind="BudgetRequest.aspx.cs" Inherits="Fundit.Web.Screen.Budget.BudgetRequest" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="em-l-container">
        <div class="em-l em-l--two-column">
            <div class="em-l__main">


                <div class="em-c-tabs em-c-tabs--underline em-js-tabs">
                    <ul class="em-c-tabs__list">
                        <li class="em-c-tabs__item">
                            <a href="#tab-panel-1" class="em-c-tabs__link em-js-tab">Project Information</a>
                        </li>
                        <!-- end em-c-tabs__item -->
                        <li class="em-c-tabs__item">
                            <a href="#tab-panel-2" class="em-c-tabs__link em-js-tab">Budget Request</a>
                        </li>
                        <!-- end em-c-tabs__item -->
                        <li class="em-c-tabs__item">
                            <a href="#tab-panel-3" class="em-c-tabs__link em-js-tab">Budget Information</a>
                        </li>
                        <!-- end em-c-tabs__item -->
                        <li class="em-c-tabs__item">
                            <a href="#tab-panel-4" class="em-c-tabs__link em-js-tab">Allocation Information</a>
                        </li>
                        <!-- end em-c-tabs__item -->
                        <li class="em-c-tabs__item">
                            <a href="#tab-panel-5" class="em-c-tabs__link em-js-tab">Corporate BIS</a>
                        </li>
                        <!-- end em-c-tabs__item -->
                    </ul>
                    <!-- end em-c-tabs__list -->
                    <div class="em-c-tabs__body em-c-tabs__body--no-border">
                        <div class="em-c-tabs__panel em-js-tabs-panel" id="tab-panel-1">
                            <fieldset class="em-c-fieldset">
                                <div class="em-l-grid em-l-grid--3up">

                                    <div class="em-l-grid__item">
                                        <div class="em-c-field em-is-readonly">
                                            <label for="" class="em-c-field__label">Work Id</label>
                                            <div class="em-c-field__body">
                                                <input type="text" id="" class="em-c-input em-c-input--small" placeholder="Placeholder" value="" readonly />
                                                <svg class="em-c-icon em-c-field__icon em-c-icon--small">
                                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-lock"></use>
                                                </svg>
                                            </div>
                                            <!-- end em-c-field__body -->
                                            <div class="em-c-field__note">Work Id</div>
                                        </div>
                                        <!-- end em-c-field -->
                                    </div>

                                    <div class="em-l-grid__item">
                                        <div class="em-c-field em-is-readonly">
                                            <label for="" class="em-c-field__label">Work Class</label>
                                            <div class="em-c-field__body">
                                                <input type="text" id="" class="em-c-input em-c-input--small" placeholder="Placeholder" value="" readonly />
                                                <svg class="em-c-icon em-c-field__icon em-c-icon--small">
                                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-lock"></use>
                                                </svg>
                                            </div>
                                            <!-- end em-c-field__body -->
                                            <div class="em-c-field__note">Work Class</div>
                                        </div>

                                    </div>

                                    <div class="em-l-grid__item"></div>

                                    <div class="em-l-grid__item">
                                        <div class="em-c-field em-is-readonly">
                                            <label for="" class="em-c-field__label">Work Name</label>
                                            <div class="em-c-field__body">
                                                <input type="text" id="" class="em-c-input em-c-input--small" placeholder="Placeholder" value="" readonly />
                                                <svg class="em-c-icon em-c-field__icon em-c-icon--small">
                                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-lock"></use>
                                                </svg>
                                            </div>
                                            <!-- end em-c-field__body -->
                                            <div class="em-c-field__note">Work Name</div>
                                        </div>
                                    </div>

                                    <div class="em-l-grid__item">
                                        <div class="em-c-field ">
                                            <label for="" class="em-c-field__label">Funding Division</label>
                                            <div class="em-c-field__body em-c-field--small">
                                                <select class="em-c-select em-c-select em-u-width-100" id="">
                                                    <option value="option-1">Chemical & Corporate Portfolio</option>
                                                    <option value="option-2" selected>Downstream Portfolio</option>
                                                    <option value="option-2">General Interest Portfolio</option>
                                                    <option value="option-2">Service Portfolio</option>
                                                    <option value="option-2">Upstream Portfolio</option>
                                                </select>
                                            </div>
                                            <div class="em-c-field__note">Funding Division</div>
                                        </div>
                                    </div>

                                    <div class="em-l-grid__item"></div>

                                    <div class="em-l-grid__item">
                                        <div class="em-c-field em-is-readonly">
                                            <label for="" class="em-c-field__label">Work Type</label>
                                            <div class="em-c-field__body">
                                                <input type="text" id="" class="em-c-input em-c-input--small" placeholder="Placeholder" value="" readonly />
                                                <svg class="em-c-icon em-c-field__icon em-c-icon--small">
                                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-lock"></use>
                                                </svg>
                                            </div>
                                            <!-- end em-c-field__body -->
                                            <div class="em-c-field__note">Work Type</div>
                                        </div>
                                    </div>

                                    <div class="em-l-grid__item">
                                        <div class="em-c-field ">
                                            <label for="" class="em-c-field__label">Funding Division Function</label>
                                            <div class="em-c-field__body em-c-field--small">
                                                <select class="em-c-select em-c-select em-u-width-100" id="">

                                                    <option value="option-1" selected>Downstream - Downstream Business Services</option>
                                                    <option value="option-2">Downstream - Downstream SHE</option>
                                                    <option value="option-2">Downstream - FLS Marketing</option>
                                                </select>
                                            </div>
                                            <div class="em-c-field__note">Funding Division Function</div>
                                        </div>
                                    </div>

                                    <div class="em-l-grid__item"></div>

                                    <div class="em-l-grid__item">
                                        <div class="em-c-field em-is-readonly">
                                            <label for="" class="em-c-field__label">Work Lead</label>
                                            <div class="em-c-field__body">
                                                <input type="text" id="" class="em-c-input em-c-input--small" placeholder="Placeholder" value="12232" readonly />
                                                <svg class="em-c-icon em-c-field__icon em-c-icon--small">
                                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-lock"></use>
                                                </svg>
                                            </div>
                                            <!-- end em-c-field__body -->
                                            <div class="em-c-field__note">Work Lead</div>
                                        </div>
                                        <!-- end em-c-field -->
                                    </div>

                                    <div class="em-l-grid__item">
                                        <div class="em-c-field em-c-field--date-picker">
                                            <label for="date-1" class="em-c-field__label">Checkpoint 3/Startup Date</label>
                                            <div class="em-c-field__body ">
                                                <input type="text" step="1" id="date-1" class="em-c-field__input em-c-input--small em-js-datepicker" value="" />
                                                <svg class="em-c-icon em-c-field__icon em-c-icon--small">
                                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-calendar"></use>
                                                </svg>
                                            </div>
                                        </div>
                                        <!-- end em-c-field--date-picker -->
                                        <!-- IMPORTANT!
     In script tag below, update the src to use the correct path to Unity's
     js directory. For example:
     http://example.com/unity-1.1.1/js/vendor/pikaday.js
	Pikaday date picker script
-->
                                        <script type="text/javascript" src="../../assets/standard/unity-1.2.0/js/vendor/pikaday.js"></script>
                                        <script>
                                            var datePickers = document.querySelectorAll('.em-js-datepicker');
                                            for (i = 0; i < datePickers.length; i++) {
                                                var picker = new Pikaday(
                                                    {
                                                        field: datePickers[i]
                                                    });
                                            }
                                        </script>
                                    </div>

                                    <div class="em-l-grid__item"></div>

                                    <div class="em-c-field ">
                                        <label for="" class="em-c-field__label">Work Scope</label>
                                        <div class="em-c-field__body">
                                            <textarea class="em-c-textarea " id="" placeholder="Placeholder" rows="4" cols="95"></textarea>
                                        </div>
                                        <!-- end em-c-field__body -->
                                        <div class="em-c-field__note">Work Scope</div>
                                    </div>
                                    <!-- end em-c-field -->

                                </div>
                            </fieldset>
                        </div>
                        <div class="em-c-tabs__panel em-js-tabs-panel" id="tab-panel-2">



                            <div class="em-c-table-object ">
                                <div class="em-c-table-object__header">
                                </div>
                                <!--end em-c-table-object__header-->
                                <div class="em-c-table-object__body">
                                    <div class="em-c-table-object__body-inner">
                                        <table class="em-c-table em-c-table--condensed" border="0">
                                            <thead>
                                                <tr class="em-c-table__header-row">
                                                    <th scope="col" class="em-c-table__header-cell custom-em-c-table__header" style="width: 200px;"></th>
                                                    <th scope="col" class="em-c-table__header-cell custom-em-c-table__header" style="text-align: center"><b>Year 1</b></th>
                                                    <th scope="col" class="em-c-table__header-cell custom-em-c-table__header" style="text-align: center"><b>Year 2</b></th>
                                                    <th scope="col" class="em-c-table__header-cell custom-em-c-table__header" style="text-align: center"><b>Year 3</b></th>
                                                    <th scope="col" class="em-c-table__header-cell custom-em-c-table__header" style="text-align: center"><b>Year 4+</b></th>
                                                    <th scope="col" class="em-c-table__header-cell custom-em-c-table__header" style="text-align: center"><b>Previously Endorsed</b></th>
                                                    <th scope="col" class="em-c-table__header-cell custom-em-c-table__header" style="text-align: center"><b>Current Cummulative</b></th>
                                                    <th scope="col" class="em-c-table__header-cell custom-em-c-table__header" style="text-align: center"><b>Total MPCE</b></th>
                                                </tr>
                                                <!-- em-c-table__header-row -->
                                            </thead>
                                            <!-- end em-c-table__header -->
                                            <tbody class="em-c-table__body ">
                                                <tr class="em-c-table--condensed">
                                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable budget-request-label">IT Expense ($k)
                                                    </td>
                                                    <td class="em-c-table__cell">
                                                        <div class="em-c-field__body">
                                                            <input type="text" id="" class="em-c-input budget-request-input" value="" />
                                                        </div>
                                                    </td>
                                                    <td class="em-c-table__cell ">
                                                        <div class="em-c-field__body">
                                                            <input type="text" id="" class="em-c-input budget-request-input" value="" />
                                                        </div>
                                                    </td>
                                                    <td class="em-c-table__cell ">
                                                        <div class="em-c-field__body">
                                                            <input type="text" id="" class="em-c-input budget-request-input" value="" />
                                                        </div>
                                                    </td>
                                                    <td class="em-c-table__cell ">
                                                        <div class="em-c-field__body">
                                                            <input type="text" id="" class="em-c-input budget-request-input" value="" />
                                                        </div>
                                                    </td>
                                                    <td class="em-c-table__cell ">
                                                        <div class="em-c-field__body">
                                                            <input type="text" id="" class="em-c-input budget-request-input" value="" />
                                                        </div>
                                                    </td>
                                                    <td class="em-c-table__cell ">
                                                        <div class="em-c-field__body">
                                                            <input type="text" id="" class="em-c-input budget-request-input" value="" />
                                                        </div>
                                                    </td>
                                                    <td class="em-c-table__cell ">
                                                        <div class="em-c-field__body">
                                                            <input type="text" id="" class="em-c-input budget-request-input" value="" />
                                                        </div>
                                                    </td>
                                                </tr>
                                                <!-- end em-c-table__row -->
                                                <tr class="em-c-table--condensed">
                                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable budget-request-label">Business Expense ($k)
                                                    </td>
                                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                                        <div class="em-c-field__body">
                                                            <input type="text" id="" class="em-c-input budget-request-input" value="" />
                                                        </div>
                                                    </td>
                                                    <td class="em-c-table__cell ">
                                                        <div class="em-c-field__body">
                                                            <input type="text" id="" class="em-c-input budget-request-input" value="" />
                                                        </div>
                                                    </td>
                                                    <td class="em-c-table__cell ">
                                                        <div class="em-c-field__body">
                                                            <input type="text" id="" class="em-c-input budget-request-input" value="" />
                                                        </div>
                                                    </td>
                                                    <td class="em-c-table__cell ">
                                                        <div class="em-c-field__body">
                                                            <input type="text" id="" class="em-c-input budget-request-input" value="" />
                                                        </div>
                                                    </td>
                                                    <td class="em-c-table__cell ">
                                                        <div class="em-c-field__body">
                                                            <input type="text" id="" class="em-c-input budget-request-input" value="" />
                                                        </div>
                                                    </td>
                                                    <td class="em-c-table__cell ">
                                                        <div class="em-c-field__body">
                                                            <input type="text" id="" class="em-c-input budget-request-input" value="" />
                                                        </div>
                                                    </td>
                                                    <td class="em-c-table__cell ">
                                                        <div class="em-c-field__body">
                                                            <input type="text" id="" class="em-c-input budget-request-input" value="" />
                                                        </div>
                                                    </td>
                                                </tr>
                                                <!-- end em-c-table__row -->
                                                <tr class="em-c-table--condensed">
                                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable budget-request-label">IT Capital ($k)
                                                    </td>
                                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                                        <div class="em-c-field__body">
                                                            <input type="text" id="" class="em-c-input budget-request-input" value="" />
                                                        </div>
                                                    </td>
                                                    <td class="em-c-table__cell ">
                                                        <div class="em-c-field__body">
                                                            <input type="text" id="" class="em-c-input budget-request-input" value="" />
                                                        </div>
                                                    </td>
                                                    <td class="em-c-table__cell ">
                                                        <div class="em-c-field__body">
                                                            <input type="text" id="" class="em-c-input budget-request-input" value="" />
                                                        </div>
                                                    </td>
                                                    <td class="em-c-table__cell ">
                                                        <div class="em-c-field__body">
                                                            <input type="text" id="" class="em-c-input budget-request-input" value="" />
                                                        </div>
                                                    </td>
                                                    <td class="em-c-table__cell ">
                                                        <div class="em-c-field__body">
                                                            <input type="text" id="" class="em-c-input budget-request-input" value="" />
                                                        </div>
                                                    </td>
                                                    <td class="em-c-table__cell ">
                                                        <div class="em-c-field__body">
                                                            <input type="text" id="" class="em-c-input budget-request-input" value="" />
                                                        </div>
                                                    </td>
                                                    <td class="em-c-table__cell ">
                                                        <div class="em-c-field__body">
                                                            <input type="text" id="" class="em-c-input budget-request-input" value="" />
                                                        </div>
                                                    </td>
                                                </tr>
                                                <!-- end em-c-table__row -->
                                                <tr class="em-c-table--condensed budget-request-label">
                                                    <td class="em-c-table__cell ">Business Capital ($k)
                                                    </td>
                                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                                        <div class="em-c-field__body">
                                                            <input type="text" id="" class="em-c-input budget-request-input" value="" />
                                                        </div>
                                                    </td>
                                                    <td class="em-c-table__cell ">
                                                        <div class="em-c-field__body">
                                                            <input type="text" id="" class="em-c-input budget-request-input" value="" />
                                                        </div>
                                                    </td>
                                                    <td class="em-c-table__cell ">
                                                        <div class="em-c-field__body">
                                                            <input type="text" id="" class="em-c-input budget-request-input" value="" />
                                                        </div>
                                                    </td>
                                                    <td class="em-c-table__cell ">
                                                        <div class="em-c-field__body">
                                                            <input type="text" id="" class="em-c-input budget-request-input" value="" />
                                                        </div>
                                                    </td>
                                                    <td class="em-c-table__cell ">
                                                        <div class="em-c-field__body">
                                                            <input type="text" id="" class="em-c-input budget-request-input" value="" />
                                                        </div>
                                                    </td>
                                                    <td class="em-c-table__cell ">
                                                        <div class="em-c-field__body">
                                                            <input type="text" id="" class="em-c-input budget-request-input" value="" />
                                                        </div>
                                                    </td>
                                                    <td class="em-c-table__cell ">
                                                        <div class="em-c-field__body">
                                                            <input type="text" id="" class="em-c-input budget-request-input" value="" />
                                                        </div>
                                                    </td>
                                                </tr>
                                                <!-- end em-c-table__row -->
                                                <tr class="em-c-table--condensed">
                                                    <td class="em-c-table__cell budget-request-label">Total ($k)
                                                    </td>
                                                    <td class="em-c-table__cell ">
                                                        <label for="" class="em-c-field__label"></label>
                                                    </td>
                                                    <td class="em-c-table__cell ">
                                                        <label for="" class="em-c-field__label"></label>
                                                    </td>
                                                    <td class="em-c-table__cell ">
                                                        <label for="" class="em-c-field__label"></label>
                                                    </td>
                                                    <td class="em-c-table__cell ">
                                                        <label for="" class="em-c-field__label"></label>
                                                    </td>
                                                    <td class="em-c-table__cell ">
                                                        <label for="" class="em-c-field__label"></label>
                                                    </td>
                                                    <td class="em-c-table__cell ">
                                                        <label for="" class="em-c-field__label"></label>
                                                    </td>
                                                    <td class="em-c-table__cell ">
                                                        <label for="" class="em-c-field__label"></label>
                                                    </td>
                                                </tr>
                                                <!-- end em-c-table__row -->

                                                <tr class="em-c-table--condensed">
                                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable"></td>
                                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="7" style="background-color: #0c69b0">
                                                        <div class="em-u-text-align-right">
                                                            <label for="" class="em-c-field__label">Percentage of Total MPCE: &nbsp;&nbsp; &nbsp; 1234</label>
                                                        </div>
                                                    </td>

                                                </tr>
                                                <!-- end em-c-table__row -->

                                                <tr class="em-c-table--condensed">
                                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable td-vertical-align-center budget-request-label">Business Led?
                                                    </td>
                                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="2">
                                                        <select class="em-c-select em-c-select em-u-width-100 budget-request-select" id="">
                                                            <option value="option-1">No</option>
                                                            <option value="option-2">Yes</option>
                                                        </select>
                                                    </td>
                                                    <td class="em-c-table__cell budget-request-label" colspan="3">Previous OPEX w/o BIS - EMIT ($k)
                                                    </td>
                                                    <td class="em-c-table__cell " colspan="2">
                                                        <div class="em-c-field__body">
                                                            <input type="text" id="" class="em-c-input budget-request-input" value="" />
                                                            <div class="em-c-field__note">Supporting Documentation Required</div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <!-- end em-c-table__row -->
                                                <tr class="em-c-table--condensed">
                                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable budget-request-label" colspan="3"></td>
                                                    <td class="em-c-table__cell td-vertical-align-center budget-request-label" colspan="3">Previous OPEX w/o BIS - Business ($k)
                                                    </td>
                                                    <td class="em-c-table__cell " colspan="2">
                                                        <div class="em-c-field__body">
                                                            <input type="text" id="" class="em-c-input budget-request-input" value="" />
                                                        </div>
                                                    </td>
                                                </tr>
                                                <!-- end em-c-table__row -->

                                            </tbody>
                                            <!-- end em-c-table__body -->
                                        </table>
                                        <!--end em-c-table-->
                                    </div>
                                    <!--end em-c-table-object__body-inner-->
                                </div>
                                <!--end em-c-table-object__body-->
                            </div>
                            <!--end em-c-table-object-->
                        </div>
                        <div class="em-c-tabs__panel em-js-tabs-panel" id="tab-panel-3">
                            <fieldset class="em-c-fieldset">
                                <div class="em-l-grid em-l-grid--3up">

                                    <div class="em-l-grid__item">
                                        <div class="em-c-field ">
                                            <label for="" class="em-c-field__label">Type of Funding</label>
                                            <div class="em-c-field__body em-c-field--small">
                                                <select class="em-c-select em-c-select em-u-width-100" id="">
                                                    <option value=""></option>
                                                    <option value="option-1">Cumulative Advance Funding</option>
                                                    <option value="option-2">Cumulative Advance Funding (>25% of MPCE)</option>
                                                    <option value="option-3">Full Funding</option>
                                                    <option value="option-4">Supplemental Funding</option>
                                                    <option value="option-5">Release of Previously Appropriated Funds</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="em-c-field__note">Type of Funding</div>
                                    </div>

                                    <div class="em-l-grid__item">
                                        <div class="em-c-field em-is-readonly">
                                            <label for="" class="em-c-field__label">Budget Category</label>
                                            <div class="em-c-field__body">
                                                <input type="text" id="" class="em-c-input em-c-input--small" placeholder="Placeholder" value="" readonly />
                                                <svg class="em-c-icon em-c-field__icon em-c-icon--small">
                                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-lock"></use>
                                                </svg>
                                            </div>
                                            <!-- end em-c-field__body -->
                                            <div class="em-c-field__note">Budget Category</div>
                                        </div>

                                    </div>

                                    <div class="em-l-grid__item"></div>

                                    <div class="em-l-grid__item">
                                        <div class="em-c-field ">
                                            <label for="" class="em-c-field__label">DOAG Required</label>
                                            <div class="em-c-field__body em-c-field--small">
                                                <select class="em-c-select em-c-select em-u-width-100 em-c-input--small" id="">
                                                    <option value=""></option>
                                                    <option value="option-1">DOAG_Level</option>
                                                    <option value="option-2">DOAG_Level 1-BD, MC, or CE ($50M)</option>
                                                    <option value="option-3">DOAG_Level 2-BD, MC, or CE ($50M)</option>
                                                    <option value="option-4">DOAG_Level 3-GSC President</option>
                                                    <option value="option-5">DOAG_Level 4-EMIT VP / IT Ops Mgr.</option>
                                                    <option value="option-6">DOAG_Level 5-EMIT Division Mgr</option>
                                                    <option value="option-7">DOAG_Level 6</option>
                                                    <option value="option-8">DOAG_Level 7</option>
                                                    <option value="option-9">DOAG_Level 8</option>
                                                    <option value="option-9">DOAG_Level 9</option>
                                                </select>
                                            </div>
                                            <div class="em-c-field__note">DOAG Required</div>
                                        </div>
                                    </div>

                                    <div class="em-l-grid__item">
                                        <div class="em-c-field ">
                                            <label for="" class="em-c-field__label">DOAG Approver</label>
                                            <div class="em-c-field__body em-c-field--small">
                                                <select class="em-c-select em-c-select em-u-width-100 em-c-input--small" id="">
                                                    <option value=""></option>
                                                    <option value="option-1">Mike Brown</option>
                                                    <option value="option-1">Susan Gate</option>
                                                </select>
                                            </div>
                                            <div class="em-c-field__note">DOAG Approver</div>
                                        </div>
                                    </div>

                                    <div class="em-l-grid__item"></div>

                                    <div class="em-l-grid__item">
                                        <div class="em-c-field em-is-readonly">
                                            <label for="" class="em-c-field__label">Requestor Name</label>
                                            <div class="em-c-field__body">
                                                <input type="text" id="" class="em-c-input em-c-input--small" placeholder="Placeholder" value="" readonly />
                                                <svg class="em-c-icon em-c-field__icon em-c-icon--small">
                                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-lock"></use>
                                                </svg>
                                            </div>
                                            <!-- end em-c-field__body -->
                                            <div class="em-c-field__note">Requestor Name</div>
                                        </div>

                                    </div>

                                    <div class="em-l-grid__item">
                                        <div class="em-c-field ">
                                            <label for="" class="em-c-field__label">Controller Endorser</label>
                                            <div class="em-c-field__body em-c-field--small">
                                                <select class="em-c-select em-c-select em-u-width-100 em-c-input--small" id="">
                                                    <option value=""></option>
                                                    <option value="option-1">Jeffrey L. Williams</option>
                                                    <option value="option-2">Suzanne Daroowala</option>
                                                    <option value="option-3">Zain Willoughby</option>
                                                </select>
                                            </div>
                                            <div class="em-c-field__note">Funding Division Function</div>
                                        </div>
                                    </div>

                                    <div class="em-l-grid__item"></div>

                                    <div class="em-l-grid__item">
                                        <div class="em-c-field">
                                            <label for="" class="em-c-field__label">Controller's Contact</label>
                                            <div class="em-c-field__body ">
                                                <input type="text" id="" class="em-c-input em-c-input--small" placeholder="Placeholder" value="" />
                                            </div>
                                            <!-- end em-c-field__body -->
                                            <div class="em-c-field__note">This is a required field</div>
                                        </div>
                                        <!-- end em-c-field -->
                                    </div>

                                    <div class="em-l-grid__item"></div>

                                    <div class="em-l-grid__item"></div>

                                    <div class="em-c-field ">
                                        <label for="" class="em-c-field__label">Asset Description</label>
                                        <div class="em-c-field__body">
                                            <textarea class="em-c-textarea " id="" placeholder="Placeholder" rows="4" cols="95"></textarea>
                                        </div>
                                        <!-- end em-c-field__body -->
                                        <div class="em-c-field__note">Asset Description</div>
                                    </div>
                                    <!-- end em-c-field -->
                                </div>
                            </fieldset>

                        </div>
                        <div class="em-c-tabs__panel em-js-tabs-panel" id="tab-panel-4">
                            <div class="em-l-grid em-l-grid--1-2-1 ">
                                <div class="em-l-grid__item">
                                    <div class="em-c-field group-button-center">
                                        <h4 class="em-c-field__label"></h4>
                                        <div class="em-c-field__body">
                                            <div class="em-c-toggle">
                                                <input class="em-c-toggle__input em-u-is-vishidden" type="radio" id="toggle-1" name="togglename" checked />
                                                <label class="em-c-toggle__label" for="toggle-1">
                                                    Hardware
                                                </label>
                                                <input class="em-c-toggle__input em-u-is-vishidden" type="radio" id="toggle-2" name="togglename" />
                                                <label class="em-c-toggle__label" for="toggle-2">
                                                    Software
                                                </label>
                                                <input class="em-c-toggle__input em-u-is-vishidden" type="radio" id="toggle-3" name="togglename" />
                                                <label class="em-c-toggle__label" for="toggle-3">
                                                    Capitalized Staffing
                                                </label>
                                                <input class="em-c-toggle__input em-u-is-vishidden" type="radio" id="toggle-4" name="togglename" />
                                                <label class="em-c-toggle__label" for="toggle-4">
                                                    Expense
                                                </label>
                                            </div>
                                            <!-- end em-c-toggle -->
                                        </div>
                                    </div>
                                    <!-- end em-c-field -->
                                </div>
                                <!-- end em-l-grid__item -->

                                <div class="em-l-grid__item">
                                    <!-- 'fpo' stands for 'for placement only'. Replace the 'fpo-block' div element with your own markup-->
                                    <div class="fpo-block fpo-block--dark">

                                        <div class="em-c-table-object ">
                                            <div class="em-c-table-object__header">

                                                <div class="em-u-text-align-left ">
                                                    <div class="em-u-font-style-light em-u-text-align-right">
                                                        <span class="em-u-font-style-regular" style="float: left">HARDWARE ALLOCATED RESOURCES</span>
                                                        <svg class="em-c-icon em-c-icon--small" style="float: left">
                                                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-plus"></use>
                                                        </svg>
                                                        <span class="em-u-font-style-regular ">Unallocated - Capex $340.44 Opex $355.00</span>
                                                    </div>
                                                </div>
                                                <br />



                                            </div>
                                            <!--end em-c-table-object__header-->
                                            <div class="em-c-table-object__body em-c-table--condensed">
                                                <div class="em-c-table-object__body-inner">
                                                    <table class="em-c-table ">
                                                        <thead class="em-c-table__header">
                                                            <tr class="em-c-table__header-row">
                                                                <th scope="col" class="em-c-table__header-cell ">STATUS
                                            <svg class="em-c-icon em-c-icon--tiny">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-lock"></use>
                                            </svg>
                                                                    <svg class="em-c-icon em-c-icon--tiny ">
                                                                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-up"></use>
                                                                    </svg>
                                                                </th>
                                                                <th scope="col" class="em-c-table__header-cell ">QUANTITY
                                            <svg class="em-c-icon em-c-icon--tiny">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-lock"></use>
                                            </svg>
                                                                    <svg class="em-c-icon em-c-icon--tiny ">
                                                                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down"></use>
                                                                    </svg>
                                                                </th>
                                                                <th scope="col" class="em-c-table__header-cell ">CAPEX ($KUSD)
                                            <svg class="em-c-icon em-c-icon--tiny">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-lock"></use>
                                            </svg>
                                                                    <svg class="em-c-icon em-c-icon--tiny ">
                                                                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down"></use>
                                                                    </svg>
                                                                </th>
                                                                <th scope="col" class="em-c-table__header-cell ">AR #
                                            <svg class="em-c-icon em-c-icon--tiny">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-lock"></use>
                                            </svg>
                                                                    <svg class="em-c-icon em-c-icon--tiny ">
                                                                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down"></use>
                                                                    </svg>
                                                                </th>
                                                                <th scope="col" class="em-c-table__header-cell ">COMPANY
                                            <svg class="em-c-icon em-c-icon--tiny">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-lock"></use>
                                            </svg>
                                                                    <svg class="em-c-icon em-c-icon--tiny ">
                                                                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down"></use>
                                                                    </svg>
                                                                </th>
                                                                <th scope="col" class="em-c-table__header-cell ">COUNTRY
                                            <svg class="em-c-icon em-c-icon--tiny">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-lock"></use>
                                            </svg>
                                                                    <svg class="em-c-icon em-c-icon--tiny ">
                                                                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down"></use>
                                                                    </svg>
                                                                </th>
                                                                <th scope="col" class="em-c-table__header-cell ">ORGANIZATION
                                            <svg class="em-c-icon em-c-icon--tiny">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-lock"></use>
                                            </svg>
                                                                    <svg class="em-c-icon em-c-icon--tiny ">
                                                                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down"></use>
                                                                    </svg>
                                                                </th>
                                                                <th scope="col" class="em-c-table__header-cell ">LOCATION
                                            <svg class="em-c-icon em-c-icon--tiny">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-lock"></use>
                                            </svg>
                                                                    <svg class="em-c-icon em-c-icon--tiny ">
                                                                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down"></use>
                                                                    </svg>
                                                                </th>
                                                                <th scope="col" class="em-c-table__header-cell ">DEPRECIATION LOCATION
                                            <svg class="em-c-icon em-c-icon--tiny">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-lock"></use>
                                            </svg>
                                                                    <svg class="em-c-icon em-c-icon--tiny ">
                                                                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down"></use>
                                                                    </svg>
                                                                </th>
                                                                <th scope="col" class="em-c-table__header-cell ">TYPE
                                            <svg class="em-c-icon em-c-icon--tiny">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-lock"></use>
                                            </svg>
                                                                    <svg class="em-c-icon em-c-icon--tiny ">
                                                                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down"></use>
                                                                    </svg>
                                                                </th>
                                                                <th scope="col" class="em-c-table__header-cell ">COST CENTER
                                            <svg class="em-c-icon em-c-icon--tiny">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-lock"></use>
                                            </svg>
                                                                    <svg class="em-c-icon em-c-icon--tiny ">
                                                                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down"></use>
                                                                    </svg>
                                                                </th>
                                                                <th scope="col" class="em-c-table__header-cell ">DESCRIPTION
                                            <svg class="em-c-icon em-c-icon--tiny">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-lock"></use>
                                            </svg>
                                                                    <svg class="em-c-icon em-c-icon--tiny ">
                                                                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down"></use>
                                                                    </svg>
                                                                </th>
                                                            </tr>
                                                            <!-- em-c-table__header-row -->
                                                        </thead>
                                                        <!-- end em-c-table__header -->
                                                        <tbody class="em-c-table__body ">
                                                            <tr class="em-c-table__row ">
                                                                <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">Open
                                                                </td>
                                                                <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">8
                                                                </td>
                                                                <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">$70.00
                                                                </td>
                                                                <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">16006001
                                                                </td>
                                                                <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">1575
                                                                </td>
                                                                <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">Unites States
                                                                </td>
                                                                <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">Organization
                                                                </td>
                                                                <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">N/A
                                                                </td>
                                                                <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">N/A
                                                                </td>
                                                                <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">Type
                                                                </td>
                                                                <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">N/A
                                                                </td>
                                                                <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">Description
                                                                </td>
                                                            </tr>
                                                            <!-- end em-c-table__row -->
                                                            <tr class="em-c-table__row ">
                                                                <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">Open
                                                                </td>
                                                                <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">8
                                                                </td>
                                                                <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">$70.00
                                                                </td>
                                                                <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">16006001
                                                                </td>
                                                                <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">1575
                                                                </td>
                                                                <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">Unites States
                                                                </td>
                                                                <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">Organization
                                                                </td>
                                                                <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">N/A
                                                                </td>
                                                                <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">N/A
                                                                </td>
                                                                <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">Type
                                                                </td>
                                                                <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">N/A
                                                                </td>
                                                                <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">Description
                                                                </td>
                                                            </tr>
                                                            <!-- end em-c-table__row -->
                                                            <tr class="em-c-table__row ">
                                                                <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">Open
                                                                </td>
                                                                <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">8
                                                                </td>
                                                                <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">$70.00
                                                                </td>
                                                                <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">16006001
                                                                </td>
                                                                <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">1575
                                                                </td>
                                                                <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">Unites States
                                                                </td>
                                                                <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">Organization
                                                                </td>
                                                                <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">N/A
                                                                </td>
                                                                <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">N/A
                                                                </td>
                                                                <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">Type
                                                                </td>
                                                                <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">N/A
                                                                </td>
                                                                <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">Description
                                                                </td>
                                                            </tr>
                                                            <tr class="em-c-table__row ">
                                                                <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">Open
                                                                </td>
                                                                <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">8
                                                                </td>
                                                                <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">$70.00
                                                                </td>
                                                                <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">16006001
                                                                </td>
                                                                <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">1575
                                                                </td>
                                                                <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">Unites States
                                                                </td>
                                                                <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">Organization
                                                                </td>
                                                                <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">N/A
                                                                </td>
                                                                <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">N/A
                                                                </td>
                                                                <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">Type
                                                                </td>
                                                                <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">N/A
                                                                </td>
                                                                <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">Description
                                                                </td>
                                                            </tr>
                                                            <tr class="em-c-table__row ">
                                                                <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">Open
                                                                </td>
                                                                <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">8
                                                                </td>
                                                                <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">$70.00
                                                                </td>
                                                                <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">16006001
                                                                </td>
                                                                <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">1575
                                                                </td>
                                                                <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">Unites States
                                                                </td>
                                                                <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">Organization
                                                                </td>
                                                                <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">N/A
                                                                </td>
                                                                <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">N/A
                                                                </td>
                                                                <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">Type
                                                                </td>
                                                                <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">N/A
                                                                </td>
                                                                <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">Description
                                                                </td>
                                                            </tr>


                                                            <!-- end em-c-table__row -->
                                                        </tbody>
                                                        <!-- end em-c-table__body -->
                                                        <tfoot class="em-c-table__footer ">
                                                            <tr class="em-c-table__footer-row ">
                                                                <td class="em-c-table__cell em-js-cell em-js-cell-editable em-u-margin-quad" colspan="20" style="background-color: #0c69b0">
                                                                    <div class="em-u-text-align-left">
                                                                        <label for="" class="em-c-field__label text-color-white">Total: &nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp; 11 &nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp; $370.00</label>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </tfoot>
                                                        <!-- end em-c-table__footer -->
                                                    </table>
                                                    <!--end em-c-table-->
                                                </div>
                                                <!--end em-c-table-object__body-inner-->
                                            </div>
                                            <!--end em-c-table-object__body-->
                                        </div>
                                        <!--end em-c-table-object-->
                                    </div>
                                </div>
                                <!-- end em-l-grid__item -->
                            </div>
                            <!-- end em-l-grid--1-2-1 -->

                        </div>
                        <div class="em-c-tabs__panel em-js-tabs-panel" id="tab-panel-5">
                            <div class="em-c-page-header em-c-page-header--small">
                                <h1 class="em-c-page-header__title grey-background">CAPITAL AND OPEX BY YEAR</h1>
                            </div>
                            <div class="em-c-table-object ">
                                <div class="em-c-table-object__header">
                                </div>
                                <!--end em-c-table-object__header-->
                                <div class="em-c-table-object__body">
                                    <div class="em-c-table-object__body-inner">
                                        <table class="em-c-table em-c-table--condensed" border="0">
                                            <thead>
                                                <tr class="em-c-table__header-row">
                                                    <th scope="col" class="em-c-table__header-cell custom-em-c-table__header" style="width: 200px;"></th>
                                                    <th scope="col" class="em-c-table__header-cell custom-em-c-table__header" style="text-align: center"><b>Year 1</b></th>
                                                    <th scope="col" class="em-c-table__header-cell custom-em-c-table__header" style="text-align: center"><b>Year 2</b></th>
                                                    <th scope="col" class="em-c-table__header-cell custom-em-c-table__header" style="text-align: center"><b>Year 3</b></th>
                                                    <th scope="col" class="em-c-table__header-cell custom-em-c-table__header" style="text-align: center"><b>Year 4+</b></th>
                                                </tr>
                                                <!-- em-c-table__header-row -->
                                            </thead>
                                            <!-- end em-c-table__header -->
                                            <tbody class="em-c-table__body ">
                                                <tr class="em-c-table--condensed">
                                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Capex Current Proposal
                                                    </td>
                                                    <td class="em-c-table__cell">
                                                        <label for="" class="em-c-field__label">Auto</label>
                                                    </td>
                                                    <td class="em-c-table__cell ">
                                                        <label for="" class="em-c-field__label">Auto</label>
                                                    </td>
                                                    <td class="em-c-table__cell ">
                                                        <label for="" class="em-c-field__label">Auto</label>
                                                    </td>
                                                    <td class="em-c-table__cell ">
                                                        <label for="" class="em-c-field__label">Auto</label>
                                                    </td>
                                                </tr>
                                                <!-- end em-c-table__row -->
                                                <tr class="em-c-table--condensed">
                                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable budget-request-label">Investment Plan
                                                    </td>
                                                    <td class="em-c-table__cell">
                                                        <select class="em-c-select em-c-select em-u-width-100 budget-request-select" id="">
                                                            <option value="option-1">Select</option>
                                                        </select>
                                                    </td>
                                                    <td class="em-c-table__cell ">
                                                        <select class="em-c-select em-c-select em-u-width-100 budget-request-select" id="">
                                                            <option value="option-1">Select</option>
                                                        </select>
                                                    </td>
                                                    <td class="em-c-table__cell ">
                                                        <select class="em-c-select em-c-select em-u-width-100 budget-request-select" id="">
                                                            <option value="option-1">Select</option>
                                                        </select>
                                                    </td>
                                                    <td class="em-c-table__cell ">
                                                        <select class="em-c-select em-c-select em-u-width-100 budget-request-select" id="">
                                                            <option value="option-1">Select</option>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <!-- end em-c-table__row -->
                                                <tr class="em-c-table--condensed">
                                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Capex Current Proposal
                                                    </td>
                                                    <td class="em-c-table__cell">
                                                        <label for="" class="em-c-field__label">Auto</label>
                                                    </td>
                                                    <td class="em-c-table__cell ">
                                                        <label for="" class="em-c-field__label">Auto</label>
                                                    </td>
                                                    <td class="em-c-table__cell ">
                                                        <label for="" class="em-c-field__label">Auto</label>
                                                    </td>
                                                    <td class="em-c-table__cell ">
                                                        <label for="" class="em-c-field__label">Auto</label>
                                                    </td>
                                                </tr>
                                                <!-- end em-c-table__row -->
                                                <tr class="em-c-table--condensed">
                                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable budget-request-label">Investment Plan
                                                    </td>
                                                    <td class="em-c-table__cell">
                                                        <select class="em-c-select em-c-select em-u-width-100 budget-request-select" id="">
                                                            <option value="option-1">Select</option>
                                                        </select>
                                                    </td>
                                                    <td class="em-c-table__cell ">
                                                        <select class="em-c-select em-c-select em-u-width-100 budget-request-select" id="">
                                                            <option value="option-1">Select</option>
                                                        </select>
                                                    </td>
                                                    <td class="em-c-table__cell ">
                                                        <select class="em-c-select em-c-select em-u-width-100 budget-request-select" id="">
                                                            <option value="option-1">Select</option>
                                                        </select>
                                                    </td>
                                                    <td class="em-c-table__cell ">
                                                        <select class="em-c-select em-c-select em-u-width-100 budget-request-select" id="">
                                                            <option value="option-1">Select</option>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <!-- end em-c-table__row -->

                                            </tbody>
                                            <!-- end em-c-table__body -->
                                        </table>
                                        <!--end em-c-table-->
                                    </div>
                                    <!--end em-c-table-object__body-inner-->
                                </div>
                                <!--end em-c-table-object__body-->
                            </div>

                            <div class="em-c-page-header em-c-page-header--small">
                                <h1 class="em-c-page-header__title grey-background">SOME TITLE HERE</h1>
                            </div>
                            <div class="em-c-table-object ">
                                <div class="em-c-table-object__header">
                                </div>
                                <!--end em-c-table-object__header-->
                                <div class="em-c-table-object__body">
                                    <div class="em-c-table-object__body-inner">
                                        <table class="em-c-table em-c-table--condensed" border="0">
                                            <thead>
                                                <tr class="em-c-table__header-row">
                                                    <th scope="col" class="em-c-table__header-cell custom-em-c-table__header" style="width: 200px;"></th>
                                                    <th scope="col" class="em-c-table__header-cell custom-em-c-table__header" style="text-align: center"><b>Current</b></th>
                                                    <th scope="col" class="em-c-table__header-cell custom-em-c-table__header" style="text-align: center"><b>Long Range</b></th>
                                                </tr>
                                                <!-- em-c-table__header-row -->
                                            </thead>
                                            <!-- end em-c-table__header -->
                                            <tbody class="em-c-table__body ">
                                                <tr class="em-c-table--condensed">
                                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable  budget-request-label">Capital Lease
                                                    </td>
                                                    <td class="em-c-table__cell">
                                                        <div class="em-c-field__body">
                                                            <input type="text" id="" class="em-c-input budget-request-input" value="" />
                                                        </div>
                                                    </td>
                                                    <td class="em-c-table__cell ">
                                                        <div class="em-c-field__body">
                                                            <input type="text" id="" class="em-c-input budget-request-input" value="" />
                                                        </div>
                                                    </td>
                                                </tr>
                                                <!-- end em-c-table__row -->
                                                <tr class="em-c-table--condensed">
                                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable budget-request-label">Deferred Charge
                                                    </td>
                                                    <td class="em-c-table__cell">
                                                        <div class="em-c-field__body">
                                                            <input type="text" id="" class="em-c-input budget-request-input" value="" />
                                                        </div>
                                                    </td>
                                                    <td class="em-c-table__cell ">
                                                        <div class="em-c-field__body">
                                                            <input type="text" id="" class="em-c-input budget-request-input" value="" />
                                                        </div>
                                                    </td>
                                                </tr>
                                                <!-- end em-c-table__row -->
                                                <tr class="em-c-table--condensed">
                                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable budget-request-label">Working Capital
                                                    </td>
                                                    <td class="em-c-table__cell">
                                                        <div class="em-c-field__body">
                                                            <input type="text" id="" class="em-c-input budget-request-input" value="" />
                                                        </div>
                                                    </td>
                                                    <td class="em-c-table__cell ">
                                                        <div class="em-c-field__body">
                                                            <input type="text" id="" class="em-c-input budget-request-input" value="" />
                                                        </div>
                                                    </td>
                                                </tr>
                                                <!-- end em-c-table__row -->
                                                <tr class="em-c-table--condensed">
                                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable budget-request-label">Assoc. Facilities
                                                    </td>

                                                    <td class="em-c-table__cell">
                                                        <div class="em-c-field__body">
                                                            <input type="text" id="" class="em-c-input budget-request-input" value="" />
                                                        </div>
                                                    </td>
                                                    <td class="em-c-table__cell ">
                                                        <div class="em-c-field__body">
                                                            <input type="text" id="" class="em-c-input budget-request-input" value="" />
                                                        </div>
                                                    </td>
                                                </tr>
                                                <!-- end em-c-table__row -->
                                                <tr class="em-c-table--condensed">
                                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Investment Credit
                                                    </td>
                                                    <td class="em-c-table__cell">
                                                        <label for="" class="em-c-field__label">Auto</label>
                                                    </td>
                                                    <td class="em-c-table__cell ">
                                                        <label for="" class="em-c-field__label">Auto</label>
                                                    </td>
                                                </tr>
                                                <!-- end em-c-table__row -->
                                                <tr class="em-c-table--condensed">
                                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Other
                                                    </td>
                                                    <td class="em-c-table__cell">
                                                        <label for="" class="em-c-field__label">Auto</label>
                                                    </td>
                                                    <td class="em-c-table__cell ">
                                                        <label for="" class="em-c-field__label">Auto</label>
                                                    </td>
                                                </tr>
                                                <!-- end em-c-table__row -->
                                            </tbody>
                                            <!-- end em-c-table__body -->
                                        </table>
                                        <!--end em-c-table-->
                                    </div>
                                    <!--end em-c-table-object__body-inner-->
                                </div>
                                <!--end em-c-table-object__body-->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end em-c-tabs -->


            </div>
            <!-- end em-l__main -->
            <div class="em-l__secondary">
                <!-- 'fpo' stands for 'for placement only'. Replace the 'fpo-block' div element with your own markup-->

                <div class="em-c-tabs  em-js-tabs">
                    <ul class="em-c-tabs__list">
                        <li class="em-c-tabs__item em-c-tabs--gray">
                            <a href="#right-tab-panel-1" class="em-c-tabs__link em-js-tab">
                                <svg class="em-c-icon em-c-icon--small">
                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-feedback"></use>
                                </svg>
                            </a>
                        </li>
                        <!-- end em-c-tabs__item -->
                        <li class="em-c-tabs__item em-c-tabs--gray">
                            <a href="#right-tab-panel-2" class="em-c-tabs__link em-js-tab">
                                <svg class="em-c-icon em-c-icon--small">
                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-feedback"></use>
                                </svg>
                            </a>
                        </li>
                        <!-- end em-c-tabs__item -->
                        <li class="em-c-tabs__item">
                            <a href="#right-tab-panel-3" class="em-c-tabs__link em-js-tab">
                                <svg class="em-c-icon em-c-icon--small">
                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-user"></use>
                                </svg>
                            </a>
                        </li>
                        <!-- end em-c-tabs__item -->
                        <li class="em-c-tabs__item">
                            <a href="#right-tab-panel-4" class="em-c-tabs__link em-js-tab">
                                <svg class="em-c-icon em-c-icon--small">
                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-document"></use>
                                </svg>
                            </a>
                        </li>
                        <!-- end em-c-tabs__item -->
                        <li class="em-c-tabs__item">
                            <a href="#right-tab-panel-5" class="em-c-tabs__link em-js-tab">
                                <svg class="em-c-icon em-c-icon--small">
                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-clipboard"></use>
                                </svg>
                            </a>
                        </li>
                        <!-- end em-c-tabs__item -->
                    </ul>
                    <!-- end em-c-tabs__list -->
                    <div class="em-c-tabs__body ">
                        <div class="em-c-tabs__panel em-js-tabs-panel" id="right-tab-panel-1">
                            <h3>Comments</h3>
                            <hr class="custom-hr" />

                            <div id="new-comment">
                                <div class="em-c-table-object ">
                                    <div class="em-c-table-object__header">
                                    </div>
                                    <!--end em-c-table-object__header-->
                                    <div class="em-c-table-object__body">
                                        <div class="em-c-table-object__body-inner">
                                            <table class="em-c-table em-c-table--condensed" border="0">
                                                <tbody class="em-c-table__body ">
                                                    <tr class="em-c-table--condensed">
                                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable budget-request-label" colspan="2">
                                                            <label>May Weather</label>
                                                        </td>
                                                    </tr>
                                                    <!-- end em-c-table__row -->
                                                    <tr class="em-c-table--condensed">
                                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                                            <span>Project Manager</span>
                                                        </td>
                                                        <td><span>16:45 &nbsp; 12/20</span></td>
                                                    </tr>
                                                    <!-- end em-c-table__row -->
                                                    <tr class="em-c-table--condensed">
                                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="2">

                                                            <div class="em-c-field ">
                                                                <div class="em-c-field__body">
                                                                    <textarea class="em-c-textarea " id="" placeholder="Start typing here" value="" rows="2"></textarea>
                                                                </div>
                                                                <!-- end em-c-field__body -->
                                                            </div>
                                                            <!-- end em-c-field -->
                                                        </td>
                                                    </tr>
                                                    <!-- end em-c-table__row -->
                                                    <tr class="em-c-table--condensed">
                                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable "></td>
                                                        <td class="float-right">
                                                            <button class="em-c-btn ">
                                                                <span class="em-c-btn__text">Post</span>
                                                            </button>
                                                        </td>
                                                    </tr>
                                                    <!-- end em-c-table__row -->
                                                </tbody>
                                                <!-- end em-c-table__body -->
                                            </table>
                                            <!--end em-c-table-->
                                        </div>
                                        <!--end em-c-table-object__body-inner-->
                                    </div>
                                    <!--end em-c-table-object__body-->
                                </div>
                                <!--end em-c-table-object-->
                            </div>
                            <div id="previous-comment">
                                <div class="em-c-table-object ">
                                    <div class="em-c-table-object__header">
                                    </div>
                                    <!--end em-c-table-object__header-->
                                    <div class="em-c-table-object__body">
                                        <div class="em-c-table-object__body-inner">
                                            <table class="em-c-table em-c-table--condensed" border="0">
                                                <tbody class="em-c-table__body ">
                                                    <tr class="em-c-table__row em-c-table--condensed">
                                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">
                                                            <div><b>Aaron Smith</b></div>
                                                            <div><span>Controller</span><span class="float-right">16:25 &nbsp;12/20</span></div>
                                                            <br />
                                                            <div class="grey-background" style="padding: 5px;">
                                                                Lorem ipsum dolor sit amet, timeam copiosae
                                                        democritum eu mel. Ex tamquam tractatos
                                                        nec, stet assentior cu qui, agam audire at pro
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <!-- end em-c-table__row -->
                                                    <tr class="em-c-table__row em-c-table--condensed">
                                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">
                                                            <div><b>Aaron Smith</b></div>
                                                            <div><span>Controller</span><span class="float-right">16:25 &nbsp;12/20</span></div>
                                                            <br />
                                                            <div class="grey-background" style="padding: 5px;">
                                                                Lorem ipsum dolor sit amet, timeam copiosae
                                                        democritum eu mel. Ex tamquam tractatos
                                                        nec, stet assentior cu qui, agam audire at pro
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <!-- end em-c-table__row -->
                                                    <tr class="em-c-table__row em-c-table--condensed">
                                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">
                                                            <div><b>Aaron Smith</b></div>
                                                            <div><span>Controller</span><span class="float-right">16:25 &nbsp;12/20</span></div>
                                                            <br />
                                                            <div class="grey-background" style="padding: 5px;">
                                                                Lorem ipsum dolor sit amet, timeam copiosae
                                                        democritum eu mel. Ex tamquam tractatos
                                                        nec, stet assentior cu qui, agam audire at pro
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <!-- end em-c-table__row -->
                                                </tbody>
                                                <!-- end em-c-table__body -->
                                            </table>
                                            <!--end em-c-table-->
                                        </div>
                                        <!--end em-c-table-object__body-inner-->
                                    </div>
                                    <!--end em-c-table-object__body-->
                                </div>
                                <!--end em-c-table-object-->
                            </div>
                        </div>
                        <div class="em-c-tabs__panel em-js-tabs-panel" id="right-tab-panel-2">
                            <h3>Activities</h3>
                            <hr class="custom-hr" />

                            <div class="em-c-table-object ">
                                <div class="em-c-table-object__header">
                                </div>
                                <!--end em-c-table-object__header-->
                                <div class="em-c-table-object__body">
                                    <div class="em-c-table-object__body-inner">
                                        <table class="em-c-table em-c-table--condensed">
                                            <tbody class="em-c-table__body ">
                                                <tr class="em-c-table__row em-c-table--condensed">
                                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">
                                                        <div><span>Name</span>&nbsp; <span>Lastame</span> &nbsp; &nbsp; &nbsp;<span>12/20/2016 15:15 PM</span></div>
                                                        <div>Currently Reviewings</div>
                                                        <br />
                                                    </td>
                                                </tr>
                                                <!-- end em-c-table__row -->
                                                <tr class="em-c-table__row em-c-table--condensed">
                                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">
                                                        <div><span>Name</span>&nbsp; <span>Lastame</span> &nbsp; &nbsp; &nbsp;<span>12/20/2016 15:15 PM</span></div>
                                                        <div>Request Sent for Review</div>
                                                        <br />
                                                    </td>
                                                </tr>
                                                <!-- end em-c-table__row -->
                                                <tr class="em-c-table__row em-c-table--condensed">
                                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">
                                                        <div><span>Name</span>&nbsp; <span>Lastame</span> &nbsp; &nbsp; &nbsp;<span>12/20/2016 15:15 PM</span></div>
                                                        <div>Rejected</div>
                                                        <br />
                                                    </td>
                                                </tr>
                                                <!-- end em-c-table__row -->
                                                <tr class="em-c-table__row em-c-table--condensed">
                                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">
                                                        <div><span>Name</span>&nbsp; <span>Lastame</span> &nbsp; &nbsp; &nbsp;<span>12/20/2016 15:15 PM</span></div>
                                                        <div>Returned from Review</div>
                                                        <br />
                                                    </td>
                                                </tr>
                                                <!-- end em-c-table__row -->
                                            </tbody>
                                            <!-- end em-c-table__body -->
                                        </table>
                                        <!--end em-c-table-->
                                    </div>
                                    <!--end em-c-table-object__body-inner-->
                                </div>
                                <!--end em-c-table-object__body-->
                            </div>
                            <!--end em-c-table-object-->
                        </div>
                        <div class="em-c-tabs__panel em-js-tabs-panel" id="right-tab-panel-3">
                            <h3>Team Directory</h3>
                            <hr class="custom-hr" />

                            <div class="em-c-table-object ">
                                <div class="em-c-table-object__header">
                                </div>
                                <!--end em-c-table-object__header-->
                                <div class="em-c-table-object__body">
                                    <div class="em-c-table-object__body-inner">
                                        <table class="em-c-table em-c-table--condensed">
                                            <tbody class="em-c-table__body ">
                                                <tr class="em-c-table__row em-c-table--condensed">
                                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">
                                                        <div><b>Mary Mayweather</b></div>
                                                        <div>Project Manager</div>
                                                        <br />
                                                        <div><span>Phone</span><span>872) 861-5295</span></div>
                                                        <div><span>Email</span>&nbsp;&nbsp;<span>mary.mayweather@exxonmobil.com</span></div>
                                                    </td>
                                                </tr>
                                                <!-- end em-c-table__row -->
                                                <tr class="em-c-table__row em-c-table--condensed">
                                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">
                                                        <div><b>Zachery Roberts</b></div>
                                                        <div>Project Manager</div>
                                                        <br />
                                                        <div><span>Phone</span><span>872) 861-5295</span></div>
                                                        <div><span>Email</span>&nbsp;&nbsp;<span>zachery.roberts@exxonmobil.com</span></div>
                                                    </td>
                                                </tr>
                                                <!-- end em-c-table__row -->
                                                <tr class="em-c-table__row em-c-table--condensed">
                                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">
                                                        <div><b>Diana Pender</b></div>
                                                        <div>Controller's Contact</div>
                                                        <br />
                                                        <div><span>Phone</span><span>(239) 8712-5445</span></div>
                                                        <div><span>Email</span>&nbsp;&nbsp;<span>diana.pender@exxonmobil.com</span></div>
                                                    </td>
                                                </tr>
                                                <!-- end em-c-table__row -->
                                            </tbody>
                                            <!-- end em-c-table__body -->
                                        </table>
                                        <!--end em-c-table-->
                                    </div>
                                    <!--end em-c-table-object__body-inner-->
                                </div>
                                <!--end em-c-table-object__body-->
                            </div>
                            <!--end em-c-table-object-->
                        </div>
                        <div class="em-c-tabs__panel em-js-tabs-panel" id="right-tab-panel-4">
                            <h3>Attachments</h3>
                            <hr class="custom-hr" />

                            <ul class="em-c-link-list ">
                                <li class="em-c-link-list__item">
                                    <a href=" #" class="em-c-link-list__link ">Project Meeting Minutes.doc
                                    </a>&nbsp;
       <svg class="em-c-icon em-c-icon--small">
           <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-trash"></use>
       </svg>
                                </li>
                                <!-- end em-c-link-list__item -->
                                <li class="em-c-link-list__item">
                                    <a href=" #" class="em-c-link-list__link ">Rough Estimates.pdf
                                    </a>
                                    &nbsp;
       <svg class="em-c-icon em-c-icon--small">
           <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-trash"></use>
       </svg>
                                </li>
                                <!-- end em-c-link-list__item -->
                            </ul>
                            <!-- end em-c-link-list -->
                            <div class="text-color-blue">
                                <svg class="em-c-icon em-c-icon--small">
                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-plus"></use>
                                </svg>
                                Add New Attachment
                            </div>
                        </div>
                        <div class="em-c-tabs__panel em-js-tabs-panel" id="right-tab-panel-5">
                            <h3>Resources</h3>
                            <hr class="custom-hr" />

                            <ul class="em-c-link-list ">
                                <li class="em-c-link-list__item text-size-12">
                                    <a href=" #" class="em-c-link-list__link ">BIS Training for POTLs/POMs/PMs/PAs
                                    </a>
                                </li>
                                <!-- end em-c-link-list__item -->
                            </ul>
                            <!-- end em-c-link-list -->
                        </div>
                    </div>
                </div>
                <!-- end em-c-tabs -->
            </div>
            <!-- end em-l__secondary -->
        </div>
        <!-- end em-l--two-column -->
    </div>
</asp:Content>
