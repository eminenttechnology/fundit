﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_includes/MasterPageWithPrint.master" AutoEventWireup="true" CodeBehind="AllocationInfo.aspx.cs" Inherits="Fundit.Web.Screen.AllocationInfo.AllocationInfo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="em-l-container">
    <div class="em-l-grid em-l-grid--1-2-1 ">
        <div class="em-l-grid__item">            
            <div class="em-c-field group-button-center">
                <h4 class="em-c-field__label"></h4>
                <div class="em-c-field__body">
                    <div class="em-c-toggle">
                        <input class="em-c-toggle__input em-u-is-vishidden" type="radio" id="toggle-1" name="togglename" checked />
                        <label class="em-c-toggle__label" for="toggle-1">
                            Hardware
                        </label>
                        <input class="em-c-toggle__input em-u-is-vishidden" type="radio" id="toggle-2" name="togglename" />
                        <label class="em-c-toggle__label" for="toggle-2">
                            Software
                        </label>
                        <input class="em-c-toggle__input em-u-is-vishidden" type="radio" id="toggle-3" name="togglename"  />
                        <label class="em-c-toggle__label" for="toggle-3">
                            Capitalized Staffing
                        </label>
                        <input class="em-c-toggle__input em-u-is-vishidden" type="radio" id="toggle-4" name="togglename"  />
                        <label class="em-c-toggle__label" for="toggle-4">
                            Expense
                        </label>
                    </div>
                    <!-- end em-c-toggle -->
                </div>                
            </div>
            <!-- end em-c-field -->
        </div>
        <!-- end em-l-grid__item -->

        <div class="em-l-grid__item">
            <!-- 'fpo' stands for 'for placement only'. Replace the 'fpo-block' div element with your own markup-->
            <div class="fpo-block fpo-block--dark">

                <div class="em-c-table-object ">
                    <div class="em-c-table-object__header">

                        <div class="em-u-text-align-left ">
                            <div class="em-u-font-style-light em-u-text-align-right">
                                <span class="em-u-font-style-regular" style="float: left">HARDWARE ALLOCATED RESOURCES</span>
                                <svg class="em-c-icon em-c-icon--small" style="float: left">
                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-plus"></use>
                                </svg>
                                <span class="em-u-font-style-regular ">Unallocated - Capex $340.44 Opex $355.00</span>
                            </div>
                        </div>
                        <br />



                    </div>
                    <!--end em-c-table-object__header-->
                    <div class="em-c-table-object__body em-c-table--condensed">
                        <div class="em-c-table-object__body-inner">
                            <table class="em-c-table ">
                                <thead class="em-c-table__header">
                                    <tr class="em-c-table__header-row">
                                        <th scope="col" class="em-c-table__header-cell ">STATUS
                                            <svg class="em-c-icon em-c-icon--tiny">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-lock"></use>
                                            </svg>
                                            <svg class="em-c-icon em-c-icon--tiny ">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-up"></use>
                                            </svg>
                                        </th>
                                        <th scope="col" class="em-c-table__header-cell ">QUANTITY
                                            <svg class="em-c-icon em-c-icon--tiny">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-lock"></use>
                                            </svg>
                                            <svg class="em-c-icon em-c-icon--tiny ">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down"></use>
                                            </svg>
                                        </th>
                                        <th scope="col" class="em-c-table__header-cell ">CAPEX ($KUSD)
                                            <svg class="em-c-icon em-c-icon--tiny">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-lock"></use>
                                            </svg>
                                            <svg class="em-c-icon em-c-icon--tiny ">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down"></use>
                                            </svg>
                                        </th>
                                        <th scope="col" class="em-c-table__header-cell ">AR #
                                            <svg class="em-c-icon em-c-icon--tiny">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-lock"></use>
                                            </svg>
                                            <svg class="em-c-icon em-c-icon--tiny ">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down"></use>
                                            </svg>
                                        </th>
                                        <th scope="col" class="em-c-table__header-cell ">COMPANY
                                            <svg class="em-c-icon em-c-icon--tiny">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-lock"></use>
                                            </svg>
                                            <svg class="em-c-icon em-c-icon--tiny ">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down"></use>
                                            </svg>
                                        </th>
                                        <th scope="col" class="em-c-table__header-cell ">COUNTRY
                                            <svg class="em-c-icon em-c-icon--tiny">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-lock"></use>
                                            </svg>
                                            <svg class="em-c-icon em-c-icon--tiny ">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down"></use>
                                            </svg>
                                        </th>
                                        <th scope="col" class="em-c-table__header-cell ">ORGANIZATION
                                            <svg class="em-c-icon em-c-icon--tiny">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-lock"></use>
                                            </svg>
                                            <svg class="em-c-icon em-c-icon--tiny ">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down"></use>
                                            </svg>
                                        </th>
                                        <th scope="col" class="em-c-table__header-cell ">LOCATION
                                            <svg class="em-c-icon em-c-icon--tiny">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-lock"></use>
                                            </svg>
                                            <svg class="em-c-icon em-c-icon--tiny ">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down"></use>
                                            </svg>
                                        </th>
                                        <th scope="col" class="em-c-table__header-cell ">DEPRECIATION LOCATION
                                            <svg class="em-c-icon em-c-icon--tiny">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-lock"></use>
                                            </svg>
                                            <svg class="em-c-icon em-c-icon--tiny ">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down"></use>
                                            </svg>
                                        </th>
                                        <th scope="col" class="em-c-table__header-cell ">TYPE
                                            <svg class="em-c-icon em-c-icon--tiny">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-lock"></use>
                                            </svg>
                                            <svg class="em-c-icon em-c-icon--tiny ">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down"></use>
                                            </svg>
                                        </th>
                                        <th scope="col" class="em-c-table__header-cell ">COST CENTER
                                            <svg class="em-c-icon em-c-icon--tiny">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-lock"></use>
                                            </svg>
                                            <svg class="em-c-icon em-c-icon--tiny ">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down"></use>
                                            </svg>
                                        </th>
                                        <th scope="col" class="em-c-table__header-cell ">DESCRIPTION
                                            <svg class="em-c-icon em-c-icon--tiny">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-lock"></use>
                                            </svg>
                                            <svg class="em-c-icon em-c-icon--tiny ">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down"></use>
                                            </svg>
                                        </th>
                                    </tr>
                                    <!-- em-c-table__header-row -->
                                </thead>
                                <!-- end em-c-table__header -->
                                <tbody class="em-c-table__body ">
                                    <tr class="em-c-table__row ">
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">Open
                                        </td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">8
                                        </td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">$70.00
                                        </td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">16006001
                                        </td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">1575
                                        </td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">Unites States
                                        </td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">Organization
                                        </td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">N/A
                                        </td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">N/A
                                        </td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">Type
                                        </td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">N/A
                                        </td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">Description
                                        </td>
                                    </tr>
                                    <!-- end em-c-table__row -->
                                    <tr class="em-c-table__row ">
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">Open
                                        </td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">8
                                        </td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">$70.00
                                        </td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">16006001
                                        </td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">1575
                                        </td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">Unites States
                                        </td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">Organization
                                        </td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">N/A
                                        </td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">N/A
                                        </td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">Type
                                        </td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">N/A
                                        </td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">Description
                                        </td>
                                    </tr>
                                    <!-- end em-c-table__row -->
                                    <tr class="em-c-table__row ">
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">Open
                                        </td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">8
                                        </td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">$70.00
                                        </td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">16006001
                                        </td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">1575
                                        </td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">Unites States
                                        </td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">Organization
                                        </td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">N/A
                                        </td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">N/A
                                        </td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">Type
                                        </td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">N/A
                                        </td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">Description
                                        </td>
                                    </tr>
                                    <tr class="em-c-table__row ">
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">Open
                                        </td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">8
                                        </td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">$70.00
                                        </td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">16006001
                                        </td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">1575
                                        </td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">Unites States
                                        </td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">Organization
                                        </td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">N/A
                                        </td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">N/A
                                        </td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">Type
                                        </td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">N/A
                                        </td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">Description
                                        </td>
                                    </tr>
                                    <tr class="em-c-table__row ">
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">Open
                                        </td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">8
                                        </td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">$70.00
                                        </td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">16006001
                                        </td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">1575
                                        </td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">Unites States
                                        </td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">Organization
                                        </td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">N/A
                                        </td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">N/A
                                        </td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">Type
                                        </td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">N/A
                                        </td>
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">Description
                                        </td>
                                    </tr>


                                    <!-- end em-c-table__row -->
                                </tbody>
                                <!-- end em-c-table__body -->
                                <tfoot class="em-c-table__footer ">
                                    <tr class="em-c-table__footer-row ">
                                        <td class="em-c-table__cell em-js-cell em-js-cell-editable em-u-margin-quad" colspan="20" style="background-color: #0c69b0">
                                            <div class="em-u-text-align-left">
                                                <label for="" class="em-c-field__label text-color-white">Total: &nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp; 11 &nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp; $370.00</label>
                                            </div>
                                        </td>
                                    </tr>
                                </tfoot>
                                <!-- end em-c-table__footer -->
                            </table>
                            <!--end em-c-table-->
                        </div>
                        <!--end em-c-table-object__body-inner-->
                    </div>
                    <!--end em-c-table-object__body-->
                </div>
                <!--end em-c-table-object-->
            </div>
        </div>
        <!-- end em-l-grid__item -->
    </div>
    <!-- end em-l-grid--1-2-1 -->
    </div>

</asp:Content>
