﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_includes/MasterPageWithPrint.master" AutoEventWireup="true" CodeBehind="re-allocation-details.aspx.cs" Inherits="Fundit.Web.Screen.Request.Search" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

   <div class="em-c-page-header em-c-page-header--small">
            <h1 class="em-c-page-header__title">General Info</h1>
    </div>
    <div class="em-l-container">
        
        <div class="em-l em-l--two-column">
            <div class="em-l__main">
                <div class="em-l-grid em-l-grid--2up">
                     <div class="em-l-grid__item" style="height:1000px;">
                        <ul class="em-c-definition-list">
                            <li class="em-c-definition-list__item">
                                <div class="em-c-definition-list__key">Work ID:</div>
                                <div class="em-c-definition-list__value">12342 </div>
                           </li>
                            <li class="em-c-definition-list__item">
                                <div class="em-c-definition-list__key">Work Name:</div>
                                <div class="em-c-definition-list__value"> GVS Day 1 IT Enablement</div>
                           </li>
                            <li class="em-c-definition-list__item">
                                <div class="em-c-definition-list__key">Funnding Type:</div>
                                <div class="em-c-definition-list__value"> Cumulative Advance Funding</div>
                           </li>
                            <li class="em-c-definition-list__item">
                                <div class="em-c-definition-list__key">Requestor:</div>
                                <div class="em-c-definition-list__value">Shantell D. Brown</div>
                           </li>
                        </ul>
                    </div>
                    <div class="em-l-grid__item">
                        <ul class="em-c-definition-list">
                            <li class="em-c-definition-list__item">
                                <div class="em-c-definition-list__key">Amount Requested:</div>
                                <div class="em-c-definition-list__value">$400K</div>
                           </li>
                            <li class="em-c-definition-list__item">
                                <div class="em-c-definition-list__key">Total Amount Approved:</div>
                                <div class="em-c-definition-list__value">$650K</div>
                           </li>
                            <li class="em-c-definition-list__item">
                                <div class="em-c-definition-list__key">Amount Spent:</div>
                                <div class="em-c-definition-list__value">$150K</div>
                           </li>
                            <li class="em-c-definition-list__item">
                                <div class="em-c-definition-list__key">Balance:</div>
                                <div class="em-c-definition-list__value">$500K</div>
                           </li>
                        </ul>
                    </div>
                </div>
                   
            </div>
            <div class="em-l__secondary">
                <div class="fpo-block"></div>
            </div>
        </div>
        <hr style="padding:0;margin:12.5px 0;" />
    </div>
    <!-- end em-l-container -->
    
    <div class="em-l-container">
        <h4 class="em-u-font-size-large em-u-font-style-light em-u-padding-bottom-double">Re-Allocation Requests</h4>

        <div class="em-c-collapsible-toolbar__panel em-js-toolbar-panel">
            <div class="em-c-toolbar">
                <div class="em-c-toolbar__item">PREVIOUS HARDWARE ALLOCATED RESOURCES ($K USD)</div>
                <div class="em-c-toolbar__item em-is-aligned-right">Unallocated - Capex $340.44 Opex $355.00</div>
            </div>
        </div>
        <div class="em-c-table-object ">
            <div class="em-c-table-object__header">
            </div>
            <!--end em-c-table-object__header-->
            <div class="em-c-table-object__body">
                <div class="em-c-table-object__body-inner">
                    <table class="em-c-table ">
                        <thead class="em-c-table__header">
                            <tr class="em-c-table__header-row">
                                <th scope="col" class="em-c-table__header-cell em-u-width-5">
                                      <button class="em-c-btn--bare">AR#
                                          <svg class="em-c-icon em-c-icon--tiny em-c-icon--green">                              
                                              <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../../assets/standard/unity-1.2.0/images/down.svg"></use>
                                          </svg>                                                                                  
                                      </button></th>
                                <th scope="col" class="em-c-table__header-cell ">QUANTITY</th>
                                <th scope="col" class="em-c-table__header-cell ">CAPEX</th>
                                <th scope="col" class="em-c-table__header-cell ">ACTUALS</th>
                                 <th scope="col" class="em-c-table__header-cell ">NEW CAPEX ALLOCATION</th>
                                <th scope="col" class="em-c-table__header-cell ">COMPANY</th>                               
                                <th scope="col" class="em-c-table__header-cell ">COUNTRY</th>
                                <th scope="col" class="em-c-table__header-cell ">ORGANIZATION</th>
                                <th scope="col" class="em-c-table__header-cell ">LOCATION</th>
                                <th scope="col" class="em-c-table__header-cell ">TYPE</th>
                                <th scope="col" class="em-c-table__header-cell ">COST CENTER</th>
                                <th scope="col" class="em-c-table__header-cell ">DESCRIPTION</th>
                                
                            </tr><!-- em-c-table__header-row -->
                        </thead>  <!-- end em-c-table__header -->
                        <tbody class="em-c-table__body" id="funding-reallocation-table">
                            <tr class="em-c-table__row ">

                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">12232</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">15</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">$70</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">70</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                    <div class="em-c-field__body">
                                        <input type="number" id="newAllocation1" class="em-c-input"  value="" />
                                    </div>
                                </td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">1575</td>                                
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Russian Federation</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Organization</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Location</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Type</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Cost Center</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Description</td>
                            </tr>
                            <tr class="em-c-table__row ">

                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">12232</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">15</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">$70</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">70</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                    <div class="em-c-field__body">
                                        <input type="number" id="newAllocation1" class="em-c-input"  value="" />
                                    </div>
                                </td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">1575</td>                                
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Russian Federation</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Organization</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Location</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Type</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Cost Center</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Description</td>
                            </tr>
                            <tr class="em-c-table__row ">

                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">12232</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">15</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">$70</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">70</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                    <div class="em-c-field__body">
                                        <input type="number" id="newAllocation1" class="em-c-input"  value="" />
                                    </div>
                                </td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">1575</td>                                
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Russian Federation</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Organization</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Location</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Type</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Cost Center</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Description</td>
                            </tr>
                            <tr class="em-c-table__row ">

                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">12232</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">15</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">$70</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">70</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                    <div class="em-c-field__body">
                                        <input type="number" id="newAllocation1" class="em-c-input"  value="" />
                                    </div>
                                </td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">1575</td>                                
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Russian Federation</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Organization</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Location</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Type</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Cost Center</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Description</td>
                            </tr>
                            <tr class="em-c-table__row ">

                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">12232</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">15</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">$70</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">70</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                    <div class="em-c-field__body">
                                        <input type="number" id="newAllocation1" class="em-c-input"  value="" />
                                    </div>
                                </td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">1575</td>                                
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Russian Federation</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Organization</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Location</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Type</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Cost Center</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Description</td>
                            </tr>
                            
                        </tbody>
                        <!-- end em-c-table__body -->
                        <tfoot class="em-c-table__footer">
                            <tr class="em-c-table__footer-row">
                            </tr>
                        </tfoot>
                        <!-- end em-c-table__footer -->
                    </table>
                    <!--end em-c-table-->
                </div>
                <!--end em-c-table-object__body-inner-->
            </div>
            <!--end em-c-table-object__body-->
        </div>
        <!--end em-c-table-object-->
    </div><!-- end em-l-container -->
</asp:Content>
<asp:Content runat="server" ID="content2" ContentPlaceHolderID="footer_section">
    <div class="em-l-container">
        <div class="align-right" style="width:auto;float:right;">
            <div class="em-c-btn-group em-c-btn-group--responsive">
                <button class="em-c-btn em-c-btn em-js-btn-selectable">
                    <span class="em-c-btn__text">Save Edits</span>
                </button>
                <button class="em-c-btn em-c-btn--primary">
                    <span class="em-c-btn__text">Submit for Approval</span>
                </button>
            </div>
        </div>
    </div>
</asp:Content>
