﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/_includes/MasterPageWithPrint.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="em-l-container">
        <div class="em-u-text-align-left">
            <h2>Region
            </h2>
        </div>
        <hr>

        <div  class="em-c-table-object__body ">
            <div  class="em-c-table-object__body-inner">
                <table  class="em-c-table  scroll " style="table-layout: fixed; width: 100%;">
                    <thead  class="em-c-table__header">
                        <tr  class="em-c-table__header-row ">
                            <th class="em-c-table__header-cell" scope="col" style="width: 20%;"></th>
                            <th  class="em-c-table__header-cell " style="width: 20%;" scope="col">
                                <span  class="em-u-font-size-small-2">REGION NAME</span>
                                <button  class="em-c-btn--bare">
                                    <!---->
                                    <svg  class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                        <use  xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                    </svg>

                                    <!---->
                                </button>
                            </th>
                            <th  class="em-c-table__header-cell em-u-margin-right " style="width: 20%;" scope="col">
                                <span  class="em-u-font-size-small-2">AR Service Center</span>
                                <button  class="em-c-btn--bare">
                                    <!---->
                                    <svg  class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                        <use  xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                    </svg>

                                    <!---->
                                </button>
                            </th>
                            <th  class="em-c-table__header-cell em-u-margin-right " style="width: 20%;" scope="col">
                                <span  class="em-u-font-size-small-2">IO Service Center</span>
                                <button  class="em-c-btn--bare">
                                    <!---->
                                    <svg  class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                        <use  xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                    </svg>

                                    <!---->
                                </button>
                            </th>
                            <th  class="em-c-table__header-cell em-u-margin-right" scope="col" style="width: 20%;">
                                <span  class="em-u-font-size-small-2">STATUS</span>
                                <button  class="em-c-btn--bare">
                                    <!---->
                                    <svg  class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                        <use  xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                    </svg>

                                    <!---->
                                </button>
                            </th>
                        </tr>

                    </thead>
                    <tbody  class="em-c-table__body">
                        <!---->
                        <tr  class="em-c-table__row ">
                            <td  class="em-c-table__cell"  style="width: 20%;">
                                <a href="#modal" class="em-c-modal__trigger em-js-modal-trigger">Edit</a>
                            </td>
                            <td  class="em-c-table__cell"  style="width: 20%;">US
                            </td>
                            <td  class="em-c-table__cell"  style="width: 20%;  word-wrap:break-word;">US
                            </td>
                            <td  class="em-c-table__cell"  style="width: 20%;  word-wrap:break-word;">Budapest
                            </td>
                            <td  class="em-c-table__cell"  style="width: 20%;">Active
                            </td>
                        </tr>
                        <tr  class="em-c-table__row ">
                            <td  class="em-c-table__cell"  style="width: 20%;">
                                <a href="#modal" class="em-c-modal__trigger em-js-modal-trigger">Edit</a>
                            </td>
                            <td  class="em-c-table__cell"  style="width: 20%;">AP
                            </td>
                            <td  class="em-c-table__cell"  style="width: 20%;  word-wrap:break-word;">AP
                            </td>
                            <td  class="em-c-table__cell"  style="width: 20%; word-wrap:break-word;">Budapest
                            </td>
                            <td  class="em-c-table__cell"  style="width: 20%;">Active
                            </td>
                        </tr>
                        <tr  class="em-c-table__row ">
                            <td  class="em-c-table__cell"  style="width: 20%;">
                                <a href="#modal" class="em-c-modal__trigger em-js-modal-trigger">Edit</a>
                            </td>
                            <td  class="em-c-table__cell"  style="width: 20%;">EAME
                            </td>
                            <td  class="em-c-table__cell"  style="width: 20%;  word-wrap:break-word;">EAME
                            </td>
                            <td  class="em-c-table__cell"  style="width: 20%;  word-wrap:break-word;">Budapest
                            </td>
                            <td  class="em-c-table__cell"  style="width: 20%;">Active
                            </td>
                        </tr>
                        <tr  class="em-c-table__row ">
                            <td  class="em-c-table__cell"  style="width: 20%;">
                                <a href="#modal" class="em-c-modal__trigger em-js-modal-trigger">Edit</a>
                            </td>
                            <td  class="em-c-table__cell"  style="width: 20%;">UNKNOWN
                            </td>
                            <td  class="em-c-table__cell"  style="width: 20%;  word-wrap:break-word;">UNKNOWN
                            </td>
                            <td  class="em-c-table__cell"  style="width: 20%;  word-wrap:break-word;">Budapest
                            </td>
                            <td  class="em-c-table__cell"  style="width: 20%;">Active
                            </td>
                        </tr>
                        <tr  class="em-c-table__row ">
                            <td  class="em-c-table__cell"  style="width: 20%">
                                <a href="#modal" class="em-c-modal__trigger em-js-modal-trigger">Edit</a>
                            </td>
                            <td  class="em-c-table__cell"  style="width: 20%;">CAN
                            </td>
                            <td  class="em-c-table__cell"  style="width: 20%;  word-wrap:break-word;">CAN
                            </td>
                            <td  class="em-c-table__cell"  style="width: 20%;  word-wrap:break-word;">Budapest
                            </td>
                            <td  class="em-c-table__cell"  style="width: 20%;">Active
                            </td>
                        </tr>

                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="em-c-modal em-js-modal em-is-closed" id="modal">
        <div class="em-c-modal__window em-js-modal-window">
            <div class="em-c-modal__header">
                <h3 class="em-c-modal__title">Edit</h3>
                <button class="em-c-btn em-c-btn--bare em-c-modal__close-btn em-js-modal-close-trigger">
                    <span class="em-c-btn__text">Close</span>
                </button>
                <!-- end em-c-btn -->
            </div>
            <!-- end em-c-modal__header -->
            <div class="em-c-modal__body em-c-text-passage em-c-text-passage--small">
                <div class="em-l-grid em-l-grid--2up ">
                    <div class="em-l-grid__item">
                        <div class="em-u-font-size-large fpo-block">
                            Region  :
                        </div>
                    </div>
                    <!-- end em-l-grid__item -->
                    <div class="em-l-grid__item">
                        <div class="em-u-font-size-large fpo-block">
                            US
                        </div>
                    </div>
                    <!-- end em-l-grid__item -->

               <div class="em-l-grid__item">
                        <div class="em-u-font-size-large fpo-block">
                            AR Service Center :
                        </div>
                    </div>
                    <!-- end em-l-grid__item -->
                    <div class="em-l-grid__item">
                        <select class="em-c-select em-c-select em-u-width-33 em-u-font-style-semibold" id="">
                            <option selected="" value="select">US</option>
                            <!---->
                            <option value="1">EAME
                            </option>
                            <option value="2">AS
                            </option>
                            <option value="3">AP
                            </option>
                            <option value="4">CAN
                            </option>
                            <option value="4">UNKNOWN
                            </option>
                        </select>
                    </div>
                    <!-- end em-l-grid__item -->
                    <div class="em-l-grid__item">
                        <div class="em-u-font-size-large fpo-block">
                            IO Service Center :
                        </div>
                    </div>
                    <!-- end em-l-grid__item -->
                    <div class="em-l-grid__item">
                        <select class="em-c-select em-c-select em-u-width-33 em-u-font-style-semibold" id="">
                            
                            <!---->
                            <option value="1">Budapest
                            </option>
                            <option value="2">AS
                            </option>
                            <option value="3">AP
                            </option>
                            <option value="4">CAN
                            </option>
                            <option value="4">UNKNOWN
                            </option>
                        </select>
                    </div>
                    <!-- end em-l-grid__item -->
            </div>
            <!-- end em-c-modal__body -->
            <div class="em-c-modal__footer em-c-text-passage em-c-text-passage--small">
                <button class="em-c-btn em-c-btn--primary em-js-modal-close-trigger">
                    <span class="em-c-btn__text">Submit</span>
                </button>
            </div>
            <!-- end em-c-modal__footer -->
        </div>
        <!-- end em-c-modal__window -->
    </div>
    <!-- end em-c-modal -->
</asp:Content>
