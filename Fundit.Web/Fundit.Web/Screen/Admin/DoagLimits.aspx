﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_includes/MasterPageWithPrint.master" AutoEventWireup="true" CodeBehind="DoagLimits.aspx.cs" Inherits="Fundit.Web.Screen.Admin.DoagLimits" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="em-l-container">
        <div class="em-u-text-align-left">
            <h2>DOAG LIMITS Admin Screen
            </h2>
        </div>
        <hr>
        <div class="em-c-field ">
            <div class="em-c-field__body">
                <select class="em-c-select em-c-select em-u-width-33 em-u-font-style-semibold" id="">
                    <option selected="" value="select">Select Profile</option>
                    <!---->
                    <option value="1">Full Funding Profile
                    </option>
                    <option value="2">Cumulative Advance Funding Profile
                    </option>
                    <option value="3">Supplement Funding Profile
                    </option>
                    <option value="4">Release of Previously Appropriated Funds Profile
                    </option>
                </select>
            </div>
        </div>

        <div>
            <div class="em-u-width-75">
                <div class="em-c-table-object em-u-margin-bottom-none ">
                    <div class="em-c-table-object__header">
                        <div class="em-c-toolbar ">
                            <div class="em-u-font-size-med-2 fpo-block">
                                Standard Profile
                            </div>
                            &nbsp; &nbsp; &nbsp;
                <img class="custom-add-button-width pointer" style="float: left" src="../../assets/standard/unity-1.2.0/images/icon_add_blue.png" />
                        </div>
                    </div>
                </div>
                <div class="em-c-table-object__body">
                    <div class="em-c-table-object__body-inner">
                        <table class="em-c-table ">
                            <thead class="em-c-table__header">
                                <tr class="em-c-table__header-row">
                                    <th scope="col" class="em-c-table__header-cell em-u-text-align-center">
                                        <div class="em-u-font-size-small-2 fpo-block">
                                            DOAG LEVEL
                                        </div>
                                    </th>
                                    <th scope="col" class="em-c-table__header-cell em-u-text-align-center">
                                        <div class="em-u-font-size-small-2 fpo-block">
                                            MAXIMUM APPROVAL
                                        </div>
                                    </th>
                                    <th scope="col" class="em-c-table__header-cell em-u-text-align-center">
                                        <div class="em-u-font-size-small-2 fpo-block">
                                            CONTROLLER'S ENDORSER
                                        </div>
                                    </th>
                                    <th scope="col" class="em-c-table__header-cell em-u-text-align-center"></th>
                                </tr>
                            </thead>
                            <tbody class="em-c-table__body">
                                <tr class="em-c-table__row">
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">
                                        <div class="em-u-font-size-med-2 fpo-block">
                                            DOAG 1 - Management Committee
                                        </div>
                                    </td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">
                                        <input id="" class="em-c-input  em-u-text-align-right" type="number" value="9999000" />
                                    </td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">
                                        <input id="" class="em-c-input  em-u-text-align-right" type="text" value="GSC Controller" />
                                    </td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">
                                        <img class="em-u-width-50 pointer" src="../../assets/standard/unity-1.2.0/images/btn-trashcan.svg" />
                                    </td>
                                </tr>
                                <tr class="em-c-table__row">
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">
                                        <div class="em-u-font-size-med-2 fpo-block">
                                            DOAG 1 - Contact Executive
                                        </div>
                                    </td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">
                                        <input id="" class="em-c-input  em-u-text-align-right" type="number" value="250000" />
                                    </td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">
                                        <input id="" class="em-c-input  em-u-text-align-right" type="text" value="GSC Controller" />
                                    </td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">
                                        <img class="em-u-width-50 pointer" src="../../assets/standard/unity-1.2.0/images/btn-trashcan.svg" />
                                    </td>
                                </tr>
                                <tr class="em-c-table__row">
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">
                                        <div class="em-u-font-size-med-2 fpo-block">
                                            DOAG 3 - GSC President
                                        </div>
                                    </td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">
                                        <input id="" class="em-c-input  em-u-text-align-right" type="number" value="50000" />
                                    </td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">
                                        <input id="" class="em-c-input  em-u-text-align-right" type="text" value="GSC Controller" />
                                    </td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">
                                        <img class="em-u-width-50 pointer" src="../../assets/standard/unity-1.2.0/images/btn-trashcan.svg" />
                                    </td>
                                </tr>
                                <tr class="em-c-table__row">
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">
                                        <div class="em-u-font-size-med-2 fpo-block">
                                            DOAG 4 - EMIT Vice President
                                        </div>
                                    </td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">
                                        <input id="" class="em-c-input  em-u-text-align-right" type="number" value="15000" />
                                    </td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">
                                        <input id="" class="em-c-input  em-u-text-align-right" type="text" value="EMIT BAR Manager" />
                                    </td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">
                                        <img class="em-u-width-50 pointer" src="../../assets/standard/unity-1.2.0/images/btn-trashcan.svg" />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <!--end em-c-table-->
                    </div>
                    <!--end em-c-table-object__body-inner-->
                </div>
                <!--end em-c-table-object__body-->
            </div>
        </div>

    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="footer_section" runat="server">
</asp:Content>
