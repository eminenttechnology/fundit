﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/_includes/MasterPageAdmin.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="em-l-container">
        <div class="em-c-page-header em-c-page-header--small">
        </div>
        <div class="em-c-collapsible-toolbar__panel em-js-toolbar-panel">
            <div class="em-c-toolbar">
                <div class="em-l-grid em-l-grid--5up ">
                    <div class="em-l-grid__item">
                        <div class="em-c-field em-c-field--small ">
                            <label for="" class="em-c-field__label">Cost Center</label>
                            <div class="em-c-field__body">
                                <input type="text" id="" class="em-c-input" value="" />
                            </div>
                        </div>
                    </div>
                    <div class="em-l-grid__item">
                        <div class="em-c-field em-c-field--small">
                            <label for="" class="em-c-field__label">RU </label>
                            <div class="em-c-field__body">
                                <input type="text" id="" class="em-c-input" value="" />
                            </div>
                        </div>
                    </div>

                    <div class="em-l-grid__item">
                        <div class="em-c-field em-c-field--small">
                            <label for="" class="em-c-field__label">Region </label>
                            <div class="em-c-field__body">
                                <input type="text" id="" class="em-c-input" value="" />
                            </div>
                        </div>
                    </div>



                    <div class="em-l-grid__item">
                        <div class="em-c-field ">
                            <button class="em-c-btn" style="margin-top: 17px;" onclick="display('display table')">
                                <span class="em-c-btn__text em-c-icon">Filter</span>
                            </button>
                            <!-- end em-c-btn -->
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- end em-l-container -->

        <div>
            <div class="em-c-table-object ">
                <div class="em-c-table-object__header">
                </div>
                <!--end em-c-table-object__header-->
                <div class="em-c-table-object__body">
                    <div class="em-c-table-object__body-inner">
                        <table class="em-c-table scroll" id="btn-table-filter" style="visibility: hidden">
                            <thead class="em-c-table__header">
                                <tr class="em-c-table__header-row">
                                    <th scope="col" class="em-c-table__header-cell " style="width:3%;"></th>
                                    <th scope="col" class="em-c-table__header-cell">Company Code
                                          <svg class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                              <use xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                          </svg>
                                    </th>
                                    <th scope="col" class="em-c-table__header-cell em-u-text-align-center" style="width: 8%;">Company Name
                                          <svg class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                              <use xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-up" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                          </svg>
                                    </th>
                                    <th scope="col" class="em-c-table__header-cell ">BA
                                                   <svg class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                                       <use xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-up" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                   </svg>
                                    </th>
                                    <th scope="col" class="em-c-table__header-cell ">Cost Center
                                                   <svg class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                                       <use xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-up" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                   </svg>
                                    </th>
                                    <th scope="col" class="em-c-table__header-cell ">FAD
                                                   <svg class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                                       <use xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-up" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                   </svg>
                                    </th>
                                    <th scope="col" class="em-c-table__header-cell " style="width: 8%;">Reqion
                                                   <svg class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                                       <use xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-up" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                   </svg>
                                    </th>
                                    <th scope="col" class="em-c-table__header-cell ">City
                                                   <svg class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                                       <use xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-up" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                   </svg>
                                    </th>
                                    <th scope="col" class="em-c-table__header-cell "> Jurisdiction Code
                                                   <svg class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                                       <use xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-up" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                   </svg>
                                    </th>

                                    <th scope="col" class="em-c-table__header-cell " style="width: 4%;">Last Modified By
                                                   <svg class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                                       <use xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-up" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                   </svg>
                                    </th>
                                    <th scope="col" class="em-c-table__header-cell " style="width: 10%;">Last Modified On
                                                   <svg class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                                       <use xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-up" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                   </svg>
                                    </th>
                                </tr>
                                <!-- em-c-table__header-row -->
                            </thead>
                            <!-- end em-c-table__header -->
                            <tbody class="em-c-table__body ">
                                <tr class="em-c-table__row ">

                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable ">
                                        <a href="#modal" class="em-c-modal__trigger em-js-modal-trigger">Edit</a>
                                    </td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">0291</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable" style="word-break: break-all;">ExxonMobil Petroleum & Chemical Holdings Inc. - India Branch</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">4400</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">C0802SN624</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">L333S</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable"> TX</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Dallas</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">77327</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Chizuoke Amaechi</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Tue Sept 1, 2018</td>

                                </tr>
                                <tr class="em-c-table__row ">

                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable ">

                                        <a href="#modal" class="em-c-modal__trigger em-js-modal-trigger">Edit</a>
                                    </td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">0290 </td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable" style="word-break: break-word;">Esso Exploration Inc.</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">4401</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">C0802SN624</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">L328</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">TX</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Houston</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">77327</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Jason Statham</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Mon Aug 17, 2018</td>

                                </tr>
                                <tr class="em-c-table__row ">

                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable ">
                                        <a href="#modal" class="em-c-modal__trigger em-js-modal-trigger">Edit</a>
                                    </td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">0289</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable" style="word-break: break-word;">Exxon Overseas Corporation</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">4400</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">C0802SN624</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">L481</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">TX</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Austin</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">77007</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Loren Butterscotch</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Thur Jun 1, 2018</td>

                                </tr>
                                <tr class="em-c-table__row ">

                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable ">
                                        <a href="#modal" class="em-c-modal__trigger em-js-modal-trigger">Edit</a>
                                    </td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">0288</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable" style="word-break: break-word;">Butterworth Systems (UK) Ltd</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">4400</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">C0802SN624</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">L284</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">TX</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Austin</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">77308</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Vin Diesel</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Fri Jul 17, 2017</td>

                                </tr>


                            </tbody>
                            <!-- end em-c-table__body -->
                            <tfoot class="em-c-table__footer">
                                <tr class="em-c-table__footer-row">
                                </tr>
                            </tfoot>
                            <!-- end em-c-table__footer -->
                        </table>
                        <!--end em-c-table-->
                    </div>
                    <!--end em-c-table-object__body-inner-->
                </div>
                <!--end em-c-table-object__body-->
            </div>
            <!--end em-c-table-object-->
        </div>
    </div>
    <!-- end em-l-container -->
    <div class="em-c-modal em-js-modal em-is-closed" id="modal">
        <div class="em-c-modal__window em-js-modal-window">
            <div class="em-c-modal__header">
                <h3 class="em-c-modal__title">Edit</h3>
                <button class="em-c-btn em-c-btn--bare em-c-modal__close-btn em-js-modal-close-trigger">
                    <span class="em-c-btn__text">Close</span>
                </button>
                <!-- end em-c-btn -->
            </div>
            <!-- end em-c-modal__header -->
            <div class="em-c-modal__body em-c-text-passage em-c-text-passage--small">
                <div class="em-l-grid em-l-grid--2up ">

                    <div class="em-l-grid__item" style="  width: 45%; ">
                        <div class="em-c-field__body  em-u-font-size-large  fpo-block" style="  margin-bottom: 2rem; ">
                            Company Code :
                        </div>

                        <div class="em-c-field__body em-u-font-size-large fpo-block" style="  margin-bottom: 2rem; ">
                            Profit Center :
                        </div>

                           <div class="em-c-field__body em-u-font-size-large fpo-block" style="  margin-bottom: 2rem; ">
                            Cost Center :
                        </div>  

                        <div class="em-c-field__body em-u-font-size-large fpo-block" style="  margin-bottom: 2rem; ">
                            BA :
                        </div>
                            <div class="em-c-field__body em-u-font-size-large fpo-block" style="  margin-bottom: 2rem; ">
                            FAD:
                        </div>


                           <div class="em-c-field__body em-u-font-size-large fpo-block" style="  margin-bottom: 2rem; ">
                            Region :
                        </div>

                        <div class="em-c-field__body em-u-font-size-large fpo-block" style="  margin-bottom: 2rem; ">
                            City :
                        </div>
                        <div class="em-c-field__body em-u-font-size-large fpo-block" style="  margin-bottom: 2rem; ">
                            Jurisdicition Code :
                        </div>
                    </div>
                    <!-- end em-l-grid__item -->
                    <div class="em-l-grid__item">
                        <div class="em-c-field__body" style="margin-bottom: 0.8rem;">
                            <input type="text" id="" class="em-c-input" value="0289" />
                        </div>

                        <div class="em-c-field__body"  style="margin-bottom: 0.8rem;">
                            <input type="text" id="" class="em-c-input" value="C0802SN624" />
                        </div>
                        <div class="em-c-field__body"  style="margin-bottom: 0.8rem;">
                            <input type="text" id="" class="em-c-input" value="C0802SN624" />
                        </div>
                        <div class="em-c-field__body"  style="margin-bottom: 0.8rem;">
                            <input type="text" id="" class="em-c-input" value="4400" />
                        </div>
                        <div class="em-c-field__body"  style="margin-bottom: 0.8rem;">
                            <input type="text" id="" class="em-c-input" value="L448" />
                        </div>
                        <div class="em-c-field__body"  style="margin-bottom: 0.8rem;">
                            <input type="text" id="" class="em-c-input" value="TX" />
                        </div>
                         <div class="em-c-field__body"  style="margin-bottom: 0.8rem;">
                            <input type="text" id="" class="em-c-input" value="Houston" />
                        </div>
                         <div class="em-c-field__body" style="margin-bottom: 0.8rem;">
                            <input type="text" id="" class="em-c-input" value="77004" />
                        </div>
                    </div>


                </div>
                <!-- end em-c-modal__body -->
                <div class="em-c-modal__footer em-c-text-passage em-c-text-passage--small pull-right em-u-padding-top">
                    <button class="em-c-btn em-c-btn--primary em-js-modal-close-trigger">
                        <span class="em-c-btn__text">Submit</span>
                    </button>
                </div>
                <!-- end em-c-modal__footer -->
            </div>
            <!-- end em-c-modal__window -->
        </div>
        <!-- end em-c-modal -->

        <script>
             function display(event) {
                 if (event === "display table") {
                     document.getElementById("btn-table-filter").style.visibility = 'visible';
                     document.getElementById("btn-assign-PAL").style.visibility = 'visible';
                 }

                 else {
                     document.getElementById("btn-table-filter").style.visibility = 'hidden';
                     document.getElementById("btn-assign-PAL").style.visibility = 'hidden';

                 }

             }

        </script>
</asp:Content>
