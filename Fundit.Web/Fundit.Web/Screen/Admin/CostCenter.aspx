﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/_includes/MasterPageWithPrint.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">


    <div _ngcontent-c7="" class="content costcenter-header">
        <div _ngcontent-c7="" class="em-l-container treeview ">
            <div class="em-u-text-align-left">
                <h2>Cost Center Admin Screens
                </h2>
            </div>
            <hr>
            <div _ngcontent-c7="" class="em-c-toolbar__item">
                <img _ngcontent-c7="" class="custom-add-button-width pointer" src="../../assets/standard/unity-1.2.0/images/icon_add_blue.png" style="float: left">
                <h5 _ngcontent-c7="" class="em-u-font-size-med em-u-margin-left-double">Add a Top Level Node</h5>
                    
            </div>
            <div _ngcontent-c7="" class="em-c-toolbar__item">
                </div>
            <div _ngcontent-c7="" class="em-l-grid--halves em-l-grid ">
                <div _ngcontent-c7="" class="em-c-field__body em-l-grid__item   treeview">
                    <app-tree-view _ngcontent-c7="" class="custom-form-control cost-center-body-overflow  treeview" id="treeview" _nghost-c8=""><div _ngcontent-c8="" class="">
    <tree-root _ngcontent-c8="">
    <tree-viewport>
    <!----><!---->
      <div style="height: auto;">
        
      <div class="angular-tree-component">
        <!----><tree-node-collection>
    <!----><!---->
      <div style="margin-top: 0px;">
        <!----><tree-node>
    <!----><!---->
      <!----><div class="tree-node-level-1 tree-node tree-node-collapsed">

        <!----><tree-node-drop-slot>
    <div class="node-drop-slot">
    </div>
  </tree-node-drop-slot>

        <tree-node-wrapper>
      <!----><div class="node-wrapper" style="padding-left: 0px;">
        <!---->
        <tree-node-expander>
    <!----><!---->
      <!----><span class="toggle-children-wrapper toggle-children-wrapper-collapsed">

        <span class="toggle-children"></span>
      </span>
      <!---->
    
  </tree-node-expander>
        <div class="node-content-wrapper" draggable="false">

          <tree-node-content>
  <!---->
  <!----><ul class="em-c-tree__list em-js-tree-dropdown em-is-active" id="" role="tabpanel" aria-labeledby="" aria-hidden="false" aria-selected="true">

				<li class="em-c-tree__item ">

					<a href="#" class="em-c-tree__link em-c-tree__link--has-children em-js-tree-dropdown-trigger" role="tab" aria-controls="" aria-expanded="false" aria-selected="false">
					   <span class="em-c-tree__text">EMIT Projects</span>
						<svg class="em-c-icon em-c-tree__icon">
							<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../../images/em-icons.svg#icon-caret-right"></use>
						</svg>
					</a>

					<ul class="em-c-tree__list em-js-tree-dropdown" id="" role="tabpanel" aria-labeledby="" aria-hidden="true" aria-selected="false">

						<li class="em-c-tree__item ">
							<a href="#" class="em-c-tree__link">
                                <span class="em-c-tree__text">EMIT PROJECTS</span>
                            </a>
						</li><!-- end em-c-nav__sublist__item -->
						<li class="em-c-tree__item ">
							<a href="#" class="em-c-tree__link">
                                <span class="em-c-tree__text">EMIT PROJECTS</span>
                            </a>
						</li><!-- end em-c-nav__sublist__item -->
						<li class="em-c-tree__item ">
							<a href="#" class="em-c-tree__link">
                                <span class="em-c-tree__text">EMIT PROJECTS</span>
                            </a>
						</li><!-- end em-c-nav__sublist__item -->

					</ul><!-- end em-c-nav__sublist -->

				</li><!-- end em-c-nav__sublist__item -->
				
			</ul>
  </tree-node-content>
        </div>
      </div>
      <!---->
      
    </tree-node-wrapper>

        <tree-node-children>
    <!----><!---->
      <!---->
    
  </tree-node-children>
        <tree-node-drop-slot>
    <div class="node-drop-slot">
    </div>
  </tree-node-drop-slot>
      </div>
      <!---->
      
    </tree-node><tree-node>
    <!----><!---->
      <!----><div class="tree-node-level-1 tree-node tree-node-collapsed">

        <!---->

        <tree-node-wrapper>
      <!----><div class="node-wrapper" style="padding-left: 0px;">
        <!---->
        <tree-node-expander>
    <!----><!---->
      <!----><span class="toggle-children-wrapper toggle-children-wrapper-collapsed">

        <span class="toggle-children"></span>
      </span>
      <!---->
    
  </tree-node-expander>
        <div class="node-content-wrapper" draggable="false">

          <tree-node-content>
  <!---->
  </tree-node-content>
  <!----><ul class="em-c-tree__list em-js-tree-dropdown em-is-active" id="" role="tabpanel" aria-labeledby="" aria-hidden="false" aria-selected="true">

				<li class="em-c-tree__item ">

					<a href="#" class="em-c-tree__link em-c-tree__link--has-children em-js-tree-dropdown-trigger" role="tab" aria-controls="" aria-expanded="false" aria-selected="false">
					   <span class="em-c-tree__text">EMIT Upstream Business IT</span>
						<svg class="em-c-icon em-c-tree__icon">
							<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../../images/em-icons.svg#icon-caret-right"></use>
						</svg>
					</a>

					<ul class="em-c-tree__list em-js-tree-dropdown" id="" role="tabpanel" aria-labeledby="" aria-hidden="true" aria-selected="false">

						<li class="em-c-tree__item ">
							<a href="#" class="em-c-tree__link">
                                <span class="em-c-tree__text">EMIT PROJECTS</span>
                            </a>
						</li><!-- end em-c-nav__sublist__item -->
						<li class="em-c-tree__item ">
							<a href="#" class="em-c-tree__link">
                                <span class="em-c-tree__text">EMIT PROJECTS</span>
                            </a>
						</li><!-- end em-c-nav__sublist__item -->
						<li class="em-c-tree__item ">
							<a href="#" class="em-c-tree__link">
                                <span class="em-c-tree__text">EMIT PROJECTS</span>
                            </a>
						</li><!-- end em-c-nav__sublist__item -->

					</ul><!-- end em-c-nav__sublist -->

				</li><!-- end em-c-nav__sublist__item -->
				
			</ul>
        </div>
      </div>
      <!---->
      
    </tree-node-wrapper>

        <tree-node-children>
    <!----><!---->
      <!---->
    
  </tree-node-children>
        <tree-node-drop-slot>
    <div class="node-drop-slot">
    </div>
  </tree-node-drop-slot>
      </div>
      <!---->
      
    </tree-node><tree-node>
    <!----><!---->
      <!----><div class="tree-node-level-1 tree-node tree-node-leaf">

        <!---->

        <tree-node-wrapper>
      <!----><div class="node-wrapper" style="padding-left: 0px;">
        <!---->
        <tree-node-expander>
    <!----><!---->
      <!---->
      <!----><span class="toggle-children-placeholder">
      </span>
    
  </tree-node-expander>
        <div class="node-content-wrapper" draggable="false">

          <tree-node-content>
  <!----><span>INF USCAN OPS NWF ONT (Ontario)</span>
  <!---->
  </tree-node-content>
        </div>
      </div>
      <!---->
      
    </tree-node-wrapper>

        <tree-node-children>
    <!----><!---->
      <!---->
    
  </tree-node-children>
        <tree-node-drop-slot>
    <div class="node-drop-slot">
    </div>
  </tree-node-drop-slot>
      </div>
      <!---->
      
    </tree-node><tree-node>
    <!----><!---->
      <!----><div class="tree-node-level-1 tree-node tree-node-leaf">

        <!---->

        <tree-node-wrapper>
      <!----><div class="node-wrapper" style="padding-left: 0px;">
        <!---->
        <tree-node-expander>
    <!----><!---->
      <!---->
      <!----><span class="toggle-children-placeholder">
      </span>
    
  </tree-node-expander>
        <div class="node-content-wrapper" draggable="false">

          <tree-node-content>
  <!----><span>INF USCAN OPS NWF ALB (Alberta)</span>
  <!---->
  </tree-node-content>
        </div>
      </div>
      <!---->
      
    </tree-node-wrapper>

        <tree-node-children>
    <!----><!---->
      <!---->
    
  </tree-node-children>
        <tree-node-drop-slot>
    <div class="node-drop-slot">
    </div>
  </tree-node-drop-slot>
      </div>
      <!---->
      
    </tree-node><tree-node>
    <!----><!---->
      <!----><div class="tree-node-level-1 tree-node tree-node-leaf">

        <!---->

        <tree-node-wrapper>
      <!----><div class="node-wrapper" style="padding-left: 0px;">
        <!---->
        <tree-node-expander>
    <!----><!---->
      <!---->
      <!----><span class="toggle-children-placeholder">
      </span>
    
  </tree-node-expander>
        <div class="node-content-wrapper" draggable="false">

          <tree-node-content>
  <!----><span>INF USCAN OPS NWF BC (British Columbia)</span>
  <!---->
  </tree-node-content>
        </div>
      </div>
      <!---->
      
    </tree-node-wrapper>

        <tree-node-children>
    <!----><!---->
      <!---->
    
  </tree-node-children>
        <tree-node-drop-slot>
    <div class="node-drop-slot">
    </div>
  </tree-node-drop-slot>
      </div>
      <!---->
      
    </tree-node><tree-node>
    <!----><!---->
      <!----><div class="tree-node-level-1 tree-node tree-node-leaf">

        <!---->

        <tree-node-wrapper>
      <!----><div class="node-wrapper" style="padding-left: 0px;">
        <!---->
        <tree-node-expander>
    <!----><!---->
      <!---->
      <!----><span class="toggle-children-placeholder">
      </span>
    
  </tree-node-expander>
        <div class="node-content-wrapper" draggable="false">

          <tree-node-content>
  <!----><span>INF USCAN OPS NWF MAN (Manitoba)</span>
  <!---->
  </tree-node-content>
        </div>
      </div>
      <!---->
      
    </tree-node-wrapper>

        <tree-node-children>
    <!----><!---->
      <!---->
    
  </tree-node-children>
        <tree-node-drop-slot>
    <div class="node-drop-slot">
    </div>
  </tree-node-drop-slot>
      </div>
      <!---->
      
    </tree-node><tree-node>
    <!----><!---->
      <!----><div class="tree-node-level-1 tree-node tree-node-leaf">

        <!---->

        <tree-node-wrapper>
      <!----><div class="node-wrapper" style="padding-left: 0px;">
        <!---->
        <tree-node-expander>
    <!----><!---->
      <!---->
      <!----><span class="toggle-children-placeholder">
      </span>
    
  </tree-node-expander>
        <div class="node-content-wrapper" draggable="false">

          <tree-node-content>
  <!----><span>INF USCAN OPS NWF NB (New Brunswick)</span>
  <!---->
  </tree-node-content>
        </div>
      </div>
      <!---->
      
    </tree-node-wrapper>

        <tree-node-children>
    <!----><!---->
      <!---->
    
  </tree-node-children>
        <tree-node-drop-slot>
    <div class="node-drop-slot">
    </div>
  </tree-node-drop-slot>
      </div>
      <!---->
      
    </tree-node><tree-node>
    <!----><!---->
      <!----><div class="tree-node-level-1 tree-node tree-node-leaf">

        <!---->

        <tree-node-wrapper>
      <!----><div class="node-wrapper" style="padding-left: 0px;">
        <!---->
        <tree-node-expander>
    <!----><!---->
      <!---->
      <!----><span class="toggle-children-placeholder">
      </span>
    
  </tree-node-expander>
        <div class="node-content-wrapper" draggable="false">

          <tree-node-content>
  <!----><span>INF USCAN OPS NWF NFLD (Newfoundland/Labrador)</span>
  <!---->
  </tree-node-content>
        </div>
      </div>
      <!---->
      
    </tree-node-wrapper>

        <tree-node-children>
    <!----><!---->
      <!---->
    
  </tree-node-children>
        <tree-node-drop-slot>
    <div class="node-drop-slot">
    </div>
  </tree-node-drop-slot>
      </div>
      <!---->
      
    </tree-node><tree-node>
    <!----><!---->
      <!----><div class="tree-node-level-1 tree-node tree-node-leaf">

        <!---->

        <tree-node-wrapper>
      <!----><div class="node-wrapper" style="padding-left: 0px;">
        <!---->
        <tree-node-expander>
    <!----><!---->
      <!---->
      <!----><span class="toggle-children-placeholder">
      </span>
    
  </tree-node-expander>
        <div class="node-content-wrapper" draggable="false">

          <tree-node-content>
  <!----><span>INF USCAN OPS NWF NS (Nova Scotia)</span>
  <!---->
  </tree-node-content>
        </div>
      </div>
      <!---->
      
    </tree-node-wrapper>

        <tree-node-children>
    <!----><!---->
      <!---->
    
  </tree-node-children>
        <tree-node-drop-slot>
    <div class="node-drop-slot">
    </div>
  </tree-node-drop-slot>
      </div>
      <!---->
      
    </tree-node><tree-node>
    <!----><!---->
      <!----><div class="tree-node-level-1 tree-node tree-node-leaf">

        <!---->

        <tree-node-wrapper>
      <!----><div class="node-wrapper" style="padding-left: 0px;">
        <!---->
        <tree-node-expander>
    <!----><!---->
      <!---->
      <!----><span class="toggle-children-placeholder">
      </span>
    
  </tree-node-expander>
        <div class="node-content-wrapper" draggable="false">

          <tree-node-content>
  <!----><span>INF USCAN OPS NWF NUNA (Nunavut)</span>
  <!---->
  </tree-node-content>
        </div>
      </div>
      <!---->
      
    </tree-node-wrapper>

        <tree-node-children>
    <!----><!---->
      <!---->
    
  </tree-node-children>
        <tree-node-drop-slot>
    <div class="node-drop-slot">
    </div>
  </tree-node-drop-slot>
      </div>
      <!---->
      
    </tree-node><tree-node>
    <!----><!---->
      <!----><div class="tree-node-level-1 tree-node tree-node-leaf">

        <!---->

        <tree-node-wrapper>
      <!----><div class="node-wrapper" style="padding-left: 0px;">
        <!---->
        <tree-node-expander>
    <!----><!---->
      <!---->
      <!----><span class="toggle-children-placeholder">
      </span>
    
  </tree-node-expander>
        <div class="node-content-wrapper" draggable="false">

          <tree-node-content>
  <!----><span>INF USCAN OPS NWF NWT (Northwest Territories)</span>
  <!---->
  </tree-node-content>
        </div>
      </div>
      <!---->
      
    </tree-node-wrapper>

        <tree-node-children>
    <!----><!---->
      <!---->
    
  </tree-node-children>
        <tree-node-drop-slot>
    <div class="node-drop-slot">
    </div>
  </tree-node-drop-slot>
      </div>
      <!---->
      
    </tree-node><tree-node>
    <!----><!---->
      <!----><div class="tree-node-level-1 tree-node tree-node-leaf">

        <!---->

        <tree-node-wrapper>
      <!----><div class="node-wrapper" style="padding-left: 0px;">
        <!---->
        <tree-node-expander>
    <!----><!---->
      <!---->
      <!----><span class="toggle-children-placeholder">
      </span>
    
  </tree-node-expander>
        <div class="node-content-wrapper" draggable="false">

          <tree-node-content>
  <!----><span>INF USCAN OPS NWF PEI (Prince Edward Island)</span>
  <!---->
  </tree-node-content>
        </div>
      </div>
      <!---->
      
    </tree-node-wrapper>

        <tree-node-children>
    <!----><!---->
      <!---->
    
  </tree-node-children>
        <tree-node-drop-slot>
    <div class="node-drop-slot">
    </div>
  </tree-node-drop-slot>
      </div>
      <!---->
      
    </tree-node><tree-node>
    <!----><!---->
      <!----><div class="tree-node-level-1 tree-node tree-node-leaf">

        <!---->

        <tree-node-wrapper>
      <!----><div class="node-wrapper" style="padding-left: 0px;">
        <!---->
        <tree-node-expander>
    <!----><!---->
      <!---->
      <!----><span class="toggle-children-placeholder">
      </span>
    
  </tree-node-expander>
        <div class="node-content-wrapper" draggable="false">

          <tree-node-content>
  <!----><span>INF USCAN OPS NWF QUEB (Quebec)</span>
  <!---->
  </tree-node-content>
        </div>
      </div>
      <!---->
      
    </tree-node-wrapper>

        <tree-node-children>
    <!----><!---->
      <!---->
    
  </tree-node-children>
        <tree-node-drop-slot>
    <div class="node-drop-slot">
    </div>
  </tree-node-drop-slot>
      </div>
      <!---->
      
    </tree-node><tree-node>
    <!----><!---->
      <!----><div class="tree-node-level-1 tree-node tree-node-leaf">

        <!---->

        <tree-node-wrapper>
      <!----><div class="node-wrapper" style="padding-left: 0px;">
        <!---->
        <tree-node-expander>
    <!----><!---->
      <!---->
      <!----><span class="toggle-children-placeholder">
      </span>
    
  </tree-node-expander>
        <div class="node-content-wrapper" draggable="false">

          <tree-node-content>
  <!----><span>INF USCAN OPS NWF SASK (Saskatchewan)</span>
  <!---->
  </tree-node-content>
        </div>
      </div>
      <!---->
      
    </tree-node-wrapper>

        <tree-node-children>
    <!----><!---->
      <!---->
    
  </tree-node-children>
        <tree-node-drop-slot>
    <div class="node-drop-slot">
    </div>
  </tree-node-drop-slot>
      </div>
      <!---->
      
    </tree-node><tree-node>
    <!----><!---->
      <!----><div class="tree-node-level-1 tree-node tree-node-leaf">

        <!---->

        <tree-node-wrapper>
      <!----><div class="node-wrapper" style="padding-left: 0px;">
        <!---->
        <tree-node-expander>
    <!----><!---->
      <!---->
      <!----><span class="toggle-children-placeholder">
      </span>
    
  </tree-node-expander>
        <div class="node-content-wrapper" draggable="false">

          <tree-node-content>
  <!----><span>INF USCAN OPS NWF YUK (Yukon)</span>
  <!---->
  </tree-node-content>
        </div>
      </div>
      <!---->
      
    </tree-node-wrapper>

        <tree-node-children>
    <!----><!---->
      <!---->
    
  </tree-node-children>
        <tree-node-drop-slot>
    <div class="node-drop-slot">
    </div>
  </tree-node-drop-slot>
      </div>
      <!---->
      
    </tree-node><tree-node>
    <!----><!---->
      <!----><div class="tree-node-level-1 tree-node tree-node-leaf">

        <!---->

        <tree-node-wrapper>
      <!----><div class="node-wrapper" style="padding-left: 0px;">
        <!---->
        <tree-node-expander>
    <!----><!---->
      <!---->
      <!----><span class="toggle-children-placeholder">
      </span>
    
  </tree-node-expander>
        <div class="node-content-wrapper" draggable="false">

          <tree-node-content>
  <!----><span>test</span>
  <!---->
  </tree-node-content>
        </div>
      </div>
      <!---->
      
    </tree-node-wrapper>

        <tree-node-children>
    <!----><!---->
      <!---->
    
  </tree-node-children>
        <tree-node-drop-slot>
    <div class="node-drop-slot">
    </div>
  </tree-node-drop-slot>
      </div>
      <!---->
      
    </tree-node><tree-node>
    <!----><!---->
      <!----><div class="tree-node-level-1 tree-node tree-node-leaf">

        <!---->

        <tree-node-wrapper>
      <!----><div class="node-wrapper" style="padding-left: 0px;">
        <!---->
        <tree-node-expander>
    <!----><!---->
      <!---->
      <!----><span class="toggle-children-placeholder">
      </span>
    
  </tree-node-expander>
        <div class="node-content-wrapper" draggable="false">

          <tree-node-content>
  <!----><span>Test</span>
  <!---->
  </tree-node-content>
        </div>
      </div>
      <!---->
      
    </tree-node-wrapper>

        <tree-node-children>
    <!----><!---->
      <!---->
    
  </tree-node-children>
        <tree-node-drop-slot>
    <div class="node-drop-slot">
    </div>
  </tree-node-drop-slot>
      </div>
      <!---->
      
    </tree-node><tree-node>
    <!----><!---->
      <!----><div class="tree-node-level-1 tree-node tree-node-leaf">

        <!---->

        <tree-node-wrapper>
      <!----><div class="node-wrapper" style="padding-left: 0px;">
        <!---->
        <tree-node-expander>
    <!----><!---->
      <!---->
      <!----><span class="toggle-children-placeholder">
      </span>
    
  </tree-node-expander>
        <div class="node-content-wrapper" draggable="false">

          <tree-node-content>
  <!----><span>Test 4</span>
  <!---->
  </tree-node-content>
        </div>
      </div>
      <!---->
      
    </tree-node-wrapper>

        <tree-node-children>
    <!----><!---->
      <!---->
    
  </tree-node-children>
        <tree-node-drop-slot>
    <div class="node-drop-slot">
    </div>
  </tree-node-drop-slot>
      </div>
      <!---->
      
    </tree-node>
      </div>
    
  </tree-node-collection>
        <!---->
      </div>
    
      </div>
    
  </tree-viewport>
  </tree-root> 
</div>


</app-tree-view>

                </div>

                 <div class="em-l-grid__item">
    <div class="em-c-field">
        <ul class="em-c-tree__list em-js-tree-dropdown em-is-active" id="" role="tabpanel" aria-labeledby="" aria-hidden="false" aria-selected="true">
                 <li _ngcontent-c7="" style="float:right;" class="custom-text-link pointer"></li>
                 <li _ngcontent-c7="" style="float:right;" class="custom-text-link pointer"> </li>
            </ul>

        <ul class="" id="" role="tabpanel"  aria-hidden="true" aria-selected="false">

						<li class="em-c-tree__item " style="float:right;">
							<a href="#" class="em-c-tree__link">
                                <span  class="custom-text-link pointer">Download Template </span>
                            </a>
						</li> 
						<li class="em-c-tree__item " style="float:right;">
							<a href="#" class="em-c-tree__link">
                                <span class="custom-text-link pointer">Upload Cost Center</span>
                            </a>

					</ul><!-- end em-c-nav__sublist -->
    </div>
  <!-- end em-l-grid__item -->
            </div>
        </div>
    </div>

</asp:Content>
