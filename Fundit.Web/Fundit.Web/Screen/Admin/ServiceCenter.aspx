﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/_includes/MasterPageWithPrint.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="em-l-container">
        <div class="em-u-text-align-left">
            <h2>Service Centers
            </h2>
        </div>
        <hr>
        <div  class="em-c-table-object__body ">
            <div  class="em-c-table-object__body-inner">
                <table  class="em-c-table  scroll " style="table-layout: fixed; width: 100%;">
                    <thead  class="em-c-table__header">
                        <tr  class="em-c-table__header-row ">
                            <th class="em-c-table__header-cell" scope="col" style="width: 20%;"></th>
                            <th  class="em-c-table__header-cell " style="width: 20%;" scope="col">
                                <span  class="em-u-font-size-small-2">NAME</span>
                                <button  class="em-c-btn--bare">
                                    <!---->
                                    <svg  class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                        <use  xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                    </svg>

                                    <!---->
                                </button>
                            </th>
                            <th  class="em-c-table__header-cell em-u-margin-right " style="width: 20%;" scope="col">
                                <span  class="em-u-font-size-small-2">MAILBOX</span>
                                <button  class="em-c-btn--bare">
                                    <!---->
                                    <svg  class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                        <use  xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                    </svg>

                                    <!---->
                                </button>
                            </th>
                            <th  class="em-c-table__header-cell em-u-margin-right " style="width: 20%;" scope="col">
                                <span  class="em-u-font-size-small-2">Lead</span>
                                <button  class="em-c-btn--bare">
                                    <!---->
                                    <svg  class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                        <use  xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                    </svg>

                                    <!---->
                                </button>
                            </th>
                            <th  class="em-c-table__header-cell em-u-margin-right" scope="col" style="width: 20%;">
                                <span  class="em-u-font-size-small-2"></span>
                                <button  class="em-c-btn--bare">
                                   

                                    <!---->
                                </button>
                            </th>
                        </tr>

                    </thead>
                    <tbody  class="em-c-table__body">
                        <!---->
                        <tr  class="em-c-table__row ">
                            <td  class="em-c-table__cell"  style="width: 20%;">
                                <a href="#modal" class="em-c-modal__trigger em-js-modal-trigger">Edit</a>
                            </td>
                            <td  class="em-c-table__cell"  style="width: 20%;">US
                            </td>
                            <td  class="em-c-table__cell"  style="width: 20%;  word-wrap:break-word;">fixed.assets.gsc.us@exxonmobil.com
                            </td>
                            <td  class="em-c-table__cell"  style="width: 20%;  word-wrap:break-word;">Bart Simpson
                            </td>
                            <td  class="em-c-table__cell"  style="width: 20%;"><a href="RegionalCoordinator.aspx">Manage Users</a>
                            </td>
                        </tr>
                        <tr  class="em-c-table__row ">
                            <td  class="em-c-table__cell"  style="width: 20%;">
                                <a href="#modal" class="em-c-modal__trigger em-js-modal-trigger">Edit</a>
                            </td>
                            <td  class="em-c-table__cell"  style="width: 20%;">AP
                            </td>
                            <td  class="em-c-table__cell"  style="width: 20%;  word-wrap:break-word;">AP.GSC.CAPEX.budget@exxonmobil.com
                            </td>
                            <td  class="em-c-table__cell"  style="width: 20%; word-wrap:break-word;">Homer Simpson
                            </td>
                            <td  class="em-c-table__cell"  style="width: 20%;"><a href="RegionalCoordinator.aspx">Manage Users</a>
                            </td>
                        </tr>
                        <tr  class="em-c-table__row ">
                            <td  class="em-c-table__cell"  style="width: 20%;">
                                <a href="#modal" class="em-c-modal__trigger em-js-modal-trigger">Edit</a>
                            </td>
                            <td  class="em-c-table__cell"  style="width: 20%;">EAME
                            </td>
                            <td  class="em-c-table__cell"  style="width: 20%;  word-wrap:break-word;">CFS.Capital.Budget.EAME@exxonmobil.com
                            </td>
                            <td  class="em-c-table__cell"  style="width: 20%;  word-wrap:break-word;">John Doe
                            </td>
                            <td  class="em-c-table__cell"  style="width: 20%;"><a href="RegionalCoordinator.aspx">Manage Users</a>
                            </td>
                        </tr>
                        <tr  class="em-c-table__row ">
                            <td  class="em-c-table__cell"  style="width: 20%;">
                                <a href="#modal" class="em-c-modal__trigger em-js-modal-trigger">Edit</a>
                            </td>
                            <td  class="em-c-table__cell"  style="width: 20%;">Budapest
                            </td>
                            <td  class="em-c-table__cell"  style="width: 20%;  word-wrap:break-word;">fixed.assets.gsc.us@exxonmobil.com
                            </td>
                            <td  class="em-c-table__cell"  style="width: 20%;  word-wrap:break-word;">Jane Doe
                            </td>
                            <td  class="em-c-table__cell"  style="width: 20%;"><a href="RegionalCoordinator.aspx">Manage Users</a>
                            </td>
                        </tr>
                        <tr  class="em-c-table__row ">
                            <td  class="em-c-table__cell"  style="width: 20%">
                                <a href="#modal" class="em-c-modal__trigger em-js-modal-trigger">Edit</a>
                            </td>
                            <td  class="em-c-table__cell"  style="width: 20%;">CAN
                            </td>
                            <td  class="em-c-table__cell"  style="width: 20%;  word-wrap:break-word;">GSC-BA-FO-AMERICAS-CAPEX@exxonmobil.com
                            </td>
                            <td  class="em-c-table__cell"  style="width: 20%;  word-wrap:break-word;">Jill Doe
                            </td>
                            <td  class="em-c-table__cell"  style="width: 20%;"><a href="RegionalCoordinator.aspx">Manage Users</a>
                            </td>
                        </tr>

                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="em-c-modal em-js-modal em-is-closed" id="modal">
        <div class="em-c-modal__window em-js-modal-window">
            <div class="em-c-modal__header">
                <h3 class="em-c-modal__title">Edit</h3>
                <button class="em-c-btn em-c-btn--bare em-c-modal__close-btn em-js-modal-close-trigger">
                    <span class="em-c-btn__text">Close</span>
                </button>
                <!-- end em-c-btn -->
            </div>
            <!-- end em-c-modal__header -->
            <div class="em-c-modal__body em-c-text-passage em-c-text-passage--small">
                <div class="em-l-grid em-l-grid--2up ">
                    <div class="em-l-grid__item">
                        <div class="em-u-font-size-large fpo-block">
                            Region  :
                        </div>
                    </div>
                    <!-- end em-l-grid__item -->
                    <div class="em-l-grid__item">
                        <div class="em-u-font-size-large fpo-block">
                            US
                        </div>
                    </div>
                    <!-- end em-l-grid__item -->
                </div>
                <!-- end em-l-grid -->
                <div class="em-l-grid em-l-grid--2up ">
                    <div class="em-l-grid__item">
                        <div class="em-u-font-size-large fpo-block">
                            Functional Mailbox :
                        </div>
                    </div>
                    <!-- end em-l-grid__item -->
                    <div class="em-l-grid__item">
                        <input id="" class="em-c-input  em-u-text-align-right" type="text" value="fixed.assets.gsc.us@exxonmobil.com" />
                    </div>
                    <!-- end em-l-grid__item -->
                </div>
                <!-- end em-l-grid -->
            </div>
            <!-- end em-c-modal__body -->
            <div class="em-c-modal__footer em-c-text-passage em-c-text-passage--small">
                <button class="em-c-btn em-c-btn--primary em-js-modal-close-trigger">
                    <span class="em-c-btn__text">Submit</span>
                </button>
            </div>
            <!-- end em-c-modal__footer -->
        </div>
        <!-- end em-c-modal__window -->
    </div>
    <!-- end em-c-modal -->
</asp:Content>
