﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/_includes/MasterPageWithPrint.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <div class="em-l-container">
        <div class="em-u-text-align-left">
            <h2>Country
            </h2>
        </div>
        <hr>

        <div class="em-c-collapsible-toolbar__panel em-js-toolbar-panel">
            <div class="em-c-toolbar">
                <div class="em-l-grid em-l-grid--3up">

                    <div class="em-l-grid__item">
                        <div class="em-c-field em-c-field--small">
                            <label for="" class="em-c-field__label">Country</label>
                            <div class="em-c-field__body">
                                <input type="text" id="" class="em-c-input" value="" />
                            </div>
                        </div>
                    </div>

                   <div class="em-l-grid__item">
                        <div class="em-c-field em-c-field--small">
                            <label for="" class="em-c-field__label">Region</label>
                            <div class="em-c-field__body">
                                <select _ngcontent-c18="" class="em-c-select em-custom-select-width em-u-margin-right-double ng-untouched ng-pristine ng-valid">
                                        <option _ngcontent-c18="" value="requestId">All</option>
                                        <option _ngcontent-c18="" value="workId">US</option>
                                        <option _ngcontent-c18="" value="workName">Canada</option>
                                        <option _ngcontent-c18="" value="statusName">Eame</option>
                                        
                                    </select>
                            </div>
                        </div>
                    </div>

              

                     
                    <div class="em-l-grid__item">
                        <div class="em-c-field ">
                            <button class="em-c-btn" style="margin-top: 17px;">
                                <span class="em-c-btn__text em-c-icon">Search</span>
                            </button>
                            <!-- end em-c-btn -->
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div _ngcontent-c9="" class="em-c-table-object__body ">
            <div _ngcontent-c9="" class="em-c-table-object__body-inner">
                <table _ngcontent-c9="" class="em-c-table  scroll " style="table-layout: fixed; width: 100%;">
                    <thead _ngcontent-c9="" class="em-c-table__header">
                        <tr _ngcontent-c9="" class="em-c-table__header-row ">
                            <th _ngcontent-c9="" class="em-c-table__header-cell em-u-margin-right " style="width: 33%;" scope="col">
                                <span _ngcontent-c9="" class="em-u-font-size-small-2">COUNTRY</span>
                                <button _ngcontent-c9="" class="em-c-btn--bare">
                                    <!---->
                                    <svg _ngcontent-c9="" class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                        <use _ngcontent-c9="" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                    </svg>

                                    <!---->
                                </button>
                            </th>
                            <th _ngcontent-c9="" class="em-c-table__header-cell " style="width: 33%;" scope="col">
                                <span _ngcontent-c9="" class="em-u-font-size-small-2">Region </span>
                                <button _ngcontent-c9="" class="em-c-btn--bare">
                                    <!---->
                                    <svg _ngcontent-c9="" class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                        <use _ngcontent-c9="" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                    </svg>

                                    <!---->
                                </button>
                            </th>
                            
                            <th _ngcontent-c9="" class="em-c-table__header-cell em-u-margin-right" scope="col" style="width: 33%;">
                                <span _ngcontent-c9="" class="em-u-font-size-small-2">STATUS</span>
                                <button _ngcontent-c9="" class="em-c-btn--bare">
                                    <!---->
                                    <svg _ngcontent-c9="" class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                        <use _ngcontent-c9="" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                    </svg>

                                    <!---->
                                </button>
                            </th>
                            <th _ngcontent-c9="" class="em-c-table__header-cell em-u-margin-right " style="width: 33%;" scope="col">
                                <span _ngcontent-c9="" class="em-u-font-size-small-2"></span>
                            </th>
                        </tr>

                    </thead>
                    <tbody _ngcontent-c9="" class="em-c-table__body">
                        <!---->
                        <tr _ngcontent-c9="" class="em-c-table__row ">
                            <td _ngcontent-c9="" class="em-c-table__cell" colspan="" style="width: 20%;">USA
                            </td>
                            <td _ngcontent-c9="" class="em-c-table__cell" colspan="" style="width: 20%;">US
                            </td>
                            
                            <td _ngcontent-c9="" class="em-c-table__cell" colspan="" style="width: 20%;">Active
                            </td>
                            <td _ngcontent-c9="" class="em-c-table__cell" colspan="" style="width: 20%;">
                                <a href="#modal" class="em-c-modal__trigger em-js-modal-trigger">Edit</a>
                            </td>
                        </tr>
                        <tr _ngcontent-c9="" class="em-c-table__row ">

                            <td _ngcontent-c9="" class="em-c-table__cell" colspan="" style="width: 20%;">Croatia
                            </td>
                            <td _ngcontent-c9="" class="em-c-table__cell" colspan="" style="width: 20%;">UNKNOWN
                            </td>
                            <td _ngcontent-c9="" class="em-c-table__cell" colspan="" style="width: 20%;">Active
                            </td>
                            <td _ngcontent-c9="" class="em-c-table__cell" colspan="" style="width: 20%;">
                                <a href="#modal" class="em-c-modal__trigger em-js-modal-trigger">Edit</a>
                            </td>
                        </tr>
                        <tr _ngcontent-c9="" class="em-c-table__row ">

                            <td _ngcontent-c9="" class="em-c-table__cell" colspan="" style="width: 20%;">Belgium
                            </td>
                            <td _ngcontent-c9="" class="em-c-table__cell" colspan="" style="width: 20%;">EAME
                            </td>
                          
                            <td _ngcontent-c9="" class="em-c-table__cell" colspan="" style="width: 20%;">Active
                            </td>
                            <td _ngcontent-c9="" class="em-c-table__cell" colspan="" style="width: 20%;">
                                <a href="#modal" class="em-c-modal__trigger em-js-modal-trigger">Edit</a>
                            </td>
                        </tr>

                        <tr _ngcontent-c9="" class="em-c-table__row ">

                            <td _ngcontent-c9="" class="em-c-table__cell" colspan="" style="width: 20%;">Brazil
                            </td>
                            <td _ngcontent-c9="" class="em-c-table__cell" colspan="" style="width: 20%;">AS
                            </td>
                            <td _ngcontent-c9="" class="em-c-table__cell" colspan="" style="width: 20%;">Active
                            </td>
                            <td _ngcontent-c9="" class="em-c-table__cell" colspan="" style="width: 20%;">
                                <a href="#modal" class="em-c-modal__trigger em-js-modal-trigger">Edit</a>
                            </td>
                        </tr>
                        <tr _ngcontent-c9="" class="em-c-table__row ">

                            <td _ngcontent-c9="" class="em-c-table__cell" colspan="" style="width: 20%;">France
                            </td>
                            <td _ngcontent-c9="" class="em-c-table__cell" colspan="" style="width: 20%;">EAME
                            </td>
                            <td _ngcontent-c9="" class="em-c-table__cell" colspan="" style="width: 20%;">EAME
                            </td>
                            <td _ngcontent-c9="" class="em-c-table__cell" colspan="" style="width: 20%;">
                                <a href="#modal" class="em-c-modal__trigger em-js-modal-trigger">Edit</a>
                            </td>
                        </tr>
                        <tr _ngcontent-c9="" class="em-c-table__row ">

                            <td _ngcontent-c9="" class="em-c-table__cell" colspan="" style="width: 20%;">Nigeria
                            </td>
                            <td _ngcontent-c9="" class="em-c-table__cell" colspan="" style="width: 20%;">EAME
                            </td>
                             <td _ngcontent-c9="" class="em-c-table__cell" colspan="" style="width: 20%;">EAME
                            </td>
                            <td _ngcontent-c9="" class="em-c-table__cell" colspan="" style="width: 20%;">
                                <a href="#modal" class="em-c-modal__trigger em-js-modal-trigger">Edit</a>
                            </td>
                        </tr>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="em-c-modal em-js-modal em-is-closed" id="modal">
        <div class="em-c-modal__window em-js-modal-window">
            <div class="em-c-modal__header">
                <h3 class="em-c-modal__title">Edit</h3>
                <button class="em-c-btn em-c-btn--bare em-c-modal__close-btn em-js-modal-close-trigger">
                    <span class="em-c-btn__text">Close</span>
                </button>
                <!-- end em-c-btn -->
            </div>
            <!-- end em-c-modal__header -->
            <div class="em-c-modal__body em-c-text-passage em-c-text-passage--small">
                <div class="em-l-grid em-l-grid--2up ">
                    <div class="em-l-grid__item">
                        <div class="em-u-font-size-large fpo-block">
                            Country  :
                        </div>
                    </div>
                    <!-- end em-l-grid__item -->
                    <div class="em-l-grid__item">
                        <div class="em-u-font-size-large fpo-block">
                            USA
                        </div>
                    </div>
                    <!-- end em-l-grid__item -->
                </div>
                <!-- end em-l-grid -->
                <div class="em-l-grid em-l-grid--2up ">
                    <div class="em-l-grid__item">
                        <div class="em-u-font-size-large fpo-block">
                            Region :
                        </div>
                    </div>
                    <!-- end em-l-grid__item -->
                    <div class="em-l-grid__item">
                        <select class="em-c-select em-c-select em-u-width-33 em-u-font-style-semibold" id="">
                            <option selected="" value="select">US</option>
                            <!---->
                            <option value="1">EAME
                            </option>
                            <option value="2">AS
                            </option>
                            <option value="3">AP
                            </option>
                            <option value="4">CAN
                            </option>
                            <option value="4">UNKNOWN
                            </option>
                        </select>
                    </div>
                    <div class="em-l-grid__item">
                        <div class="em-u-font-size-large fpo-block">
                            AR Service Center :
                        </div>
                    </div>
                    <!-- end em-l-grid__item -->
                    <div class="em-l-grid__item">
                        <select class="em-c-select em-c-select em-u-width-33 em-u-font-style-semibold" id="">
                            <option selected="" value="select">US</option>
                            <!---->
                            <option value="1">EAME
                            </option>
                            <option value="2">AS
                            </option>
                            <option value="3">AP
                            </option>
                            <option value="4">CAN
                            </option>
                            <option value="4">UNKNOWN
                            </option>
                        </select>
                    </div>
                    <!-- end em-l-grid__item -->
                    <div class="em-l-grid__item">
                        <div class="em-u-font-size-large fpo-block">
                            IO Service Center :
                        </div>
                    </div>
                    <!-- end em-l-grid__item -->
                    <div class="em-l-grid__item">
                        <select class="em-c-select em-c-select em-u-width-33 em-u-font-style-semibold" id="">
                            <option selected="" value="select">US</option>
                            <!---->
                            <option value="1">EAME
                            </option>
                            <option value="2">AS
                            </option>
                            <option value="3">AP
                            </option>
                            <option value="4">CAN
                            </option>
                            <option value="4">UNKNOWN
                            </option>
                        </select>
                    </div>
                    <!-- end em-l-grid__item -->
                </div>
                <!-- end em-l-grid -->
            </div>
            <!-- end em-c-modal__body -->
            <div class="em-c-modal__footer em-c-text-passage em-c-text-passage--small">
                <button class="em-c-btn em-c-btn--primary em-js-modal-close-trigger">
                    <span class="em-c-btn__text">Submit</span>
                </button>
            </div>
            <!-- end em-c-modal__footer -->
        </div>
        <!-- end em-c-modal__window -->
    </div>
    <!-- end em-c-modal -->
</asp:Content>
