﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_includes/MasterPageWithPrint.master" AutoEventWireup="true" CodeBehind="Users.aspx.cs" Inherits="Fundit.Web.Screen.Admin.Users" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="em-l-container">
        <div class="em-u-text-align-left">
            <h2>Users Search Criteria
            </h2>
        </div>
        <hr>
        <div class="em-c-collapsible-toolbar__panel em-js-toolbar-panel">
            <div class="em-c-toolbar">
                <div class="em-l-grid em-l-grid--3up">

                    <div class="em-l-grid__item">
                        <div class="em-c-field em-c-field--small">
                            <label for="" class="em-c-field__label">Network ID</label>
                            <div class="em-c-field__body">
                                <input type="text" id="" class="em-c-input" value="" />
                            </div>
                        </div>
                    </div>

                    <div class="em-l-grid__item">
                        <div class="em-c-field em-c-field--small">
                            <label for="" class="em-c-field__label">Name</label>
                            <div class="em-c-field__body">
                                <input type="text" id="" class="em-c-input" value="" />
                            </div>
                        </div>
                    </div>
                    <div class="em-l-grid__item">
                        <div class="em-c-field ">
                            <button class="em-c-btn" style="margin-top: 17px;">
                                <span class="em-c-btn__text em-c-icon">Search</span>
                            </button>
                            <!-- end em-c-btn -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end em-l-container -->

        <div>
            <div class="em-c-table-object ">
                <div class="em-c-table-object__header">
                </div>
                <!--end em-c-table-object__header-->
                <div class="em-c-table-object__body">
                    <div class="em-c-table-object__body-inner">
                        <table class="em-c-table ">
                            <thead class="em-c-table__header">
                                <tr class="em-c-table__header-row">
                                    <th scope="col" class="em-c-table__header-cell ">Network ID
                                <button class="em-c-btn--bare">
                                    <!---->
                                    <svg class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                        <use xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                    </svg>

                                    <!---->
                                </button>
                                    </th>
                                    <th scope="col" class="em-c-table__header-cell ">Name
                                <button class="em-c-btn--bare">
                                    <!---->
                                    <svg class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                        <use xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                    </svg>

                                    <!---->
                                </button>
                                    </th>
                                    <th scope="col" class="em-c-table__header-cell ">Role(s)
                                                    <button class="em-c-btn--bare">
                                                        <!---->
                                                        <svg class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                                            <use xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                        </svg>

                                                        <!---->
                                                    </button>
                                    </th>
                                    <th scope="col" class="em-c-table__header-cell ">Status
                                                                        <button class="em-c-btn--bare">
                                                                            <!---->
                                                                            <svg class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                                                                <use xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                                            </svg>

                                                                            <!---->
                                                                        </button>
                                    </th>
                                </tr>
                                <!-- em-c-table__header-row -->
                            </thead>
                            <!-- end em-c-table__header -->
                            <tbody class="em-c-table__body ">
                                <tr class="em-c-table__row ">

                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">ICLEVER</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Lever, Ian C </td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Requester</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Active</td>

                                </tr>
                                <tr class="em-c-table__row ">

                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">LKFLOYD</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Floyd, Karen L</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">System Admin, Capital Advisor, Capital Coordinator, Controller, Requester</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Active</td>

                                </tr>
                                <tr class="em-c-table__row ">

                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">THICKMA</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Hickman, Theo</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Controller, Requester</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Active</td>

                                </tr>
                                <tr class="em-c-table__row ">

                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">KDHARP1</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Harper, Daniel</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Capital Advisor, Capital Coordinator, Controller, Requester</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Active</td>

                                </tr>
                                <tr class="em-c-table__row ">

                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">PLANTOS</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Lantos, Peter</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Requester</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">De-Activated</td>

                                </tr>

                            </tbody>
                            <!-- end em-c-table__body -->
                            <tfoot class="em-c-table__footer">
                                <tr class="em-c-table__footer-row">
                                </tr>
                            </tfoot>
                            <!-- end em-c-table__footer -->
                        </table>
                        <!--end em-c-table-->
                    </div>
                    <!--end em-c-table-object__body-inner-->
                </div>
                <!--end em-c-table-object__body-->
            </div>
            <!--end em-c-table-object-->
        </div>
    </div>
    <!-- end em-l-container -->
</asp:Content>
