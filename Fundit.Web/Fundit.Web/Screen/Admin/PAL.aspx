﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/_includes/MasterPageAdmin.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="em-l-container">
        <div class="em-c-page-header em-c-page-header--small">
          
        </div>
        <div class="em-c-collapsible-toolbar__panel em-js-toolbar-panel">
            <div class="em-c-toolbar">
                <div class="em-l-grid em-l-grid--5up ">
                    <div class="em-l-grid__item">
                        <div class="em-c-field em-c-field--small ">
                            <label for="" class="em-c-field__label">Work Id</label>
                            <div class="em-c-field__body">
                                <input type="text" id="" class="em-c-input" value="" />
                            </div>
                        </div>
                    </div>
                    <div class="em-l-grid__item">
                        <div class="em-c-field em-c-field--small">
                            <label for="" class="em-c-field__label">Work Type</label>
                               <select  class="em-c-select em-c-select em-u-width-100 em-u-font-style-semibold em-u-font-size-med control-text">
                                  <option value="-500" selected>-- Select Work Type --</option>
                                   <option >Unknown </option>
                                   <option >PlaceHolder </option>
                                   <option >Project </option>
                                   <option >SWI </option>
                                   <option >Program </option>
                                   <option >Member Project </option>

                               </select>
                        </div>
                    </div>

                    <div class="em-l-grid__item">
                        <div class="em-c-field em-c-field--small">
                            <label for="" class="em-c-field__label">Project Manager</label>
                            <div class="em-c-field__body">
                                <input type="text" id="" class="em-c-input" value="" />
                            </div>
                        </div>
                    </div>

                    <div class="em-l-grid__item">
                        <div class="em-c-field em-c-field--small">
                            <label for="" class="em-c-field__label">POTL </label>
                            <div class="em-c-field__body">
                                <input type="text" id="" class="em-c-input" value="" />
                            </div>
                        </div>
                    </div>
                    <div class="em-l-grid__item">
                        <div class="em-c-field em-c-field--small">
                            <label for="" class="em-c-field__label">Controller </label>
                              <select  class="em-c-select em-c-select em-u-width-100 em-u-font-style-semibold em-u-font-size-med control-text">
                                  <option value="-500" selected>-- Select Controller --</option>
                                   <option >Jason Stataham </option>
                                   <option >Vin Diesel </option>
                                   <option >Mike Phelps </option>
                                   <option >Lewis Hamilton </option>
                                   <option >John Doe </option>
                                   <option >Vanessa Holmes </option>

                               </select>
                        </div>
                    </div>
                    <div class="em-l-grid__item">
                        <div class="em-c-field em-c-field--small">
                            <label for="" class="em-c-field__label">Funding Division </label>
                            <select class="em-c-select em-c-select em-u-width-100 em-u-font-style-semibold em-u-font-size-med control-text">
                                <option  value="-500">-- Select Funding Division --</option>
                                <!---->
                                <option  value="7">Internal
                                </option>
                                <option  value="8">Chemical
                                </option>
                                <option  value="9">Service
                                </option>
                                <option  value="10">IOL
                                </option>
                                <option  value="11">Downstream
                                </option>
                                <option value="12">Upstream
                                </option>
                                <option value="13">Corporate
                                </option>
                                <option value="14">General Interest
                                </option>

                            </select>
                        </div>
                    </div>
                    <div class="em-l-grid__item">
                        <div class="em-c-field em-c-field--small">
                            <label for="" class="em-c-field__label">Funding Division Function </label>
                            <select class="em-c-select em-c-select em-u-width-100 em-u-font-style-semibold em-u-font-size-med control-text">
                                <option value="-500">-- Select Funding Division Function --</option>
                                <!---->
                                <option  value="63">Chemical Business Services
                                </option>
                                <option value="110">Chemical Cross-BP
                                </option>
                                <option value="114">Chemical Manufacturing
                                </option>
                                <option value="150">Chemical Films
                                </option>
                                <option value="188">Chemical Technology
                                </option>
                                <option value="191">Chemical Marketing
                                </option>
                            </select>
                        </div>
                    </div>
                     <div class="em-l-grid__item">
                        <div class="em-c-field em-c-field--small">
                            <label for="" class="em-c-field__label">Project Size </label>
                             <select class="em-c-select em-c-select em-u-width-100 em-u-font-style-semibold em-u-font-size-med control-text">
                                <option value="-500">-- Select Project Size --</option>
                                <!---->
                                <option  value="63">0- 1000
                                </option>
                                <option value="110">1000-5000
                                </option>
                                <option value="114">5000-100000
                                </option>
                            </select>
                        </div>
                    </div>
                           <div class="em-l-grid__item">
                        <div class="em-c-field em-c-field--small">
                            <label for="" class="em-c-field__label">Work Status </label>
                             <select class="em-c-select em-c-select em-u-width-100 em-u-font-style-semibold em-u-font-size-med control-text">
                                <option value="-500">-- Select Work Status --</option>
                                <!---->
                                <option  value="63">Active
                                </option>
                                <option value="110">Inactive
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="em-l-grid__item">
                        <div class="em-c-field ">
                            <button class="em-c-btn" style="margin-top: 17px;"  onclick="display('display table')">
                                <span class="em-c-btn__text em-c-icon">Filter</span>
                            </button>
                            <!-- end em-c-btn -->
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- end em-l-container -->

        <div>
            <div class="em-c-table-object ">
                <div class="em-c-table-object__header">
                </div>
                <!--end em-c-table-object__header-->
                <div class="em-c-table-object__body">
                    <div class="em-c-table-object__body-inner">
                        <table class="em-c-table scroll" id="btn-table-filter" style="visibility: hidden" >
                            <thead class="em-c-table__header">
                                <tr class="em-c-table__header-row">
                                    <th scope="col" class="em-c-table__header-cell "  style="width: 12%;">
                        <input  type="checkbox" name="checkbox-1" value="" class="em-c-input-group__control" onchange="checkAll(this)"></th>
                                    <th scope="col" class="em-c-table__header-cell em-u-width-5">Work Id
                                          <svg class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                              <use xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                          </svg>
                                    </th>
                                    <th scope="col" class="em-c-table__header-cell em-u-text-align-center" style="width: 12%; padding-left:2rem;">Work Name
                                          <svg class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                              <use xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-up" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                          </svg>
                                    </th>
                                    <th scope="col" class="em-c-table__header-cell ">Work Type
                                                   <svg class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                                       <use xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-up" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                   </svg>
                                    </th>
                                    <th scope="col" class="em-c-table__header-cell ">POTL
                                                   <svg class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                                       <use xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-up" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                   </svg>
                                    </th>
                                    <th scope="col" class="em-c-table__header-cell ">Controller
                                                   <svg class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                                       <use xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-up" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                   </svg>
                                    </th>
                                    <th scope="col" class="em-c-table__header-cell ">Project Manager
                                                   <svg class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                                       <use xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-up" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                   </svg>
                                    </th>
                                    <th scope="col" class="em-c-table__header-cell ">Funding Division
                                                   <svg class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                                       <use xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-up" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                   </svg>
                                    </th>
                                    <th scope="col" class="em-c-table__header-cell ">Funding Division Function
                                                   <svg class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                                       <use xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-up" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                   </svg>
                                    </th>
                                    <th scope="col" class="em-c-table__header-cell ">Project Size
                                                   <svg class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                                       <use xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-up" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                   </svg>
                                    </th>
                                    <th scope="col" class="em-c-table__header-cell ">Work Status
                                                   <svg class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                                       <use xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-up" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                   </svg>
                                    </th>
                                    <th scope="col" class="em-c-table__header-cell ">Last Modified By
                                                   <svg class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                                       <use xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-up" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                   </svg>
                                    </th>
                                    <th scope="col" class="em-c-table__header-cell ">Last Modified On
                                                   <svg class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                                       <use xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-up" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                   </svg>
                                    </th>
                                </tr>
                                <!-- em-c-table__header-row -->
                            </thead>
                            <!-- end em-c-table__header -->
                            <tbody class="em-c-table__body ">
                                <tr class="em-c-table__row ">
                                    
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable ">
                        <input  type="checkbox" name="checkbox-1" value="" class="em-c-input-group__control">
                                           <a href="#modal" class="em-c-modal__trigger em-js-modal-trigger">Edit</a>
                                    </td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">66273</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable" style="word-break:break-all;">Transactional Data Transparency (TDT) Project2-ROW</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Member Project</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Jane Doe</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Kebir Hrašovec</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Jasmine Milne</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Internal</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Chemical Business Services</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">5000-10000</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Active</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Chizuoke Amaechi</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Tue Sept 1, 2018</td>

                                </tr>
                                <tr class="em-c-table__row " >

                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable "  >

                                        <input type="checkbox" name="checkbox-1" value="" class="em-c-input-group__control">
                                           <a href="#modal" class="em-c-modal__trigger em-js-modal-trigger">Edit</a>
                                    </td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">72324 </td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable" style="word-break:break-word;">2018 EMBSI Contracts & Agreements DB Application</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Program</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Sherlock Holmes</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">João, Sousa Silva</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Muslim, Batukayev</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Chemical</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Chemical Cross-BP</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">0-1000</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Active</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Ole Solberg</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Mon Aug 17, 2018</td>

                                </tr>
                                <tr class="em-c-table__row " >

                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable "  >

                                        <input type="checkbox" name="checkbox-1" value="" class="em-c-input-group__control">
                                           <a href="#modal" class="em-c-modal__trigger em-js-modal-trigger">Edit</a>
                                    </td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">73122</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable" style="word-break:break-word;">BPT1 tactical mobil App for NDG</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Project</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Vin Diesel</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Courtright, Anne O</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Doe, John</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Service</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Chemical Manufacturing</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">0-5000</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Inactive</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Elise Røkke</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Thur Jun 1, 2018</td>

                                </tr>
                                <tr class="em-c-table__row " >

                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable "  >
                                        <input type="checkbox" name="checkbox-1" value="" class="em-c-input-group__control">
                                           <a href="#modal" class="em-c-modal__trigger em-js-modal-trigger">Edit</a>
                                    </td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">72788</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable" style="word-break:break-word;">Sitecore Advanced Capabilities</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">SWI</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Florence Abata</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Ellis, Chante M</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Chen, Vivian L</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Downstream</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Chemical Technology</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">5000-10000</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Active</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Annalise Walker</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Fri Jul 17, 2017</td>

                                </tr>
                                <tr class="em-c-table__row " >

                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable "  >
                                        <input type="checkbox" name="checkbox-1" value="" class="em-c-input-group__control">
                                           <a href="#modal" class="em-c-modal__trigger em-js-modal-trigger">Edit</a>
                                    </td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">35582 </td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable" style="word-break:break-word;">Satellites Field Development (SFD1)</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">PlaceHolder</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Jason Statham</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Anne Fewell</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">James Johnson</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Internal</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Chemical Films</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">0-1000</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Inactive</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Dylan Robertson</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Wed Jul 1, 2017</td>

                                </tr>

                            </tbody>
                            <!-- end em-c-table__body -->
                            <tfoot class="em-c-table__footer">
                                <tr class="em-c-table__footer-row">
                                </tr>
                            </tfoot>
                            <!-- end em-c-table__footer -->
                        </table>
                        <!--end em-c-table-->
                    </div>
                    <!--end em-c-table-object__body-inner-->
                </div>
                <!--end em-c-table-object__body-->
            </div>
            <!--end em-c-table-object-->
        </div>
    </div>
    <!-- end em-l-container -->
     <div class="em-c-modal em-js-modal em-is-closed" id="modal">
        <div class="em-c-modal__window em-js-modal-window">
            <div class="em-c-modal__header">
                <h3 class="em-c-modal__title">Assign A Controller</h3>
                <button class="em-c-btn em-c-btn--bare em-c-modal__close-btn em-js-modal-close-trigger">
                    <span class="em-c-btn__text">Close</span>
                </button>
                <!-- end em-c-btn -->
            </div>
            <!-- end em-c-modal__header -->
            <div class="em-c-modal__body em-c-text-passage em-c-text-passage--small">
                <div class="em-l-grid em-l-grid--2up ">
                   
               <div class="em-l-grid__item">
                        <div class="em-u-font-size-large fpo-block">
                           Controller :
                        </div>
                    </div>
                    <!-- end em-l-grid__item -->
                    <div class="em-l-grid__item">
                      <select  class="em-c-select em-c-select em-u-width-100 em-u-font-style-semibold em-u-font-size-med control-text">
                                  <option value="-500" selected>-- Select Controller --</option>
                                   <option >Jason Stataham </option>
                                   <option >Vin Diesel </option>
                                   <option >Mike Phelps </option>
                                   <option >Lewis Hamilton </option>
                                   <option >John Doe </option>
                                   <option >Vanessa Holmes </option>

                               </select>
                    </div>
                  
              
            </div>
            <!-- end em-c-modal__body -->
            <div class="em-c-modal__footer em-c-text-passage em-c-text-passage--small pull-right em-u-padding-top">
                <button class="em-c-btn em-c-btn--primary em-js-modal-close-trigger" >
                    <span class="em-c-btn__text">Submit</span>
                </button>
            </div>
            <!-- end em-c-modal__footer -->
        </div>
        <!-- end em-c-modal__window -->
    </div>
    <!-- end em-c-modal -->

         <script>
             function display(event) {
                 if (event === "display table") {
                     document.getElementById("btn-table-filter").style.visibility = 'visible';
                     document.getElementById("btn-assign-PAL").style.visibility = 'visible';
                 }

                 else {
                     document.getElementById("btn-table-filter").style.visibility = 'hidden';
                     document.getElementById("btn-assign-PAL").style.visibility = 'hidden';

                 }

             }
             function checkAll(ele) {
                 var checkboxes = document.getElementsByTagName('input');
                 if (ele.checked) {
                     for (var i = 0; i < checkboxes.length; i++) {
                         if (checkboxes[i].type == 'checkbox') {
                             checkboxes[i].checked = true;
                         }
                     }
                 } else {
                     for (var i = 0; i < checkboxes.length; i++) {
                         console.log(i)
                         if (checkboxes[i].type == 'checkbox') {
                             checkboxes[i].checked = false;
                         }
                     }
                   }
               }
         </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="footer_section" runat="server">
    <div class="align-right" style="width: auto; float: right;">
        <div class="em-c-btn-group em-c-btn-group--responsive">  
            <a  href="#modal" class="em-c-modal__trigger em-js-modal-trigger em-c-btn em-c-btn--primary " style="visibility:hidden; text-decoration:none;" id="btn-assign-PAL" name="assign-PAL" >
                <span style="color:#fff;" class="em-c-btn__text">Assign
                </span>
            </a>

        </div>
    </div>
</asp:Content>
