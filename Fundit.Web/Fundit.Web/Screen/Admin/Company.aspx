﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_includes/MasterPageWithPrint.master" AutoEventWireup="true" CodeBehind="Company.aspx.cs"  %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="em-l-container">
        <div class="em-u-text-align-left">
            <h2>Company Search Criteria
            </h2>
        </div>
        <hr>
        <div class="em-c-collapsible-toolbar__panel em-js-toolbar-panel">
            <div class="em-c-toolbar">
                <div class="em-l-grid em-l-grid--5up">

                    <div class="em-l-grid__item">
                        <div class="em-c-field em-c-field--small">
                            <label for="" class="em-c-field__label">Company Code</label>
                            <div class="em-c-field__body">
                                <input type="text" id="" class="em-c-input" value="" />
                            </div>
                        </div>
                    </div>

                    <div class="em-l-grid__item">
                        <div class="em-c-field em-c-field--small">
                            <label for="" class="em-c-field__label">Company Name</label>
                            <div class="em-c-field__body">
                                <input type="text" id="" class="em-c-input" value="" />
                            </div>
                        </div>
                    </div>

                     <div class="em-l-grid__item">
                        <div class="em-c-field em-c-field--small">
                            <label for="" class="em-c-field__label">AR Service Center</label>
                            <div class="em-c-field__body">
                                <select _ngcontent-c18="" class="em-c-select em-custom-select-width em-u-margin-right-double ng-untouched ng-pristine ng-valid">
                                        <option _ngcontent-c18="" value="requestId">All</option>
                                        <option _ngcontent-c18="" value="workId">US</option>
                                        <option _ngcontent-c18="" value="workName">Canada</option>
                                        <option _ngcontent-c18="" value="statusName">Eame</option>
                                        
                                    </select>
                            </div>
                        </div>
                    </div>

                     <div class="em-l-grid__item">
                        <div class="em-c-field em-c-field--small">
                            <label for="" class="em-c-field__label">IO Service Center</label>
                            <div class="em-c-field__body">
                                 <select _ngcontent-c18="" class="em-c-select em-custom-select-width em-u-margin-right-double ng-untouched ng-pristine ng-valid">
                                        <option _ngcontent-c18="" value="requestId">All</option>
                                        <option _ngcontent-c18="" value="workId">Cortiba</option>
                                        <option _ngcontent-c18="" value="workName">Budapest</option>
                                        
                                        
                                    </select>
                            </div>
                        </div>
                    </div>
                    <div class="em-l-grid__item">
                        <div class="em-c-field ">
                            <button class="em-c-btn" style="margin-top: 17px;">
                                <span class="em-c-btn__text em-c-icon">Search</span>
                            </button>
                            <!-- end em-c-btn -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end em-l-container -->
        <div>
            <div class="em-c-table-object ">
                <div class="em-c-table-object__header">
                </div>
                <!--end em-c-table-object__header-->
                <div class="em-c-table-object__body">
                    <div class="em-c-table-object__body-inner">
                        <table class="em-c-table ">
                            <thead class="em-c-table__header">
                                <tr class="em-c-table__header-row"> 
                                    <th scope="col" class="em-c-table__header-cell "></th>
                                    <th scope="col" class="em-c-table__header-cell ">  Code 
                                <button class="em-c-btn--bare">
                                    <!---->
                                    <svg class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                        <use xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                    </svg>

                                    <!---->
                                </button>
                                    </th>
                                    <th scope="col" class="em-c-table__header-cell "> Name
                                <button class="em-c-btn--bare">
                                    <!---->
                                    <svg class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                        <use xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                    </svg>

                                    <!---->
                                </button>
                                    </th>

                                    <th scope="col" class="em-c-table__header-cell " >IO
                                    <button class="em-c-btn--bare">
                                      <svg class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                           <use xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                  </button>
                                    </th>
                                     <th scope="col" class="em-c-table__header-cell " >AR
                                    <button class="em-c-btn--bare">
                                      <svg class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                           <use xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                  </button>
                                    </th>
                                     <th scope="col" class="em-c-table__header-cell " >Status
                                    <button class="em-c-btn--bare">
                                      <svg class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                           <use xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                  </button>
                                    </th>
                                      <th scope="col" class="em-c-table__header-cell "> Updated By
                                <button class="em-c-btn--bare">
                                    <!---->
                                    <svg class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                        <use xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                    </svg>

                                    <!---->
                                </button>
                                    </th>

                                      <th scope="col" class="em-c-table__header-cell "> Modified On 
                                <button class="em-c-btn--bare">
                                    <!---->
                                    <svg class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                        <use xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                    </svg>

                                    <!---->
                                </button>
                                    </th>
                                </tr>
                                <!-- em-c-table__header-row -->
                            </thead>
                            <!-- end em-c-table__header -->
                            <tbody class="em-c-table__body ">
                                <tr class="em-c-table__row ">
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                <a href="#modal" class="em-c-modal__trigger em-js-modal-trigger">Edit</a></td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">0100</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable" style="width:25%">Exxon Mobil Corporation </td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">US</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Budapest</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable" >Active</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Chris, J. Burgos</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Thur Feb 15, 2018 17:06:27.980</td>

                                </tr>
                                <tr class="em-c-table__row ">
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                        
                                <a href="#modal" class="em-c-modal__trigger em-js-modal-trigger">Edit</a>
                                    </td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">0119</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Mei Foo Properties Limited</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">US</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Budapest</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Active</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Elizabeth, B. Smith</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Fri Jan 3, 2018 17:06:27.980</td>

                                </tr>
                                <tr class="em-c-table__row ">
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                        
                                <a href="#modal" class="em-c-modal__trigger em-js-modal-trigger">Edit</a>
                                    </td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">0218</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Esso Italiana S.r.l. - Cons.</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">US</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Budapest</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable" >Active</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Tammy, M. Packard</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Wed Nov 13, 2017 12:06:27.980</td>

                                </tr>
                                <tr class="em-c-table__row ">
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                        
                                <a href="#modal" class="em-c-modal__trigger em-js-modal-trigger">Edit</a>
                                    </td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">0124</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Imperial Oil Limited - Cons.</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">US</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Budapest</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable" >Inactive</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Geneva, M. Hartley</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Thur Oct 1, 2017 10:06:27.980</td>

                                </tr>
                                <tr class="em-c-table__row ">
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">
                                        
                                <a href="#modal" class="em-c-modal__trigger em-js-modal-trigger">Edit</a>
                                    </td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">0289</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Exxon Overseas Corporation	</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">US</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Budapest</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable"   >Inactive</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">James, K. Smith</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Mon Sept 1, 2017 11:06:27.980</td>

                                </tr>

                            </tbody>
                            <!-- end em-c-table__body -->
                            <tfoot class="em-c-table__footer">
                                <tr class="em-c-table__footer-row">
                                </tr>
                            </tfoot>
                            <!-- end em-c-table__footer -->
                        </table>
                        <!--end em-c-table-->
                    </div>
                    <!--end em-c-table-object__body-inner-->
                </div>
                <!--end em-c-table-object__body-->
            </div>
            <!--end em-c-table-object-->
        </div>
    </div>
    <!-- end em-l-container -->

 <div class="em-c-modal em-js-modal em-is-closed" id="modal">
        <div class="em-c-modal__window em-js-modal-window">
            <div class="em-c-modal__header">
                <h3 class="em-c-modal__title">Edit Regional Coordinator</h3>
                <button class="em-c-btn em-c-btn--bare em-c-modal__close-btn em-js-modal-close-trigger">
                    <span class="em-c-btn__text">Close</span>
                </button>
                <!-- end em-c-btn -->
            </div>
            <!-- end em-c-modal__header -->
            <div class="em-c-modal__body em-c-text-passage em-c-text-passage--small">
                <div class="em-l-grid em-l-grid--2up ">
                    <div class="em-l-grid__item">
                        <div class="em-u-font-size-large fpo-block">
                            AR Service Center :
                        </div>
                    </div>
                    <!-- end em-l-grid__item -->
                    <div class="em-l-grid__item">
                        <select class="em-c-select em-u-font-style-semibold" id="">
                            <option selected="" value="select">US</option>
                            <!---->
                            <option value="1">EAME
                            </option>
                            <option value="2">AP
                            </option>
                            <option value="3">CAN
                            </option>
                            <option value="4">AS
                            </option>
                            <option value="4">UNKNOWN
                            </option>
                        </select>
                        
                    </div>
                    <!-- end em-l-grid__item -->
                    <div class="em-l-grid__item">
                        <div class="em-u-font-size-large fpo-block">
                            IO Service Center :
                        </div>
                    </div>
                    <!-- end em-l-grid__item -->
                    <div class="em-l-grid__item">
                        <select class="em-c-select em-u-font-style-semibold" id="">
                            <option selected="" value="select">Budapest</option>
                            <!---->
                            <option value="1">Coritiba 
                            </option>
                           
                        </select>
                        
                    </div>
                    <!-- end em-l-grid__item -->
                </div>
                <!-- end em-l-grid -->
            </div>
            <!-- end em-c-modal__body -->
            <div class="em-c-modal__footer em-c-text-passage em-c-text-passage--small pull-right">
                <button class="em-c-btn em-c-btn--primary em-js-modal-close-trigger">
                    <span class="em-c-btn__text">Submit</span>
                </button>
            </div>
            <!-- end em-c-modal__footer -->
        </div>
        <!-- end em-c-modal__window -->
    </div>
    <!-- end em-c-modal -->
</asp:Content>