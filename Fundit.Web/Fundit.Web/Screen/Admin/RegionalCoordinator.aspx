﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_includes/MasterPageWithPrint.master" AutoEventWireup="true" CodeBehind="RegionalCoordinator.aspx.cs" Inherits="Fundit.Web.Screen.Admin.RegionalCoordinator" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="em-l-container">
        <div class="em-u-text-align-left">
            <div class="em-u-text-align-left">
                <h2>Service Center - Budapest Users

                </h2>
            </div>
            <hr>
        </div>
        <div class="em-c-table-object__body">
            <div class="em-c-table-object__body-inner">
                <table class="em-c-table ">
                    <thead class="em-c-table__header">
                        <tr class="em-c-table__header-row">
                            <th scope="col" class="em-c-table__header-cell"></th>
                            
                            <th scope="col" class="em-c-table__header-cell">
                                <div class="em-u-font-size-small-2 fpo-block">
                                    User
                                <button class="em-c-btn--bare">
                                    <!---->
                                    <svg class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                        <use xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                    </svg>

                                    <!---->
                                </button>
                                </div>
                            </th>
                            
                            
                            <th scope="col" class="em-c-table__header-cell">
                                <div class="em-u-font-size-small-2 fpo-block">
                                   
                                </div>
                            </th>
                        </tr>
                    </thead>
                    <tbody class="em-c-table__body">
                        <tr class="em-c-table__row">
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">
                                <a href="#modal" class="em-c-modal__trigger em-js-modal-trigger">Edit</a>
                            </td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">
                                <div class="em-u-font-size-med-2 fpo-block">
                                    John Doe
                                </div>
                            </td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">
                                <div class="em-u-font-size-med-2 fpo-block">
                                    <a href="#">Delete</a>
                                </div>
                            </td>
                            
                        </tr>
                        <tr class="em-c-table__row">
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">
                                <a href="#modal" class="em-c-modal__trigger em-js-modal-trigger">Edit</a>
                            </td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">
                                <div class="em-u-font-size-med-2 fpo-block">
                                    Jane Doe
                                </div>
                            </td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">
                                <div class="em-u-font-size-med-2 fpo-block">
                                    <a href="#">Delete</a>
                                </div>
                            </td>
                            
                        </tr>
                        <tr class="em-c-table__row">
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">
                                <a href="#modal" class="em-c-modal__trigger em-js-modal-trigger">Edit</a>
                            </td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">
                                <div class="em-u-font-size-med-2 fpo-block">
                                    Jill Doe
                                </div>
                            </td>
                            <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="">
                                <div class="em-u-font-size-med-2 fpo-block">
                                    <a href="#">Delete</a>
                                </div>
                            </td>
                            
                        </tr>
                    </tbody>
                </table>
                <!--end em-c-table-->
            </div>
            <!--end em-c-table-object__body-inner-->
        </div>
        <!--end em-c-table-object__body-->
    </div>

    <div class="em-c-modal em-js-modal em-is-closed" id="modal">
        <div class="em-c-modal__window em-js-modal-window">
            <div class="em-c-modal__header">
                <h3 class="em-c-modal__title">Edit User</h3>
                <button class="em-c-btn em-c-btn--bare em-c-modal__close-btn em-js-modal-close-trigger">
                    <span class="em-c-btn__text">Close</span>
                </button>
                <!-- end em-c-btn -->
            </div>
            <!-- end em-c-modal__header -->
            <div class="em-c-modal__body em-c-text-passage em-c-text-passage--small">

                <!-- end em-l-grid -->
                <div class="em-l-grid em-l-grid--2up ">
                    <div class="em-l-grid__item">
                        <div class="em-u-font-size-large fpo-block">
                            User  :
                        </div>
                     
                    </div>
                    <!-- end em-l-grid__item -->
                    <div class="em-l-grid__item">
                        <select class="em-c-select em-u-font-style-semibold" id="">
                            <option selected="" value="select">Jaworski, Gabriela Munique</option>
                            <!---->
                            <option value="1">Pransopon, Panjarat
                            </option>
                            <option value="2">Garcia Colomer, Lisette
                            </option>
                            <option value="3">Azola, Gabriela P
                            </option>
                            <option value="4">Melnyk, Darya
                            </option>
                        </select>
                        
                    </div>
                    <!-- end em-l-grid__item -->
                </div>
                <!-- end em-l-grid -->
            </div>
            <!-- end em-c-modal__body -->
            <div class="em-c-modal__footer em-c-text-passage em-c-text-passage--small pull-right">
                <button class="em-c-btn em-c-btn--primary em-js-modal-close-trigger">
                    <span class="em-c-btn__text">Submit</span>
                </button>
            </div>
            <!-- end em-c-modal__footer -->
        </div>
        <!-- end em-c-modal__window -->
    </div>
    <!-- end em-c-modal -->
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="footer_section" runat="server">
</asp:Content>
