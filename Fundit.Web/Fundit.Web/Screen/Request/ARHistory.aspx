﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/_includes/MasterPageWithPrint.master" CodeBehind="ARHistory.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <div class="em-l-container arsetup-container-overflow">
         <div class="em-c-collapsible-toolbar__panel em-js-toolbar-panel">
                                    <div class="em-c-toolbar ">
                                        <div class="em-c-toolbar__item ">
                                            <span class="em-u-font-style-semibold em-u-font-size-med-3">AR HISTORY</span>
                                        </div>

                                    </div>
        <div class="em-c-table-object ">
            <div class="em-c-table-object__header">
            </div>
            <!--end em-c-table-object__header-->
            <div class="em-c-table-object__body">
                <div class="em-c-table-object__body-inner">
                    <table class="em-c-table ">
                        <thead class="em-c-table__header">
                            <tr class="em-c-table__header-row">
                                <th scope="col" class="em-c-table__header-cell ">AR #</th>
                                <th scope="col" class="em-c-table__header-cell ">Asset Quantity</th>
                                <th scope="col" class="em-c-table__header-cell ">Amount</th>
                                <th scope="col" class="em-c-table__header-cell ">Created On</th>
                                <th scope="col" class="em-c-table__header-cell ">Created By</th>
                            </tr>
                            <!-- em-c-table__header-row -->
                        </thead>
                        <!-- end em-c-table__header -->
                        <tbody class="em-c-table__body ">
                            <tr class="em-c-table__row ">
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">1281</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">0</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">$1,900.00</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Tue Aug 14, 2018 11:00 AM</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Pascal, John</td>
                            </tr>
                            <tr class="em-c-table__row ">
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">1181</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">20</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">$1,000.00</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Wed Mar 14, 2018 11:00 AM</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Mulla, Kevin</td>
                            </tr>

                            <tr class="em-c-table__row ">

                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">1126</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">20</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">$600.00</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Fri Dec 14, 2017 11:00 AM</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Secrets, Victoria</td>
                            </tr>

                            <tr class="em-c-table__row ">

                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">1116</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">20</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">$200.00</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Mon Oct 1, 2017 11:00 AM</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Secrets, Victoria</td>
                            </tr>


                        </tbody>
                        <!-- end em-c-table__body -->
                        <tfoot class="em-c-table__footer">
                            <tr class="em-c-table__footer-row">
                            </tr>
                        </tfoot>
                        <!-- end em-c-table__footer -->
                    </table>
                    <!--end em-c-table-->
                </div>
                <!--end em-c-table-object__body-inner-->
            </div>
            <!--end em-c-table-object__body-->
        </div>
    </div>
    <!--end em-c-table-object-->

</asp:Content>
