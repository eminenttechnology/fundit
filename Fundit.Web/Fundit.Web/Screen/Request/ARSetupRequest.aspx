﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/_includes/MasterPageWithPrint.master" CodeBehind="ARSetupRequest.aspx.cs" Inherits="Fundit.Web.Screen.Request.ARSetupRequest" %>


<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <div class="content">
        <div class="page-wrapper">
            <div class="page-content" id="pageContent" style="flex-basis: 97%;">
                <div class="em-l-container arsetup-container-overflow">
                    <div class="em-c-table-object ">
                        <div class="em-c-table-object__header">
                            <div class="em-c-collapsible-toolbar em-js-collapsible-toolbar">

                                <div class="em-c-collapsible-toolbar__panel em-js-toolbar-panel">
                                    <div class="em-c-toolbar ">
                                        <div class="em-c-toolbar__item ">
                                            <span class="em-u-font-style-semibold em-u-font-size-med-3">ALLOCATED RESOURCES</span>
                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                        <div class="em-c-table-object__body">
                            <div class="em-c-table-object__body-inner">
                                <table class="scroll em-c-table">
                                    <thead class="em-c-table__header">
                                        <tr class="em-c-table__header-row">
                                            <th class="em-c-table__header-cell" scope="col">
                                                <span class="em-u-font-size-small-2 ">QUANTITY</span>
                                                <button class="em-c-btn--bare em-u-text-align-left">
                                                    <!---->
                                                    <svg class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                                        <use xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>

                                                    <!---->
                                                </button>
                                            </th>
                                            <th class="em-c-table__header-cell" scope="col">
                                                <span class="em-u-font-size-small-2 ">CAPITAL($KUSD)</span>
                                                <button class="em-c-btn--bare em-u-text-align-left">
                                                    <!---->
                                                    <svg class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                                        <use xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>

                                                    <!---->
                                                </button>
                                            </th>
                                            <th class="em-c-table__header-cell" scope="col">
                                                <span class="em-u-font-size-small-2 ">AR#</span>
                                                <button class="em-c-btn--bare em-u-text-align-left">
                                                    <!---->
                                                    <svg class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                                        <use xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>

                                                    <!---->
                                                </button>
                                            </th>
                                            <th class="em-c-table__header-cell" scope="col">
                                                <span class="em-u-font-size-small-2 ">COMPANY CODE</span>
                                                <button class="em-c-btn--bare em-u-text-align-left">
                                                    <!---->
                                                    <svg class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                                        <use xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>

                                                    <!---->
                                                </button>
                                            </th>
                                            <th class="em-c-table__header-cell" scope="col">
                                                <span class="em-u-font-size-small-2 ">ORGANIZATION</span>
                                                <button class="em-c-btn--bare em-u-text-align-left">
                                                    <!---->
                                                    <svg class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                                        <use xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>

                                                    <!---->
                                                </button>
                                            </th>
                                            <th class="em-c-table__header-cell em-u-width-10 em-u-padding-left-none em-u-padding-right-none" scope="col">
                                                <span class="em-u-font-size-small-2 ">WBS</span>
                                                <button class="em-c-btn--bare em-u-text-align-left">
                                                    <!---->
                                                    <svg class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                                        <use xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>

                                                    <!---->
                                                </button>
                                            </th>
                                            <th class="em-c-table__header-cell em-u-width-10 em-u-padding-left-none em-u-padding-right-none" scope="col">
                                                <span class="em-u-font-size-small-2 ">NETWORK ORDER</span>
                                                <button class="em-c-btn--bare em-u-text-align-left">
                                                    <!---->
                                                    <svg class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                                        <use xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>

                                                    <!---->
                                                </button>
                                            </th>
                                            <th class="em-c-table__header-cell" scope="col">
                                                <span class="em-u-font-size-small-2 ">TYPE</span>
                                                <button class="em-c-btn--bare em-u-text-align-left">
                                                    <!---->
                                                    <svg class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                                        <use xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>

                                                    <!---->
                                                </button>
                                            </th>
                                            <th class="em-c-table__header-cell" scope="col">
                                                <span class="em-u-font-size-small-2 ">COST CENTER</span>
                                                <button class="em-c-btn--bare em-u-text-align-left">
                                                    <!---->
                                                    <svg class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                                        <use xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>

                                                    <!---->
                                                </button>
                                            </th>
                                            <th class="em-c-table__header-cell" scope="col">
                                                <span class="em-u-font-size-small-2 ">DESCRIPTION</span>
                                                <button class="em-c-btn--bare em-u-text-align-left">
                                                    <!---->
                                                    <svg class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                                        <use xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>

                                                    <!---->
                                                </button>
                                            </th>
                                        </tr>

                                    </thead>

                                    <tbody class="em-c-table__body ">
                                        <!---->
                                        <tr class="em-c-table__row">
                                            <td class="em-c-table__cell em-u-text-align-left em-js-cell em-js-cell-editable" colspan="">
                                                <span class="em-u-font-size-med">55</span>
                                            </td>
                                            <td class="em-c-table__cell em-u-text-align-right em-js-cell em-js-cell-editable" colspan="">
                                                <span class="em-u-font-size-med">$67.00</span>
                                            </td>
                                            <td class="em-c-table__cell em-u-text-align-left em-js-cell em-js-cell-editable" colspan="">
                                                <span class="em-u-font-size-med">180021002</span>
                                            </td>
                                            <td class="em-c-table__cell em-u-text-align-left em-js-cell em-js-cell-editable" colspan="">
                                                <span class="em-u-font-size-med">0970</span>
                                            </td>
                                            <td class="em-c-table__cell em-u-text-align-left em-js-cell em-js-cell-editable" colspan="">
                                                <span class="em-u-font-size-med"></span>
                                            </td>
                                            <td class="em-c-table__cell em-u-text-align-center em-js-cell em-js-cell-editable em-u-padding-left-none 
                            em-u-padding-right-none"
                                                colspan="">
                                                <!---->
                                                <div>
                                                    <input class="em-c-input em-u-padding-none em-u-margin-right-half ng-untouched ng-pristine ng-valid" id="wbs-description" type="text">
                                                </div>
                                                <!---->
                                            </td>
                                            <td class="em-c-table__cell em-u-text-align-center em-js-cell em-js-cell-editable em-u-padding-left-none 
                            em-u-padding-right-none"
                                                colspan="">
                                                <!---->
                                                <div>
                                                    <input class="em-c-input em-u-padding-none em-u-margin-left-half ng-untouched ng-pristine ng-valid" id="network-order-request" type="text">
                                                </div>
                                                <!---->
                                            </td>
                                            <td class="em-c-table__cell em-u-text-align-left em-js-cell em-js-cell-editable" colspan="">
                                                <div class="em-u-font-size-med em-u-padding-left">Hardware</div>
                                            </td>
                                            <td class="em-c-table__cell em-u-text-align-left em-js-cell em-js-cell-editable" colspan="">
                                                <span class="em-u-font-size-med">C00000789</span>
                                            </td>
                                            <td class="em-c-table__cell em-u-text-align-left em-js-cell em-js-cell-editable" colspan="">
                                                <span class="em-u-font-size-med">HANA Acceptance</span>
                                            </td>
                                        </tr>
                                        <!---->
                                        <tr class="em-c-table__row">
                                            <td class="em-c-table__cell em-u-text-align-left em-js-cell em-js-cell-editable" colspan="">
                                                <span class="em-u-font-size-med">55</span>
                                            </td>
                                            <td class="em-c-table__cell em-u-text-align-right em-js-cell em-js-cell-editable" colspan="">
                                                <span class="em-u-font-size-med">$67.00</span>
                                            </td>
                                            <td class="em-c-table__cell em-u-text-align-left em-js-cell em-js-cell-editable" colspan="">
                                                <span class="em-u-font-size-med">180021002</span>
                                            </td>
                                            <td class="em-c-table__cell em-u-text-align-left em-js-cell em-js-cell-editable" colspan="">
                                                <span class="em-u-font-size-med">1570</span>
                                            </td>
                                            <td class="em-c-table__cell em-u-text-align-left em-js-cell em-js-cell-editable" colspan="">
                                                <span class="em-u-font-size-med"></span>
                                            </td>
                                            <td class="em-c-table__cell em-u-text-align-center em-js-cell em-js-cell-editable em-u-padding-left-none 
                            em-u-padding-right-none"
                                                colspan="">
                                                <!---->
                                                <div>
                                                    <input class="em-c-input em-u-padding-none em-u-margin-right-half ng-untouched ng-pristine ng-valid" id="wbs-description" type="text">
                                                </div>
                                                <!---->
                                            </td>
                                            <td class="em-c-table__cell em-u-text-align-center em-js-cell em-js-cell-editable em-u-padding-left-none 
                            em-u-padding-right-none"
                                                colspan="">
                                                <!---->
                                                <div>
                                                    <input class="em-c-input em-u-padding-none em-u-margin-left-half ng-untouched ng-pristine ng-valid" id="network-order-request" type="text">
                                                </div>
                                                <!---->
                                            </td>
                                            <td class="em-c-table__cell em-u-text-align-left em-js-cell em-js-cell-editable" colspan="">
                                                <div class="em-u-font-size-med em-u-padding-left">Hardware</div>
                                            </td>
                                            <td class="em-c-table__cell em-u-text-align-left em-js-cell em-js-cell-editable" colspan="">
                                                <span class="em-u-font-size-med">C0802MRU90</span>
                                            </td>
                                            <td class="em-c-table__cell em-u-text-align-left em-js-cell em-js-cell-editable" colspan="">
                                                <span class="em-u-font-size-med">Hana Licenses</span>
                                            </td>
                                        </tr>
                                    </tbody>

                                    <tfoot class="em-c-table__footer">
                                        <tr class="em-c-table__footer-row custom-table-background">
                                            <td class="em-c-table__footer-cell em-u-font-size-med-2 ">TOTAL:</td>
                                            <td class="em-c-table__footer-cell em-u-font-size-med-2 em-u-width-10">110</td>
                                            <td class="em-c-table__footer-cell em-u-text-align-left em-u-font-size-med-2 " colspan="9">$134.00</td>
                                        </tr>
                                    </tfoot>

                                </table>

                            </div>

                        </div>

                    </div>

                </div>
            </div>


        </div>
    </div>
</asp:Content>
