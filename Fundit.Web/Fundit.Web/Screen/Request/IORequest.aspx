﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/_includes/MasterPageWithPrint.master" CodeBehind="IORequest.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">


    <div class="content">
        <div class="page-wrapper">
            <div class="page-content" id="pageContent">
                <div class=" arsetup-container-overflow" style="width:80%">


                    <div _ngcontent-c19="" class="em-l-container reallocation-header">
          <h1 _ngcontent-c19="" class="em-u-font-size-xl" style="letter-spacing: 1px;">General Info</h1>

          <div _ngcontent-c19="" class="em-u-padding-bottom-half em-l em-l--two-column ">
            <div _ngcontent-c19="" class="em-l__main em-u-max-width-30">
              <div _ngcontent-c19="" class=" em-u-margin-bottom-none">
                <table _ngcontent-c19="" class="em-c-table em-c-table--condensed " style="min-width:0!important;">
                  <thead _ngcontent-c19="" class="em-c-table__header">
                    <tr _ngcontent-c19="" class="em-c-table__header-row " style="background:#fff;">
                      <th _ngcontent-c19="" class="em-c-table__header-cell " scope="col"></th>
                      <th _ngcontent-c19="" class="em-c-table__header-cell " scope="col"></th>
                    </tr>
                  </thead>
                  <tbody _ngcontent-c19="" class="em-c-table__body  ">
                    <tr _ngcontent-c19="" class=" em-c-table--condensed">
                      <td _ngcontent-c19="" style="width: 50px;">
                        <label _ngcontent-c19="" class="em-u-font-size-med-2 info-request-status em-u-padding-bottom"> Work Id: </label>
                      </td>
                      <td _ngcontent-c19="" class="em-u-font-size-med em-c-table__cell  em-u-padding-bottom">
                        72926
                      </td>
                    </tr>
                    <tr _ngcontent-c19="">
                      <td _ngcontent-c19="" class="">
                        <label _ngcontent-c19="" class="em-u-font-size-med-2 info-request-status em-u-padding-bottom"> Work Name: </label>
                      </td>
                      <td _ngcontent-c19="" class="em-u-font-size-med em-c-table__cell  em-u-padding-bottom ">
                        BLADE
                    </td></tr>
                    <tr _ngcontent-c19="">
                      <td _ngcontent-c19="">
                        <label _ngcontent-c19="" class="em-u-font-size-med-2 info-request-status em-u-padding-bottom"> Work Lead: </label>
                      </td>
                      <td _ngcontent-c19="" class=" em-u-font-size-med em-c-table__cell em-u-padding-bottom">
                      Herrera, Jesse J
                      </td>
                    </tr>
                    <tr _ngcontent-c19="">
                      <td _ngcontent-c19="">
                        <label _ngcontent-c19="" class="em-u-font-size-med-2 info-request-status em-u-padding-bottom"> Requester: </label>
                      </td>
                      <td _ngcontent-c19="" class="em-u-font-size-med em-c-table__cell em-u-padding-bottom-half">
                       	Nia Hall
                      </td>
                    </tr>
                    <tr _ngcontent-c19="">
                      <td _ngcontent-c19="">
                        <label _ngcontent-c19="" class="em-u-font-size-med-2 info-request-status em-u-padding-bottom"> Created On: </label>
                      </td>
                      <td _ngcontent-c19="" class="em-u-font-size-med em-c-table__cell em-u-padding-bottom-half">
                       Tue Jul 03, 2018 10:39 AM
                      </td>
                    </tr>
                    <!---->
                    
                  </tbody>
                </table>
              </div>
            </div>
            <div _ngcontent-c19="" class="em-u-max-width-30">
              <div _ngcontent-c19="" class="em-u-margin-bottom-none  ">
                <table _ngcontent-c19="" class="em-c-table em-c-table--condensed " style="min-width:0!important;">
                  <thead _ngcontent-c19="" class="em-c-table__header">
                    <tr _ngcontent-c19="" class="em-c-table__header-row " style="background:#fff;">
                      <th _ngcontent-c19="" class="em-c-table__header-cell " scope="col"></th>
                      <th _ngcontent-c19="" class="em-c-table__header-cell " scope="col"></th>
                    </tr>
                  </thead>
                  <tbody _ngcontent-c19="" class="em-c-table__body  ">
                    <tr _ngcontent-c19="" class=" em-c-table--condensed">
                      <td _ngcontent-c19="" style="width: 50px;">
                        <label _ngcontent-c19="" class="em-u-font-size-med-2 info-request-status em-u-padding-bottom"> Controller: </label>
                      </td>
                      <td _ngcontent-c19="" class="em-u-font-size-med em-c-table__cell  em-u-padding-bottom">
                        72926
                      </td>
                    </tr>
                    <tr _ngcontent-c19="">
                      <td _ngcontent-c19="" class="">
                        <label _ngcontent-c19="" class="em-u-font-size-med-2 info-request-status em-u-padding-bottom"> Service Center: </label>
                      </td>
                      <td _ngcontent-c19="" class="em-u-font-size-med em-c-table__cell  em-u-padding-bottom ">
                        Budapest
                    </td></tr>
                    <tr _ngcontent-c19="">
                      <td _ngcontent-c19="">
                        <label _ngcontent-c19="" style="width: 200px;" class="em-u-font-size-med-2  em-u-padding-bottom"> Service Center Owner: </label>
                      </td>
                      <td _ngcontent-c19="" class=" em-u-font-size-med em-c-table__cell em-u-padding-bottom" style="width: 200px;">
                      Doe, Jane
                      </td>
                    </tr>
                    <tr _ngcontent-c19="">
                      <td _ngcontent-c19="">
                        <label _ngcontent-c19="" class="em-u-font-size-med-2 info-request-status em-u-padding-bottom"> Service Center Approver: </label>
                      </td>
                      <td _ngcontent-c19="" class="em-u-font-size-med em-c-table__cell em-u-padding-bottom-half">
                       	Nia Hall
                      </td>
                    </tr>
                    <tr _ngcontent-c19="">
                      <td _ngcontent-c19="">
                        <label _ngcontent-c19="" class="em-u-font-size-med-2 info-request-status em-u-padding-bottom"> Approved On: </label>
                      </td>
                      <td _ngcontent-c19="" class="em-u-font-size-med em-c-table__cell em-u-padding-bottom-half">
                       Tue Jul 03, 2018 10:39 AM
                      </td>
                    </tr>
                    <!---->
                    
                  </tbody>
                </table>
              </div>
            </div>
            <hr _ngcontent-c19="" class=" hr-no">

          </div>
        </div>


                   

                </div>
            </div>


        </div>

        <div _ngcontent-c19="" class="reallocation-table-wrapper ">
         <div class="em-c-table-object " >
                        <div class="em-c-table-object__header">
                            <div class="em-c-collapsible-toolbar em-js-collapsible-toolbar">

                                <div class="em-c-collapsible-toolbar__panel em-js-toolbar-panel">
                                    <div class="em-c-toolbar ">
                                        <div class="em-c-toolbar__item ">
                                            <span class="em-u-font-style-semibold em-u-font-size-med-3">IO REQUEST</span>
                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                        <div _ngcontent-c9="" class="em-c-table-object__body-inner">
              <table _ngcontent-c9="" class="em-c-table  scroll " style="table-layout:fixed; width:100%;">
                <thead _ngcontent-c9="" class="em-c-table__header">
                  <tr _ngcontent-c9="" class="em-c-table__header-row ">
                    <th _ngcontent-c9="" class="em-c-table__header-cell" scope="col" style="width: 7.7%;">
                    </th>
                    <th _ngcontent-c9="" class="em-c-table__header-cell " scope="col" style="width: 7.7%;">
                      <span _ngcontent-c9="" class="em-u-font-size-small-2">STATUS</span>
                      <button _ngcontent-c9="" class="em-c-btn--bare">
                        <!---->
                          <svg _ngcontent-c9="" class="em-c-icon em-c-icon--tiny em-c-icon--green">
                            <use _ngcontent-c9="" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                          </svg>
                        
                        <!---->
                      </button>
                    </th>
                    
                    <th _ngcontent-c9="" class="em-c-table__header-cell em-u-margin-right" scope="col" style="word-wrap: break-word; width: 7.7%;">
                      <span _ngcontent-c9="" class="em-u-font-size-small-2">IO #</span>
                      <button _ngcontent-c9="" class="em-c-btn--bare">
                        <!---->
                          <svg _ngcontent-c9="" class="em-c-icon em-c-icon--tiny em-c-icon--green">
                            <use _ngcontent-c9="" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                          </svg>
                        
                        <!---->
                      </button>
                    </th>
                    <th _ngcontent-c9="" class="em-c-table__header-cell " scope="col" style="width: 7.7%;">
                      <span _ngcontent-c9="" class="em-u-font-size-small-2">COMPANY CODE</span>
                      <button _ngcontent-c9="" class="em-c-btn--bare">
                        <!---->
                          <svg _ngcontent-c9="" class="em-c-icon em-c-icon--tiny em-c-icon--green">
                            <use _ngcontent-c9="" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                          </svg>
                        
                        <!---->
                      </button>
                    </th>
                    <th _ngcontent-c9="" class="em-c-table__header-cell " scope="col" style="width: 7.7%;word-wrap: break-word;">
                      <span _ngcontent-c9="" class="em-u-font-size-small-2">COUNTRY</span>
                      <button _ngcontent-c9="" class="em-c-btn--bare">
                        <!---->
                          <svg _ngcontent-c9="" class="em-c-icon em-c-icon--tiny em-c-icon--green">
                            <use _ngcontent-c9="" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                          </svg>
                        
                        <!---->
                      </button>
                    </th>
                    <th _ngcontent-c9="" class="em-c-table__header-cell" scope="col" style="width: 7.7%;word-wrap: break-word;">
                      <span _ngcontent-c9="" class="em-u-font-size-small-2">COST CENTER</span>
                      <button _ngcontent-c9="" class="em-c-btn--bare">
                        <!---->
                          <svg _ngcontent-c9="" class="em-c-icon em-c-icon--tiny em-c-icon--green">
                            <use _ngcontent-c9="" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                          </svg>
                        
                        <!---->
                      </button>
                    </th>
                    <th _ngcontent-c9="" class="em-c-table__header-cell em-u-margin-right " scope="col" style="width: 7.7%;">
                      <span _ngcontent-c9="" class="em-u-font-size-small-2">RECEIVING ORDER</span>
                      <button _ngcontent-c9="" class="em-c-btn--bare">
                        <!---->
                          <svg _ngcontent-c9="" class="em-c-icon em-c-icon--tiny em-c-icon--green">
                            <use _ngcontent-c9="" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                          </svg>
                        
                        <!---->
                      </button>
                    </th>
                    <th _ngcontent-c9="" class="em-c-table__header-cell " scope="col" style="word-wrap: break-word; width: 7.7%;">
                      <span _ngcontent-c9="" class="em-u-font-size-small-2"> DESCRIPTION </span>
                      <button _ngcontent-c9="" class="em-c-btn--bare">
                        <!---->
                          <svg _ngcontent-c9="" class="em-c-icon em-c-icon--tiny em-c-icon--green">
                            <use _ngcontent-c9="" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                          </svg>
                        
                        <!---->
                      </button>
                    </th>
                   
                    <th _ngcontent-c9="" class="em-c-table__header-cell " scope="col" style="width: 7.7%;">
                    </th>
                  </tr>
                  
                </thead>
                <tbody _ngcontent-c9="" class="em-c-table__body">
                  <!----><tr _ngcontent-c9="" class="em-c-table__row "  onclick="window.location.href='../../Screen/Request/IORequest.aspx'">
                    <td _ngcontent-c9="" class="em-c-table__cell" colspan="" style="width: 7.7%;">
                      <a _ngcontent-c9="" class="custom-text-link pointer">Edit</a>
                    </td>
                    <td _ngcontent-c9="" class="em-c-table__cell" colspan="" style="width: 7.7%;">Closed
                    </td>
                    <td _ngcontent-c9="" class="em-c-table__cell" colspan="" style="width: 7.7%;">842226
                    </td>
                    <td _ngcontent-c9="" class="em-c-table__cell em-u-margin-right" colspan="" style="width: 7.7%;">2299
                    </td>
                    <td _ngcontent-c9="" class="em-c-table__cell" colspan="" style="width: 7.7%;">Canada
                    </td>
                    <td _ngcontent-c9="" class="em-c-table__cell" colspan="" style="width: 7.7%;">124082
                    </td>
                    </td>
                    <td _ngcontent-c9="" class="em-c-table__cell break-character " colspan="" style="width: 7.7%; word-wrap: break-word;">G802075385
                    </td>
                    <td _ngcontent-c9="" class="em-c-table__cell" colspan="" style="width: 7.7%; word-wrap: break-word;">Sarnia Radio Remediation

                    </td>
                    <td _ngcontent-c9="" class="em-c-table__cell" colspan="" style="width: 7.7%;">
                      <!---->
                    </td>
                  </tr><tr _ngcontent-c9="" onclick="window.location.href='../../Screen/Request/IORequest.aspx'" class="em-c-table__row ">
                    <td _ngcontent-c9="" class="em-c-table__cell" colspan="" style="width: 7.7%;">
                      <a _ngcontent-c9="" class="custom-text-link pointer">Edit</a>
                    </td>
                    <td _ngcontent-c9="" class="em-c-table__cell" colspan="" style="width: 7.7%;">Closed
                    </td>
                    <td _ngcontent-c9="" class="em-c-table__cell" colspan="" style="width: 7.7%;">846892
                    </td>
                    <td _ngcontent-c9="" class="em-c-table__cell em-u-margin-right" colspan="" style="width: 7.7%;">524
                    </td>
                    <td _ngcontent-c9="" class="em-c-table__cell" colspan="" style="width: 7.7%;">Italy
                    </td>
                    <td _ngcontent-c9="" class="em-c-table__cell" colspan="" style="width: 7.7%;">13821
                    </td>
                    </td>
                    <td _ngcontent-c9="" class="em-c-table__cell break-character " colspan="" style="width: 7.7%; word-wrap: break-word;">G802074983
                    </td>
                    <td _ngcontent-c9="" class="em-c-table__cell" colspan="" style="width: 7.7%; word-wrap: break-word;">Phone devices < $2K
                    </td>
                    <td _ngcontent-c9="" class="em-c-table__cell" colspan="" style="width: 7.7%;">
                      <!---->
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>

                    </div>
            </div>
    </div>
</asp:Content>
