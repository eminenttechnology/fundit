﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_includes/MasterPageWithPrint.master" AutoEventWireup="true"  %>


<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

   <div   class="em-c-table-object__body">
      <div   class="em-c-table-object__body-inner ">
        <div   class="em-l-container reallocation-header">
          <h1   class="em-u-font-size-xl" style="letter-spacing: 1px;">General Info</h1>

          <div   class="em-u-padding-bottom-half em-l em-l--two-column ">
            <div   class="em-l__main em-u-max-width-30">
              <div   class=" em-u-margin-bottom-none">
                <table   class="em-c-table em-c-table--condensed " style="min-width:0!important;">
                  <thead   class="em-c-table__header">
                    <tr   class="em-c-table__header-row " style="background:#fff;">
                      <th   class="em-c-table__header-cell " scope="col"></th>
                      <th   class="em-c-table__header-cell " scope="col"></th>
                    </tr>
                  </thead>
                  <tbody   class="em-c-table__body  ">
                    <tr   class=" em-c-table--condensed">
                      <td   style="width: 50px;">
                        <label   class="em-u-font-size-med-2 info-request-status em-u-padding-bottom"> Work Id: </label>
                      </td>
                      <td   class="em-u-font-size-med em-c-table__cell  em-u-padding-bottom">
                        72926
                      </td>
                    </tr>
                    <tr  >
                      <td   class="">
                        <label   class="em-u-font-size-med-2 info-request-status em-u-padding-bottom"> Work Name: </label>
                      </td>
                      <td   class="em-u-font-size-med em-c-table__cell  em-u-padding-bottom ">
                        BLADE
                    </td></tr>
                    <tr  >
                      <td  >
                        <label   class="em-u-font-size-med-2 info-request-status em-u-padding-bottom"> Work Lead: </label>
                      </td>
                      <td   class=" em-u-font-size-med em-c-table__cell em-u-padding-bottom">
                      Herrera, Jesse J
                      </td>
                    </tr>
                    <tr  >
                      <td  >
                        <label   class="em-u-font-size-med-2 info-request-status em-u-padding-bottom"> Requester: </label>
                      </td>
                      <td   class="em-u-font-size-med em-c-table__cell em-u-padding-bottom-half">
                       	Nia Hall
                      </td>
                    </tr>
                    <tr  >
                      <td  >
                        <label   class="em-u-font-size-med-2 info-request-status em-u-padding-bottom"> Created On: </label>
                      </td>
                      <td   class="em-u-font-size-med em-c-table__cell em-u-padding-bottom-half">
                       Tue Jul 03, 2018 10:39 AM
                      </td>
                    </tr>
                    <!---->
                    
                  </tbody>
                </table>
              </div>
            </div>
            <div   class="em-l__secondary">
              <div   class="em-u-margin-bottom-none  ">
                <table   class="em-c-table em-c-table--condensed " style="min-width:384px;">
                  <thead   class="em-c-table__header em-u-margin-right-double">
                    <tr   class="em-c-table__header-row " style="background:#fff;">
                      <th   class="custom-em-c-table__header em-c-table__header-cell info-request-status-header " scope="col"></th>
                      <th   class="em-u-font-size-med-2 custom-em-c-table__header " scope="col" style="color:#111122;width: 100px;text-align:right;">Capital </th>
                      <th   class="em-u-font-size-med-2 custom-em-c-table__header " scope="col" style="color:#111122;width: 100px;text-align:right;">Expense</th>
                    </tr>
                  </thead>
                  <tbody   class="em-c-table__body  ">

                    <tr   class="request-status-header border-bottom em-u-padding-top">
                      <td   class="request-status-header em-u-padding-bottom-half em-u-padding-top-half">
                        <label   class="em-u-font-size-med-2">Total Amount Approved: </label>
                      </td>
                      <td   class="em-u-font-size-med em-c-table__cell em-u-padding-bottom-half em-u-padding-top-half em-u-text-align-right">
                        $1100K	
                      </td>
                      <td   class="em-u-font-size-med em-c-table__cell em-u-padding-bottom-half em-u-padding-top-half em-u-text-align-right">
                        $1450K
                      </td>
                    </tr>

                    <tr   class="em-u-padding-top em-u-margin-top border-bottom">
                      <td   class="request-status-header  ">
                        <label   class="em-u-font-size-med-2 em-u-padding-bottom-half em-u-padding-top-half"> Amount Spent: </label>
                      </td>

                      <td   class="em-u-font-size-med em-c-table__cell em-u-padding-bottom-half em-u-padding-top-half  em-u-text-align-right">
                        $0K	
                      </td>
                      <td   class="em-u-font-size-med em-c-table__cell em-u-padding-bottom-half em-u-padding-top-half em-u-text-align-right">
                        $0K
                      </td>
                    </tr>
                    <tr   class="border-bottom">
                      <td   class="request-status-header">
                        <label   class="em-u-font-size-med-2 em-u-padding-bottom-half em-u-padding-top-half"> Balance: </label>
                      </td>
                      <td   class="em-u-font-size-med em-c-table__cell em-u-padding-bottom-half em-u-padding-top-half em-u-padding-bottom em-u-text-align-right">
                       $1100K	
                      </td>
                      <td   class="em-u-font-size-med em-c-table__cell em-u-padding-bottom-half em-u-padding-top-half em-u-padding-bottom em-u-text-align-right">
                        $1450K
                      </td>
                    </tr>
                    <tr   class="request-status-header border-bottom em-u-padding-top">
                      <td   class="request-status-header em-u-padding-bottom-half em-u-padding-top-half">
                        <label   class="em-u-font-size-med-2">Unallocated: </label>
                      </td>
                      <td   class="em-u-font-size-med em-c-table__cell em-u-padding-bottom-half em-u-padding-top-half em-u-text-align-right" id="totalUnallocatedCapexCol">
                        -$89,431.00K	
                      </td>
                      <td   class="em-u-font-size-med em-c-table__cell em-u-padding-bottom-half em-u-padding-top-half em-u-text-align-right">
                        -
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <hr   class=" hr-no">

          </div>
        </div>
        <div   class="reallocation-table-wrapper">
          <hr   class=" hr-no">
          <h1   class="em-u-font-size-xl" style="letter-spacing: 1px;">Re-Allocation Requests</h1>
          <div   class="em-c-toolbar">
            <div   class="em-c-toolbar__item">
              <h6   class=" em-c-headings em-u-margin-bottom-none" style="color:#545459;">PREVIOUSLY ALLOCATED RESOURCES ($K)</h6>
            </div>
            <div   class="em-c-toolbar__item">
              <!---->
            </div>
          </div>

          <div   class=" em-c-table-object__body-inner" style="overflow:auto; direction:rtl;">
            <table   class="em-c-table scroll" style="overflow-x: hidden;">
              <thead   class="em-c-table__header " style="display:block;">
                <tr   class="em-c-table__header-row scroll-left">
                  <th   class="em-c-table__header-cell  fixed-header" scope="col" style="width:6.3%;"></th>
                  <th   class="em-c-table__header-cell  fixed-header " scope="col" style="width:6.3%;">
                    <button   class="em-c-btn--bare">
                      <!---->
                        <svg   class="em-c-icon em-c-icon--tiny em-c-icon--green">
                          <use   xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                        </svg>
                      
                      <!---->
                    </button>
                    <span   class="em-u-font-size-small-2">AR#</span>
                  </th>
                  <th   class="em-c-table__header-cell  fixed-header" scope="col" style="width:6.3%;">
                    <button   class="em-c-btn--bare">
                      <!---->
                        <svg   class="em-c-icon em-c-icon--tiny em-c-icon--green">
                          <use   xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                        </svg>
                      
                      <!---->
                    </button>
                    <span   class="em-u-font-size-small-2">QUANTITY</span>
                  </th>
                  <th   class="em-c-table__header-cell  fixed-header" scope="col" style="width:6.3%;">
                    <button   class="em-c-btn--bare">
                      <!---->
                        <svg   class="em-c-icon em-c-icon--tiny em-c-icon--green">
                          <use   xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                        </svg>
                      
                      <!---->
                    </button>
                    <span   class="em-u-font-size-small-2">LAST APPROVED AMOUNT</span>
                  </th>
                  <th   class="em-c-table__header-cell  fixed-header" scope="col" style="width:6.3%;">
                    <button   class="em-c-btn--bare">
                      <!---->
                        <svg   class="em-c-icon em-c-icon--tiny em-c-icon--green">
                          <use   xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                        </svg>
                      
                      <!---->
                    </button>
                    <span   class="em-u-font-size-small-2">ACTUALS</span>
                  </th>
                  <th   class="em-c-table__header-cell fixed-header" scope="col" style="width:6.3%;">
                    <button   class="em-c-btn--bare">
                      <!---->
                        <svg   class="em-c-icon em-c-icon--tiny em-c-icon--green">
                          <use   xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                        </svg>
                      
                      <!---->
                    </button>
                    <span   class="em-u-font-size-small-2">NEW CAPITAL ALLOCATION</span>
                  </th>
                  <th   class="em-c-table__header-cell fixed-header" scope="col" style="width:6.3%;">
                    <button   class="em-c-btn--bare">
                      <!---->
                        <svg   class="em-c-icon em-c-icon--tiny em-c-icon--green">
                          <use   xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                        </svg>
                      
                      <!---->
                    </button>
                    <span   class="em-u-font-size-small-2">NEW QUANTITY</span>
                  </th>
                  <th   class="em-c-table__header-cell fixed-header" scope="col" style="width:6.3%;">
                      <button   class="em-c-btn--bare">
                        <!---->
                          <svg   class="em-c-icon em-c-icon--tiny em-c-icon--green">
                            <use   xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                          </svg>
                        
                        <!---->
                      </button>
                      <span   class="em-u-font-size-small-2">STATUS</span>
                  </th>
                  <th   class="em-c-table__header-cell fixed-header" scope="col" style="width:6.3%;">
                    <button   class="em-c-btn--bare">
                      <!---->
                        <svg   class="em-c-icon em-c-icon--tiny em-c-icon--green">
                          <use   xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                        </svg>
                      
                      <!---->
                    </button>
                    <span   class="em-u-font-size-small-2">COMPANY</span>
                  </th>
                  <th   class="em-c-table__header-cell fixed-header " scope="col" style="width:6.3%;">
                    <button   class="em-c-btn--bare">
                      <!---->
                        <svg   class="em-c-icon em-c-icon--tiny em-c-icon--green">
                          <use   xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                        </svg>
                      
                      <!---->
                    </button>
                    <span   class="em-u-font-size-small-2">COUNTRY</span>
                  </th>
                  <th   class="em-c-table__header-cell fixed-header" scope="col" style="width:6.3%;">
                    <button   class="em-c-btn--bare">
                      <!---->
                        <svg   class="em-c-icon em-c-icon--tiny em-c-icon--green">
                          <use   xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                        </svg>
                      
                      <!---->
                    </button>
                    <span   class="em-u-font-size-small-2"> WBS</span>
                  </th>
                  <th   class="em-c-table__header-cell fixed-header" scope="col" style="width:6.3%;">
                    <button   class="em-c-btn--bare">
                      <!---->
                        <svg   class="em-c-icon em-c-icon--tiny em-c-icon--green">
                          <use   xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                        </svg>
                      
                      <!---->
                    </button>
                    <span   class="em-u-font-size-small-2">NETWORK ORDER</span>
                  </th>

                  <th   class="em-c-table__header-cell fixed-header" scope="col" style="width:6.3%;">
                    <button   class="em-c-btn--bare">
                      <!---->
                        <svg   class="em-c-icon em-c-icon--tiny em-c-icon--green">
                          <use   xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                        </svg>
                      
                      <!---->
                    </button>
                    <span   class="em-u-font-size-small-2">PURCHASE ORG COST CENTER</span>
                  </th>
                  <th   class="em-c-table__header-cell fixed-header" scope="col" style="width:6.3%;">
                    <button   class="em-c-btn--bare">
                      <!---->
                        <svg   class="em-c-icon em-c-icon--tiny em-c-icon--green">
                          <use   xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                        </svg>
                      
                      <!---->
                    </button>
                    <span   class="em-u-font-size-small-2">TYPE</span>
                  </th>
                  <th   class="em-c-table__header-cell fixed-header" scope="col" style="width:6.3%;">
                    <button   class="em-c-btn--bare">
                      <!---->
                        <svg   class="em-c-icon em-c-icon--tiny em-c-icon--green">
                          <use   xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                        </svg>
                      
                      <!---->
                    </button>
                    <span   class="em-u-font-size-small-2">ASSET DESCRIPTION</span>
                  </th>
                  <th   class="em-c-table__header-cell fixed-header" scope="col" style="width:6.3%;">
                  </th>
                </tr>
                
              </thead>
              
              <tbody   class="em-c-table__body">
                <!----><tr   class="view-only em-c-table__row scroll-left" style="width:100%!important; height:100%!important;">
                  <td   class="em-c-table__cell" colspan="" style="width:6.3%;">
                    <!---->
                    <!----><a   class="allocation-action-view custom-text-link pointer">View</a>
                  </td>
                  <td   class="em-c-table__cell " colspan="" style="width:6.3%;">14242001
                  </td>
                  <td   class="em-c-table__cell em-u-text-align-right  " colspan="" style="width:6.3%;">64
                  </td>
                  <td   class="em-c-table__cell em-u-text-align-right  " colspan="" style="width:6.3%;">607.00
                  </td>
                  <td   class="em-c-table__cell em-u-text-align-right  " colspan="" style="width:6.3%;">606.20
                  </td>
                    <td   class="em-c-table__cell em-u-text-align-right  " colspan="" style="width:6.3%;">607.00
                    </td>
                      <td   class="em-c-table__cell em-u-text-align-right  " colspan="" style="width:6.3%;;">64.00
                      </td>
                    
                  <td   colspan="" style="width:6.3%;" class="em-c-table__cell  em-u-text-align-center allocation-status-Closed">Closed
                    </td>
                  <td   class="em-c-table__cell em-u-text-align-center " colspan="" style=" width:6.3%; word-wrap:break-word;">0970 - ExxonMobil Global Services Company
                  </td>
                  <td   class="em-c-table__cell em-u-text-align-center " colspan="" style="width:6.3%;;">
                  </td>
                  <td   class="em-c-table__cell em-u-text-align-center " colspan="" style="width:6.3%;">KC.14.048.C.001
                  </td>
                  <td   class="em-c-table__cell em-u-text-align-center " colspan="" style="width:6.3%;">
                  </td>
                  <td   class="em-c-table__cell em-u-text-align-center " colspan="" style="width:6.3%;">C0802MRU90
                  </td>
                  <td   class="em-c-table__cell em-u-text-align-center ellipsis " colspan="" style="width:6.3%;">Software
                  </td>
                  <td   class="em-c-table__cell em-u-text-align-center ellipsis " colspan="" style="width:6.3%; ">Hana Licenses
                  </td>
                  <td   class="em-c-table__cell  ellipsis " colspan="" style="width:6.3%;">
                    <!---->
                  </td>
                </tr><tr   class="view-only em-c-table__row scroll-left" style="width:100%!important; height:100%!important;">
                  <td   class="em-c-table__cell" colspan="" style="width:6.3%;">
                    <!---->
                    <!----><a   class="allocation-action-view custom-text-link pointer">View</a>
                  </td>
                  <td   class="em-c-table__cell " colspan="" style="width:6.3%;">14242002
                  </td>
                  <td   class="em-c-table__cell em-u-text-align-right  " colspan="" style="width:6.3%;">12
                  </td>
                  <td   class="em-c-table__cell em-u-text-align-right  " colspan="" style="width:6.3%;">1,645.00
                  </td>
                  <td   class="em-c-table__cell em-u-text-align-right  " colspan="" style="width:6.3%;">1,642.86
                  </td>
                  <td   class="em-c-table__cell em-u-text-align-right " colspan="" style="width:6.3%;">
                  <!---->
                    <td   class="em-c-table__cell em-u-text-align-right  " colspan="" style="width:6.3%;">1,645.00
                    </td>
                  
                </td>
                  <!---->

                  <td   class="em-c-table__cell em-u-text-align-right " colspan="" style="width:6.3%;">
                    <!---->
                      <td   class="em-c-table__cell em-u-text-align-right  " colspan="" style="width:6.3%;;">12.00
                      </td>
                    
                    <!---->
                  </td>
                  <td   colspan="" style="width:6.3%;" class="em-c-table__cell  em-u-text-align-center allocation-status-Closed">Closed
                    </td>
                  <td   class="em-c-table__cell em-u-text-align-center " colspan="" style=" width:6.3%; word-wrap:break-word;">0970 - ExxonMobil Global Services Company
                  </td>
                  <td   class="em-c-table__cell em-u-text-align-center " colspan="" style="width:6.3%;;">
                  </td>
                  <td   class="em-c-table__cell em-u-text-align-center " colspan="" style="width:6.3%;">KC.14.048.C.002
                  </td>
                  <td   class="em-c-table__cell em-u-text-align-center " colspan="" style="width:6.3%;">
                  </td>
                  <td   class="em-c-table__cell em-u-text-align-center " colspan="" style="width:6.3%;">C0802FMS93
                  </td>
                  <td   class="em-c-table__cell em-u-text-align-center ellipsis " colspan="" style="width:6.3%;">Hardware
                  </td>
                  <td   class="em-c-table__cell em-u-text-align-center ellipsis " colspan="" style="width:6.3%; ">HANA Acceptance
                  </td>
                  <td   class="em-c-table__cell  ellipsis " colspan="" style="width:6.3%;">
                    <!---->
                  </td>
                </tr><tr   class="view-only em-c-table__row scroll-left" style="width:100%!important; height:100%!important;">
                  <td   class="em-c-table__cell" colspan="" style="width:6.3%;">
                    <!---->
                    <!----><a   class="allocation-action-view custom-text-link pointer">View</a>
                  </td>
                  <td   class="em-c-table__cell " colspan="" style="width:6.3%;">14242003
                  </td>
                  <td   class="em-c-table__cell em-u-text-align-right  " colspan="" style="width:6.3%;">3
                  </td>
                  <td   class="em-c-table__cell em-u-text-align-right  " colspan="" style="width:6.3%;">1,272.945
                  </td>
                  <td   class="em-c-table__cell em-u-text-align-right  " colspan="" style="width:6.3%;">1,043.70
                  </td>
                  <td   class="em-c-table__cell em-u-text-align-right " colspan="" style="width:6.3%;">
                  <!---->
                    <td   class="em-c-table__cell em-u-text-align-right  " colspan="" style="width:6.3%;">1,272.945
                    </td>
                  
                </td>
                  <!---->

                  <td   class="em-c-table__cell em-u-text-align-right " colspan="" style="width:6.3%;">
                    <!---->
                      <td   class="em-c-table__cell em-u-text-align-right  " colspan="" style="width:6.3%;;">3.00
                      </td>
                    
                    <!---->
                  </td>
                  <td   colspan="" style="width:6.3%;" class="em-c-table__cell  em-u-text-align-center allocation-status-Closed">Closed
                    </td>
                  <td   class="em-c-table__cell em-u-text-align-center " colspan="" style=" width:6.3%; word-wrap:break-word;">0970 - ExxonMobil Global Services Company
                  </td>
                  <td   class="em-c-table__cell em-u-text-align-center " colspan="" style="width:6.3%;;">
                  </td>
                  <td   class="em-c-table__cell em-u-text-align-center " colspan="" style="width:6.3%;">KC.14.048.C.005
                  </td>
                  <td   class="em-c-table__cell em-u-text-align-center " colspan="" style="width:6.3%;">
                  </td>
                  <td   class="em-c-table__cell em-u-text-align-center " colspan="" style="width:6.3%;">C0802FMS34
                  </td>
                  <td   class="em-c-table__cell em-u-text-align-center ellipsis " colspan="" style="width:6.3%;">Hardware
                  </td>
                  <td   class="em-c-table__cell em-u-text-align-center ellipsis " colspan="" style="width:6.3%; ">HANA Acceptance
                  </td>
                  <td   class="em-c-table__cell  ellipsis " colspan="" style="width:6.3%;">
                    <!---->
                  </td>
                </tr><tr   class="view-only em-c-table__row scroll-left" style="width:100%!important; height:100%!important;">
                  <td   class="em-c-table__cell" colspan="" style="width:6.3%;">
                    <!---->
                    <!----><a   class="allocation-action-view custom-text-link pointer">View</a>
                  </td>
                  <td   class="em-c-table__cell " colspan="" style="width:6.3%;">14242004
                  </td>
                  <td   class="em-c-table__cell em-u-text-align-right  " colspan="" style="width:6.3%;">0
                  </td>
                  <td   class="em-c-table__cell em-u-text-align-right  " colspan="" style="width:6.3%;">8,000.00
                  </td>
                  <td   class="em-c-table__cell em-u-text-align-right  " colspan="" style="width:6.3%;">7,828.52
                  </td>
                  <td   class="em-c-table__cell em-u-text-align-right " colspan="" style="width:6.3%;">
                  <!---->
                    <td   class="em-c-table__cell em-u-text-align-right  " colspan="" style="width:6.3%;">8,000.00
                    </td>
                  
                </td>
                  <!---->

                  <td   class="em-c-table__cell em-u-text-align-right " colspan="" style="width:6.3%;">
                    <!---->
                      <td   class="em-c-table__cell em-u-text-align-right  " colspan="" style="width:6.3%;;">0.00
                      </td>
                    
                    <!---->
                  </td>
                  <td   colspan="" style="width:6.3%;" class="em-c-table__cell  em-u-text-align-center allocation-status-Closed">Closed
                    </td>
                  <td   class="em-c-table__cell em-u-text-align-center " colspan="" style=" width:6.3%; word-wrap:break-word;">0970 - ExxonMobil Global Services Company
                  </td>
                  <td   class="em-c-table__cell em-u-text-align-center " colspan="" style="width:6.3%;;">
                  </td>
                  <td   class="em-c-table__cell em-u-text-align-center " colspan="" style="width:6.3%;">KC.14.048.C.004
                  </td>
                  <td   class="em-c-table__cell em-u-text-align-center " colspan="" style="width:6.3%;">
                  </td>
                  <td   class="em-c-table__cell em-u-text-align-center " colspan="" style="width:6.3%;">C082D5200
                  </td>
                  <td   class="em-c-table__cell em-u-text-align-center ellipsis " colspan="" style="width:6.3%;">Capitalized Staffing
                  </td>
                  <td   class="em-c-table__cell em-u-text-align-center ellipsis " colspan="" style="width:6.3%; ">Cap Staffing
                  </td>
                  <td   class="em-c-table__cell  ellipsis " colspan="" style="width:6.3%;">
                    <!---->
                  </td>
                </tr><tr   class="view-only em-c-table__row scroll-left" style="width:100%!important; height:100%!important;">
                  <td   class="em-c-table__cell" colspan="" style="width:6.3%;">
                    <!---->
                    <!----><a   class="allocation-action-view custom-text-link pointer">View</a>
                  </td>
                  <td   class="em-c-table__cell " colspan="" style="width:6.3%;">14242005
                  </td>
                  <td   class="em-c-table__cell em-u-text-align-right  " colspan="" style="width:6.3%;">8
                  </td>
                  <td   class="em-c-table__cell em-u-text-align-right  " colspan="" style="width:6.3%;">217.00
                  </td>
                  <td   class="em-c-table__cell em-u-text-align-right  " colspan="" style="width:6.3%;">216.72
                  </td>
                  <td   class="em-c-table__cell em-u-text-align-right " colspan="" style="width:6.3%;">
                  <!---->
                    <td   class="em-c-table__cell em-u-text-align-right  " colspan="" style="width:6.3%;">217.00
                    </td>
                  
                </td>
                  <!---->

                  <td   class="em-c-table__cell em-u-text-align-right " colspan="" style="width:6.3%;">
                    <!---->
                      <td   class="em-c-table__cell em-u-text-align-right  " colspan="" style="width:6.3%;;">8.00
                      </td>
                    
                    <!---->
                  </td>
                  <td   colspan="" style="width:6.3%;" class="em-c-table__cell  em-u-text-align-center allocation-status-Closed">Closed
                    </td>
                  <td   class="em-c-table__cell em-u-text-align-center " colspan="" style=" width:6.3%; word-wrap:break-word;">0970 - ExxonMobil Global Services Company
                  </td>
                  <td   class="em-c-table__cell em-u-text-align-center " colspan="" style="width:6.3%;;">
                  </td>
                  <td   class="em-c-table__cell em-u-text-align-center " colspan="" style="width:6.3%;">KC.14.048.C.006
                  </td>
                  <td   class="em-c-table__cell em-u-text-align-center " colspan="" style="width:6.3%;">
                  </td>
                  <td   class="em-c-table__cell em-u-text-align-center " colspan="" style="width:6.3%;">C0802ANL05
                  </td>
                  <td   class="em-c-table__cell em-u-text-align-center ellipsis " colspan="" style="width:6.3%;">Software
                  </td>
                  <td   class="em-c-table__cell em-u-text-align-center ellipsis " colspan="" style="width:6.3%; ">Tableau Server Licenses
                  </td>
                  <td   class="em-c-table__cell  ellipsis " colspan="" style="width:6.3%;">
                    <!---->
                  </td>
                </tr>

                
              </tbody>
              <tfoot   class="em-c-table__footer scroll-left">
                <tr   class="em-c-table__footer-row custom-table-background em-u-font-size-med-2 ">
                  <td   class="em-c-table__footer-cell em-u-font-size-med-2" colspan="5" style="width: 90%;">TOTAL :</td>
                  <td   class="em-c-table__footer-cell em-u-font-size-med-2 ">$11,741.95</td>
                  <td   class="em-c-table__footer-cell em-u-font-size-med-2 " colspan="10"> </td>
                </tr>
              </tfoot>

            </table>
            
          </div>
        </div>
      </div>

    </div>

   


</asp:Content>
