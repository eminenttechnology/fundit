﻿<%@ Page Language="C#" MasterPageFile="~/_includes/MasterPageWithPrint.master" AutoEventWireup="true"%>

<%@ Register Src="~/UserControls/ARCloseRequestProjectInfo.ascx" TagPrefix="uc1" TagName="RequestProjectInfo" %>
<%@ Register Src="~/UserControls/ARCloseRequest.ascx" TagPrefix="uc1" TagName="ARCloseRequest" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="em-l-container">
    <div class="em-c-tabs em-u-margin-bottom-none em-c-tabs--underline em-js-tabs">
        <ul class="em-c-tabs__list" style="width: 100%">
            <li class="em-c-tabs__item" style="width: 250px">
                <a href="#tab-panel-1" class="em-c-tabs__link em-js-tab ">Project Information</a>
            </li>
            <!-- end em-c-tabs__item -->
            <li class="em-c-tabs__item" style="width: 250px">
                <a href="#tab-panel-2" class="em-c-tabs__link em-js-tab ">Appropriation Request</a>
            </li>
            <!-- end em-c-tabs__item -->
        </ul>
        <!-- end em-c-tabs__list -->
        <div class="em-c-tabs__body em-c-tabs__body--no-border em-u-padding-bottom-none">
            <div class="em-c-tabs__panel em-js-tabs-panel" id="tab-panel-1">
                <uc1:RequestProjectInfo runat="server" ID="RequestProjectInfo" />
            </div>
            <div class="em-c-tabs__panel em-js-tabs-panel" id="tab-panel-2">
                <uc1:ARCloseRequest runat="server" ID="ARCloseRequest" />
            </div>

        </div>
    </div>
    <!-- end em-c-tabs -->
</div>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="footer_section" runat="server">
    <div class="align-right" style="width: auto; float: right;">
        <div class="em-c-btn-group em-c-btn-group--responsive">
            <!---->
            <button class="em-c-btn em-c-btn--primary" id="closeRequest" name="closeRequest">
                <span class="em-c-btn__text">Close AR Request</span>
            </button>
        </div>
    </div>
</asp:Content>