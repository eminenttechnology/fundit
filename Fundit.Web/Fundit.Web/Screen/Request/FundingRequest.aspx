﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_includes/MasterPageWithPrint.master" AutoEventWireup="true" %>

<%@ Register Src="~/UserControls/RequestProjectInfo.ascx" TagPrefix="uc1" TagName="RequestProjectInfo" %>
<%@ Register Src="~/UserControls/RequestAllocationDetails.ascx" TagPrefix="uc1" TagName="RequestAllocationDetails" %>
<%@ Register Src="~/UserControls/RequestCorporate.ascx" TagPrefix="uc1" TagName="RequestCorporate" %>
<%@ Register Src="~/UserControls/RequestFundingRequest.ascx" TagPrefix="uc1" TagName="RequestFundingRequest" %>





<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="em-l-container">
        <div class="em-c-tabs em-u-margin-bottom-none em-c-tabs--underline em-js-tabs">
            <ul class="em-c-tabs__list" style="width: 100%">
                <li class="em-c-tabs__item" style="width: 250px">
                    <a href="#tab-panel-1" class="em-c-tabs__link em-js-tab ">Project Information</a>
                </li>
                <!-- end em-c-tabs__item -->
                <li class="em-c-tabs__item" style="width: 250px">
                    <a href="#tab-panel-2" class="em-c-tabs__link em-js-tab ">Funding Request</a>
                </li>
                <!-- end em-c-tabs__item -->
                <li class="em-c-tabs__item" style="width: 250px">
                    <a href="#tab-panel-3" class="em-c-tabs__link em-js-tab ">Allocation Detail</a>
                </li>
                <!-- end em-c-tabs__item -->
                <li class="em-c-tabs__item" style="width: 250px">
                    <a href="#tab-panel-4" class="em-c-tabs__link em-js-tab ">Corporate BIS</a>
                </li>

            </ul>
            <!-- end em-c-tabs__list -->
            <div class="em-c-tabs__body em-c-tabs__body--no-border em-u-padding-bottom-none">
                <div class="em-c-tabs__panel em-js-tabs-panel" id="tab-panel-1">
                    <uc1:RequestProjectInfo runat="server" ID="RequestProjectInfo" />
                </div>
                <div class="em-c-tabs__panel em-js-tabs-panel" id="tab-panel-2">
                    <uc1:RequestFundingRequest runat="server" ID="RequestFundingRequest" />

                </div>
                <div class="em-c-tabs__panel em-js-tabs-panel" id="tab-panel-3">
                    <uc1:RequestAllocationDetails runat="server" ID="RequestAllocationDetails" />
                </div>
                <div class="em-c-tabs__panel em-js-tabs-panel" id="tab-panel-4">
                    <uc1:RequestCorporate runat="server" ID="RequestCorporate" />

                </div>

            </div>
        </div>
        <!-- end em-c-tabs -->

    </div>
    <script>

        function changeStateDropdown(value) {
            if (value === "0970" || value === "0910") {
                document.getElementById("btn-state-dropdown").style.visibility = 'visible';
                document.getElementById("btn-state-div").style.visibility = 'visible';
                document.getElementById("btn-state-label").style.visibility = 'visible';
                document.getElementById("support-org-label").style.visibility = 'visible';
                document.getElementById("support-org-input").style.visibility = 'visible';

                document.getElementById("support-org-input").style.removeProperty("display");
                document.getElementById("btn-state-dropdown").style.removeProperty("display");
                document.getElementById("btn-city-dropdown").style.removeProperty("display");
                document.getElementById("city-label").style.removeProperty("display");
                document.getElementById("btn-state-label").style.removeProperty("display");
                 document.getElementById("installation-location-label").style.removeProperty("display");
                document.getElementById("show-installation-location").style.removeProperty("display");
                document.getElementById("installation-location-div-block").style.removeProperty("display");
                document.getElementById("support-org-label").style.removeProperty("display");
                document.getElementById("support-org-input").style.removeProperty("display");
            }

            else {
                document.getElementById("btn-state-dropdown").style.display = 'none';
                document.getElementById("btn-city-dropdown").style.display = 'none';
                document.getElementById("city-label").style.display = 'none';
                document.getElementById("btn-state-label").style.display = 'none';
                document.getElementById("installation-location-label").style.display = 'none';
                document.getElementById("show-installation-location").style.display = 'none';
                document.getElementById("installation-location-div-block").style.display = 'none';
                document.getElementById("support-org-label").style.display = 'none';
                document.getElementById("support-org-input").style.display = 'none';


            }
        }

        $(document).ready(function () {
            $('#btn-state-dropdown').change(function () {
                var Texas = ["Select city", "Austin", "Dallas", "Houston" ];
                var Alaska = ["Select city","Anchorage", "Fairbanks", "Juneau", "Sitka", "Ketchikan"];
                var Arizona = ["Select city", "Benson", "Buckeye", "Bullhead City"];
                var Arkansas = ["Select city","Springdale", "Fort Smith", "Fayetteville", "Jonesboro"];

                var option = '';
                var str = "var SelectedState = " + $(this)[0].value;
                eval(str);
                for (var i = 0; i < SelectedState.length; i++) {
                    option += '<option value="' + SelectedState[i] + '">' + SelectedState[i] + '</option>';
                }

                var Cities = "<select style='width: 100%; ' id= 'btn-city-select'>" + option + "</select>";


                document.getElementById("btn-city-dropdown").style.visibility = 'visible';
                document.getElementById("city-label").style.visibility = 'visible';
                $('#btn-city-dropdown').html('');
                $('#btn-city-dropdown').append(Cities);

            });

            $('#btn-city-dropdown').change(function () {

                var Fairbanks = ["Select Installation Location", "99701", "99702", "99705", "99707"]
                var Austin = ["Select Installation Location", "73301", "73344"];
                var Dallas = ["Select Installation Location", "75001", "75034"];
                var Houston = ["Select Installation Location", "73501", "73344", "75001"];
                var Anchorage = ["Select Installation Location", "99501", "99502", "99503", "99504"];
                var Benson = ["Select Installation Location", "85117", "85119", "85120"];
                var Springdale = ["Select Installation Location", "72762", "72764", "72765"];
                var option = '';
                var installationOption = '';
                var childElement = $(this)[0].childNodes[0];
                
                var str = "var SelectedInstallationLocation = " + childElement.value;
                eval(str);
                for (var i = 0; i < SelectedInstallationLocation.length; i++) {
                    option += '<option value="' + SelectedInstallationLocation[i] + '">' + SelectedInstallationLocation[i] + '</option>';
                }
                var InstallationLocation = "<select style='width: 100%; visibility: visible;'>" + option + "</select>";

                document.getElementById("installation-location-label").style.visibility = 'visible';
                document.getElementById("show-installation-location").style.visibility = 'visible';
              
                $('#show-installation-location').html('');
                $('#show-installation-location').append(InstallationLocation);

            });
            
        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="footer_section" runat="server">
    <div class="align-right" style="width: auto; float: right;">
        <div class="em-c-btn-group em-c-btn-group--responsive">
            <!---->
            <button class="em-c-btn em-c-btn--primary" id="cancelDraft" name="cancel-draft">
                <span class="em-c-btn__text">Cancel Draft</span>
            </button>
            <!---->
            <!---->
            <button class="em-c-btn" id="submitReview" name="submitReview">
                <span class="em-c-btn__text">Submit for Review</span>
            </button>
            <!---->
            <!---->
            <!---->
            <!---->
            <!---->
        </div>
    </div>
</asp:Content>
