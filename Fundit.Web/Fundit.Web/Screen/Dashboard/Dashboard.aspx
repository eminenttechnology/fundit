﻿<%@ Page Language="C#" MasterPageFile="~/_includes/MasterPage.master" AutoEventWireup="true" CodeBehind="Dashboard.aspx.cs" Inherits="Fundit.Web.Screen.Dashboard.Dashboard" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="em-l-container">
        <div>

            <%--<div class="em-c-btn__text" style="width:200px;"><h3>Today’s Actions</h3></div>--%>
            <div class="em-u-text-align-right">

                <%-- <b><span class="em-c-btn__text em-u-font-style-bold automation-title dashboard-title" style=" height:30px; width:167px; float: left !important;">Today’s Actions</span><b />
                --%>
                <div class="em-c-page-header em-c-page-header--small">
                    <h3 class="em-c-page-header__title em-u-font-style-bold automation-subtitle" style="float: left !important;">Today’s Actions</h3>

                </div>
                <li class="em-c-primary-nav__item em-c-primary-nav__item--search-trigger">
                                <button class="em-c-btn em-c-primary-nav__link  em-c-primary-nav__link--has-children em-js-nav-dropdown-trigger">
                                    <span> Create New Request</span>
                                    
                                </button>
                                <ul class="em-c-primary-nav__sublist em-js-nav-dropdown " style=" background: #0c69b0;top: 43px; width: 14.71%;">
                                    <li class="em-c-primary-nav__subitem">
                                        <a href="../Request/FundingRequest.aspx" class="em-c-primary-nav__sublink">Funding Request</a>
                                    </li>
                                    <!-- end em-c-nav__sublist__item -->
                                    <li class="em-c-primary-nav__subitem">
                                        <a href="../Request/ReallocationRequest.aspx" class="em-c-primary-nav__sublink">Reallocation Request</a>
                                    </li>
                                    <!-- end em-c-nav__sublist__item -->
                                    <li class="em-c-primary-nav__subitem">
                                        <a href="../IO/Create.aspx" class="em-c-primary-nav__sublink">Internal Order Request</a>
                                    </li>

                                </ul>
                    </li>
        <%--        <button class="em-c-btn ">
                    <span class="em-c-btn__text">Re-Allocate Funds</span>
                </button>--%>
            </div>

        </div>

        <hr />

        <div id="request-status">

            <div class="em-l-grid em-l-grid--5up ">
                <div class="em-l-grid__item">
                    <button class=" em-c-btn em-c-tile em-c-btn-deepblue" style="background-color: #112 !important;">
                        <span class="em-c-btn__text" style="color: #fff !important;"><span class="em-c-tile__headline">4</span> &nbsp; &nbsp; Drafting</span>
                    </button>
                </div>
                <!-- end em-l-grid__item -->
                <div class="em-l-grid__item">
                    <button class=" em-c-btn em-c-tile em-c-btn-deepblue" style="background-color: #3a397b !important;">
                        <span class="em-c-btn__text" style="color: #fff !important;"><span class="em-c-tile__headline">1</span> &nbsp; &nbsp; Reviewing</span>
                    </button>
                </div>
                <!-- end em-l-grid__item -->
                <div class="em-l-grid__item">
                    <button class=" em-c-btn em-c-tile em-c-btn-deepblue" style="background-color: #3a397b !important;">
                        <span class="em-c-btn__text" style="color: #fff !important;"><span class="em-c-tile__headline">2</span> &nbsp; &nbsp; Approving</span>
                    </button>
                </div>
                <!-- end em-l-grid__item -->
                <div class="em-l-grid__item">
                    <button class=" em-c-btn em-c-tile em-c-btn-deepblue" style="background-color: #3a397b !important;">
                        <span class="em-c-btn__text" style="color: #fff !important;"><span class="em-c-tile__headline">6</span> &nbsp; &nbsp; Configuring  </span>
                    </button>
                </div>
                <!-- end em-l-grid__item -->
                <div class="em-l-grid__item">
                    <button class=" em-c-btn em-c-tile em-c-btn-deepblue" style="background-color: #3a397b !important;">
                        <span class="em-c-btn__text" style="color: #fff !important;"><span class="em-c-tile__headline">8</span> &nbsp; &nbsp; Completed</span>
                    </button>
                </div>
                <!-- end em-l-grid__item -->
            </div>
            <!-- end em-l-grid -->
        </div>



        <div class="em-c-page-header em-c-page-header--small">
            <h3 class="em-c-page-header__title">Drafting</h3>
        </div>
        <hr />
        <div class="em-c-toolbar">
            <div class="em-c-field" style="margin-bottom: 0px">
                <div class="em-c-field__body">
                    <select class="em-c-select em-c-select">
                        <option value="">Sort By</option>
                        <option value="option-1">Work Name</option>
                        <option value="option-2">Status</option>
                    </select>
                </div>
            </div>
        </div>


        <div class="em-l-grid em-l-grid--4up ">
            <div class="em-l-grid__item">
                <div class="card-background-top-blue" style="height: 10px; background-color: #3190d9;"></div>
                <div class="em-c-card vcard" onclick="window.location.href='../Request/FundingRequest.aspx'">
                    <div class="em-c-card__body">
                        <div class="em-c-media-block em-c-media-block--small">
                            <!-- end em-c-media-block__media -->
                            <div class="em-c-media-block__body">
                                <h2 class="em-c-media-block__headline fn">66273
                                </h2>

                                <h3 class="em-c-media-block__desc"><span class='org'>Transactional Data Transparency (TDT) Project2-ROW</span></h3>
                                <br />
                                <h3 class="em-c-media-block__desc"><span class='org'>Current Cumulative $1,900.00K</span></h3>

                                <div class="em-c-media-block__desc"><span class='org'>Total Cost Outlook $1,900.00K</span></div>
                            </div>
                            <!-- end em-c-media-block__body -->
                        </div>
                        <!-- end em-c-media-block -->
                    </div>

                </div>
                <!-- end em-c-card -->
            </div>
            <!-- end em-l-grid__item -->
            <div class="em-l-grid__item" style="flex-direction: column; padding: .5rem;">
                <div class="card-background-top-blue" style="height: 10px;"></div>
                <div class="em-c-card vcard" onclick="window.location.href='../Request/ReallocationRequest.aspx'">
                    <div class="em-c-card__body">
                        <div class="em-c-media-block em-c-media-block--small">
                            <!-- end em-c-media-block__media -->
                            <div class="em-c-media-block__body">
                                <h2 class="em-c-media-block__headline fn">72310 
                                </h2>

                                <h3 class="em-c-media-block__desc"><span class='org'>2018 EMRE VB6 Tool Re-Platform R7.00401.1.01</span></h3>
                                <br />
                                <h3 class="em-c-media-block__desc"><span class='org'>Current Cumulative $11,100.00K</span></h3>

                                <div class="em-c-media-block__desc"><span class='org'>Total Cost Outlook $12,400.00K</span></div>
                            </div>
                            <!-- end em-c-media-block__body -->
                        </div>
                        <!-- end em-c-media-block -->
                    </div>

                </div>
                <!-- end em-c-card -->
            </div>
            <!-- end em-l-grid__item -->
            <div class="em-l-grid__item">
                <div class="card-background-top-blue" style="height: 10px; background-color: #3190d9;"></div>
                <div class="em-c-card vcard" onclick="window.location.href='../Request/ARSetupRequest.aspx'">
                    <div class="em-c-card__body">
                        <div class="em-c-media-block em-c-media-block--small">
                            <!-- end em-c-media-block__media -->
                            <div class="em-c-media-block__body">
                                <h2 class="em-c-media-block__headline fn">69353
                                </h2>

                                <h3 class="em-c-media-block__desc"><span class='org'>GPASS - Implementation 2018 - 2019</span></h3>
                                <br />
                                <h3 class="em-c-media-block__desc"><span class='org'>Current Cumulative $12,235.70K</span></h3>

                                <div class="em-c-media-block__desc"><span class='org'>Total Cost Outlook $7,950.50K</span></div>
                            </div>
                            <!-- end em-c-media-block__body -->
                        </div>
                        <!-- end em-c-media-block -->
                    </div>

                </div>
                <!-- end em-c-card -->
            </div>
            <!-- end em-l-grid__item -->
            <div class="em-l-grid__item">
                <div class="card-background-top-blue" style="height: 10px; background-color: #3190d9;"></div>
                <div class="em-c-card vcard" onclick="window.location.href='../Request/IORequest.aspx'">
                    <div class="em-c-card__body">
                        <div class="em-c-media-block em-c-media-block--small">
                            <!-- end em-c-media-block__media -->
                            <div class="em-c-media-block__body">
                                <h2 class="em-c-media-block__headline fn">56564 
                                </h2>

                                <h3 class="em-c-media-block__desc"><span class='org'>DTAM-Nigeria-EEPNL-UEHM</span></h3>
                                <br />
                                <h3 class="em-c-media-block__desc"><span class='org'>Current Cumulative $23,900.00K</span></h3>

                                <div class="em-c-media-block__desc"><span class='org'>Total Cost Outlook $8,670.00K</span></div>
                            </div>
                            <!-- end em-c-media-block__body -->
                        </div>
                        <!-- end em-c-media-block -->
                    </div>

                </div>
                <!-- end em-c-card -->
            </div>
            <!-- end em-l-grid__item -->
        </div>
        <!-- end em-l-grid -->
    </div>
</asp:Content>

