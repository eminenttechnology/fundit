﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_includes/MasterPage.master" AutoEventWireup="true" CodeBehind="Search.aspx.cs" Inherits="Fundit.Web.Screen.Request.Search" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="em-l-container">
        <div class="em-c-page-header em-c-page-header--small">
            <h1 class="em-c-page-header__title">Work Search Criteria</h1>
        </div>
        <div class="em-c-collapsible-toolbar__panel em-js-toolbar-panel">
            <div class="em-c-toolbar">
                <div class="em-l-grid em-l-grid--5up ">
                    <div class="em-l-grid__item">
                        <div class="em-c-field em-c-field--small ">
                            <label for="" class="em-c-field__label">Work Id</label>
                            <div class="em-c-field__body">
                                <input type="text" id="" class="em-c-input" value="" />
                            </div>
                        </div>
                    </div>
                    <div class="em-l-grid__item">
                        <div class="em-c-field em-c-field--small">
                            <label for="" class="em-c-field__label">Work Name</label>
                            <div class="em-c-field__body">
                                <input type="text" id="" class="em-c-input" value="" />
                            </div>
                        </div>
                    </div>

                    <div class="em-l-grid__item">
                        <div class="em-c-field em-c-field--small">
                            <label for="" class="em-c-field__label">Project Manager</label>
                            <div class="em-c-field__body">
                                <input type="text" id="" class="em-c-input" value="" />
                            </div>
                        </div>
                    </div>

                    <div class="em-l-grid__item">
                        <div class="em-c-field em-c-field--small">
                            <label for="" class="em-c-field__label">Work Name</label>
                            <div class="em-c-field__body">
                                <input type="text" id="" class="em-c-input" value="" />
                            </div>
                        </div>
                    </div>
                    <div class="em-l-grid__item">
                        <div class="em-c-field ">
                            <button class="em-c-btn" style="margin-top: 17px;">
                                <span class="em-c-btn__text em-c-icon">Search</span>
                            </button>
                            <!-- end em-c-btn -->
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- end em-l-container -->

        <div>
            <div class="em-c-table-object ">
                <div class="em-c-table-object__header">
                </div>
                <!--end em-c-table-object__header-->
                <div class="em-c-table-object__body">
                    <div class="em-c-table-object__body-inner">
                        <table class="em-c-table ">
                            <thead class="em-c-table__header">
                                <tr class="em-c-table__header-row">
                                    <th scope="col" class="em-c-table__header-cell em-u-width-5">Work Id</th>
                                    <th scope="col" class="em-c-table__header-cell ">Work Name</th>
                                    <th scope="col" class="em-c-table__header-cell ">Project Manager</th>
                                    <th scope="col" class="em-c-table__header-cell ">Controller</th>
                                    <th scope="col" class="em-c-table__header-cell ">Approved Amount</th>
                                </tr>
                                <!-- em-c-table__header-row -->
                            </thead>
                            <!-- end em-c-table__header -->
                            <tbody class="em-c-table__body ">
                                <tr class="em-c-table__row " onclick="window.location.href='ProjectInfo.aspx'">

                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">66273</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Transactional Data Transparency (TDT) Project2-ROW</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Tertuliano, Jayme C</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Doe, John</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">$1,900K</td>

                                </tr>
                                <tr class="em-c-table__row " onclick="window.location.href='ProjectInfo.aspx'">

                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">72324 </td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">2018 EMBSI Contracts & Agreements DB Application</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Rolim, Camila</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Hickman, Theo</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">$3,200K</td>

                                </tr>
                                <tr class="em-c-table__row " onclick="window.location.href='ProjectInfo.aspx'">

                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">73122</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">BPT1 tactical mobil App for NDG</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Courtright, Anne O</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Doe, John</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">$900K</td>

                                </tr>
                                <tr class="em-c-table__row " onclick="window.location.href='ProjectInfo.aspx'">

                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">72788</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Sitecore Advanced Capabilities</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Ellis, Chante M</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Chen, Vivian L</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">$1,500K</td>

                                </tr>
                                <tr class="em-c-table__row " onclick="window.location.href='ProjectInfo.aspx'">

                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">35582 </td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Satellites Field Development (SFD1)</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Walter, Andrea</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">Abete, Cindy M</td>
                                    <td class="em-c-table__cell em-js-cell em-js-cell-editable">$1890K</td>

                                </tr>

                            </tbody>
                            <!-- end em-c-table__body -->
                            <tfoot class="em-c-table__footer">
                                <tr class="em-c-table__footer-row">
                                </tr>
                            </tfoot>
                            <!-- end em-c-table__footer -->
                        </table>
                        <!--end em-c-table-->
                    </div>
                    <!--end em-c-table-object__body-inner-->
                </div>
                <!--end em-c-table-object__body-->
            </div>
            <!--end em-c-table-object-->
        </div>
    </div>
    <!-- end em-l-container -->
</asp:Content>
