﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_includes/MasterPageWithPrint.master" AutoEventWireup="true" %>


<%@ Register Src="~/UserControls/ProjectInfo.ascx" TagPrefix="uc1" TagName="RequestProjectInfo" %>
<%@ Register Src="~/UserControls/ProjectARs.ascx" TagPrefix="uc1" TagName="ProjectARs" %>
<%@ Register Src="~/UserControls/ProjectInternalOrders.ascx" TagPrefix="uc1" TagName="ProjectInternalOrders" %>
<%@ Register Src="~/UserControls/ProjectBudget.ascx" TagPrefix="uc1" TagName="ProjectBudget" %>
<%@ Register Src="~/UserControls/ProjectAllRequests.ascx" TagPrefix="uc1" TagName="ProjectAllRequests" %>





<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">



    <div class="em-l-container">

        <div class="em-c-alert em-c-alert--warning" role="alert" style="height: 53px">
            <svg class="em-c-icon em-c-alert__icon" style="visibility: hidden">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../../images/em-icons.svg#icon-warning"></use>
            </svg>
            <div class="em-c-alert__body">
                There are 2 pending requests in progress
            </div>
            <div class="em-c-alert__actions">
                <button class="em-c-text-btn em-c-alert--warning">
                    View Pending Requests
                </button>
                <!-- end em-c-btn -->
            </div>
        </div>
        <!-- end em-c-alert -->

        <div class="em-c-tabs em-u-margin-bottom-none em-c-tabs--underline em-js-tabs">
            <ul class="em-c-tabs__list" style="width: 100%">
                <li class="em-c-tabs__item">
                    <a href="#tab-panel-1" class="em-c-tabs__link em-js-tab" style="background: #fff" onclick="change(1)">Project Information</a>
                </li>
                <!-- end em-c-tabs__item -->
                <li class="em-c-tabs__item">
                    <a href="#tab-panel-2" class="em-c-tabs__link em-js-tab" onclick="change(2)">Approved Budgets</a>
                </li>
                <!-- end em-c-tabs__item -->
                <li class="em-c-tabs__item">
                    <a href="#tab-panel-3" class="em-c-tabs__link em-js-tab" onclick="change(3)">Appropriation Requests</a>
                </li>
                <!-- end em-c-tabs__item -->
                <li class="em-c-tabs__item">
                    <a href="#tab-panel-4" class="em-c-tabs__link em-js-tab" onclick="change(4)">Internal Orders</a>
                </li>
                <!-- end em-c-tabs__item -->
                <li class="em-c-tabs__item">
                    <a href="#tab-panel-5" class="em-c-tabs__link em-js-tab" onclick="change(5)">All Requests</a>
                </li>

            </ul>
            <!-- end em-c-tabs__list -->
            <div class="em-c-tabs__body em-c-tabs__body--no-border em-u-padding-bottom-none">
                <div class="em-c-tabs__panel em-js-tabs-panel" id="tab-panel-1">
                    <uc1:RequestProjectInfo runat="server" ID="RequestProjectInfo" />
                </div>
                <div class="em-c-tabs__panel em-js-tabs-panel" id="tab-panel-2">
                    <uc1:ProjectBudget runat="server" ID="ProjectBudget" />

                </div>
                <div class="em-c-tabs__panel em-js-tabs-panel" id="tab-panel-3">
                    <uc1:ProjectARs runat="server" ID="ProjectARs" />
                </div>
                <div class="em-c-tabs__panel em-js-tabs-panel" id="tab-panel-4">
                    <uc1:ProjectInternalOrders runat="server" ID="ProjectInternalOrders" />

                </div>
                <div class="em-c-tabs__panel em-js-tabs-panel" id="tab-panel-5">
                    <uc1:ProjectAllRequests runat="server" ID="ProjectAllRequests" />

                </div>

            </div>


            <div class="em-c-modal em-js-modal em-is-closed" id="modal">
                <div class="em-c-modal__window em-js-modal-window">
                    <div class="em-c-modal__header">
                        <h3 class="em-c-modal__title">Edit</h3>
                        <button class="em-c-btn em-c-btn--bare em-c-modal__close-btn em-js-modal-close-trigger">
                            <span class="em-c-btn__text">Close</span>
                        </button>
                    </div>
                    <div class="em-c-modal__body em-c-text-passage em-c-text-passage--small">
                        <div class="em-l-grid em-l-grid--2up ">
                            <div class="em-l-grid__item">


                                <div class="em-c-field ">
                                    <div class="form-group">
                                        <label class="em-c-field__label">
                                            <span style="float: left">COMPANY CODE</span>

                                            <span style="clear: both;"></span>
                                        </label>
                                        <div class="em-c-field__body" style="clear: both;">
                                            <input id="" class="em-c-input  em-u-text-align" type="text" value="0572 : EMPC (DS Belgium BR)" />
                                        </div>
                                    </div>

                                    <div class="em-c-field__note">
                                    </div>
                                </div>

                                <div class="em-c-field ">
                                    <div class="form-group">
                                        <label class="em-c-field__label" for="">NETWORK ORDER</label>
                                        <div class="em-c-field__body">
                                            <input id="" class="em-c-input  em-u-text-align" type="text" value="" />
                                        </div>

                                        <div class="em-c-field__note"></div>
                                    </div>

                                </div>
             
                            </div>
                            <div class="em-l-grid__item">
                                <div class="em-c-field em-u-margin-left">
                                    <div class="form-group">
                                        <label class="em-c-field__label" for="">ASSET DESCRIPTION</label>
                                        <div class="em-c-field__body">
                                            <input id="" class="em-c-input  em-u-text-align" type="text" value="Network Devices" />
                                        </div>

                                    </div>
                                </div>

                                <div class="em-c-field ">
                                    <div class="form-group" style="margin-top: 1.5rem; margin-left: 1.2rem;">
                                        <label class="em-c-field__label" for="">WBS</label>
                                        <div class="em-c-field__body">
                                            <input id="" class="em-c-input  em-u-text-align" type="text" value="C1.00879" />
                                        </div>
                                    </div>
                                    <div class="em-c-field__note"></div>
                                </div>

                            </div>


                        </div>
                    </div>
                    <div style="float: right;" class="em-c-modal__footer em-c-text-passage em-c-text-passage--small">
                        <button class="em-c-btn em-c-btn--primary em-js-modal-close-trigger">
                            <span class="em-c-btn__text">Submit</span>
                        </button>
                    </div>
                </div>
            </div>


              <div class="em-c-modal em-js-modal em-is-closed" id="modal2">
                <div class="em-c-modal__window em-js-modal-window">
                    <div class="em-c-modal__header">
                        <h3 class="em-c-modal__title">Edit</h3>
                        <button class="em-c-btn em-c-btn--bare em-c-modal__close-btn em-js-modal-close-trigger">
                            <span class="em-c-btn__text">Close</span>
                        </button>
                    </div>
                    <div class="em-c-modal__body em-c-text-passage em-c-text-passage--small">
                        <div class="em-l-grid em-l-grid--2up ">
                            <div class="em-l-grid__item">


                                <div class="em-c-field ">
                                    <div class="form-group">
                                        <label class="em-c-field__label">
                                            <span style="float: left">COMPANY CODE</span>

                                            <span style="clear: both;"></span>
                                        </label>
                                        <div class="em-c-field__body" style="clear: both;">
                                            <input id="" class="em-c-input  em-u-text-align" type="text" value="0572 : EMPC (DS Belgium BR)" />
                                        </div>
                                    </div>

                                    <div class="em-c-field__note">
                                    </div>
                                </div>

                                <div class="em-c-field ">
                                    <div class="form-group">
                                        <label class="em-c-field__label" for="">NETWORK ORDER</label>
                                        <div class="em-c-field__body">
                                            <input id="" class="em-c-input  em-u-text-align" type="text" value="" />
                                        </div>

                                        <div class="em-c-field__note"></div>
                                    </div>

                                </div>
            
                            </div>
                            <div class="em-l-grid__item">
                                <div class="em-c-field em-u-margin-left">
                                    <div class="form-group">
                                        <label class="em-c-field__label" for="">ASSET DESCRIPTION</label>
                                        <div class="em-c-field__body">
                                            <input id="" class="em-c-input  em-u-text-align" type="text" value="Network Devices" />
                                        </div>

                                    </div>
                                </div>

                                <div class="em-c-field ">
                                    <div class="form-group" style="margin-top: 1.5rem; margin-left: 1.2rem;">
                                        <label class="em-c-field__label" for="">WBS</label>
                                        <div class="em-c-field__body">
                                            <input id="" class="em-c-input  em-u-text-align" type="text" value="C1.00879" />
                                        </div>
                                    </div>
                                    <div class="em-c-field__note"></div>
                                </div>

                            </div>


                        </div>
                    </div>
                    <div style="float: right;" class="em-c-modal__footer em-c-text-passage em-c-text-passage--small">
                        <button class="em-c-btn em-c-btn--primary em-js-modal-close-trigger">
                            <span class="em-c-btn__text">Submit</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <!-- end em-c-tabs -->
    </div>

    <script>
    function change(tab)
    {
        if (tab === 3) {
            document.getElementById("btn-close-io").style.visibility = 'hidden';
            document.getElementById("btn-download-io").style.visibility = 'hidden';
            document.getElementById("btn-close-AR").style.visibility = 'visible';
            document.getElementById("btn-download-ar").style.visibility = 'visible';
        }
        else if (tab === 4) {
            document.getElementById("btn-download-ar").style.visibility = 'hidden';
            document.getElementById("btn-close-AR").style.visibility = 'hidden';
            document.getElementById("btn-close-io").style.visibility = 'visible';
            document.getElementById("btn-download-io").style.visibility = 'visible';
        }
        else {
            document.getElementById("btn-close-io").style.visibility = 'hidden';
            document.getElementById("btn-close-AR").style.visibility = 'hidden';
            document.getElementById("btn-download-ar").style.visibility = 'hidden';
            document.getElementById("btn-download-io").style.visibility = 'hidden';

        }
        
    }
</script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="footer_section" runat="server">
    <div class="align-right" style="width: auto; float: right;">
        <div class="em-c-btn-group em-c-btn-group--responsive">

            <button class="em-c-btn em-c-btn--primary" id="btn-close-AR" onclick="window.location.href='../Request/ARCloseRequest.aspx'"
                style="visibility: hidden" name="btn-close-AR">
                <span class="em-c-btn__text">Close AR </span>
            </button>

             <button class="em-c-btn em-c-btn--primary" id="btn-download-ar" style="visibility:hidden" name="btn-download-ar">
       
                <span class="em-c-btn__text">Download</span>
            </button>

        </div>
    </div>
    <%-- <div class="dropup align-right " style="width: auto; float: right;">
  <button class="dropbtn em-c-btn em-c-btn--primary " id="btn-close-io" style="visibility:hidden" name="btn-close-io">Close IO</button>
  <div class="dropup-content">
    <a href="../IO/CloseRequest.aspx"> Bulk Close Out</a>
    <a href="../IO/CloseRequest.aspx"> Close Out</a>
  </div>
</div>--%>
    <div class="align-right" style="width: auto; float: right;">
        <div class="em-c-btn-group em-c-btn-group--responsive">

            <button onclick="window.location.href='../IO/CloseRequest.aspx'" class="em-c-btn em-c-btn--primary" id="btn-close-io" style="visibility: hidden" name="btn-close-io">

                <span class="em-c-btn__text">Close IO</span>
            </button>

            <button class="em-c-btn em-c-btn--primary" id="btn-download-io" style="visibility:hidden" name="btn-download-io">
       
                <span class="em-c-btn__text">Download</span>
            </button>

        </div>
    </div>


</asp:Content>
