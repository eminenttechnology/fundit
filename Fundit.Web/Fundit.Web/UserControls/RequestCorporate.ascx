﻿<%@ Control Language="C#" AutoEventWireup="true" %>
 <fieldset class="em-c-fieldset">
    <div class="em-c-table-object ">
      <div class="em-c-table-object__header">
        <div class="em-c-toolbar ">
          <div class="em-c-toolbar__item ">
            <label for="" class="em-c-field__label ">CAPITAL AND EXPENSE APPROPRIATION BY YEAR ($M)</label>
          </div>
          <!-- end em-c-toolbar__item -->
        </div>
        <!-- end em-c-toolbar -->
      </div>
      <br/>

      <div class="em-c-table-object__body">
        <div class="em-c-table-object__body-inner">
          <table class="em-c-table em-c-table--condensed" border="0">
            <thead>
              <tr class="em-c-table__header-row">
                <th scope="col" class=" custom-em-c-table__header">
                </th>
                <th scope="col" class="custom-em-c-table__header" style="text-align: center">Year 1</th>
                <th scope="col" class="custom-em-c-table__header" style="text-align: center">Year 2</th>
                <th scope="col" class="custom-em-c-table__header" style="text-align: center">Year 3</th>
                <th scope="col" class="custom-em-c-table__header" style="text-align: center">Year 4+</th>

              </tr>
              <!-- em-c-table__header-row -->
            </thead>
            <!-- end em-c-table__header -->
            <tbody class="em-c-table__body ">
              <tr class="em-u-padding-top-half">
                <td class="em-c-table__cell"></td>
              </tr>
              <tr class="em-c-table--condensed">
                <td class="em-js-cell em-js-cell-editable">
                  <div class="em-u-font-style-semibold em-u-padding-bottom-half"> Capex Current Proposal </div>
                  <td class="">
                    <div class="em-c-field__body em-u-padding-left-quad">
                      <input [readonly]="requestInReadOnlyState?'readonly':''" type="text" id="" class="em-c-input input-corporate" [(ngModel)]="activeRequest.capexCurrentProposalYear1Amount"
                        name="CapexCurrentProposalYear1Amount" />
                    </div>
                  </td>
                  <td class="">
                    <div class="em-c-field__body em-u-padding-left-half">
                      <input [readonly]="requestInReadOnlyState?'readonly':''" type="text" id="" class="em-c-input input-corporate" [(ngModel)]="activeRequest. capexCurrentProposalYear2Amount"
                        name="CapexCurrentProposalYear2Amount" />
                    </div>
                  </td>
                  <td class="" align="middle">
                    <div class="em-c-field__body em-u-padding-left-half">
                      <input [readonly]="requestInReadOnlyState?'readonly':''" type="text" id="" class="em-c-input input-corporate" [(ngModel)]="activeRequest.capexCurrentProposalYear3Amount"
                        name="CapexCurrentProposalYear3Amount" />
                    </div>
                  </td>
                  <td class="">
                    <div class="em-c-field__body em-u-padding-left-half em-u-padding-right-quad">
                      <input [readonly]="requestInReadOnlyState?'readonly':''" type="text" id="" class="em-c-input input-corporate" [(ngModel)]="activeRequest.capexCurrentProposalYear4Amount"
                        name="CapexCurrentProposalYear4Amount" />
                    </div>
                  </td>
              </tr>
              <!-- end em-c-table__row -->
              <tr class="em-c-table--condensed">
                <td class="em-js-cell em-js-cell-editable ">
                  <div class="em-u-font-style-semibold em-u-padding-bottom-half"> Capital Appropriation Plan </div>
                </td>

                <td class="">
                  <div class="em-c-field__body em-u-padding-left-quad">
                    <input [readonly]="requestInReadOnlyState?'readonly':''" type="text" id="" class="em-c-input input-corporate" [(ngModel)]="activeRequest.capitalAppropriationPlanYear1Amount"
                      name="CapitalAppropriationPlanYear1Amount" />
                  </div>
                </td>
                <td class="">
                  <div class="em-c-field__body em-u-padding-left-half">
                    <input [readonly]="requestInReadOnlyState?'readonly':''" type="text" id="" class="em-c-input input-corporate" [(ngModel)]="activeRequest.capitalAppropriationPlanYear2Amount"
                      name="CapitalAppropriationPlanYear2Amount" />
                  </div>
                </td>
                <td class="" align="middle">
                  <div class="em-c-field__body em-u-padding-left-half">
                    <input [readonly]="requestInReadOnlyState?'readonly':''" type="text" id="" class="em-c-input input-corporate" [(ngModel)]="activeRequest.capitalAppropriationPlanYear3Amount"
                      name="CapitalAppropriationPlanYear3Amount" />
                  </div>
                </td>
                <td class="">
                  <div class="em-c-field__body em-u-padding-left-half em-u-padding-right-quad">
                    <input [readonly]="requestInReadOnlyState?'readonly':''" type="text" id="" class="em-c-input input-corporate" [(ngModel)]="activeRequest.capitalAppropriationPlanYear4Amount"
                      name="CapitalAppropriationPlanYear4Amount" />
                  </div>
                </td>
                <td></td>
                <td></td>
              </tr>
              <!-- end em-c-table__row -->
              <tr class="em-c-table--condensed">
                <td class="em-js-cell em-js-cell-editable ">
                  <div class="em-u-font-style-semibold em-u-padding-bottom-half"> OPEX Current Proposal</div>
                </td>
                <td class="">
                  <div class="em-c-field__body em-u-padding-left-quad">
                    <input [readonly]="requestInReadOnlyState?'readonly':''" type="text" id="" class="em-c-input input-corporate" [(ngModel)]="activeRequest.opexCurrentProposalYear1Amount"
                      name="OpexCurrentProposalYear1Amount" />
                  </div>
                </td>
                <td class="">
                  <div class="em-c-field__body em-u-padding-left-half">
                    <input [readonly]="requestInReadOnlyState?'readonly':''" type="text" id="" class="em-c-input input-corporate" [(ngModel)]="activeRequest.opexCurrentProposalYear2Amount"
                      name="OpexCurrentProposalYear2Amount" />
                  </div>
                </td>
                <td class="" align="middle">
                  <div class="em-c-field__body em-u-padding-left-half">
                    <input [readonly]="requestInReadOnlyState?'readonly':''" type="text" id="" class="em-c-input input-corporate" [(ngModel)]="activeRequest. opexCurrentProposalYear3Amount"
                      name=" OpexCurrentProposalYear3Amount" />
                  </div>
                </td>
                <td class="">
                  <div class="em-c-field__body  em-u-padding-left-half em-u-padding-right-quad">
                    <input [readonly]="requestInReadOnlyState?'readonly':''" type="text" id="" class="em-c-input input-corporate" [(ngModel)]="activeRequest.opexCurrentProposalYear4Amount"
                      name="OpexCurrentProposalYear4Amount" />
                  </div>
                </td>

              </tr>
              <!-- end em-c-table__row -->
              <tr class="em-c-table--condensed ">
                <td class="">
                  <div class="em-u-font-style-semibold em-u-padding-bottom-half">Expense Appropriation Plan </div>
                </td>
                <td class="em-js-cell em-js-cell-editable">
                  <div class="em-c-field__body em-u-padding-left-quad">
                    <input [readonly]="requestInReadOnlyState?'readonly':''" type="text" id="" class="em-c-input input-corporate" [(ngModel)]="activeRequest.expenseAppropriationPlanYear1Amount"
                      name="ExpenseAppropriationPlanYear1Amount" />
                  </div>
                </td>

                <td class="">
                  <div class="em-c-field__body em-u-padding-left-half">
                    <input [readonly]="requestInReadOnlyState?'readonly':''" type="text" id="" class="em-c-input input-corporate" [(ngModel)]="activeRequest.expenseAppropriationPlanYear2Amount"
                      name="ExpenseAppropriationPlanYear2Amount" />
                  </div>
                </td>
                <td class="" align="middle">
                  <div class="em-c-field__body em-u-padding-left-half">
                    <input [readonly]="requestInReadOnlyState?'readonly':''" type="text" id="" class="em-c-input input-corporate" [(ngModel)]="activeRequest.expenseAppropriationPlanYear3Amount"
                      name="expenseAppropriationPlanYear3Amount" />
                  </div>
                </td>
                <td class="">
                  <div class="em-c-field__body em-u-padding-left-half em-u-padding-right-quad">
                    <input [readonly]="requestInReadOnlyState?'readonly':''" type="text" id="" class="em-c-input input-corporate" [(ngModel)]="activeRequest.expenseAppropriationPlanYear4Amount"
                      name="expenseAppropriationPlanYear4Amount" />
                  </div>
                </td>


              </tr>
              <tr>
                <td colspan="5" valign="middle" align="middle">
                  <div class="em-c-table-object__header">
                    <div class="em-c-toolbar ">
                      <div class="em-c-toolbar__item ">
                        <label for="" class="em-c-field__label ">OTHER INVESTMENT METRICS</label>
                      </div>
                      <!-- end em-c-toolbar__item -->
                    </div>
                    <!-- end em-c-toolbar -->
                  </div>
                </td>
              </tr>
              <tr>
                <td></td>
                <td class="em-js-cell em-js-cell-editable " style="text-align: center">
                  <div class="em-u-font-style-semibold">Current</div>
                </td>


                <td class="em-js-cell em-js-cell-editable " style="text-align: center">
                  <div class="em-u-font-style-semibold">Long Range</div>
                </td>
                <td></td>
                <td></td>

              </tr>
              <tr class="em-u-padding-top-half">
                <td class="em-c-table__cell"></td>
              </tr>
              <tr class="em-c-table--condensed">
                <td class="em-js-cell em-js-cell-editable ">
                  <div class="em-u-font-style-semibold em-u-padding-bottom"> Capital Lease</div>
                </td>

                <td class="">
                  <div class="em-c-field__body  em-u-padding-left-quad">
                    <input [readonly]="requestInReadOnlyState?'readonly':''" type="text" id="" class="em-c-input input-corporate em-u-margin-bottom"
                      [(ngModel)]="activeRequest.capitalLeaseCurrent" name="CapitalLeaseCurrent" />
                  </div>
                </td>
                <td class="">
                  <div class="em-c-field__body em-u-padding-left-half">
                    <input [readonly]="requestInReadOnlyState?'readonly':''" type="text" id="" class="em-c-input input-corporate em-u-margin-bottom"
                      [(ngModel)]="activeRequest.capitalLeaseLongRange" name="CapitalLeaseLongRange" />
                  </div>
                </td>
                <td class=""></td>
                <td class=""></td>
              </tr>
              <tr class="em-c-table--condensed">
                <td class="em-js-cell em-js-cell-editable ">
                  <div class="em-u-font-style-semibold em-u-padding-bottom"> Deferred Charge</div>
                </td>

                <td class="">
                  <div class="em-c-field__body em-u-padding-left-quad">
                    <input [readonly]="requestInReadOnlyState?'readonly':''" type="text" id="" class="em-c-input input-corporate em-u-margin-bottom"
                      [(ngModel)]="activeRequest.deferredChargeCurrent" name="DeferredChargeCurrent" />
                  </div>
                </td>
                <td class="">
                  <div class="em-c-field__body em-u-padding-left-half">
                    <input [readonly]="requestInReadOnlyState?'readonly':''" type="text" id="" class="em-c-input input-corporate em-u-margin-bottom"
                      [(ngModel)]="activeRequest.deferredChargeLongRange" name="DeferredChargeLongRanget" />
                  </div>
                </td>
                <td colspan="2"></td>
              </tr>
              <tr class="em-c-table--condensed ">
                <td class="">
                  <div class="em-u-font-style-semibold em-u-padding-bottom">Working Capital </div>
                </td>
                <td class="em-js-cell em-js-cell-editable">
                  <div class="em-c-field__body  em-u-padding-left-quad">
                    <input [readonly]="requestInReadOnlyState?'readonly':''" type="text" id="" class="em-c-input input-corporate em-u-margin-bottom"
                      [(ngModel)]="activeRequest.workingCapitalCurrent" name="workingCapitalCurrentt" />
                  </div>
                </td>

                <td class="">
                  <div class="em-c-field__body  em-u-padding-left-half">
                    <input [readonly]="requestInReadOnlyState?'readonly':''" type="text" id="" class="em-c-input input-corporate em-u-margin-bottom"
                      [(ngModel)]="activeRequest. workingCapitalLongRange" name=" workingCapitalLongRange" />
                  </div>
                </td>
                <td colspan="2"></td>
                <tr class="em-c-table--condensed ">
                  <td class=" ">
                    <div class="em-u-font-style-semibold em-u-padding-bottom">Assoc. Facilities</div>
                  </td>
                  <td class="em-js-cell em-js-cell-editable">
                    <div class="em-c-field__body em-u-padding-left-quad">
                      <input [readonly]="requestInReadOnlyState?'readonly':''" type="text" id="" class="em-c-input input-corporate em-u-margin-bottom"
                        [(ngModel)]="activeRequest.assocFacilitiesCurrent" name="AssocFacilitiesCurrent" />
                    </div>
                  </td>

                  <td class="">
                    <div class="em-c-field__body em-u-padding-left-half">
                      <input [readonly]="requestInReadOnlyState?'readonly':''" type="text" id="" class="em-c-input input-corporate em-u-margin-bottom"
                        [(ngModel)]="activeRequest.assocFacilitiesLongRange" name="AssocFacilitiesLongRange"
                      />
                    </div>
                  </td>
                  <td colspan="2"></td>
                  <tr class="em-c-table--condensed ">
                    <td class=" ">
                      <div class="em-u-font-style-semibold em-u-padding-bottom">Investment Credit</div>
                    </td>
                    <td class="em-js-cell em-js-cell-editable">
                      <div class="em-c-field__body em-u-padding-left-quad">
                        <input [readonly]="requestInReadOnlyState?'readonly':''" type="text" id="" class="em-c-input input-corporate em-u-margin-bottom"
                          [(ngModel)]="activeRequest.investmentCreditCurrent" name="InvestmentCreditCurrent"
                        />
                      </div>
                    </td>
                    <td class="em-js-cell em-js-cell-editable">
                      <div class="em-c-field__body em-u-padding-left-half">
                        <input [readonly]="requestInReadOnlyState?'readonly':''" type="text" id="" class="em-c-input input-corporate em-u-margin-bottom"
                          [(ngModel)]="activeRequest.investmentCreditLongRange" name="InvestmentCreditLongRange"
                        />
                      </div>
                    </td>

                    <tr class="em-c-table--condensed ">
                      <td class=" ">
                        <div class="em-u-font-style-semibold em-u-padding-bottom">Other </div>
                      </td>
                      <td class=" em-js-cell em-js-cell-editable">
                        <div class="em-c-field__body em-u-padding-left-quad">
                          <input [readonly]="requestInReadOnlyState?'readonly':''" type="text" id="" class="em-c-input input-corporate em-u-margin-bottom"
                            [(ngModel)]="activeRequest.otherCurrent" name="OtherCurrent" />
                        </div>
                      </td>

                      <td class="">
                        <div class="em-c-field__body em-u-padding-left-half">
                          <input [readonly]="requestInReadOnlyState?'readonly':''" type="text" id="" class="em-c-input input-corporate em-u-margin-bottom"
                            [(ngModel)]="activeRequest.otherLongRange" name="OtherLongRange" />
                        </div>
                      </td>
                      <td colspan="2"></td>
                    </tr>
            </tbody>
            <!-- end em-c-table__body -->
          </table>
          <!--end em-c-table-->
        </div>
      </div>
    </div>
  </fieldset>