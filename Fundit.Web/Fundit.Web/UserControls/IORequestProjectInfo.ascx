﻿<%@ Control Language="C#" AutoEventWireup="true" %>
  <label _ngcontent-c3="" class="em-c-field__label" for="" id="workNameTitle"> 66273 - Transactional Data Transparency (TDT) Project2-ROW</label>
  <br _ngcontent-c3="">
  <!---->
<fieldset _ngcontent-c3="" class="em-c-fieldset ">
  <div _ngcontent-c3="" class="em-l-grid--halves em-u-margin-right-quad">
    <div _ngcontent-c3="" class="em-l-grid__item em-u-margin-right-quad">
      <!----><div _ngcontent-c3="" class="form-group-row">
        <div _ngcontent-c3="" class="em-c-field ">
          <label _ngcontent-c3="" class="em-c-field__label" for="">Company Code</label>
          <!----><div _ngcontent-c3="" class="em-c-field__body em-c-field--small">
            <input _ngcontent-c3="" class="em-c-input ng-untouched ng-pristine ng-valid" id="tcodeName" name="tcodeName" placeholder="Placeholder" type="text">
          </div>
          <div _ngcontent-c3="" class="em-c-field__note"> </div>
        </div>
      </div>
      <div _ngcontent-c3="" class="form-group-row">
        <div _ngcontent-c3="" class="em-c-field ">
          <label _ngcontent-c3="" class="em-c-field__label" for="">Controller's Contact</label>
          <!----><div _ngcontent-c3="" class="em-c-field__body">
            <div _ngcontent-c3="" class="em-c-field__body em-c-field--small">
              <select _ngcontent-c3="" class="em-c-select em-u-width-100 em-u-font-style-semibold control-text ng-untouched ng-pristine ng-valid" name="controller">
                <option _ngcontent-c3="" selected="" value="-500">-- Select Controller's Contact --</option>
                <!----><option _ngcontent-c3="" value="74249490">Abete, Cindy M</option><option _ngcontent-c3="" selected value="84921141">Araqsousi, Rock</option><option _ngcontent-c3="" value="39130889">Barranco, Anna</option><option _ngcontent-c3="" value="57450800">Buries, Martha K</option><option _ngcontent-c3="" value="87990935">Chen, Vivian L</option><option _ngcontent-c3="" value="09377967">Courtright, Anne O</option><option _ngcontent-c3="" value="03692959">Floyd, Karen L</option><option _ngcontent-c3="" value="22096645">Garner, Jamie L</option><option _ngcontent-c3="" value="78501834">Harper, Daniel</option><option _ngcontent-c3="" value="43259118">Henson, Russell A</option><option _ngcontent-c3="" value="72391500">Hickman, Theo</option><option _ngcontent-c3="" value="49709035">Hong, Jessica A</option><option _ngcontent-c3="" value="5641e2d6">Lali Lobzhanidze</option><option _ngcontent-c3="" value="31731090">Lane, Florence K</option><option _ngcontent-c3="" value="74934329">Li, Sue X</option><option _ngcontent-c3="" value="12243466">Malbari, Jumana</option><option _ngcontent-c3="" value="5641e2c7">Nia Hall</option><option _ngcontent-c3="" value="71650228">Parsons, Stephanie M</option><option _ngcontent-c3="" value="B741e2r8">Philip Ochu</option><option _ngcontent-c3="" value="24759537">Quim, Michele D</option><option _ngcontent-c3="" value="96284823">Samson, Matthew</option><option _ngcontent-c3="" value="5641e2b7">Yuichi Fujiki</option>
              </select>
            </div>
          </div>
          <div _ngcontent-c3="" class="em-c-field__note">Assigned Controller's reviewer </div>
        </div>
      </div>
      <div _ngcontent-c3="" class="form-group-row">
        <div _ngcontent-c3="" class="em-c-field ">
          <label _ngcontent-c3="" class="em-c-field__label" for="">
            POTL/POM
            <!---->
          </label>
          <!----><div _ngcontent-c3="" class="em-c-field__body em-c-field--small">
            <app-dropdown-input _ngcontent-c3="" id="potl-input" name="potl" _nghost-c13=""><div _ngcontent-c13="" class="auto-complete-dropdown">
    <input _ngcontent-c13="" autocomplete="off" class="em-c-input ng-untouched ng-pristine ng-valid" type="text" id="potl-input" value="Nia, Hall">
    <div _ngcontent-c13="" class="dropdown" id="ch6hi6wQxEHB12uI">
        <!---->
    </div>
</div></app-dropdown-input>
          </div>
          <div _ngcontent-c3="" class="em-c-field__note"> </div>
        </div>
      </div>
    </div>
    <div _ngcontent-c3="" class="em-l-grid__item em-u-padding-right">
              <div  class="form-group-row">
        <div  class="em-c-field ">
          <label  class="em-c-field__label" for="">Service Center</label>
          <div  class="em-c-field__body em-c-field--small">
           <label _ngcontent-c3="" class="em-c-field__label" for="">Budapest</label>
          </div>
          <div  class="em-c-field__note"> </div>
        </div>
      </div>
      <div  class="form-group-row">
        <div  class="em-c-field ">
          <label  class="em-c-field__label" for="">Service Center Owner</label>
          <div  class="em-c-field__body em-c-field--small">
                         <select _ngcontent-c3="" class="em-c-select em-u-width-100 em-u-font-style-semibold control-text ng-untouched ng-pristine ng-valid" name="controller">
                <option _ngcontent-c3="" selected="" value="-500">John Doe</option>
                <!----><option _ngcontent-c3="" value="74249490">Sherlock , Holmes M</option><option _ngcontent-c3="" value="84921141">Dan, Rock</option>
                             <option _ngcontent-c3="" value="39130889">Jessica, Anna</option><option _ngcontent-c3="" value="57450800">Bria, Myles K</option>
                             <option _ngcontent-c3="" value="87990935">Jacie, Chan L</option><option _ngcontent-c3="" value="09377967">Brighton, Janice O</option>

              </select>
          </div>
          <div  class="em-c-field__note"> </div>
        </div>
      </div>
    </div>
  </div>
</fieldset>
