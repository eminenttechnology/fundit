﻿<%@ Control Language="C#" AutoEventWireup="true" %>
 <div _ngcontent-c9="" class="em-c-table-object__body ">
            <div _ngcontent-c9="" class="em-c-table-object__body-inner">
              <table _ngcontent-c9="" class="em-c-table  scroll " style="table-layout:fixed; width:100%;">
                <thead _ngcontent-c9="" class="em-c-table__header">
                  <tr _ngcontent-c9="" class="em-c-table__header-row ">
                    <th _ngcontent-c9="" class="em-c-table__header-cell" scope="col" style="width: 7.7%;">
                    </th>
                    <th _ngcontent-c9="" class="em-c-table__header-cell " scope="col" style="width: 7.7%;">
                      <span _ngcontent-c9="" class="em-u-font-size-small-2">STATUS</span>
                      <button _ngcontent-c9="" class="em-c-btn--bare">
                        <!---->
                          <svg _ngcontent-c9="" class="em-c-icon em-c-icon--tiny em-c-icon--green">
                            <use _ngcontent-c9="" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                          </svg>
                        
                        <!---->
                      </button>
                    </th>
                    
                    <th _ngcontent-c9="" class="em-c-table__header-cell em-u-margin-right" scope="col" style="word-wrap: break-word; width: 7.7%;">
                      <span _ngcontent-c9="" class="em-u-font-size-small-2">IO #</span>
                      <button _ngcontent-c9="" class="em-c-btn--bare">
                        <!---->
                          <svg _ngcontent-c9="" class="em-c-icon em-c-icon--tiny em-c-icon--green">
                            <use _ngcontent-c9="" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                          </svg>
                        
                        <!---->
                      </button>
                    </th>
                    <th _ngcontent-c9="" class="em-c-table__header-cell " scope="col" style="width: 7.7%;">
                      <span _ngcontent-c9="" class="em-u-font-size-small-2">COMPANY CODE</span>
                      <button _ngcontent-c9="" class="em-c-btn--bare">
                        <!---->
                          <svg _ngcontent-c9="" class="em-c-icon em-c-icon--tiny em-c-icon--green">
                            <use _ngcontent-c9="" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                          </svg>
                        
                        <!---->
                      </button>
                    </th>
                    <th _ngcontent-c9="" class="em-c-table__header-cell " scope="col" style="width: 7.7%;word-wrap: break-word;">
                      <span _ngcontent-c9="" class="em-u-font-size-small-2">COUNTRY</span>
                      <button _ngcontent-c9="" class="em-c-btn--bare">
                        <!---->
                          <svg _ngcontent-c9="" class="em-c-icon em-c-icon--tiny em-c-icon--green">
                            <use _ngcontent-c9="" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                          </svg>
                        
                        <!---->
                      </button>
                    </th>
                    <th _ngcontent-c9="" class="em-c-table__header-cell" scope="col" style="width: 7.7%;word-wrap: break-word;">
                      <span _ngcontent-c9="" class="em-u-font-size-small-2">COST CENTER</span>
                      <button _ngcontent-c9="" class="em-c-btn--bare">
                        <!---->
                          <svg _ngcontent-c9="" class="em-c-icon em-c-icon--tiny em-c-icon--green">
                            <use _ngcontent-c9="" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                          </svg>
                        
                        <!---->
                      </button>
                    </th>
                    <th _ngcontent-c9="" class="em-c-table__header-cell em-u-margin-right " scope="col" style="width: 7.7%;">
                      <span _ngcontent-c9="" class="em-u-font-size-small-2">RECEIVING ORDER</span>
                      <button _ngcontent-c9="" class="em-c-btn--bare">
                        <!---->
                          <svg _ngcontent-c9="" class="em-c-icon em-c-icon--tiny em-c-icon--green">
                            <use _ngcontent-c9="" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                          </svg>
                        
                        <!---->
                      </button>
                    </th>
                    <th _ngcontent-c9="" class="em-c-table__header-cell " scope="col" style="word-wrap: break-word; width: 7.7%;">
                      <span _ngcontent-c9="" class="em-u-font-size-small-2"> DESCRIPTION </span>
                      <button _ngcontent-c9="" class="em-c-btn--bare">
                        <!---->
                          <svg _ngcontent-c9="" class="em-c-icon em-c-icon--tiny em-c-icon--green">
                            <use _ngcontent-c9="" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                          </svg>
                        
                        <!---->
                      </button>
                    </th>
                   
                    <th _ngcontent-c9="" class="em-c-table__header-cell " scope="col" style="width: 7.7%;">
                    </th>
                  </tr>
                  
                </thead>
                <tbody _ngcontent-c9="" class="em-c-table__body">
                  <!----><tr _ngcontent-c9="" class="em-c-table__row "   >
                    <td _ngcontent-c9="" class="em-c-table__cell" colspan="" style="width: 7.7%;">
                        <input id="inline-check-1" type="checkbox" name="checkbox-1" value="" class="em-c-input-group__control">
                      <a _ngcontent-c9=""  href="#modal" class="em-c-modal__trigger em-js-modal-trigger" style="padding-left: 1rem;"> Edit </a>
                    </td>
                    <td _ngcontent-c9="" class="em-c-table__cell" colspan="" style="width: 7.7%;">Closed
                    </td>
                    <td _ngcontent-c9="" class="em-c-table__cell" colspan="" style="width: 7.7%;">842226
                    </td>
                    <td _ngcontent-c9="" class="em-c-table__cell em-u-margin-right" colspan="" style="width: 7.7%;">2299
                    </td>
                    <td _ngcontent-c9="" class="em-c-table__cell" colspan="" style="width: 7.7%;">Canada
                    </td>
                    <td _ngcontent-c9="" class="em-c-table__cell" colspan="" style="width: 7.7%;">124082
                    </td>
                    </td>
                    <td _ngcontent-c9="" class="em-c-table__cell break-character " colspan="" style="width: 7.7%; word-wrap: break-word;">G802075385
                    </td>
                    <td _ngcontent-c9="" class="em-c-table__cell" colspan="" style="width: 7.7%; word-wrap: break-word;">Sarnia Radio Remediation

                    </td>
                    <td _ngcontent-c9="" class="em-c-table__cell" colspan="" style="width: 7.7%;">
                      <!---->
                    </td>
                  </tr><tr _ngcontent-c9=""  class="em-c-table__row ">
                    <td _ngcontent-c9="" class="em-c-table__cell" colspan="" style="width: 7.7%;">
                        <input id="inline-check-2" type="checkbox" name="checkbox-1" value="" class="em-c-input-group__control">
                      <a _ngcontent-c9=""  href="#modal" class="em-c-modal__trigger em-js-modal-trigger" style="padding-left: 1rem;"> Edit </a>
                    </td>
                    <td _ngcontent-c9="" class="em-c-table__cell" colspan="" style="width: 7.7%;">Closed
                    </td>
                    <td _ngcontent-c9="" class="em-c-table__cell" colspan="" style="width: 7.7%;">846892
                    </td>
                    <td _ngcontent-c9="" class="em-c-table__cell em-u-margin-right" colspan="" style="width: 7.7%;">524
                    </td>
                    <td _ngcontent-c9="" class="em-c-table__cell" colspan="" style="width: 7.7%;">Italy
                    </td>
                    <td _ngcontent-c9="" class="em-c-table__cell" colspan="" style="width: 7.7%;">13821
                    </td>
                    </td>
                    <td _ngcontent-c9="" class="em-c-table__cell break-character " colspan="" style="width: 7.7%; word-wrap: break-word;">G802074983
                    </td>
                    <td _ngcontent-c9="" class="em-c-table__cell" colspan="" style="width: 7.7%; word-wrap: break-word;">Phone devices < $2K
                    </td>
                    <td _ngcontent-c9="" class="em-c-table__cell" colspan="" style="width: 7.7%;">
                      <!---->
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>