﻿<%@ Control Language="C#" AutoEventWireup="true" %>
  <label  class="em-c-field__label" for="" id="workNameTitle"> 66273 - Transactional Data Transparency (TDT) Project2-ROW</label>
  <br >
  
<fieldset  class="em-c-fieldset ">
  <div  class="em-l-grid--halves em-u-margin-right-quad">
    <div  class="em-l-grid__item em-u-margin-right-quad">

      <div  class="form-group-row">
        <div  class="em-c-field ">
          <label  class="em-c-field__label" for="">
            Work Lead
            
          </label>
          <div  class="em-c-field__body em-c-field--small">
    <input value="Thaís Oliveira Almeida" autocomplete="off" class="em-c-input ng-untouched ng-pristine ng-valid" type="text" id="work-lead-input">
    <div  class="dropdown"  >
        
    </div>
</div>
          </div>
          
      </div>
      <div  class="form-group-row">
        <div  class="em-c-field ">
          <label  class="em-c-field__label" for="">Controller's Contact</label>
          <div  class="em-c-field__body">
            <div  class="em-c-field__body em-c-field--small">
              <input value="Buries, Martha K" class="em-c-select em-u-width-100 em-u-font-style-semibold control-text ng-untouched ng-pristine ng-valid" name="controller" />
            </div>
          </div>
          <div  class="em-c-field__note">Assigned Controller's reviewer </div>
        </div>
      </div>
      <div  class="form-group-row">
        <div  class="em-c-field ">
          <label  class="em-c-field__label" for="">POTL/POM</label>
          <div  class="em-c-field__body">
            <div  class="em-c-field__body em-c-field--small">
              <input value="Martim, Carvalho Rocha" class="em-c-select em-u-width-100 em-u-font-style-semibold control-text ng-untouched ng-pristine ng-valid" name="controller" />
            </div>
          </div>
          <div  class="em-c-field__note"> </div>
        </div>
      </div>
    </div>
    <div  class="em-l-grid__item em-u-padding-right">
      

      <div  class="form-group-row">
        <div  class="em-c-field ">
          <label  class="em-c-field__label" for="">Service Center Owner</label>
          <div  class="em-c-field__body em-c-field--small">
            <input value="John Doe" class="em-c-select em-u-width-100 em-u-font-style-semibold control-text ng-untouched ng-pristine ng-valid" name="ServiceCenterOwner" />
            
          </div>
          <div  class="em-c-field__note"> </div>
        </div>
      </div>

        <div  class="form-group-row">
                <div   class="em-c-field ">
                    <label  class="em-c-field__label" for="">I/O Description</label>
                    <div   class="em-c-field__body">
                        <textarea placeholder="Sarnia Radio Remediation" class="em-c-textarea ng-untouched ng-pristine ng-valid" cols="95" name="ioDescription"  rows="4"></textarea>
                    </div>
                    <div   class="em-c-field__note" style="white-space: pre-wrap;"></div>
                </div>
            </div>
    </div>
  </div>
</fieldset>
