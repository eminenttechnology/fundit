﻿<%@ Control Language="C#" AutoEventWireup="true" %>
  <div _ngcontent-c9="" class="em-c-table-object__body ">
      <div class="em-c-table-object__header">
            <div _ngcontent-c9="" class="em-c-table-object__body-inner">
              <table _ngcontent-c9="" class="em-c-table  scroll " style="table-layout:fixed; width:100%;">
                <thead _ngcontent-c9="" class="em-c-table__header">
                  <tr _ngcontent-c9="" class="em-c-table__header-row ">
                    <th _ngcontent-c9="" class="em-c-table__header-cell" scope="col" style="width: 7.7%;">
                    </th>
                    <th _ngcontent-c9="" class="em-c-table__header-cell " scope="col" style="width: 7.7%;">
                      <span _ngcontent-c9="" class="em-u-font-size-small-2">STATUS</span>
                      <button _ngcontent-c9="" class="em-c-btn--bare">
                        <!---->
                          <svg _ngcontent-c9="" class="em-c-icon em-c-icon--tiny em-c-icon--green">
                            <use _ngcontent-c9="" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                          </svg>
                        
                        <!---->
                      </button>
                    </th>
                    
                    <th _ngcontent-c9="" class="em-c-table__header-cell em-u-margin-right" scope="col" style="word-wrap: break-word; width: 7.7%;">
                      <span _ngcontent-c9="" class="em-u-font-size-small-2">IO #</span>
                      <button _ngcontent-c9="" class="em-c-btn--bare">
                        <!---->
                          <svg _ngcontent-c9="" class="em-c-icon em-c-icon--tiny em-c-icon--green">
                            <use _ngcontent-c9="" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                          </svg>
                        
                        <!---->
                      </button>
                    </th>
                      <th _ngcontent-c9="" class="em-c-table__header-cell " scope="col" style="width: 7.7%;">
                      <span _ngcontent-c9="" class="em-u-font-size-small-2">COMPANY CODE</span>
                      <button _ngcontent-c9="" class="em-c-btn--bare">
                        <!---->
                          <svg _ngcontent-c9="" class="em-c-icon em-c-icon--tiny em-c-icon--green">
                            <use _ngcontent-c9="" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                          </svg>
                        
                        <!---->
                      </button>
                    </th>
                    <th _ngcontent-c9="" class="em-c-table__header-cell " scope="col" style="width: 7.7%;word-wrap: break-word;">
                      <span _ngcontent-c9="" class="em-u-font-size-small-2">COUNTRY</span>
                      <button _ngcontent-c9="" class="em-c-btn--bare">
                        <!---->
                          <svg _ngcontent-c9="" class="em-c-icon em-c-icon--tiny em-c-icon--green">
                            <use _ngcontent-c9="" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                          </svg>
                        
                        <!---->
                      </button>
                    </th>
                    <th _ngcontent-c9="" class="em-c-table__header-cell " scope="col" style="width: 7.7%;word-wrap: break-word;">
                      <span _ngcontent-c9="" class="em-u-font-size-small-2">PROVINCE</span>
                      <button _ngcontent-c9="" class="em-c-btn--bare">
                        <!---->
                          <svg _ngcontent-c9="" class="em-c-icon em-c-icon--tiny em-c-icon--green">
                            <use _ngcontent-c9="" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                          </svg>
                        
                        <!---->
                      </button>
                    </th>
                    <th _ngcontent-c9="" class="em-c-table__header-cell" scope="col" style="width: 7.7%;word-wrap: break-word;">
                      <span _ngcontent-c9="" class="em-u-font-size-small-2">PAY ROLL COST CENTER</span>
                      <button _ngcontent-c9="" class="em-c-btn--bare">
                        <!---->
                          <svg _ngcontent-c9="" class="em-c-icon em-c-icon--tiny em-c-icon--green">
                            <use _ngcontent-c9="" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                          </svg>
                        
                        <!---->
                      </button>
                    </th>
                      <th _ngcontent-c9="" class="em-c-table__header-cell" scope="col" style="width: 7.7%;word-wrap: break-word;">
                      <span _ngcontent-c9="" class="em-u-font-size-small-2">RESPONSIBLE EMIT ORG</span>
                      <button _ngcontent-c9="" class="em-c-btn--bare">
                        <!---->
                          <svg _ngcontent-c9="" class="em-c-icon em-c-icon--tiny em-c-icon--green">
                            <use _ngcontent-c9="" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                          </svg>
                        
                        <!---->
                      </button>
                    </th>
                    <th _ngcontent-c9="" class="em-c-table__header-cell em-u-margin-right " scope="col" style="width: 7.7%;">
                      <span _ngcontent-c9="" class="em-u-font-size-small-2">RECEIVING ORDER</span>
                      <button _ngcontent-c9="" class="em-c-btn--bare">
                        <!---->
                          <svg _ngcontent-c9="" class="em-c-icon em-c-icon--tiny em-c-icon--green">
                            <use _ngcontent-c9="" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                          </svg>
                        
                        <!---->
                      </button>
                    </th>
                    <th _ngcontent-c9="" class="em-c-table__header-cell " scope="col" style="word-wrap: break-word; width: 7.7%;">
                      <span _ngcontent-c9="" class="em-u-font-size-small-2"> DESCRIPTION </span>
                      <button _ngcontent-c9="" class="em-c-btn--bare">
                        <!---->
                          <svg _ngcontent-c9="" class="em-c-icon em-c-icon--tiny em-c-icon--green">
                            <use _ngcontent-c9="" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                          </svg>
                        
                        <!---->
                      </button>
                    </th>
                   
                    <th _ngcontent-c9="" class="em-c-table__header-cell " scope="col" style="width: 7.7%;">
                    </th>
                  </tr>
                  
                </thead>
                <tbody _ngcontent-c9="" class="em-c-table__body">
                  <!----><tr _ngcontent-c9="" class="em-c-table__row " >
                    <td _ngcontent-c9="" class="em-c-table__cell" colspan="" style="width: 7.7%;">
                      <a _ngcontent-c9="" href="#modal2" class="em-c-modal__trigger em-js-modal-trigger">Edit</a>
                    </td>
                    <td _ngcontent-c9="" class="em-c-table__cell" colspan="" style="width: 7.7%;">Closed
                    </td>
                    <td _ngcontent-c9="" class="em-c-table__cell" colspan="" style="width: 7.7%;">842226
                    </td>
                    <td _ngcontent-c9="" class="em-c-table__cell em-u-margin-right" colspan="" style="width: 7.7%;">2299
                    </td>
                    <td _ngcontent-c9="" class="em-c-table__cell" colspan="" style="width: 7.7%;">Canada
                    </td>
                      <td _ngcontent-c9="" class="em-c-table__cell" colspan="" style="width: 7.7%;">Quebec
                    </td>
                    <td _ngcontent-c9="" class="em-c-table__cell" colspan="" style="width: 7.7%;">2299-122994
                    </td>
                      <td _ngcontent-c9="" class="em-c-table__cell" colspan="" style="width: 7.7%;">INF USCAN OPS NWF QUEB
                    </td>
                    </td>
                    <td _ngcontent-c9="" class="em-c-table__cell break-character " colspan="" style="width: 7.7%; word-wrap: break-word;">G802075385
                    </td>
                    <td _ngcontent-c9="" class="em-c-table__cell" colspan="" style="width: 7.7%; word-wrap: break-word;">Sarnia Radio Remediation

                    </td>
                    <td _ngcontent-c9="" class="em-c-table__cell" colspan="" style="width: 7.7%;">
                      <a href="#modal1" class="em-c-modal__trigger em-js-modal-trigger">Cancel</a>
                    </td>
                  </tr><tr _ngcontent-c9="" class="em-c-table__row ">
                    <td _ngcontent-c9="" class="em-c-table__cell" colspan="" style="width: 7.7%;">
                      <a _ngcontent-c9="" href="#modal2" class="em-c-modal__trigger em-js-modal-trigger">Edit</a>
                    </td>
                    <td _ngcontent-c9="" class="em-c-table__cell" colspan="" style="width: 7.7%;">Closed
                    </td>
                    <td _ngcontent-c9="" class="em-c-table__cell" colspan="" style="width: 7.7%;">846892
                    </td>
                    <td _ngcontent-c9="" class="em-c-table__cell em-u-margin-right" colspan="" style="width: 7.7%;">524
                    </td>
                    <td _ngcontent-c9="" class="em-c-table__cell" colspan="" style="width: 7.7%;">USA
                    </td>
                    <td _ngcontent-c9="" class="em-c-table__cell" colspan="" style="width: 7.7%;">
                    </td>
                      <td _ngcontent-c9="" class="em-c-table__cell" colspan="" style="width: 7.7%;">C0805U5441
                    </td>
                      <td _ngcontent-c9="" class="em-c-table__cell" colspan="" style="width: 7.7%;"> UIT EMEC ME/Tech/BD Tech Supp
                    </td>
                    </td>
                    <td _ngcontent-c9="" class="em-c-table__cell break-character " colspan="" style="width: 7.7%; word-wrap: break-word;">G802074983
                    </td>
                    <td _ngcontent-c9="" class="em-c-table__cell" colspan="" style="width: 7.7%; word-wrap: break-word;">Phone devices < $2K
                    </td>
                    <td _ngcontent-c9="" class="em-c-table__cell" colspan="" style="width: 7.7%;">
                      <a href="#modal1" class="em-c-modal__trigger em-js-modal-trigger">Cancel</a>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>




<div class="em-c-modal em-js-modal em-is-closed" id="modal2">
    <div class="em-c-modal__window em-js-modal-window">
        <div class="em-c-modal__header">
            <h3 class="em-c-modal__title">Edit</h3>
            <button class="em-c-btn em-c-btn--bare em-c-modal__close-btn em-js-modal-close-trigger">
                <span class="em-c-btn__text">Close</span>
            </button>
            <!-- end em-c-btn -->
        </div>
        <!-- end em-c-modal__header -->
        <div class="em-c-modal__body em-c-text-passage em-c-text-passage--small">
            <div class="em-l-grid em-l-grid--2up ">
                <div class="em-l-grid__item">


                    <div class="em-c-field ">
                        <div class="form-group">
                            <label class="em-c-field__label">
                                <span style="float: left">COMPANY CODE</span>

                                <span style="clear: both;"></span>
                            </label>
                            <div class="em-c-field__body" style="clear: both;">
                                <input id="" class="em-c-input  em-u-text-align" type="text" value="2299 " />
                            </div>
                        </div>

                        <div class="em-c-field__note">
                        </div>
                    </div>

                    <div class="em-c-field ">
                        <div class="form-group">
                            <label class="em-c-field__label" for="">PROFIT CENTER #</label>
                            <div class="em-c-field__body">
                                <input id="" class="em-c-input  em-u-text-align" type="text" value="" />
                            </div>

                            <div class="em-c-field__note"></div>
                        </div>

                    </div>
                    <div class="em-c-field " >
                        <div class="form-group">
                            <label class="em-c-field__label" for="">FAD #</label>
                            <div class="em-c-field__body">
                                <input id="" class="em-c-input  em-u-text-align" type="text" value="" />
                            </div>
                        </div>
                        <div class="em-c-field__note"></div>
                    </div>

                 
                   <div class="em-c-field ">
                        <div class="form-group">
                            <label class="em-c-field__label" for="">SERVICE CENTER OWNER</label>
                            <div class="em-c-field__body">
                                <input id="" class="em-c-input  em-u-text-align" type="text" value="" />
                            </div>

                            <div class="em-c-field__note"></div>
                        </div>

                    </div>

                       <div class="em-c-field__body em-c-field--small">
                        <select class="em-c-select em-u-width-100 em-u-font-style-semibold control-text ng-untouched ng-pristine ng-valid" name=" ">
                            <option value="-500">-- Select Status --</option>
                            <!---->
                            <option value="2">Pending
                            </option>
                            <option _ngcontent-c3="" value="0">Closed
                            </option>
                        </select>
                    </div>

                </div>

                <div class="em-l-grid__item">
                    <div class="em-c-field em-u-margin-left">
                        <div class="form-group">
                            <label class="em-c-field__label" for=""> DESCRIPTION</label>
                            <div class="em-c-field__body">
                                <input id="" class="em-c-input  em-u-text-align" type="text" value="Network Devices" />
                            </div>

                        </div>
                    </div>
                    
                    <div class="em-c-field ">
                        <div class="form-group" style="margin-top: 1.5rem; margin-left: 1.2rem;">
                            <label class="em-c-field__label" for="">RESPONSIBLE EMIT ORG</label>
                            <div class="em-c-field__body">
                                <input id="" class="em-c-input  em-u-text-align" type="text" value=" INF USCAN OPS NWF QUEB" />
                            </div>
                        </div>
                        <div class="em-c-field__note"></div>
                    </div>

                    
                    <div class="em-c-field " style="margin-left: 1.2rem;">
                        <div class="form-group">
                            <label class="em-c-field__label" for="">BUSINESS AREA</label>
                            <div class="em-c-field__body">
                                <input id="" class="em-c-input  em-u-text-align" type="text" value=" " />
                            </div>
                        </div>
                        <div class="em-c-field__note"></div>
                    </div>
                     <div class="em-c-field ">
                        <div class="form-group">
                            <label class="em-c-field__label" for="">SERVICE CENTER </label>
                            <div class="em-c-field__body">
                                <input id="" class="em-c-input  em-u-text-align" type="text" value="" />
                            </div>

                            <div class="em-c-field__note"></div>
                        </div>

                    </div>

                </div>

            </div>
        </div>
        <div style="float: right;" class="em-c-modal__footer em-c-text-passage em-c-text-passage--small">
            <button class="em-c-btn em-c-btn--primary em-js-modal-close-trigger">
                <span class="em-c-btn__text">Submit</span>
            </button>
        </div>
        <!-- end em-c-modal__footer -->
    </div>
    <!-- end em-c-modal__window -->
</div>
<!-- end em-c-modal -->

