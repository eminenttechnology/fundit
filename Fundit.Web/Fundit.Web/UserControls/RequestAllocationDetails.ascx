﻿<%@ Control Language="C#" AutoEventWireup="true" %>
<div   class="" name="allocation-tab-group">
    <div   class="custom-groupbutton-center">
        <div   class="em-c-field em-c-field--toggle">
            <div   class="em-c-field__body em-c-field--small">
                <div   class="em-c-toggle">
                    <input   class="em-c-toggle__input em-u-is-vishidden" id="toggle-1" type="radio" value="toggle-1">
                    <label   class="em-c-toggle__label" for="toggle-1" name="toggleCapitalTab">
                        Capital
                    </label>
                    <input   class="em-c-toggle__input em-u-is-vishidden" id="toggle-2" type="radio" value="toggle-2">
                    <label   class="em-c-toggle__label" for="toggle-2" name="toggleExpenseTab">
                        Expenses
                    </label>
                </div>

            </div>

        </div>
    </div>
    <div   class="" id="allocationTabForm">
        <div   class="em-c-table-object em-u-margin-right">
            <!---->
            <div  >
                <div   class="em-c-table-object__header em-u-margin-bottom-half">
                    <div   class="em-c-toolbar__item em-is-aligned-left">
                        <span   class="em-u-font-style-semibold" style="float: left">CAPITAL ALLOCATED RESOURCES </span>&nbsp;&nbsp;
              <!----><a href="#modal" class="em-c-modal__trigger em-js-modal-trigger">
                        <img   class="custom-add-button-width pointer em-u-margin-left-half" name="add-allocation-button" src="../../assets/standard/unity-1.2.0/images/icon_add_blue.png" style="float: left">
                  </a>  
                  </div>
                </div>


                <div   class="em-c-table-object__body ">
                    <div   class="em-c-table-object__body-inner">
                        <table   class="em-c-table  scroll " style="table-layout: fixed; width: 100%;">
                            <thead   class="em-c-table__header">
                                <tr   class="em-c-table__header-row ">
                                    <th   class="em-c-table__header-cell" scope="col" style="width: 7.7%;"></th>
                                    <th   class="em-c-table__header-cell " scope="col" style="width: 7.7%;">
                                        <span   class="em-u-font-size-small-2">STATUS</span>
                                        <button   class="em-c-btn--bare">
                                            <!---->
                                            <svg   class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                                <use   xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>

                                            <!---->
                                        </button>
                                    </th>
                                    <th   class="em-c-table__header-cell em-u-margin-right " scope="col" style="width: 7.7%;">
                                        <span   class="em-u-font-size-small-2">QUANTITY</span>
                                        <button   class="em-c-btn--bare">
                                            <!---->
                                            <svg   class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                                <use   xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>

                                            <!---->
                                        </button>
                                    </th>
                                    <th   class="em-c-table__header-cell em-u-margin-right" scope="col" style="word-wrap: break-word; width: 7.7%;">
                                        <span   class="em-u-font-size-small-2">AR ALLOCATION ($K)</span>
                                        <button   class="em-c-btn--bare">
                                            <!---->
                                            <svg   class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                                <use   xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>

                                            <!---->
                                        </button>
                                    </th>
                                    <th   class="em-c-table__header-cell " scope="col" style="width: 7.7%;">
                                        <span   class="em-u-font-size-small-2">AR #</span>
                                        <button   class="em-c-btn--bare">
                                            <!---->
                                            <svg   class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                                <use   xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>

                                            <!---->
                                        </button>
                                    </th>
                                    <th   class="em-c-table__header-cell " scope="col" style="width: 7.7%; word-wrap: break-word;">
                                        <span   class="em-u-font-size-small-2">COMPANY CODE</span>
                                        <button   class="em-c-btn--bare">
                                            <!---->
                                            <svg   class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                                <use   xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>

                                            <!---->
                                        </button>
                                    </th>
                                    <th   class="em-c-table__header-cell" scope="col" style="width: 7.7%; word-wrap: break-word;">
                                        <span   class="em-u-font-size-small-2">COUNTRY</span>
                                        <button   class="em-c-btn--bare">
                                            <!---->
                                            <svg   class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                                <use   xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>

                                            <!---->
                                        </button>
                                    </th>
                                    <th   class="em-c-table__header-cell " scope="col" style="width: 7.7%;">
                                        <span   class="em-u-font-size-small-2">WBS</span>
                                        <button   class="em-c-btn--bare">
                                            <!---->
                                            <svg   class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                                <use   xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>

                                            <!---->
                                        </button>
                                    </th>
                                    <th   class="em-c-table__header-cell " scope="col" style="width: 7.7%; word-wrap: break-word;">
                                        <span   class="em-u-font-size-small-2">Network Order</span>
                                        <button   class="em-c-btn--bare">
                                            <!---->
                                            <svg   class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                                <use   xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>

                                            <!---->
                                        </button>
                                    </th>
                                    <th   class="em-c-table__header-cell em-u-margin-right " scope="col" style="width: 7.7%;">
                                        <span   class="em-u-font-size-small-2">INSTALLATION LOCATION COST CENTER</span>
                                        <button   class="em-c-btn--bare">
                                            <!---->
                                            <svg   class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                                <use   xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>

                                            <!---->
                                        </button>
                                    </th>
                                    <th   class="em-c-table__header-cell " scope="col" style="word-wrap: break-word; width: 7.7%;">wb
                      <span   class="em-u-font-size-small-2">ASSET DESCRIPTION </span>
                                        <button   class="em-c-btn--bare">
                                            <!---->
                                            <svg   class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                                <use   xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>

                                            <!---->
                                        </button>
                                    </th>
                                    <th   class="em-c-table__header-cell " scope="col" style="width: 7.7%;">
                                        <span   class="em-u-font-size-small-2">ASSET TYPE</span>
                                        <button   class="em-c-btn--bare">
                                            <!---->
                                            <svg   class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                                <use   xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>

                                            <!---->
                                        </button>
                                    </th>
                                    <th   class="em-c-table__header-cell " scope="col" style="width: 7.7%;"></th>
                                </tr>

                            </thead>
                            <tbody   class="em-c-table__body">
                                <!---->
                                <tr   class="em-c-table__row ">
                                    <td   class="em-c-table__cell" colspan="" style="width: 7.7%;">
                                        <a href="#modal" class="em-c-modal__trigger em-js-modal-trigger">Edit</a>
                                    </td>
                                    <td   class="em-c-table__cell" colspan="" style="width: 7.7%;">Closed
                                    </td>
                                    <td   class="em-c-table__cell" colspan="" style="width: 7.7%;">1
                                    </td>
                                    <td   class="em-c-table__cell" colspan="" style="width: 7.7%;">70.00
                                    </td>
                                    <td   class="em-c-table__cell em-u-margin-right" colspan="" style="width: 7.7%;">15182001
                                    </td>
                                    <td   class="em-c-table__cell" colspan="" style="width: 7.7%;">0524
                                    </td>
                                    <td   class="em-c-table__cell" colspan="" style="width: 7.7%;"></td>
                                    <td   class="em-c-table__cell break-character" colspan="" style="width: 7.7%;">C1.00879
                                    </td>
                                    <td   class="em-c-table__cell break-character" colspan="" style="width: 7.7%;"></td>
                                    <td   class="em-c-table__cell break-character " colspan="" style="width: 7.7%; word-wrap: break-word;">N/A
                                    </td>
                                    <td   class="em-c-table__cell" colspan="" style="width: 7.7%; word-wrap: break-word;">various
                                    </td>
                                    <td   class="em-c-table__cell" colspan="" style="width: 7.7%;">Hardware
                                    </td>
                                    <td   class="em-c-table__cell" colspan="" style="width: 7.7%;">
                                        <!---->
                                    </td>
                                </tr>
                                <tr   class="em-c-table__row ">
                                    <td   class="em-c-table__cell" colspan="" style="width: 7.7%;">
                                        <a href="#modal" class="em-c-modal__trigger em-js-modal-trigger">Edit</a>
                                    </td>
                                    <td   class="em-c-table__cell" colspan="" style="width: 7.7%;">Closed
                                    </td>
                                    <td   class="em-c-table__cell" colspan="" style="width: 7.7%;">1
                                    </td>
                                    <td   class="em-c-table__cell" colspan="" style="width: 7.7%;">110.00
                                    </td>
                                    <td   class="em-c-table__cell em-u-margin-right" colspan="" style="width: 7.7%;">15182002
                                    </td>
                                    <td   class="em-c-table__cell" colspan="" style="width: 7.7%;">0524
                                    </td>
                                    <td   class="em-c-table__cell" colspan="" style="width: 7.7%;"></td>
                                    <td   class="em-c-table__cell break-character" colspan="" style="width: 7.7%;">C1.00880
                                    </td>
                                    <td   class="em-c-table__cell break-character" colspan="" style="width: 7.7%;"></td>
                                    <td   class="em-c-table__cell break-character " colspan="" style="width: 7.7%; word-wrap: break-word;">C0802MRU60
                                    </td>
                                    <td   class="em-c-table__cell" colspan="" style="width: 7.7%; word-wrap: break-word;">TMS6
                                    </td>
                                    <td   class="em-c-table__cell" colspan="" style="width: 7.7%;">Software
                                    </td>
                                    <td   class="em-c-table__cell" colspan="" style="width: 7.7%;">
                                        <!---->
                                    </td>
                                </tr>
                            </tbody>
                            <tfoot   class="em-c-table__footer">
                                <tr   class="em-c-table__footer-row  custom-table-background em-u-font-size-med-2 ">
                                    <td   class="em-c-table__footer-cell em-u-font-size-med-2 " style="max-width: 7.7%;">TOTAL:</td>
                                    <td   class="em-c-table__footer-cell em-u-font-size-med-2 " style="max-width: 7.7%;"></td>
                                    <td   class="em-c-table__footer-cell em-u-font-size-med-2 " style="max-width: 7.7%;">2</td>
                                    <td   class="em-c-table__footer-cell em-u-font-size-med-2 " style="max-width: 7.7%;">$180.00K</td>
                                    <td   class="em-c-table__footer-cell custom-table-text-right em-u-font-size-med" colspan="9">
                                        <span   class=" em-u-font-style-bold">Unallocated - Capex &nbsp;&nbsp;</span>$20,000.00</td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>

            </div>

            <!---->
        </div>

    </div>
</div>



<div class="em-c-modal em-js-modal em-is-closed" id="modal">
    <div class="em-c-modal__window em-js-modal-window">
        <div class="em-c-modal__header">
            <h3 class="em-c-modal__title">Edit</h3>
            <button class="em-c-btn em-c-btn--bare em-c-modal__close-btn em-js-modal-close-trigger">
                <span class="em-c-btn__text">Close</span>
            </button>
            <!-- end em-c-btn -->
        </div>
        <!-- end em-c-modal__header -->
        <div class="em-c-modal__body em-c-text-passage em-c-text-passage--small">
            <div class="em-l-grid em-l-grid--2up ">
                <div class="em-l-grid__item">


                    <div class="em-c-field ">
                        <div class="form-group" style="margin-bottom: -1rem;">
                            <label class="em-c-field__label">
                                <span style="float: left">COMPANY CODE</span>

                                <span style="clear: both;"></span>
                            </label>
                            <div class="em-c-field__body" style="clear: both;">
                                <input id="" class="em-c-input  em-u-text-align" type="text" onchange="changeStateDropdown(event.target.value)" value="" />
                            </div>
                        </div>

                        <div class="em-c-field__note">
                        </div>
                    </div>

                    <div class="em-c-field ">
                        <div class="form-group">
                            <label class="em-c-field__label" for="">ASSET TYPE</label>
                            <div class="em-c-field__body">
                                <select style="width: 100% !important">
                                    <option value="Full CloseOut">Hardware</option>
                                    <option value="Partial CloseOut">Software</option>
                                    <option value="Partial CloseOut">Capitalized Staffing</option>
                                </select>
                            </div>
                        </div>
                        <div class="em-c-field__note"></div>
                    </div>

                    <div class="em-c-field ">
                        <div class="form-group">
                            <label class="em-c-field__label" for="">QUANTITY</label>
                            <div class="em-c-field__body">
                                <input id="" class="em-c-input  em-u-text-align" type="text" value="" />
                            </div>
                        </div>
                        <div class="em-c-field__note"></div>
                    </div>

                    <div class="em-c-field ">
                        <div class="form-group" style="visibility: hidden" id= "btn-state-div">
                            <label class="em-c-field__label"  style="visibility: hidden"  id= "btn-state-label"for="">STATE </label>
                            <div class="em-c-field__body">
                                <select style="width: 100% !important; visibility: hidden" id= "btn-state-dropdown" >
                                    <option value="select ">Select State</option>
                                    <option value="Texas">Texas</option>
                                    <option value="Alaska">Alaska</option>
                                    <option value="Arizona">Arizona</option>
                                    <option value="Arkansas">Arkansas</option>
                                    
                                </select>


                            </div>
                        </div>
                        <div class="em-c-field__note"></div>
                    </div>
          
                    
                          <div class="em-c-field " id="installation-location-div-block"  style="">
                        <label class="em-c-field__label" id="installation-location-label" style="visibility: hidden; " for="">INSTALLATION LOCATION </label>
                        <div class="form-group"  id="show-installation-location" style="visibility: hidden; ">

                        </div>
                    </div>
                </div>
                <div class="em-l-grid__item">
                    <div class="em-c-field em-u-margin-left">
                        <div class="form-group" style="margin-bottom: 0.5rem;">
                            <label class="em-c-field__label" for="">ASSET DESCRIPTION</label>
                            <div class="em-c-field__body">
                                <input id="" class="em-c-input  em-u-text-align" type="text" value="Network Devices" />
                            </div>

                        </div>
                    </div>



                    <div class="em-c-field " style="margin-left: 1.2rem;">
                        <div class="form-group">
                            <label style="margin-bottom: 1rem;" class="em-c-field__label" for="">AR ALLOCATION ($K)</label>
                            <div class="em-c-field__body">
                                <input id="" class="em-c-input  em-u-text-align" type="text" value="50" />
                            </div>
                        </div>
                        <div class="em-c-field__note"></div>
                    </div>

                    <div class="em-c-field " style="margin-left: 1.2rem;">
                        <div class="form-group">
                            <label class="em-c-field__label" id="support-org-label" style="visibility:hidden; ">SUPPORT ORGANIZATION</label>
                            <div class="em-c-field__body">

                                <input id="support-org-input" style="visibility:hidden;" class="em-c-input  em-u-text-align" type="text" value="" />
                            </div>
                        </div>
                        <div class="em-c-field__note"></div>
                    </div>


                              
                    <div class="em-c-field " style="margin-left:1.2rem;">
                        
                            <label class="em-c-field__label" id="city-label" style="visibility:hidden;" for="">CITY </label>
                        <div class="form-group" id="btn-city-dropdown" >
                        </div>
                        <div class="em-c-field__note"></div>
                    </div>

                   

                </div>


            </div>
        </div>
        <div style="float: right;" class="em-c-modal__footer em-c-text-passage em-c-text-passage--small">
            <button class="em-c-btn em-c-btn--primary em-js-modal-close-trigger">
                <span class="em-c-btn__text">Submit</span>
            </button>
        </div>
        <!-- end em-c-modal__footer -->
    </div>
    <!-- end em-c-modal__window -->

</div>




