﻿<%@ Control Language="C#" AutoEventWireup="true" %>
<div class="em-c-table-object__body ">
    <div class="em-c-table-object__body-inner">
        <table class="em-c-table  scroll " style="table-layout: fixed; width: 100%;">
            <thead class="em-c-table__header">
                <tr class="em-c-table__header-row ">
                    <th class="em-c-table__header-cell" scope="col" style="width: 7.7%;"></th>
                    <th class="em-c-table__header-cell " scope="col" style="width: 7.7%;">
                        <span class="em-u-font-size-small-2">STATUS</span>
                        <button class="em-c-btn--bare">

                            <svg class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                <use xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>


                        </button>
                    </th>
                    <th class="em-c-table__header-cell em-u-margin-right " scope="col" style="width: 7.7%;">
                        <span class="em-u-font-size-small-2">QUANTITY</span>
                        <button class="em-c-btn--bare">

                            <svg class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                <use xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>


                        </button>
                    </th>
                    <th class="em-c-table__header-cell em-u-margin-right" scope="col" style="word-wrap: break-word; width: 7.7%;">
                        <span class="em-u-font-size-small-2">AR ALLOCATION ($K)</span>
                        <button class="em-c-btn--bare">

                            <svg class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                <use xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>


                        </button>
                    </th>
                    <th class="em-c-table__header-cell " scope="col" style="width: 7.7%;">
                        <span class="em-u-font-size-small-2">AR #</span>
                        <button class="em-c-btn--bare">

                            <svg class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                <use xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>


                        </button>
                    </th>
                    <th class="em-c-table__header-cell " scope="col" style="width: 7.7%; word-wrap: break-word;">
                        <span class="em-u-font-size-small-2">COMPANY CODE</span>
                        <button class="em-c-btn--bare">

                            <svg class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                <use xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>


                        </button>
                    </th>
                    <th class="em-c-table__header-cell" scope="col" style="width: 7.7%; word-wrap: break-word;">
                        <span class="em-u-font-size-small-2">COUNTRY</span>
                        <button class="em-c-btn--bare">

                            <svg class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                <use xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>


                        </button>
                    </th>
                    <th class="em-c-table__header-cell " scope="col" style="width: 7.7%;">
                        <span class="em-u-font-size-small-2">WBS</span>
                        <button class="em-c-btn--bare">

                            <svg class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                <use xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>


                        </button>
                    </th>
                    <th class="em-c-table__header-cell " scope="col" style="width: 7.7%; word-wrap: break-word;">
                        <span class="em-u-font-size-small-2">NETWORK ORDER</span>
                        <button class="em-c-btn--bare">

                            <svg class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                <use xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>


                        </button>
                    </th>
                    <th class="em-c-table__header-cell em-u-margin-right " scope="col" style="width: 7.7%;">
                        <span class="em-u-font-size-small-2">INSTALLATION COST CENTER</span>
                        <button class="em-c-btn--bare">

                            <svg class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                <use xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>


                        </button>
                    </th>
                    <th class="em-c-table__header-cell " scope="col" style="word-wrap: break-word; width: 7.7%;">
                        <span class="em-u-font-size-small-2">ASSET DESCRIPTION </span>
                        <button class="em-c-btn--bare">

                            <svg class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                <use xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>


                        </button>
                    </th>
                    <th class="em-c-table__header-cell " scope="col" style="width: 7.7%;">
                        <span class="em-u-font-size-small-2">ASSET TYPE</span>
                        <button class="em-c-btn--bare">

                            <svg class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                <use xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>


                        </button>
                    </th>
                    <th class="em-c-table__header-cell " scope="col" style="width: 7.7%;"></th>
                </tr>

            </thead>
            <tbody class="em-c-table__body">
                <tr class="em-c-table__row ">
                    <td class="em-c-table__cell" colspan="" style="width: 7.7%;">
                        <input  type="checkbox" name="checkbox-1" value="" class="em-c-input-group__control"> 
                      <a  href="#modal2" class="em-c-modal__trigger em-js-modal-trigger" style="padding-left: 1rem;"> Edit </a>
                    </td>
                    <td class="em-c-table__cell" colspan="" style="width: 7.7%;">Pending
                    </td>
                    <td class="em-c-table__cell" colspan="" style="width: 7.7%;">1
                    </td>
                    <td class="em-c-table__cell" colspan="" style="width: 7.7%;">70.00
                    </td>
                    <td class="em-c-table__cell em-u-margin-right" colspan="" style="width: 7.7%;">15182001
                    </td>
                    <td class="em-c-table__cell" colspan="" style="width: 7.7%;">0572
                    </td>
                    <td class="em-c-table__cell" colspan="" style="width: 7.7%;">Belgium
                    </td>
                    <td class="em-c-table__cell break-character" colspan="" style="width: 7.7%;">C1.00879
                    </td>
                    <td class="em-c-table__cell break-character" colspan="" style="width: 7.7%;"></td>
                    <td class="em-c-table__cell break-character " colspan="" style="width: 7.7%; word-wrap: break-word;">C0802W1PMO
                    </td>
                    <td class="em-c-table__cell" colspan="" style="width: 7.7%; word-wrap: break-word;">Network Devices
                    </td>
                    <td class="em-c-table__cell" colspan="" style="width: 7.7%;">Hardware
                    </td>
                    <td class="em-c-table__cell" colspan="" style="width: 7.7%; margin-top: -2rem;padding-top: 0rem;">
                        <svg class="em-c-icon" style="width:20px;"  onclick="window.location.href='../Request/ARHistory.aspx'">
                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../../images/em-icons.svg#icon-caution"></use>
                        </svg>
                    </td>
                </tr>
                <tr class="em-c-table__row ">
                    <td class="em-c-table__cell" colspan="" style="width: 7.7%;">
                        <input id="inline-check-1" type="checkbox" name="checkbox-1" value="" class="em-c-input-group__control">
                      <a _ngcontent-c9="" href="#modal2" class="em-c-modal__trigger em-js-modal-trigger" style="padding-left: 1rem;"> Edit </a> 
                    </td>
                    <td class="em-c-table__cell" colspan="" style="width: 7.7%;">Closed
                    </td>
                    <td class="em-c-table__cell" colspan="" style="width: 7.7%;">1
                    </td>
                    <td class="em-c-table__cell" colspan="" style="width: 7.7%;">110.00
                    </td>
                    <td class="em-c-table__cell em-u-margin-right" colspan="" style="width: 7.7%;">15182002
                    </td>
                    <td class="em-c-table__cell" colspan="" style="width: 7.7%;">3459
                    </td>
                    <td class="em-c-table__cell" colspan="" style="width: 7.7%;">Croatia
                    </td>
                    <td class="em-c-table__cell break-character" colspan="" style="width: 7.7%;">C1.00880
                    </td>
                    <td class="em-c-table__cell break-character" colspan="" style="width: 7.7%;"></td>
                    <td class="em-c-table__cell break-character " colspan="" style="width: 7.7%; word-wrap: break-word;">N/A
                    </td>
                    <td class="em-c-table__cell" colspan="" style="width: 7.7%; word-wrap: break-word;">Network Licenses
                    </td>
                    <td class="em-c-table__cell" colspan="" style="width: 7.7%;">Software
                    </td>
                    <td class="em-c-table__cell" colspan="" style="width: 7.7%; margin-top: -2rem;padding-top: 0rem;">
                        <svg class="em-c-icon" style="width:20px;"  onclick="window.location.href='../Request/ARHistory.aspx'">
                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../../images/em-icons.svg#icon-caution"></use>
                        </svg>

                    </td>
                </tr>
            </tbody>
            <tfoot class="em-c-table__footer">
                <tr class="em-c-table__footer-row  custom-table-background em-u-font-size-med-2 ">
                    <td class="em-c-table__footer-cell em-u-font-size-med-2 " style="max-width: 7.7%;">TOTAL:</td>
                    <td class="em-c-table__footer-cell em-u-font-size-med-2 " style="max-width: 7.7%;"></td>
                    <td class="em-c-table__footer-cell em-u-font-size-med-2 " style="max-width: 7.7%;">2</td>
                    <td class="em-c-table__footer-cell em-u-font-size-med-2 " style="max-width: 7.7%;">$180.00K</td>
                    <td class="em-c-table__footer-cell custom-table-text-right em-u-font-size-med" colspan="9">
                        <span class=" em-u-font-style-bold">Unallocated - Capex &nbsp;&nbsp;</span>$20,000.00</td>
                </tr>
            </tfoot>
        </table>
    </div>
</div>

