﻿<%@ Control Language="C#" AutoEventWireup="true" %>
  <div _ngcontent-c9="" class="em-c-table-object__body ">
      <div class="em-c-table-object__header">
                            <div class="em-c-collapsible-toolbar em-js-collapsible-toolbar">
                                <div class="em-c-collapsible-toolbar__panel em-js-toolbar-panel">
                                    <div class="em-c-toolbar ">
                                        <div class="em-c-toolbar__item ">
                                            <span class="em-u-font-style-semibold em-u-font-size-med-3"> ADD I/O
                                                <img  href="#modal" class="em-c-modal__trigger em-js-modal-trigger" class="em-c-btn--bare" _ngcontent-c5="" 
                                            name="add-allocation-button" src="../../assets/standard/unity-1.2.0/images/icon_add_blue.png" style="float: left">
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
            <div _ngcontent-c9="" class="em-c-table-object__body-inner">
              <table _ngcontent-c9="" class="em-c-table  scroll " style="table-layout:fixed; width:100%;">
                <thead _ngcontent-c9="" class="em-c-table__header">
                  <tr _ngcontent-c9="" class="em-c-table__header-row ">
                    <th _ngcontent-c9="" class="em-c-table__header-cell" scope="col" style="width: 7.7%;">
                    </th>
                    <th _ngcontent-c9="" class="em-c-table__header-cell " scope="col" style="width: 7.7%;">
                      <span _ngcontent-c9="" class="em-u-font-size-small-2">STATUS</span>
                      <button _ngcontent-c9="" class="em-c-btn--bare">
                        <!---->
                          <svg _ngcontent-c9="" class="em-c-icon em-c-icon--tiny em-c-icon--green">
                            <use _ngcontent-c9="" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                          </svg>
                        
                        <!---->
                      </button>
                    </th>
                    
                    <th _ngcontent-c9="" class="em-c-table__header-cell em-u-margin-right" scope="col" style="word-wrap: break-word; width: 7.7%;">
                      <span _ngcontent-c9="" class="em-u-font-size-small-2">IO #</span>
                      <button _ngcontent-c9="" class="em-c-btn--bare">
                        <!---->
                          <svg _ngcontent-c9="" class="em-c-icon em-c-icon--tiny em-c-icon--green">
                            <use _ngcontent-c9="" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                          </svg>
                        
                        <!---->
                      </button>
                    </th>
                      <th _ngcontent-c9="" class="em-c-table__header-cell " scope="col" style="width: 7.7%;">
                      <span _ngcontent-c9="" class="em-u-font-size-small-2">TYPE</span>
                      <button _ngcontent-c9="" class="em-c-btn--bare">
                        <!---->
                          <svg _ngcontent-c9="" class="em-c-icon em-c-icon--tiny em-c-icon--green">
                            <use _ngcontent-c9="" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                          </svg>
                        
                        <!---->
                      </button>
                    </th>
                           <th _ngcontent-c9="" class="em-c-table__header-cell " scope="col" style="word-wrap: break-word; width: 7.7%;">
                      <span _ngcontent-c9="" class="em-u-font-size-small-2"> DESCRIPTION </span>
                      <button _ngcontent-c9="" class="em-c-btn--bare">
                        <!---->
                          <svg _ngcontent-c9="" class="em-c-icon em-c-icon--tiny em-c-icon--green">
                            <use _ngcontent-c9="" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                          </svg>
                        
                        <!---->
                      </button>
                    </th>
                   
                    <th _ngcontent-c9="" class="em-c-table__header-cell em-u-margin-right " scope="col" style="width: 7.7%;">
                      <span _ngcontent-c9="" class="em-u-font-size-small-2">RECEIVING ORDER</span>
                      <button _ngcontent-c9="" class="em-c-btn--bare">
                        <!---->
                          <svg _ngcontent-c9="" class="em-c-icon em-c-icon--tiny em-c-icon--green">
                            <use _ngcontent-c9="" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                          </svg>
                        
                        <!---->
                      </button>
                    </th>
                  
                    <th _ngcontent-c9="" class="em-c-table__header-cell" scope="col" style="width: 7.7%;word-wrap: break-word;">
                      <span _ngcontent-c9="" class="em-u-font-size-small-2">PAY ROLL COST CENTER</span>
                      <button _ngcontent-c9="" class="em-c-btn--bare">
                        <!---->
                          <svg _ngcontent-c9="" class="em-c-icon em-c-icon--tiny em-c-icon--green">
                            <use _ngcontent-c9="" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                          </svg>
                        
                        <!---->
                      </button>
                    </th>
                      <th _ngcontent-c9="" class="em-c-table__header-cell" scope="col" style="width: 7.7%;word-wrap: break-word;">
                      <span _ngcontent-c9="" class="em-u-font-size-small-2">RESPONSIBLE EMIT ORG</span>
                      <button _ngcontent-c9="" class="em-c-btn--bare">
                        <!---->
                          <svg _ngcontent-c9="" class="em-c-icon em-c-icon--tiny em-c-icon--green">
                            <use _ngcontent-c9="" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                          </svg>
                        
                        <!---->
                      </button>
                    </th>
                  </tr>
                  
                </thead>
                <tbody _ngcontent-c9="" class="em-c-table__body">
                  <!----><tr _ngcontent-c9="" class="em-c-table__row " >
                    <td _ngcontent-c9="" class="em-c-table__cell" colspan="" style="width: 7.7%;">
                      <a _ngcontent-c9="" href="#modal2" class="em-c-modal__trigger em-js-modal-trigger">Edit</a>
                    </td>
                    <td _ngcontent-c9="" class="em-c-table__cell" colspan="" style="width: 7.7%;">Closed
                    </td>
                    <td _ngcontent-c9="" class="em-c-table__cell" colspan="" style="width: 7.7%;">842226
                    </td>
                    <td _ngcontent-c9="" class="em-c-table__cell em-u-margin-right" colspan="" style="width: 7.7%;">Timewriting
                    </td>
                    <td _ngcontent-c9="" class="em-c-table__cell" colspan="" style="width: 7.7%; word-wrap: break-word;">Sarnia Radio Remediation

                    </td>
                    <td _ngcontent-c9="" class="em-c-table__cell break-character " colspan="" style="width: 7.7%; word-wrap: break-word;">G802075385
                    </td>
                    <td _ngcontent-c9="" class="em-c-table__cell" colspan="" style="width: 7.7%;">2299-122994
                    </td>
                      <td _ngcontent-c9="" class="em-c-table__cell" colspan="" style="width: 7.7%;">INF USCAN OPS NWF QUEB
                    </td>
                  </tr><tr _ngcontent-c9="" class="em-c-table__row ">
                    <td _ngcontent-c9="" class="em-c-table__cell" colspan="" style="width: 7.7%;">
                      <a _ngcontent-c9="" href="#modal2" class="em-c-modal__trigger em-js-modal-trigger">Edit</a>
                    </td>
                    <td _ngcontent-c9="" class="em-c-table__cell" colspan="" style="width: 7.7%;">Closed
                    </td>
                    <td _ngcontent-c9="" class="em-c-table__cell" colspan="" style="width: 7.7%;">846892
                    </td>
                    <td _ngcontent-c9="" class="em-c-table__cell em-u-margin-right" colspan="" style="width: 7.7%;">Travel & Expense
                    </td>
                    <td _ngcontent-c9="" class="em-c-table__cell break-character " colspan="" style="width: 7.7%; word-wrap: break-word;">G802074983
                    </td>
                    <td _ngcontent-c9="" class="em-c-table__cell" colspan="" style="width: 7.7%; word-wrap: break-word;">Phone devices < $2K
                    </td>
                      <td _ngcontent-c9="" class="em-c-table__cell" colspan="" style="width: 7.7%;">C0805U5441
                    </td>
                      <td _ngcontent-c9="" class="em-c-table__cell" colspan="" style="width: 7.7%;"> UIT EMEC ME/Tech/BD Tech Supp
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>



<div class="em-c-modal em-js-modal em-is-closed" id="modal">
    <div class="em-c-modal__window em-js-modal-window">
        <div class="em-c-modal__header">
            <h3 class="em-c-modal__title">Add</h3>
            <button class="em-c-btn em-c-btn--bare em-c-modal__close-btn em-js-modal-close-trigger">
                <span class="em-c-btn__text">Close</span>
            </button>
            <!-- end em-c-btn -->
        </div>
        <!-- end em-c-modal__header -->
        <div class="em-c-modal__body em-c-text-passage em-c-text-passage--small">
            <div class="em-l-grid em-l-grid--2up ">
                <div class="em-l-grid__item">

                    

                    <div class="em-c-field ">
                        <div class="form-group">
                            <label class="em-c-field__label" for="">COMPANY CODE</label>
                            <div class="em-c-field__body">
                                <input id="" class="em-c-input  em-u-text-align" type="text" value="" />
                            </div>

                            <div class="em-c-field__note"></div>
                        </div>

                    </div>

                                     

                    <div class="em-c-field ">
                        <div class="form-group">
                            <label class="em-c-field__label">
                                <span style="float: left">TYPE</span>

                                <span style="clear: both;"></span>
                            </label>
                            <div class="em-c-field__body" style="clear: both;">
                                <select style="width: 100% !important;">
                                  <option value="Travel">Travel</option>
                                  <option value="Expense">Expense</option>
                                  <option value="Timewriting">Timewriting</option>
                                </select>
                            </div>
                        </div>

                        <div class="em-c-field__note">
                        </div>
                    </div>

                    <div class="em-c-field ">
                        <div class="form-group">
                            <label class="em-c-field__label" for="">WBS CUSTOMER</label>
                            <div class="em-c-field__body">
                                <input id="" class="em-c-input  em-u-text-align" type="text" value=" " />
                            </div>
                        </div>
                        <div class="em-c-field__note"></div>
                    </div>

                    <div class="em-c-field " >
                        <div class="form-group">
                            <label class="em-c-field__label" for=""> PROFIT CENTER</label>
                            <div class="em-c-field__body">
                                <input id="" class="em-c-input  em-u-text-align" type="text" value="" />
                            </div>
                        </div>
                        <div class="em-c-field__note"></div>
                    </div>

                    <div class="em-c-field " >
                        <div class="form-group">
                            <label class="em-c-field__label" for="">EMIT COST CENTER</label>
                            <div class="em-c-field__body">
                                <input id="" class="em-c-input  em-u-text-align" type="text" value="" />
                            </div>
                        </div>
                        <div class="em-c-field__note"></div>
                    </div>

                    

                

                    <div class="em-c-field " >
                        <div class="form-group">
                            <label class="em-c-field__label" for=""> PROVINCE </label>
                            <div class="em-c-field__body">
                                <select style="width: 100% !important;">
                                    <option value="">QUEBEC</option>
                                    <option value="">MANITOBA</option>
                                    <option value="">ONTARIO</option>
                                </select>
                            </div>
                        </div>
                        <div class="em-c-field__note"></div>
                    </div>

                </div>
                <div class="em-l-grid__item">
                    <div class="em-c-field em-u-margin-left">
                        <div class="form-group">
                            <label class="em-c-field__label" for=""> DESCRIPTION</label>
                            <div class="em-c-field__body">
                                <input id="" class="em-c-input  em-u-text-align" type="text" value="" />
                            </div>

                        </div>
                    </div>
                    
                    <div class="em-c-field ">
                        <div class="form-group" style="margin-top: 1.5rem; margin-left: 1.2rem;">
                            <label class="em-c-field__label" for="">FAD #</label>
                            <div class="em-c-field__body">
                                <input id="" class="em-c-input  em-u-text-align" type="text" value=" " />
                            </div>
                        </div>
                        <div class="em-c-field__note"></div>
                    </div>

                    
                    <div class="em-c-field " style="margin-left: 1.2rem;">
                        <div class="form-group">
                            <label class="em-c-field__label" for="">RESPONSIBLE EMIT ORG</label>
                            <div class="em-c-field__body">
                                <input id="" class="em-c-input  em-u-text-align" type="text" value=" " />
                            </div>
                        </div>
                        <div class="em-c-field__note"></div>
                    </div>

                    <div class="em-c-field " style="margin-left: 1.2rem;">
                        <div class="form-group">
                            <label class="em-c-field__label" for="">PAY ROLL COST CENTER</label>
                            <div class="em-c-field__body">
                                <input id="" class="em-c-input  em-u-text-align" type="text" value=" " />
                            </div>
                        </div>
                        <div class="em-c-field__note"></div>
                    </div> 

                    <div class="em-c-field " style="margin-left: 1.2rem;" >
                        <div class="form-group">
                            <label class="em-c-field__label" for=""> RECEIVING ORDER </label>
                            <div class="em-c-field__body">
                                <select style="width: 100% !important;">
                                    <option value="">EMT000287</option>
                                </select>
                            </div>
                        </div>
                        <div class="em-c-field__note"></div>
                    </div>  


                </div>




            </div>
        </div>
        <div style="float: right;" class="em-c-modal__footer em-c-text-passage em-c-text-passage--small">
            <button class="em-c-btn em-c-btn--primary em-js-modal-close-trigger">
                <span class="em-c-btn__text">Submit</span>
            </button>
        </div>
        <!-- end em-c-modal__footer -->
    </div>
    <!-- end em-c-modal__window -->
</div>
<!-- end em-c-modal -->


<div class="em-c-modal em-js-modal em-is-closed" id="modal2">
    <div class="em-c-modal__window em-js-modal-window">
        <div class="em-c-modal__header">
            <h3 class="em-c-modal__title">Edit</h3>
            <button class="em-c-btn em-c-btn--bare em-c-modal__close-btn em-js-modal-close-trigger">
                <span class="em-c-btn__text">Close</span>
            </button>
            <!-- end em-c-btn -->
        </div>
        <!-- end em-c-modal__header -->
        <div class="em-c-modal__body em-c-text-passage em-c-text-passage--small">
            <div class="em-l-grid em-l-grid--2up ">
                <div class="em-l-grid__item">

                    

                    <div class="em-c-field ">
                        <div class="form-group">
                            <label class="em-c-field__label" for="">COMPANY CODE</label>
                            <div class="em-c-field__body">
                                <input id="" class="em-c-input  em-u-text-align" type="text" value="0325 : ExxonMobil Canada Ltd." />
                            </div>

                            <div class="em-c-field__note"></div>
                        </div>

                    </div>

                                     

                    <div class="em-c-field ">
                        <div class="form-group">
                            <label class="em-c-field__label">
                                <span style="float: left">TYPE</span>

                                <span style="clear: both;"></span>
                            </label>
                            <div class="em-c-field__body" style="clear: both;">
                                <select style="width: 100% !important;">
                                  <option value="Travel">Travel</option>
                                  <option value="Expense" selected>Expense</option>
                                  <option value="Timewriting">Timewriting</option>
                                </select>
                            </div>
                        </div>

                        <div class="em-c-field__note">
                        </div>
                    </div>

                    <div class="em-c-field ">
                        <div class="form-group">
                            <label class="em-c-field__label" for="">WBS CUSTOMER</label>
                            <div class="em-c-field__body">
                                <input id="" class="em-c-input  em-u-text-align" type="text" value=" " />
                            </div>
                        </div>
                        <div class="em-c-field__note"></div>
                    </div>

                    

                    <div class="em-c-field " >
                        <div class="form-group">
                            <label class="em-c-field__label" for="">EMIT COST CENTER</label>
                            <div class="em-c-field__body">
                                <input id="" class="em-c-input  em-u-text-align" type="text" value="" />
                            </div>
                        </div>
                        <div class="em-c-field__note"></div>
                    </div>

                    <div class="em-c-field " >
                        <div class="form-group">
                            <label class="em-c-field__label" for=""> PROFIT CENTER</label>
                            <div class="em-c-field__body">
                                <input id="" class="em-c-input  em-u-text-align" type="text" value="" />
                            </div>
                        </div>
                        <div class="em-c-field__note"></div>
                    </div>

                    

                

                    <div class="em-c-field " >
                        <div class="form-group">
                            <label class="em-c-field__label" for=""> PROVINCE </label>
                            <div class="em-c-field__body">
                                <select style="width: 100% !important;">
                                    <option value="">QUEBEC</option>
                                    <option value="" selected>MANITOBA</option>
                                    <option value="">ONTARIO</option>
                                </select>
                            </div>
                        </div>
                        <div class="em-c-field__note"></div>
                    </div>

                </div>
                <div class="em-l-grid__item">'

                    <div class="em-c-field em-u-margin-left" style="margin-top: -2rem;">
                        <div class="form-group">
                            <label class="em-c-field__label" for=""> I/O #</label>
                            <div class="em-c-field__body">
                                <input id="" class="em-c-input  em-u-text-align" type="text" value="842226" />
                            </div>

                        </div>
                    </div>

                    <div class="em-c-field em-u-margin-left" style="margin-top: 2rem;">
                        <div class="form-group">
                            <label class="em-c-field__label" for=""> DESCRIPTION</label>
                            <div class="em-c-field__body">
                                <input id="" class="em-c-input  em-u-text-align" type="text" value="Travel Expenses" />
                            </div>

                        </div>
                    </div>
                    
                    <div class="em-c-field ">
                        <div class="form-group" style="margin-top: 1.5rem; margin-left: 1.2rem;">
                            <label class="em-c-field__label" for="">FAD #</label>
                            <div class="em-c-field__body">
                                <input id="" class="em-c-input  em-u-text-align" type="text" value="FAD90001" />
                            </div>
                        </div>
                        <div class="em-c-field__note"></div>
                    </div>

                    
                    <div class="em-c-field " style="margin-left: 1.2rem;">
                        <div class="form-group">
                            <label class="em-c-field__label" for="">RESPONSIBLE EMIT ORG</label>
                            <div class="em-c-field__body">
                                <input id="" class="em-c-input  em-u-text-align" type="text" value="EMIT Upstream Business IT" />
                            </div>
                        </div>
                        <div class="em-c-field__note"></div>
                    </div>

                    <div class="em-c-field " style="margin-left: 1.2rem;">
                        <div class="form-group">
                            <label class="em-c-field__label" for="">PAY ROLL COST CENTER</label>
                            <div class="em-c-field__body">
                                <input id="" class="em-c-input  em-u-text-align" type="text" value=" " />
                            </div>
                        </div>
                        <div class="em-c-field__note"></div>
                    </div> 

                    <div class="em-c-field " style="margin-left: 1.2rem;">
                        <div class="form-group">
                            <label class="em-c-field__label" for=""> RECEIVING ORDER </label>
                            <div class="em-c-field__body">
                                <select style="width: 100% !important;">
                                    <option value="">EMT000287</option>
                                </select>
                            </div>
                        </div>
                        <div class="em-c-field__note"></div>
                    </div>  


                </div>




            </div>
        </div>
        <div style="float: right;margin-top: -3rem!important;" class="em-c-modal__footer em-c-text-passage em-c-text-passage--small">
            <button class="em-c-btn em-c-btn--primary em-js-modal-close-trigger">
                <span class="em-c-btn__text">Submit</span>
            </button>
        </div>
        <!-- end em-c-modal__footer -->
    </div>
    <!-- end em-c-modal__window -->
</div>
<!-- end em-c-modal -->

