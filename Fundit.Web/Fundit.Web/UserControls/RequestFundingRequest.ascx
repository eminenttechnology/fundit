﻿<%@ Control Language="C#" AutoEventWireup="true" %>
<div class="em-l-container">

    <div class="em-c-table-object ">
      <div class="em-c-table-object__header">
      </div>
      <!--end em-c-table-object__header-->
      <div class="em-c-table-object__body">
        <div class="em-c-table-object__body-inner">
          <table class="em-c-table em-c-table--condensed em-u-padding-bottom" border="0">
            <thead>
              <tr class="em-c-table__header-row em-u-font-style-bold">
                <th scope="col" class=" custom-em-c-table__header">
                  Funding Request ($K)</th>
                <th scope="col" class="custom-em-c-table__header" style="text-align: center">Previously Endorsed</th>
                <th scope="col" class="custom-em-c-table__header" style="text-align: center">Incremental Request</th>
                <th scope="col" class="custom-em-c-table__header" style="text-align: center">Current Cumulative</th>
                <th scope="col" class="custom-em-c-table__header" style="text-align: center">Total Project (MPCE)</th>
                <th></th>
              </tr>
              <!-- em-c-table__header-row -->
            </thead>
            <!-- end em-c-table__header -->
            <tbody class="em-c-table__body em-u-padding-bottom">
              <tr class="em-u-padding-top-half">
                <td class="em-c-table__cell"></td>
              </tr>
              <tr class="em-u-padding-top-half">
                <td class="em-u-padding-left-none">
                  <div class="em-u-font-style-regular em-u-font-style-semibold em-u-text-align-left"> IT OPEX </div>
                </td>

                <td class="em-c-table__cell  ">
                  <div class="em-c-field__body em-c-field--small " *ngIf="togglePreviouslyEndorsed && !requestInReadOnlyState; else elseItOpex">  
                    <span *ngIf="!requestInReadOnlyState">
                        <label _ngcontent-c8="" class="em-c-field__body em-c-field--small input-budget-request">
                        100.00
                      </label>
                    </span>
                  </div>
                </td>
                <td class="em-c-table__cell">
                  <div *ngIf="!requestInReadOnlyState" class="em-c-field__body em-c-field--small">
                    <label _ngcontent-c8="" class="em-c-field__body em-c-field--small input-budget-request">
                      0
                    </label>                                  
                  </div>
                  <div *ngIf="requestInReadOnlyState" class="em-c-field__body em-c-field--small em-u-text-align-right">
                    <label>

                    </label>
                  </div>
                </td>
                <td class="em-c-table__cell em-u-text-align-right" align="middle">
                  <div class="em-c-field__body em-c-field--small">
                    <label name="EmitOpexCurrentCumulativeAmount" id="EmitOpexCurrentCumulativeAmount" (onchange)="requestSummation()">

                    </label>
                  </div>
                </td>
                <td class="em-c-table__cell">
                  <div *ngIf="!requestInReadOnlyState" class="em-c-field__body em-c-field--small">
                    <input type="number" id="" class="em-c-input input-funding-request" 
                    [(ngModel)]="activeRequest.emitOpexMPCEAmount"
                      name="EmitOpexMPCEAmount" appInputType [inputType]="'positiveNumber'" min="0" (keyup)="requestSummation()"
                    />
                  </div>
                  <div *ngIf="requestInReadOnlyState" class="em-c-field__body em-c-field--small em-u-text-align-right">
                    <label>

                    </label>
                  </div>
                </td>
                <td class="em-c-table__cell  "></td>
              </tr>
              <!-- end em-c-table__row -->
              <tr class="">
                <td class="em-u-padding-left-none">
                  <div class="em-u-font-style-regular em-u-font-style-semibold em-u-text-align-left">BUS OPEX </div>
                </td>
                <td class="em-c-table__cell ">
                  <div class="em-c-field__body em-c-field--small " *ngIf="togglePreviouslyEndorsed && !requestInReadOnlyState; else elseBusOpex">
                    <input type="number" id="" class="em-c-input input-budget-request"
                    [(ngModel)]="activeRequest.businessOpexPreviouslyEndorsedAmount" name="BusinessOpexPreviouslyEndorsedAmount" appInputType [inputType]="'positiveNumber'"
                      min="0" (keyup)="requestSummation()" (change)="onWatchedInputChanged()" style=" margin-left:18px !important;"
                    />
                    <span *ngIf="!requestInReadOnlyState">
                      <img (click)="toggleAttachmentModal()" class="attachmentIcon" src="../../assets/standard/unity-1.2.0/images/active.svg" alt="Attachment"
                      />
                    </span>
                  </div>

                  <ng-template #elseBusOpex>
                    <div class="toggle-previously-endorsed">
                      <label class="em-c-field__body em-c-field--small input-budget-request">
                        
                      </label>
                    </div>
                  </ng-template>
                </td>
                <td class="em-c-table__cell ">
                  <div *ngIf="!requestInReadOnlyState || activeRequest.businessLed" class="em-c-field__body em-c-field--small">
                    <input type="number" id="" class="em-c-input input-funding-request"                                   
                    [(ngModel)]="activeRequest.businessOpexRequestedAmount" name="BusinessOpexRequestedAmount"
                      appInputType [inputType]="'positiveNumber'" min="0" (keyup)="requestSummation()" (change)="onWatchedInputChanged()"
                    />
                  </div>
                  <div *ngIf="requestInReadOnlyState" class="em-c-field__body em-c-field--small em-u-text-align-right">
                    <label>

                    </label>
                  </div>                                
                </td>
                <td class="em-c-table__cell em-u-text-align-right " align="middle">
                  <div class="em-c-field__body">
                    <label name="BusinessOpexCurrentCumulativeAmount">

                    </label>
                  </div>
                </td>
                <td class="em-c-table__cell ">
                  <div *ngIf="!requestInReadOnlyState" class="em-c-field__body em-c-field--small">
                    <input type="number" id="" class="em-c-input input-funding-request"                               
                    [(ngModel)]="activeRequest.businessOpexMPCEAmount " name="BusinessOpexMPCEAmount" appInputType [inputType]="'positiveNumber'" min="0" (keyup)="requestSummation()"
                    />
                  </div>
                  <div *ngIf="requestInReadOnlyState" class="em-c-field__body em-c-field--small em-u-text-align-right">
                    <label>

                    </label>
                  </div> 

                </td>
                <td class="em-c-table__cell  "></td>

              </tr>
              <!-- end em-c-table__row -->
              <tr class="">
                <td class="em-u-padding-left-none ">
                  <div class="em-u-font-style-regular em-u-font-style-semibold em-u-text-align-left">IT CAPEX </div>
                </td>
                <td class="em-c-table__cell em-u-padding-bottom-none">

                  <div class="toggle-previously-endorsed">
                    <label class="em-c-field__body em-c-field--small input-budget-request">
                     
                    </label>
                  </div>
                </td>
                <td class="em-c-table__cell em-u-padding-bottom-none">
                  <div *ngIf="!requestInReadOnlyState" class="em-c-field__body em-c-field--small">
                    <input type="number" id="" class="em-c-input input-funding-request"                           
                    [(ngModel)]="activeRequest.emitCapexRequestedAmount" name="EmitCapexRequestedAmount" appInputType [inputType]="'positiveNumber'" min="0" (keyup)="requestSummation()"
                      (change)="onWatchedInputChanged()" />
                  </div>
                  <div *ngIf="requestInReadOnlyState" class="em-c-field__body em-c-field--small em-u-text-align-right">
                    <label>

                    </label>
                  </div>                                 
                </td>
                <td class="em-c-table__cell em-u-text-align-right em-u-padding-bottom-none" align="middle">
                  <div class="em-c-field__body">
                    <label name="EmitCapexCurrentCumulativeAmount">

                    </label>
                  </div>
                </td>
                <td class="em-c-table__cell em-u-padding-bottom-none">
                  <div *ngIf="!requestInReadOnlyState" class="em-c-field__body em-c-field--small">
                    <input type="number" id="" class="em-c-input input-funding-request"                        
                    [(ngModel)]="activeRequest.emitCapexMPCEAmount " name="EmitCapexMPCEAmount" appInputType [inputType]="'positiveNumber'" min="0" (keyup)="requestSummation()"
                      (change)="onWatchedInputChanged()" />
                  </div>
                  <div *ngIf="requestInReadOnlyState" class="em-c-field__body em-c-field--small em-u-text-align-right">
                    <label>

                    </label>
                  </div>
                </td>
                <td class="em-c-table__cell em-u-padding-bottom-none "></td>
              </tr>
              <!-- end em-c-table__row -->
              <tr class=" ">
                <td class="em-u-padding-left-none">
                  <div class="em-u-font-style-regular em-u-font-style-semibold em-u-text-align-left">BUS CAPEX </div>
                </td>
                <td class="em-c-table__cell em-js-cell em-js-cell-editable">

                  <div class="toggle-previously-endorsed">
                    <label class="em-c-field__body em-c-field--small input-budget-request">
                      
                    </label>
                  </div>
                </td>

                <td class="em-c-table__cell">
                  <div  *ngIf="!requestInReadOnlyState || activeRequest.businessLed"  class="em-c-field__body em-c-field--small">
                    <input type="number" id="" class="em-c-input input-funding-request"                   
                      [(ngModel)]="activeRequest.businessCapexRequestedAmount " name="BusinessCapexRequestedAmount"
                      appInputType [inputType]="'positiveNumber'" min="0" (keyup)="requestSummation()" (change)="onWatchedInputChanged()"
                    />
                  </div>
                  <div *ngIf="requestInReadOnlyState" class="em-c-field__body em-c-field--small em-u-text-align-right">
                    <label>

                    </label>
                  </div>
                </td>
                <td class="em-c-table__cell em-u-text-align-right" align="middle">
                  <div class="em-c-field__body">
                    <label name="BusinessCapexCurrentCumulativeAmount">

                    </label>
                  </div>
                </td>
                <td class="em-c-table__cell ">
                  <div *ngIf="!requestInReadOnlyState" class="em-c-field__body em-c-field--small">
                    <input type="number" id="" class="em-c-input input-funding-request"                                                
                    [(ngModel)]="activeRequest.businessCapexMPCEAmount" name="BusinessCapexMPCEAmount" appInputType [inputType]="'positiveNumber'" min="0" (keyup)="requestSummation()"
                      (change)="onWatchedInputChanged()" />
                  </div>
                  <div *ngIf="requestInReadOnlyState" class="em-c-field__body em-c-field--small em-u-text-align-right">
                    <label>

                    </label>
                  </div>
                </td>
                <td class="em-c-table__cell  "></td>
              </tr>
              <!-- end em-c-table__row -->
              <tr class="" style="background-color: #0c69b0;">
                <td class="em-c-table__cell em-js-cell em-js-cell-editable td-total" valign="middle">
                  <label class="em-c-field__label td-total">Total ($K)</label>
                </td>
                <td class="em-c-table__cell em-js-cell em-js-cell-editable td-total " valign="middle" align="middle">
                 
                </td>
                <td class="em-c-table__cell em-js-cell em-js-cell-editable td-total" valign="middle" align="middle">

                </td>
                <td class="em-c-table__cell em-js-cell em-js-cell-editable td-total em-u-text-align-right" valign="middle" align="middle">
                  
                </td>
                <td class="em-c-table__cell em-js-cell em-js-cell-editable td-total " valign="middle" align="middle">
                 
                </td>
                <td class="td-total" align="right " colspan="3" valign="middle">
                  <label style="margin-right:30px;" class="td-total"> 10% of total project</label>
                </td>
                <td class="em-c-table__cell  "></td>
              </tr>

              <tr class="bottom-row">
                <td class="em-c-table__cell em-js-cell em-js-cell-editable em-u-font-style-semibold em-u-font-size-med em-u-padding-left-none em-u-padding-top-double">Funding Type</td>
                <td class="em-c-table__cell em-js-cell em-js-cell-editable em-u-font-style-bold em-u-font-size-med em-u-padding-top-double"></td>
                <td class="em-c-table__cell em-js-cell em-js-cell-editable em-u-padding-top-double" colspan="2">
                  <select [disabled]="requestInReadOnlyState" class="em-c-select em-c-select em-u-width-100 em-u-font-style-semibold em-u-font-size-med control-text"
                    [(ngModel)]="activeRequest.fundingTypeId" name="fundingType" (change)="onWatchedInputChanged()">
                    <option value="-500" selected>-- Select Funding Type --</option>
                    <option [value]="fundingType.value" *ngFor="let fundingType of fundingTypes" [selected]="fundingType.value === activeRequest.fundingTypeId?'selected':''">
                      
                    </option>
                  </select>
                </td>
                <td class="" colspan="4">

                </td>
              </tr>

              <tr class="em-c-table--condensed bottom-row em-u-padding-left-half ">
                <td class="em-c-table__cell em-js-cell em-js-cell-editable em-u-padding-left-none em-u-font-style-semibold em-u-font-size-med-2 em-u-padding-top-double "
                  colspan="3">
                  <label class="em-u-font-style-bold" style="letter-spacing: 0.4px; font-size:1.312rem;">Funding Endorsements</label>
                </td>
                <td class="em-c-table__cell em-js-cell em-js-cell-editable em-u-font-style-semibold em-u-font-size-med"></td>
                <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="4">

                </td>
              </tr>
              <tr id="doag-level-row">
                <td class=" em-u-font-style-semibold em-u-padding-left-none em-u-font-size-med em-u-padding-top" colspan="2">DOAG Level</td>
                <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="2">
                  <select *ngIf="!requestInReadOnlyState" [disabled]="requestInReadOnlyState" name="doagLevel" class="em-c-select em-c-select em-u-width-100 em-u-font-style-semibold control-text"
                    [(ngModel)]="activeRequest.doagLevel" (change)="onDoagLeveChanged($event)">
                    <option value="-500" selected>-- Select DOAG level --</option>
                    <option *ngFor="let doagLevel of doagLevels" [value]="doagLevel.value" [selected]="doagLevel.value === activeRequest.doagLevel?'selected':''">
                      </option>
                  </select>
                  <label *ngIf="requestInReadOnlyState" class="em-u-font-style-semibold em-u-font-size-med em-u-padding-top em-u-padding-bottom  ">

                  </label>
                </td>
                <td class="" colspan="4"></td>
              </tr>
              <tr class="em-c-table--condensed">
                <td class="em-c-table__cell em-js-cell em-js-cell-editable em-u-padding-left-none em-u-font-style-semibold em-u-font-size-med  em-u-padding-top-half "
                  colspan="2" valign="middle">Controller Endorser</td>
                <td class="em-c-table__cell em-js-cell em-js-cell-editable em-u-padding-top-half" colspan="2">
                  <label class="em-u-font-style-semibold em-u-font-size-med em-u-padding-top em-u-padding-bottom  ">

                  </label>
                </td>
                <td class="" colspan="4">

                </td>
              </tr>
              <tr id="doag_approver-row">
                <td class="em-c-table__cell em-js-cell em-js-cell-editable em-u-font-style-semibold em-u-padding-left-none em-u-font-size-med em-u-padding-top"
                  colspan="2">DOAG Approver</td>
                <td class="em-c-table__cell em-js-cell em-js-cell-editable" colspan="2">
                  <select *ngIf="!requestInReadOnlyState" [disabled]="requestInReadOnlyState" name="doagApprover" class="em-c-select em-c-select em-u-width-100 em-u-font-style-semibold control-text"
                    [(ngModel)]="activeRequest.doagApproverId">
                    <option value="-500" selected>-- Select DOAG Approver --</option>
                    <option *ngFor="let approver of approversList" [value]="approver.userId" [selected]="approver.userId === activeRequest.doagApproverId?'selected':''">
                      </option>
                  </select>
                  <label *ngIf="requestInReadOnlyState" class="em-u-font-style-semibold em-u-font-size-med em-u-padding-top em-u-padding-bottom  ">

                  </label>
                </td>
                <td class="" colspan="4"></td>
              </tr>
            </tbody>
          </table>
          <div *ngIf="showModal">
            <app-file-upload [showModal]="showModal" [documentType]="documentType" (modalEvent)="closeAttachmentModal($event)" (uploadDetail)="refreshAttachmentList($event)"
              [requestId]="requestId"> </app-file-upload>
          </div>
        </div>
        <!--end em-c-table-object__body-inner-->
      </div>
      <!--end em-c-table-object__body-->
    </div>

</div>