﻿<%@ Control Language="C#" AutoEventWireup="true" %>
  <label _ngcontent-c3="" class="em-c-field__label" for="" id="workNameTitle"> 66273 - Transactional Data Transparency (TDT) Project2-ROW</label>
  <br _ngcontent-c3="">
  <!---->
<fieldset _ngcontent-c3="" class="em-c-fieldset ">
  <div _ngcontent-c3="" class="em-l-grid--halves em-u-margin-right-quad">
    <div _ngcontent-c3="" class="em-l-grid__item em-u-margin-right-quad">
      <!----><div _ngcontent-c3="" class="form-group-row">
        <div _ngcontent-c3="" class="em-c-field ">
          <label _ngcontent-c3="" class="em-c-field__label" for="TCode">T Code</label>
          <!----><div _ngcontent-c3="" class="em-c-field__body em-c-field--small">
          <span style="opacity: 1; left: 581px; top: 9px; width: 19px; min-width: 19px; height: 13px; position: absolute; background-image: url(&quot;data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB3aWR0aD0iMTciIGhlaWdodD0iMTIiIHZpZXdCb3g9IjAgMCAxNyAxMiI+IDxkZWZzPiA8cGF0aCBpZD0iYSIgZD0iTTcuOTA5IDEuNDYybDIuMTIxLjg2NHMtLjY3MS4xMy0xLjIwOS4yOTRjMCAwIC40MzcuNjM0Ljc3LjkzOC4zOTEtLjE4LjY1Ny0uMjQ4LjY1Ny0uMjQ4LS44MTEgMS42NjgtMi45NzkgMi43MDMtNC41MyAyLjcwMy0uMDkzIDAtLjQ4Mi0uMDA2LS43MjcuMDE1LS40MzUuMDIxLS41ODEuMzgtLjM3NC40NzMuMzczLjIwMSAxLjE0My42NjIuOTU4IDEuMDA5QzUuMiA4LjAwMy45OTkgMTEgLjk5OSAxMWwuNjQ4Ljg4Nkw2LjEyOSA4LjYzQzguNjAyIDYuOTQ4IDEyLjAwNiA2IDE1IDZoM1Y1aC00LjAwMWMtMS4wNTggMC0yLjA0LjEyMi0yLjQ3My0uMDItLjQwMi0uMTMzLS41MDItLjY3OS0uNDU1LTEuMDM1YTcuODcgNy44NyAwIDAgMSAuMTg3LS43MjljLjAyOC0uMDk5LjA0Ni0uMDc3LjE1NS0uMDk5LjU0LS4xMTIuNzc3LS4wOTUuODIxLS4xNi4xNDYtLjI0NS4yNTQtLjk3NC4yNTQtLjk3NEw3LjU2OS4zODlzLjIwMiAxLjAxMy4zNCAxLjA3M3oiLz4gPC9kZWZzPiA8dXNlIGZpbGw9IiNiNmI2YjYiIGZpbGwtcnVsZT0iZXZlbm9kZCIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTEpIiB4bGluazpocmVmPSIjYSIvPiA8L3N2Zz4=&quot;); background-repeat: no-repeat; background-position: 0px 0px; border: none; display: inline; visibility: visible; z-index: auto;"></span></div>
          <div _ngcontent-c3="" class=" em-c-field--small em-c-field__label em-c-field__note">T999 </div>
        </div>
      </div>
      <!----><div _ngcontent-c3="" class="form-group-row">
        <div _ngcontent-c3="" class="em-c-field ">
          <label _ngcontent-c3="" class="em-c-field__label" for="">T-Code Description</label>
          <div _ngcontent-c3="" class=" em-c-field--small em-c-field__label em-c-field__note"> All EMIT Projects < $50M </div>
        </div>
      </div>
      <div _ngcontent-c3="" class="form-group-row">
        <div _ngcontent-c3="" class="em-c-field ">
          <label _ngcontent-c3="" class="em-c-field__label" for="">
            Work Lead
            <!---->
          </label>
          <!----><div _ngcontent-c3="" class="em-c-field__body em-c-field--small">
            <app-dropdown-input _ngcontent-c3="" id="work-lead-input" name="work-lead" _nghost-c13=""><div _ngcontent-c13="" class="auto-complete-dropdown">
    <input _ngcontent-c13="" value="George, Owen" autocomplete="off" class="em-c-input ng-untouched ng-pristine ng-valid" type="text" id="work-lead-input">
    <div _ngcontent-c13="" class="dropdown" id="tTabyUmvb7KbgcVg">
        <!---->
    </div>
</div></app-dropdown-input>
          </div>
          <!---->
        </div>
      </div>
      <div _ngcontent-c3="" class="form-group-row">
        <div _ngcontent-c3="" class="em-c-field ">
          <label _ngcontent-c3="" class="em-c-field__label" for="">Controller's Contact</label>
          <!----><div _ngcontent-c3="" class="em-c-field__body">
            <div _ngcontent-c3="" class="em-c-field__body em-c-field--small">
              <select _ngcontent-c3="" class="em-c-select em-u-width-100 em-u-font-style-semibold control-text ng-untouched ng-pristine ng-valid" name="controller">
                <option _ngcontent-c3="" selected="" value="-500">-- Select Controller's Contact --</option>
                <!----><option _ngcontent-c3="" value="74249490">Abete, Cindy M</option><option _ngcontent-c3="" value="84921141">Araqsousi, Rock</option><option _ngcontent-c3="" value="39130889">Barranco, Anna</option><option _ngcontent-c3="" value="57450800">Buries, Martha K</option><option _ngcontent-c3="" value="87990935">Chen, Vivian L</option><option _ngcontent-c3="" value="09377967">Courtright, Anne O</option><option _ngcontent-c3="" value="03692959">Floyd, Karen L</option><option _ngcontent-c3="" value="22096645">Garner, Jamie L</option><option _ngcontent-c3="" value="78501834">Harper, Daniel</option><option _ngcontent-c3="" value="43259118">Henson, Russell A</option><option _ngcontent-c3="" value="72391500">Hickman, Theo</option><option _ngcontent-c3="" value="49709035">Hong, Jessica A</option><option _ngcontent-c3="" value="5641e2d6">Lali Lobzhanidze</option><option _ngcontent-c3="" value="31731090">Lane, Florence K</option><option _ngcontent-c3="" value="74934329">Li, Sue X</option><option _ngcontent-c3="" value="12243466">Malbari, Jumana</option><option _ngcontent-c3="" value="5641e2c7">Nia Hall</option><option _ngcontent-c3="" value="71650228">Parsons, Stephanie M</option><option _ngcontent-c3="" value="B741e2r8">Philip Ochu</option><option _ngcontent-c3="" value="24759537">Quim, Michele D</option><option _ngcontent-c3="" value="96284823">Samson, Matthew</option><option _ngcontent-c3="" value="5641e2b7">Yuichi Fujiki</option>
              </select>
            </div>
          </div>
          <div _ngcontent-c3="" class="em-c-field__note">Assigned Controller's reviewer </div>
        </div>
      </div>
      <div _ngcontent-c3="" class="form-group-row">
        <div _ngcontent-c3="" class="em-c-field ">
          <label _ngcontent-c3="" class="em-c-field__label" for="">
            POTL/POM
            <!---->
          </label>
          <!----><div _ngcontent-c3="" class="em-c-field__body em-c-field--small">
            <app-dropdown-input _ngcontent-c3="" id="potl-input" name="potl" _nghost-c13=""><div _ngcontent-c13="" class="auto-complete-dropdown">
    <input _ngcontent-c13="" value=" Jesse, Garnier" autocomplete="off" class="em-c-input ng-untouched ng-pristine ng-valid" type="text" id="potl-input">
    <div _ngcontent-c13="" class="dropdown" id="ch6hi6wQxEHB12uI">
        <!---->
    </div>
</div></app-dropdown-input>
          </div>
          <div _ngcontent-c3="" class="em-c-field__note"> </div>
        </div>
      </div>
    </div>
    <div _ngcontent-c3="" class="em-l-grid__item em-u-padding-right">
      <!----><div _ngcontent-c3="" class="form-group-row">
        <div _ngcontent-c3="" class="em-c-field ">
          <label _ngcontent-c3="" class="em-c-field__label" for="">Funding Division</label>
          <!----><div _ngcontent-c3="" class="em-c-field__body em-c-field--small">
            <select _ngcontent-c3="" class="em-c-select em-u-width-100 em-u-font-style-semibold control-text ng-untouched ng-pristine ng-valid" name="Division">
              <option _ngcontent-c3="" value="-500">-- Select Funding Division --</option>
              <!----><option _ngcontent-c3="" value="7">
                Internal
              </option><option _ngcontent-c3="" value="8">
                Chemical
              </option><option _ngcontent-c3="" value="9">
                Service
              </option><option _ngcontent-c3="" value="10">
                IOL
              </option><option _ngcontent-c3="" value="11">
                Downstream
              </option><option _ngcontent-c3="" value="12">
                Upstream
              </option><option _ngcontent-c3="" value="13">
                Corporate
              </option><option _ngcontent-c3="" value="14">
                General Interest
              </option>
            </select>
          </div>
          <div _ngcontent-c3="" class="em-c-field__note"> </div>
        </div>
      </div>
      <!----><div _ngcontent-c3="" class="form-group-row">
        <div _ngcontent-c3="" class="em-c-field ">
          <label _ngcontent-c3="" class="em-c-field__label" for="">Funding Division Function</label>
          <!----><div _ngcontent-c3="" class="em-c-field__body em-c-field--small">
            <select _ngcontent-c3="" class="em-c-select em-u-width-100 em-u-font-style-semibold control-text ng-untouched ng-pristine ng-valid" name="DivisionFunction">
              <option _ngcontent-c3="" value="-500">-- Select Funding Division Function --</option>
              <!----><option _ngcontent-c3="" value="61">
                Downstream F &amp; L - Cust Svc
              </option><option _ngcontent-c3="" value="62">
                Downstream Lubes &amp; Specialties
              </option><option _ngcontent-c3="" value="77">
                Downstream F &amp; L - Retail GBU
              </option><option _ngcontent-c3="" value="78">
                Downstream Fuels Marketing Retail
              </option><option _ngcontent-c3="" value="82">
                Downstream Business Services
              </option><option _ngcontent-c3="" value="92">
                Downstream Fuels Marketing Customer Service
              </option><option _ngcontent-c3="" value="94">
                Downstream R&amp;E Central Project Office
              </option><option _ngcontent-c3="" value="106">
                DBS Controllers
              </option><option _ngcontent-c3="" value="107">
                Downstream Research &amp; Engineering
              </option><option _ngcontent-c3="" value="109">
                Downstream Pipeline
              </option><option _ngcontent-c3="" value="120">
                DBS Tax
              </option><option _ngcontent-c3="" value="132">
                Downstream Refining
              </option><option _ngcontent-c3="" value="134">
                Downstream IT Cross Business Lines
              </option><option _ngcontent-c3="" value="137">
                Downstream Supply &amp; Transportation (HQ)
              </option><option _ngcontent-c3="" value="139">
                Downstream Fuels &amp; Lubricants
              </option><option _ngcontent-c3="" value="143">
                Downstream LVC
              </option><option _ngcontent-c3="" value="154">
                Downstream FVC
              </option><option _ngcontent-c3="" value="159">
                Downstream F &amp; L - Aviation GBU
              </option><option _ngcontent-c3="" value="174">
                Downstream Fuels Marketing I&amp;W
              </option><option _ngcontent-c3="" value="177">
                Downstream R&amp;E General Interest
              </option><option _ngcontent-c3="" value="187">
                Downstream Fuels Marketing Aviation
              </option><option _ngcontent-c3="" value="205">
                Downstream Refining &amp; Supply
              </option><option _ngcontent-c3="" value="207">
                Downstream Operations
              </option><option _ngcontent-c3="" value="208">
                Downstream Distribution
              </option>
            </select>
          </div>
          <div _ngcontent-c3="" class="em-c-field__note"> </div>
        </div>
      </div>
      <div _ngcontent-c3="" class="form-group-row">
      </div>
    </div>
  </div>
</fieldset>
