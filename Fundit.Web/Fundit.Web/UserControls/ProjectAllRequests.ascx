﻿<%@ Control Language="C#" AutoEventWireup="true" %>
<div _ngcontent-c18="" class="em-c-table-object__header">
                    <div _ngcontent-c18="" class="em-c-collapsible-toolbar em-js-collapsible-toolbar">

                        <div _ngcontent-c18="" class="em-c-collapsible-toolbar__panel em-js-toolbar-panel">
                            <div _ngcontent-c18="" class="em-c-toolbar " style="height:5rem;">

                                <label _ngcontent-c18="" class="e em-c-field__label em-is-aligned-right" for="" style="margin-left:57rem;">Filters</label>
                                <div _ngcontent-c18="" class="em-c-field__body em-c-toolbar__item em-is-aligned-right">
                                    <select _ngcontent-c18="" class="em-c-select em-custom-select-width em-u-margin-right-double ng-untouched ng-pristine ng-valid">
                                        <option _ngcontent-c18="" value="requestId">Display...</option>
                                        <option _ngcontent-c18="" value="workId">All Pending Requests</option>
                                        <option _ngcontent-c18="" value="workName">Funding Requests</option>
                                        <option _ngcontent-c18="" value="statusName">Reallocation Requests</option>
                                        
                                    </select>
                                </div>
                                

                                
                            </div>
                            
                        </div>
                        
                    </div>
                    
                </div>
 <div class="em-c-table-object ">
            <div class="em-c-table-object__header">
            </div>
            <!--end em-c-table-object__header-->
            <div class="em-c-table-object__body">
                <div class="em-c-table-object__body-inner">
                    <table class="em-c-table ">
                        <thead class="em-c-table__header">
                            <tr class="em-c-table__header-row">
                                <th scope="col" class="em-c-table__header-cell em-u-width-5">Request Id</th>
                                <th scope="col" class="em-c-table__header-cell ">Request Type</th>
                                <th scope="col" class="em-c-table__header-cell ">Status</th>
                                <th scope="col" class="em-c-table__header-cell ">Project Manager</th>
                                <th scope="col" class="em-c-table__header-cell ">Controller</th>
                                <th scope="col" class="em-c-table__header-cell ">Approved Amount</th>
                            </tr>
                            <!-- em-c-table__header-row -->
                        </thead>
                        <!-- end em-c-table__header -->
                        <tbody class="em-c-table__body ">
                            <tr class="em-c-table__row " onclick="window.location.href='../Request/FundingRequest.aspx'">

                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">9641</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Funding Request</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Reviewing</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Rolim, Camila</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Li, Sue X</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">$1,900K</td>
                               
                            </tr>
                            <tr class="em-c-table__row " onclick="window.location.href='../Request/ReallocationRequest.aspx'">

                               <td class="em-c-table__cell em-js-cell em-js-cell-editable">7890</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Reallocation Request</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Approving</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Adefisayo, Odunlami A</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Araqsousi, Rock </td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">$5,000K</td>
                               
                            </tr>
                            <tr class="em-c-table__row " onclick="window.location.href='../Request/ARSetupRequest.aspx'">

                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">6579</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Appropriation Request</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Completed</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Jire, Peter N</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Samson, Matthew</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">$900K</td>
                               
                            </tr>
                            <tr class="em-c-table__row " onclick="window.location.href='../IO/Create.aspx'">

                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">5769</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Internal Order Request</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Completed</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Abd Ghani, Mohd Azrool Ihsan</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Lane, Florence K</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">$10,986K</td>
                               
                            </tr>
                            <tr class="em-c-table__row " onclick="window.location.href='../IO/CloseRequest.aspx'">

                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">5369</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Internal Order Close Request</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Completed</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Ivor, Mercia</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Augustinus, Fabiana K</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">$1,876K</td>
                               
                            </tr>
                            <tr class="em-c-table__row " onclick="window.location.href='../Request/ARCloseRequest.aspx'">

                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">4769</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">AR Close Request</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Completed</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Abioye, Careen</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Hilarius, Maurinus</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">$8,000K</td>
                               
                            </tr>
                           
                         
                        </tbody>
                        <!-- end em-c-table__body -->
                        <tfoot class="em-c-table__footer">
                            <tr class="em-c-table__footer-row">
                            </tr>
                        </tfoot>
                        <!-- end em-c-table__footer -->
                    </table>
                    <!--end em-c-table-->
                </div>
                <!--end em-c-table-object__body-inner-->
            </div>
            <!--end em-c-table-object__body-->
        </div>
        <!--end em-c-table-object-->