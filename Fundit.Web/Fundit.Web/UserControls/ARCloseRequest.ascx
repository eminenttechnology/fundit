﻿<%@ Control Language="C#" AutoEventWireup="true" %>
<div class="em-c-table-object__body ">
    <div class="em-c-table-object__body-inner">
        <table class="em-c-table  scroll " style="table-layout: fixed; width: 100%;">
            <thead class="em-c-table__header">
                <tr class="em-c-table__header-row ">
                    <th class="em-c-table__header-cell" scope="col" style="width: 7.7%;"></th>
                    <th class="em-c-table__header-cell " scope="col" style="width: 7.7%;">
                        <span class="em-u-font-size-small-2">STATUS</span>
                        <button class="em-c-btn--bare">

                            <svg class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                <use xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>


                        </button>
                    </th>
                    <th class="em-c-table__header-cell em-u-margin-right " scope="col" style="width: 7.7%;">
                        <span class="em-u-font-size-small-2">QUANTITY</span>
                        <button class="em-c-btn--bare">

                            <svg class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                <use xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>


                        </button>
                    </th>
                    <th class="em-c-table__header-cell em-u-margin-right" scope="col" style="word-wrap: break-word; width: 7.7%;">
                        <span class="em-u-font-size-small-2">AR ALLOCATION ($K)</span>
                        <button class="em-c-btn--bare">

                            <svg class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                <use xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>


                        </button>
                    </th>
                    <th class="em-c-table__header-cell " scope="col" style="width: 7.7%;">
                        <span class="em-u-font-size-small-2">AR #</span>
                        <button class="em-c-btn--bare">

                            <svg class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                <use xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>


                        </button>
                    </th>
                    <th class="em-c-table__header-cell " scope="col" style="width: 7.7%; word-wrap: break-word;">
                        <span class="em-u-font-size-small-2">COMPANY CODE</span>
                        <button class="em-c-btn--bare">

                            <svg class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                <use xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>


                        </button>
                    </th>
                    <th class="em-c-table__header-cell" scope="col" style="width: 7.7%; word-wrap: break-word;">
                        <span class="em-u-font-size-small-2">COUNTRY</span>
                        <button class="em-c-btn--bare">

                            <svg class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                <use xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>


                        </button>
                    </th>
                    <th class="em-c-table__header-cell " scope="col" style="width: 7.7%;">
                        <span class="em-u-font-size-small-2">WBS</span>
                        <button class="em-c-btn--bare">

                            <svg class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                <use xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>


                        </button>
                    </th>
                    <th class="em-c-table__header-cell " scope="col" style="width: 7.7%; word-wrap: break-word;">
                        <span class="em-u-font-size-small-2">NETWORK ORDER</span>
                        <button class="em-c-btn--bare">

                            <svg class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                <use xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>


                        </button>
                    </th>
                    <th class="em-c-table__header-cell em-u-margin-right " scope="col" style="width: 7.7%;">
                        <span class="em-u-font-size-small-2">INSTALLATION COST CENTER</span>
                        <button class="em-c-btn--bare">

                            <svg class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                <use xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>


                        </button>
                    </th>
                    <th class="em-c-table__header-cell " scope="col" style="word-wrap: break-word; width: 7.7%;">
                        <span class="em-u-font-size-small-2">ASSET DESCRIPTION </span>
                        <button class="em-c-btn--bare">

                            <svg class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                <use xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>


                        </button>
                    </th>
                    <th class="em-c-table__header-cell " scope="col" style="width: 7.7%;">
                        <span class="em-u-font-size-small-2">ASSET TYPE</span>
                        <button class="em-c-btn--bare">

                            <svg class="em-c-icon em-c-icon--tiny em-c-icon--green">
                                <use xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>


                        </button>
                    </th>
                    <th class="em-c-table__header-cell " scope="col" style="width: 7.7%;"></th>
                </tr>

            </thead>
            <tbody class="em-c-table__body">
                <tr class="em-c-table__row ">
                    <td class="em-c-table__cell" colspan="" style="width: 7.7%;">
                        <a href="#modal" class="em-c-modal__trigger em-js-modal-trigger">Edit</a>
                    </td>
                    <td class="em-c-table__cell" colspan="" style="width: 7.7%;">Pending
                    </td>
                    <td class="em-c-table__cell" colspan="" style="width: 7.7%;">1
                    </td>
                    <td class="em-c-table__cell" colspan="" style="width: 7.7%;">70.00
                    </td>
                    <td class="em-c-table__cell em-u-margin-right" colspan="" style="width: 7.7%;">15182001
                    </td>
                    <td class="em-c-table__cell" colspan="" style="width: 7.7%;">0572
                    </td>
                    <td class="em-c-table__cell" colspan="" style="width: 7.7%;">Belgium
                    </td>
                    <td class="em-c-table__cell break-character" colspan="" style="width: 7.7%;">C1.00879
                    </td>
                    <td class="em-c-table__cell break-character" colspan="" style="width: 7.7%;"></td>
                    <td class="em-c-table__cell break-character " colspan="" style="width: 7.7%; word-wrap: break-word;">C0802W1PMO
                    </td>
                    <td class="em-c-table__cell" colspan="" style="width: 7.7%; word-wrap: break-word;">Network Devices
                    </td>
                    <td class="em-c-table__cell" colspan="" style="width: 7.7%;">Hardware
                    </td>
                    <td class="em-c-table__cell" colspan="" style="width: 7.7%;">
                        <a href="#modal1" class="em-c-modal__trigger em-js-modal-trigger">Cancel</a>
                    </td>
                </tr>
                <tr class="em-c-table__row ">
                    <td class="em-c-table__cell" colspan="" style="width: 7.7%;">
                        <a href="#modal" class="em-c-modal__trigger em-js-modal-trigger">Edit</a>
                    </td>
                    <td class="em-c-table__cell" colspan="" style="width: 7.7%;">Closed
                    </td>
                    <td class="em-c-table__cell" colspan="" style="width: 7.7%;">1
                    </td>
                    <td class="em-c-table__cell" colspan="" style="width: 7.7%;">110.00
                    </td>
                    <td class="em-c-table__cell em-u-margin-right" colspan="" style="width: 7.7%;">15182002
                    </td>
                    <td class="em-c-table__cell" colspan="" style="width: 7.7%;">3459
                    </td>
                    <td class="em-c-table__cell" colspan="" style="width: 7.7%;">Croatia
                    </td>
                    <td class="em-c-table__cell break-character" colspan="" style="width: 7.7%;">C1.00880
                    </td>
                    <td class="em-c-table__cell break-character" colspan="" style="width: 7.7%;"></td>
                    <td class="em-c-table__cell break-character " colspan="" style="width: 7.7%; word-wrap: break-word;">N/A
                    </td>
                    <td class="em-c-table__cell" colspan="" style="width: 7.7%; word-wrap: break-word;">Network Licenses
                    </td>
                    <td class="em-c-table__cell" colspan="" style="width: 7.7%;">Software
                    </td>
                    <td class="em-c-table__cell" colspan="" style="width: 7.7%;">
                     <a href="#modal1" class="em-c-modal__trigger em-js-modal-trigger">Cancel</a>
                    </td>
                </tr>
            </tbody>
            <tfoot class="em-c-table__footer">
                <tr class="em-c-table__footer-row  custom-table-background em-u-font-size-med-2 ">
                    <td class="em-c-table__footer-cell em-u-font-size-med-2 " style="max-width: 7.7%;">TOTAL:</td>
                    <td class="em-c-table__footer-cell em-u-font-size-med-2 " style="max-width: 7.7%;"></td>
                    <td class="em-c-table__footer-cell em-u-font-size-med-2 " style="max-width: 7.7%;">2</td>
                    <td class="em-c-table__footer-cell em-u-font-size-med-2 " style="max-width: 7.7%;">$180.00K</td>
                    <td class="em-c-table__footer-cell custom-table-text-right em-u-font-size-med" colspan="9">
                        <span class=" em-u-font-style-bold">Unallocated - Capex &nbsp;&nbsp;</span>$20,000.00</td>
                </tr>
            </tfoot>
        </table>
    </div>
</div>

<div class="em-c-modal em-js-modal em-is-closed" id="modal">
    <div class="em-c-modal__window em-js-modal-window">
        <div class="em-c-modal__header">
            <h3 class="em-c-modal__title">Edit</h3>
            <button class="em-c-btn em-c-btn--bare em-c-modal__close-btn em-js-modal-close-trigger">
                <span class="em-c-btn__text">Close</span>
            </button>
            <!-- end em-c-btn -->
        </div>
        <!-- end em-c-modal__header -->
        <div class="em-c-modal__body em-c-text-passage em-c-text-passage--small">
            <div class="em-l-grid em-l-grid--2up ">
                <div class="em-l-grid__item">


                    <div class="em-c-field ">
                        <div class="form-group">
                            <label class="em-c-field__label">
                                <span style="float: left">COMPANY CODE</span>

                                <span style="clear: both;"></span>
                            </label>
                            <div class="em-c-field__body" style="clear: both;">
                                <input id="" class="em-c-input  em-u-text-align" type="text" value="0572 : EMPC (DS Belgium BR)" />
                            </div>
                        </div>

                        <div class="em-c-field__note">
                        </div>
                    </div>

                    <div class="em-c-field ">
                        <div class="form-group">
                            <label class="em-c-field__label" for="">NETWORK ORDER</label>
                            <div class="em-c-field__body">
                                <input id="" class="em-c-input  em-u-text-align" type="text" value="" />
                            </div>

                            <div class="em-c-field__note"></div>
                        </div>

                    </div>
                    <div class="em-c-field " >
                        <div class="form-group">
                            <label class="em-c-field__label" for="">PURCHASE ORG COST CENTER</label>
                            <div class="em-c-field__body">
                                <input id="" class="em-c-input  em-u-text-align" type="text" value="C0802W1PMO" />
                            </div>
                        </div>
                        <div class="em-c-field__note"></div>
                    </div>
                    <div class="em-c-field " >
                        <div class="form-group">
                            <label class="em-c-field__label" for="">LOCATION</label>
                            <div class="em-c-field__body">
                                <input id="" class="em-c-input  em-u-text-align" type="text" value="USA" />
                            </div>
                        </div>
                        <div class="em-c-field__note"></div>
                    </div>


                    <div class="em-c-field ">
                        <div class="form-group">
                            <label class="em-c-field__label" for="">CAPITALIZATION DATE</label>
                            <div class="em-c-field__body">
                             <input id="" class="em-c-input  em-u-text-align" type="text" value="20/09/2018" />
                            </div>
                        </div>
                        <div class="em-c-field__note"></div>
                    </div> 


                                                            <div class="em-c-field " >
                        <div class="form-group">
                            <label class="em-c-field__label" for="">SERIAL NUMBER</label>
                            <div class="em-c-field__body">
                                <textarea rows="4" cols="30">
                                    AR00001
</textarea>
                            </div>
                        </div>
                        <div class="em-c-field__note"></div>
                    </div>


                </div>
                <div class="em-l-grid__item">
                    <div class="em-c-field em-u-margin-left">
                        <div class="form-group">
                            <label class="em-c-field__label" for="">ASSET DESCRIPTION</label>
                            <div class="em-c-field__body">
                                <input id="" class="em-c-input  em-u-text-align" type="text" value="Network Devices" />
                            </div>

                        </div>
                    </div>
                    
                    <div class="em-c-field ">
                        <div class="form-group" style="margin-top: 1.5rem; margin-left: 1.2rem;">
                            <label class="em-c-field__label" for="">WBS</label>
                            <div class="em-c-field__body">
                                <input id="" class="em-c-input  em-u-text-align" type="text" value="C1.00879" />
                            </div>
                        </div>
                        <div class="em-c-field__note"></div>
                    </div>

                    
                    <div class="em-c-field " style="margin-left: 1.2rem;">
                        <div class="form-group">
                            <label class="em-c-field__label" for="">INSTALLATION COST CENTER</label>
                            <div class="em-c-field__body">
                                <input id="" class="em-c-input  em-u-text-align" type="text" value="C0802W1PMO" />
                            </div>
                        </div>
                        <div class="em-c-field__note"></div>
                    </div>
                                        <div class="em-c-field " style="margin-left: 1.2rem;" >
                        <div class="form-group">
                            <label class="em-c-field__label" for="">SUPPORT ORGANIZATION</label>
                            <div class="em-c-field__body">
                                <input id="" class="em-c-input  em-u-text-align" type="text" value="C0802W1PMO" />
                            </div>
                        </div>
                        <div class="em-c-field__note"></div>
                    </div>

                                        <div class="em-c-field " style="margin-left: 1.2rem;">
                        <div class="form-group">
                            <label class="em-c-field__label" for="">CLOSEOUT TYPE</label>
                            <div class="em-c-field__body">
                                <select style="width: 100% !important">
                              <option value="Full CloseOut">Full CloseOut</option>
                              <option value="Partial CloseOut">Partial CloseOut</option>
                            </select>
                            </div>
                        </div>
                        <div class="em-c-field__note"></div>
                    </div>

                      <div class="em-c-field " style="margin-left: 1.2rem;" >
                        <div class="form-group">
                            <label class="em-c-field__label" for="">INVOICES</label>
                            <div class="em-c-field__body">
                                <textarea rows="4" cols="30">
                                    992121
</textarea>
                            </div>
                        </div>
                        <div class="em-c-field__note"></div>
                    </div>

                </div>


            </div>
        </div>
        <div style="float: right;" class="em-c-modal__footer em-c-text-passage em-c-text-passage--small">
            <button class="em-c-btn em-c-btn--primary em-js-modal-close-trigger">
                <span class="em-c-btn__text">Submit</span>
            </button>
        </div>
        <!-- end em-c-modal__footer -->
    </div>
    <!-- end em-c-modal__window -->
</div>
<!-- end em-c-modal -->


<div class="em-c-modal em-js-modal em-is-closed" id="modal1">
    <div class="em-c-modal__window">
      <div class="em-c-modal__header">
        <div class="em-u-font-style-bold fpo-block">
        </div>
        <button class="em-c-btn em-c-btn--bare em-c-modal__close-btn em-js-modal-close-trigger">
          <svg class="em-c-icon em-c-icon--small">
            <use xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-x" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
          </svg>
        </button>
        
      </div>
      
      <div class="em-c-modal__body em-c-text-passage em-c-text-passage--small " id="request-cancellation-modal">
        <div class="em-l-container">
          <div class=" em-l em-l--two-column">
            <div class="em-l__main">
              <form id="requestCancellationForm" name="requestCancellationForm" novalidate="" class="ng-untouched ng-pristine ng-valid">
                <div class="em-l-grid__item">
                  <div class="em-c-field ">
                    <label class="em-c-field__label" for="">BUSINESS JUSTIFICATION REASON</label>
                    <div class="em-c-field__body em-c-field--small">
                      <select style="width: 100% !important">
                          <option>INACTIVE</option>
                      </select>
                    </div>
                  </div>
                </div>
              </form>
            </div>
            <div class="em-l__secondary">
              <!---->
            </div>
          </div>
        </div>
      </div>
      <div class="em-c-modal__footer em-c-text-passage em-c-text-passage--small">
        <div class="em-btn-custom-pos">
          <button class="em-c-btn em-c-btn--primary btn-custom-margin em-custom-padding" id="cancelRequestButton">
            <span class="em-c-btn__text">Confirm Cancel</span>
          </button>
        </div>
      </div>
    </div>
</div>
<!-- end em-c-modal -->