﻿<%@ Control Language="C#" AutoEventWireup="true" %>



<div _ngcontent-c3="" class="em-l-grid--halves em-u-margin-right-quad">
        <div _ngcontent-c3="" class="em-l-grid__item em-u-margin-right-quad">
            
<table>

    <tr>
        <td style="padding-bottom:8px"  width="220" class="em-c-field__label">T Code</td>
        <td>T3907 - All EMIT Projects < $50M</td>
    </tr>
    <tr>
        <td style="padding-bottom:8px"  class="em-c-field__label">Work Lead</td>
        <td>Tertuliano, Jayme C</td>
    </tr>
    <tr>
        <td style="padding-bottom:8px"  class="em-c-field__label">Startup Date/CP 3</td>
        <td>Jun 06 2019</td>
    </tr>
    <tr>
        <td style="padding-bottom:8px"  class="em-c-field__label">Controller's Contact</td>
        <td>Doe, John</td>
    </tr>
    
    
    <tr >
        <td style="padding-bottom:8px"  class="em-c-field__label" rowspan="3">Work Description</td>
        <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</td>
    </tr>

</table>

        </div>
        <div _ngcontent-c3="" class="em-l-grid__item em-u-padding-right">
            

<table>

    <tr >
        <td style="padding-bottom:8px" class="em-c-field__label" width="220">Funding Division</td>
        <td>Downstream</td>
    </tr>
    <tr>
        <td style="padding-bottom:8px"  class="em-c-field__label" >Funding Division Function</td>
        <td>Downstream IT Cross Business Lines</td>
    </tr>
    <tr>
        <td style="padding-bottom:8px"  class="em-c-field__label">Business Led Project</td>
        <td>YES</td>
    </tr>
    
    <tr>
        <td style="padding-bottom:8px"  class="em-c-field__label">POTL/POM</td>
        <td>Doe, Jane</td>
    </tr>
    <tr>
        <td style="padding-bottom:8px"  class="em-c-field__label">Work Classification</td>
        <td>Discretionary</td>
    </tr>

</table>
        </div>
    </div>



