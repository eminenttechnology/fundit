﻿<%@ Control Language="C#" AutoEventWireup="true" %>
 
  <fieldset _ngcontent-c4="" class="em-c-fieldset">

    <div _ngcontent-c4="" class="em-c-table-object ">
      <div _ngcontent-c4="" class="em-c-table-object__header">
      </div>
      
      <div _ngcontent-c4="" class="em-c-table-object__body">
        <div _ngcontent-c4="" class="em-c-table-object__body-inner">
          <table _ngcontent-c4="" border="0" class="em-c-table em-c-table--condensed em-u-padding-bottom">
            <thead _ngcontent-c4="">
              <tr _ngcontent-c4="" class="em-c-table__header-row em-u-font-style-bold">
                <th _ngcontent-c4="" class=" custom-em-c-table__header" scope="col">
                  Funding Request ($K)</th>
                <th _ngcontent-c4="" class="custom-em-c-table__header" scope="col" style="text-align: center">Previously Endorsed</th>
                <th _ngcontent-c4="" class="custom-em-c-table__header" scope="col" style="text-align: center">Incremental Request</th>
                <th _ngcontent-c4="" class="custom-em-c-table__header" scope="col" style="text-align: center">Current Cumulative</th>
                <th _ngcontent-c4="" class="custom-em-c-table__header" scope="col" style="text-align: center">Total Project (MPCE)</th>
                <th _ngcontent-c4=""></th>
              </tr>
              
            </thead>
            
            <tbody _ngcontent-c4="" class="em-c-table__body em-u-padding-bottom">
              <tr _ngcontent-c4="" class="em-u-padding-top-half">
                <td _ngcontent-c4="" class="em-c-table__cell"></td>
              </tr>
              <tr _ngcontent-c4="" class="em-u-padding-top-half">
                <td _ngcontent-c4="" class="em-u-padding-left-none">
                  <div _ngcontent-c4="" class="em-u-font-style-regular em-u-font-style-semibold em-u-text-align-left"> IT OPEX </div>
                </td>

                <td _ngcontent-c4="" class="em-c-table__cell  ">
                  <!---->
                    <div _ngcontent-c4="" class="toggle-previously-endorsed">
                      <label _ngcontent-c4="" class="em-c-field__body em-c-field--small input-budget-request">
                        800.00
                      </label>
                    </div>
                  


                  <!---->

                </td>
                <td _ngcontent-c4="" class="em-c-table__cell">
                  <!---->
                  <!----><div _ngcontent-c4="" class="em-c-field__body em-c-field--small em-u-text-align-right">
                    <label _ngcontent-c4="">500.00
                    </label>
                  </div>
                </td>
                <td _ngcontent-c4="" align="middle" class="em-c-table__cell em-u-text-align-right">
                  <div _ngcontent-c4="" class="em-c-field__body em-c-field--small">
                    <label _ngcontent-c4="" id="EmitOpexCurrentCumulativeAmount" name="EmitOpexCurrentCumulativeAmount">1,300.00
                    </label>
                  </div>
                </td>
                <td _ngcontent-c4="" class="em-c-table__cell">
                  <!---->
                  <!----><div _ngcontent-c4="" class="em-c-field__body em-c-field--small em-u-text-align-right">
                    <label _ngcontent-c4="">27,919.47
                    </label>
                  </div>
                </td>
                <td _ngcontent-c4="" class="em-c-table__cell  "></td>
              </tr>
              
              <tr _ngcontent-c4="" class="">
                <td _ngcontent-c4="" class="em-u-padding-left-none">
                  <div _ngcontent-c4="" class="em-u-font-style-regular em-u-font-style-semibold em-u-text-align-left">BUS OPEX </div>
                </td>
                <td _ngcontent-c4="" class="em-c-table__cell ">
                  <!---->
                    <div _ngcontent-c4="" class="toggle-previously-endorsed">
                      <label _ngcontent-c4="" class="em-c-field__body em-c-field--small input-budget-request">
                        0.00
                      </label>
                    </div>
                  

                  <!---->
                </td>
                <td _ngcontent-c4="" class="em-c-table__cell ">
                  <!---->
                  <!----><div _ngcontent-c4="" class="em-c-field__body em-c-field--small em-u-text-align-right">
                    <label _ngcontent-c4="">0.00
                    </label>
                  </div>                                
                </td>
                <td _ngcontent-c4="" align="middle" class="em-c-table__cell em-u-text-align-right ">
                  <div _ngcontent-c4="" class="em-c-field__body">
                    <label _ngcontent-c4="" name="BusinessOpexCurrentCumulativeAmount">0.00</label>
                  </div>
                </td>
                <td _ngcontent-c4="" class="em-c-table__cell ">
                  <!---->
                  <!----><div _ngcontent-c4="" class="em-c-field__body em-c-field--small em-u-text-align-right">
                    <label _ngcontent-c4="">0.00
                    </label>
                  </div> 

                </td>
                <td _ngcontent-c4="" class="em-c-table__cell  "></td>

              </tr>
              
              <tr _ngcontent-c4="" class="">
                <td _ngcontent-c4="" class="em-u-padding-left-none ">
                  <div _ngcontent-c4="" class="em-u-font-style-regular em-u-font-style-semibold em-u-text-align-left">IT CAPEX </div>
                </td>
                <td _ngcontent-c4="" class="em-c-table__cell em-u-padding-bottom-none">

                  <div _ngcontent-c4="" class="toggle-previously-endorsed">
                    <label _ngcontent-c4="" class="em-c-field__body em-c-field--small input-budget-request">
                      600
                    </label>
                  </div>
                </td>
                <td _ngcontent-c4="" class="em-c-table__cell em-u-padding-bottom-none">
                  <!---->
                  <!----><div _ngcontent-c4="" class="em-c-field__body em-c-field--small em-u-text-align-right">
                    <label _ngcontent-c4="">0.00
                    </label>
                  </div>                                 
                </td>
                <td _ngcontent-c4="" align="middle" class="em-c-table__cell em-u-text-align-right em-u-padding-bottom-none">
                  <div _ngcontent-c4="" class="em-c-field__body">
                    <label _ngcontent-c4="" name="EmitCapexCurrentCumulativeAmount">600.00</label>
                  </div>
                </td>
                <td _ngcontent-c4="" class="em-c-table__cell em-u-padding-bottom-none">
                  <!---->
                  <!----><div _ngcontent-c4="" class="em-c-field__body em-c-field--small em-u-text-align-right">
                    <label _ngcontent-c4="">12,882.45
                    </label>
                  </div>
                </td>
                <td _ngcontent-c4="" class="em-c-table__cell em-u-padding-bottom-none "></td>
              </tr>
              
              <tr _ngcontent-c4="" class="">
                <td _ngcontent-c4="" class="em-u-padding-left-none">
                  <div _ngcontent-c4="" class="em-u-font-style-regular em-u-font-style-semibold em-u-text-align-left">BUS CAPEX </div>
                </td>
                <td _ngcontent-c4="" class="em-c-table__cell em-js-cell em-js-cell-editable">

                  <div _ngcontent-c4="" class="toggle-previously-endorsed">
                    <label _ngcontent-c4="" class="em-c-field__body em-c-field--small input-budget-request">
                      0
                    </label>
                  </div>
                </td>

                <td _ngcontent-c4="" class="em-c-table__cell">
                  <!---->
                  <!----><div _ngcontent-c4="" class="em-c-field__body em-c-field--small em-u-text-align-right">
                    <label _ngcontent-c4="">0.00
                    </label>
                  </div>
                </td>
                <td _ngcontent-c4="" align="middle" class="em-c-table__cell em-u-text-align-right">
                  <div _ngcontent-c4="" class="em-c-field__body">
                    <label _ngcontent-c4="" name="BusinessCapexCurrentCumulativeAmount">0.00</label>
                  </div>
                </td>
                <td _ngcontent-c4="" class="em-c-table__cell ">
                  <!---->
                  <!----><div _ngcontent-c4="" class="em-c-field__body em-c-field--small em-u-text-align-right">
                    <label _ngcontent-c4="">0.00
                    </label>
                  </div>
                </td>
                <td _ngcontent-c4="" class="em-c-table__cell  "></td>
              </tr>
              
              <tr _ngcontent-c4="" class="" style="background-color: #0c69b0;">
                <td _ngcontent-c4="" class="em-c-table__cell em-js-cell em-js-cell-editable td-total" valign="middle">
                  <label _ngcontent-c4="" class="em-c-field__label td-total">Total ($K)</label>
                </td>
                <td _ngcontent-c4="" align="middle" class="em-c-table__cell em-js-cell em-js-cell-editable td-total " valign="middle">
                  1,400.00
                </td>
                <td _ngcontent-c4="" align="middle" class="em-c-table__cell em-js-cell em-js-cell-editable td-total" valign="middle">
                  500.00
                </td>
                <td _ngcontent-c4="" align="middle" class="em-c-table__cell em-js-cell em-js-cell-editable td-total em-u-text-align-right" valign="middle">
                  1,900.00
                </td>
                <td _ngcontent-c4="" align="middle" class="em-c-table__cell em-js-cell em-js-cell-editable td-total " valign="middle">
                  40,801.92
                </td>
                <td _ngcontent-c4="" align="right " class="td-total" colspan="3" valign="middle">
                  <label _ngcontent-c4="" class="td-total" style="margin-right:30px;"> 4.66% of total project</label>
                </td>
                <td _ngcontent-c4="" class="em-c-table__cell  "></td>
              </tr>

              
            </tbody>
          </table>
          <!---->
        </div>
        
      </div>
      
    </div>
  </fieldset>

<Br />
<Br />

 <div class="em-c-table-object ">
            <div class="em-c-table-object__header">
            </div>
            <!--end em-c-table-object__header-->
            <div class="em-c-table-object__body">
                <div class="em-c-table-object__body-inner">
                    <table class="em-c-table ">
                        <thead class="em-c-table__header">
                            <tr class="em-c-table__header-row">
                                <th scope="col" class="em-c-table__header-cell em-u-width-5">Request Id</th>
                                <th scope="col" class="em-c-table__header-cell ">Funding Type</th>
                                <th scope="col" class="em-c-table__header-cell ">Approved On</th>
                                <th scope="col" class="em-c-table__header-cell ">DOAG</th>
                                <th scope="col" class="em-c-table__header-cell ">Approved Amount</th>
                            </tr>
                        </thead>
                        <tbody class="em-c-table__body ">
                            <tr class="em-c-table__row " onclick="window.location.href='../Request/FundingRequest.aspx'">
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">9314</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Cumulative Advance Commitment</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Tue Aug 14, 2018 </td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Doe, John</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">$1,900.00</td>
                     
                               
                            </tr>
                            <tr class="em-c-table__row " onclick="window.location.href='../Request/FundingRequest.aspx'">

                              <td class="em-c-table__cell em-js-cell em-js-cell-editable">8913</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Cumulative Advance Commitment</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Thur Feb 15, 2018 </td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Smith, John</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">$1,000.00</td>
                               
                            </tr>
                            <tr class="em-c-table__row " onclick="window.location.href='../Request/FundingRequest.aspx'">
                                
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">7790</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Full Funding</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Fri Aug 25, 2017</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Holmes, Sherlock</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">$700.00</td>
                               
                            </tr>
                            <tr class="em-c-table__row " onclick="window.location.href='../Request/FundingRequest.aspx'">

                             <td class="em-c-table__cell em-js-cell em-js-cell-editable">6653</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Full Funding</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Wed Jul 17, 2017</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">Mull, Anna</td>
                                <td class="em-c-table__cell em-js-cell em-js-cell-editable">$500.00</td>
                               
                            </tr>
                           
                         
                        </tbody>
                        <!-- end em-c-table__body -->
                        <tfoot class="em-c-table__footer">
                            <tr class="em-c-table__footer-row">
                            </tr>
                        </tfoot>
                        <!-- end em-c-table__footer -->
                    </table>
                    <!--end em-c-table-->
                </div>
                <!--end em-c-table-object__body-inner-->
            </div>
            <!--end em-c-table-object__body-->
        </div>
        <!--end em-c-table-object-->