﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="System.Web.UI.UserControl" %>


<div class="em-c-header__nav-container em-js-nav-panel">
    <nav id="nav" class="em-c-primary-nav" role="navigation">
        <ul class="em-c-primary-nav__list">
            <li class="em-c-primary-nav__item ">
                <a href="#" class="em-c-primary-nav__link  em-c-primary-nav__link--has-children em-js-nav-dropdown-trigger">Home
				
				
							<svg class="em-c-icon em-c-primary-nav__icon" height="10" width="10">
                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down"></use>
                            </svg>
                </a>
                <!--end em-c-primary-nav__link-->
                <ul class="em-c-primary-nav__sublist em-js-nav-dropdown ">
                    <li class="em-c-primary-nav__subitem">
                        <a href="../Home/Home-Admin.aspx" class="em-c-primary-nav__sublink">Home - Admin</a>
                    </li>
                    <li class="em-c-primary-nav__subitem">
                        <a href="../Home/Home-Controller.aspx" class="em-c-primary-nav__sublink">Home - Controller</a>
                    </li>
                    <li class="em-c-primary-nav__subitem">
                        <a href="../Home/Home-Executive.aspx" class="em-c-primary-nav__sublink">Home - Executive</a>
                    </li>
                    <li class="em-c-primary-nav__subitem">
                        <a href="../Home/Home-PCA.aspx" class="em-c-primary-nav__sublink">Home - PCA</a>
                    </li>
                    <li class="em-c-primary-nav__subitem">
                        <a href="../Home/Home-PM.aspx" class="em-c-primary-nav__sublink">Home - Project Manager</a>
                    </li>
                    <!-- end em-c-nav__sublist__item -->
                </ul>
                <!-- end em-c-nav__sublist -->
            </li>
            <!-- end em-c-nav__list__item -->
            <li class="em-c-primary-nav__item ">
                <a href="../Project/Search.aspx" class="em-c-primary-nav__link">Project</a>
            </li>
            <!-- end em-c-nav__list__item -->
            <li class="em-c-primary-nav__item ">
                <a href="#" class="em-c-primary-nav__link  em-c-primary-nav__link--has-children em-js-nav-dropdown-trigger">Request
				
				
							<svg class="em-c-icon em-c-primary-nav__icon" height="10" width="10">
                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down"></use>
                            </svg>
                </a>
                <!--end em-c-primary-nav__link-->
                <ul class="em-c-primary-nav__sublist em-js-nav-dropdown ">
                    <li class="em-c-primary-nav__subitem">
                        <a href="../Requests/Search.aspx" class="em-c-primary-nav__sublink">Search</a>
                    </li>
                    <li class="em-c-primary-nav__subitem">
                        <a href="../Requests/NewRequest.aspx" class="em-c-primary-nav__sublink">New Request</a>
                    </li>
                   <%-- <li class="em-c-primary-nav__subitem">
                        <a href="../Requests/ARAssignment.aspx" class="em-c-primary-nav__sublink">New Appropriation Request</a>
                    </li>--%>
                    <!-- end em-c-nav__sublist__item -->
                </ul>
                <!-- end em-c-nav__sublist -->
            </li>
            <!-- end em-c-nav__list__item -->


            <%-- <li class="em-c-primary-nav__item ">
                                    <a href="#" class="em-c-primary-nav__link ">Request
				
				
                                    </a>
                                    <!--end em-c-primary-nav__link-->
                                </li>
                                <!-- end em-c-nav__list__item -->--%>
            <li class="em-c-primary-nav__item ">
                <a href="#" class="em-c-primary-nav__link ">Report
				
                </a>
                <!--end em-c-primary-nav__link-->
            </li>
            <!-- end em-c-nav__list__item -->
            <li class="em-c-primary-nav__item ">
                <a href="#" class="em-c-primary-nav__link  em-c-primary-nav__link--has-children em-js-nav-dropdown-trigger">Admin
				
							        <svg class="em-c-icon em-c-primary-nav__icon" style="height: 10px; width: 10px;">
                                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../../assets/standard/unity-1.2.0/images/em-icons.svg#icon-caret-down"></use>
                                    </svg>
                </a>
                <!--end em-c-primary-nav__link-->
                <ul class="em-c-primary-nav__sublist">
                    <li class="em-c-primary-nav__subitem">
                        <a href="../Admin/DOAGList.aspx" class="em-c-primary-nav__sublink">DOAG</a>
                    </li>
                    <li class="em-c-primary-nav__subitem">
                        <a href="../Admin/ReferenceDataList.aspx" class="em-c-primary-nav__sublink">Reference Data</a>
                    </li>
                    <!-- end em-c-nav__sublist__item -->
                    <li class="em-c-primary-nav__subitem">
                        <a href="../Admin/UserList.aspx" class="em-c-primary-nav__sublink">Users</a>
                    </li>
                    <!-- end em-c-nav__sublist__item -->
                </ul>
                <!-- end em-c-nav__sublist -->
            </li>
            <!-- end em-c-nav__list__item -->
        </ul>
        <!-- end em-c-nav__list -->
    </nav>
    <!-- end em-c-nav -->
</div>
